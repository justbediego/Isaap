﻿using Automation.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.DataAccess
{
    public class AutomationContextPhysical : DbContext
    {
        public AutomationContextPhysical():base("AutomationContext")
        {
        }
        public DbSet<ActionLog> ActionLogs { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Association> Associations { get; set; }
        public DbSet<Attachment> Attachments { get; set; }
        public DbSet<ExpertReport> ExpertReports { get; set; }
        public DbSet<Club> Clubs { get; set; }
        public DbSet<County> Counties { get; set; }
        public DbSet<CountyInspector> CountyInspectors { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<FieldOfActivity> FieldsOfActivity { get; set; }
        public DbSet<FieldOfStudy> FieldsOfStudy { get; set; }
        public DbSet<Flow> Flows { get; set; }
        public DbSet<InspectionOffice> InspectionOffices { get; set; }
        public DbSet<ProductionType> ProductionTypes { get; set; }
        public DbSet<Province> Provinces { get; set; }
        public DbSet<ProvinceInspector> ProvinceInspectors { get; set; }
        public DbSet<ProvinceManager> ProvinceManagers { get; set; }
        public DbSet<Staff> Staff { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Setting> Settings { get; set; }
        public DbSet<Document.AcademicCertification> AcademicCertifications { get; set; }
        public DbSet<Document.WorkExperience> WorkExperiences { get; set; }
        public DbSet<Document.SafetyCertification> SafetyCertifications { get; set; }
        public DbSet<RequirementRule> RequirementRules { get; set; }
        public DbSet<Document.ExpertContact> Contacts { get; set; }
        public DbSet<Document.Workshop> Workshops { get; set; }
        public DbSet<AttachmentData> AttachmentData { get; set; }
        public DbSet<Analyzer> Analyzers { get; set; }
    }
    public class AutomationContext : AutomationContextPhysical
    {
        public class EntityFrameworkConfiguration : DbConfiguration
        {
            public EntityFrameworkConfiguration()
            {
                AddInterceptor(new SoftDeleteInterceptor());
            }
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var conv = new AttributeToTableAnnotationConvention<SoftDeleteAttribute, string>(
                "SoftDeleteColumnName",
                (type, attributes) => attributes.Single().ColumnName);
            modelBuilder.Conventions.Add(conv);
        }
        public override int SaveChanges()
        {
            var entries = ChangeTracker.Entries().Where(entry => entry.Entity is Base).ToList();
            //last updated
            foreach (var entry in entries)
                ((Base)entry.Entity).DateModified = DateTime.Now;
            return base.SaveChanges();
        }
    }
}
