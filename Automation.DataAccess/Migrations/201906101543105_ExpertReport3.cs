namespace Automation.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExpertReport3 : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.ExpertReports", new[] { "Document_Id" });
            AlterColumn("dbo.ExpertReports", "Document_ID", c => c.Int(nullable: false));
            CreateIndex("dbo.ExpertReports", "Document_ID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.ExpertReports", new[] { "Document_ID" });
            AlterColumn("dbo.ExpertReports", "Document_ID", c => c.Int());
            CreateIndex("dbo.ExpertReports", "Document_Id");
        }
    }
}
