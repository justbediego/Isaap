namespace Automation.DataAccess.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class actionLogsAdded : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ActionLogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        User_ID = c.Int(),
                        ActionType = c.String(maxLength: 10),
                        ActionName = c.String(maxLength: 500),
                        IP = c.String(maxLength: 20),
                        Data = c.String(maxLength: 1000),
                        Identity = c.String(maxLength: 100),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "Deleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.User_ID)
                .Index(t => t.User_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ActionLogs", "User_ID", "dbo.Users");
            DropIndex("dbo.ActionLogs", new[] { "User_ID" });
            DropTable("dbo.ActionLogs",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "Deleted" },
                });
        }
    }
}
