namespace Automation.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class isDeactivatedAdded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Staff", "IsDeactivated", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Staff", "IsDeactivated");
        }
    }
}
