// <auto-generated />
namespace Automation.DataAccess.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class guidImage : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(guidImage));
        
        string IMigrationMetadata.Id
        {
            get { return "201812012305123_guidImage"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
