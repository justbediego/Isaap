namespace Automation.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExportReport2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ExpertReports", "File_Id", "dbo.Attachments");
            DropIndex("dbo.ExpertReports", new[] { "File_Id" });
            AddColumn("dbo.ExpertReports", "ReportType", c => c.Int(nullable: false));
            AddColumn("dbo.ExpertReports", "ForDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.ExpertReports", "IsVisible", c => c.Boolean(nullable: false));
            AlterColumn("dbo.ExpertReports", "File_ID", c => c.Int(nullable: false));
            CreateIndex("dbo.ExpertReports", "File_ID");
            AddForeignKey("dbo.ExpertReports", "File_ID", "dbo.Attachments", "Id", cascadeDelete: true);
            DropColumn("dbo.ExpertReports", "Date");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ExpertReports", "Date", c => c.DateTime(nullable: false));
            DropForeignKey("dbo.ExpertReports", "File_ID", "dbo.Attachments");
            DropIndex("dbo.ExpertReports", new[] { "File_ID" });
            AlterColumn("dbo.ExpertReports", "File_ID", c => c.Int());
            DropColumn("dbo.ExpertReports", "IsVisible");
            DropColumn("dbo.ExpertReports", "ForDate");
            DropColumn("dbo.ExpertReports", "ReportType");
            CreateIndex("dbo.ExpertReports", "File_Id");
            AddForeignKey("dbo.ExpertReports", "File_Id", "dbo.Attachments", "Id");
        }
    }
}
