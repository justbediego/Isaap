namespace Automation.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class guidImage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Attachments", "Key", c => c.Guid(nullable: false, defaultValueSql: "NEWID()"));
        }

        public override void Down()
        {
            DropColumn("dbo.Attachments", "Key");
        }
    }
}
