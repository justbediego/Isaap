namespace Automation.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class expertReport_multipleFiles : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ExpertReports", "File_ID", "dbo.Attachments");
            DropIndex("dbo.ExpertReports", new[] { "File_ID" });
            AddColumn("dbo.Attachments", "ExpertReport_ID", c => c.Int());
            CreateIndex("dbo.Attachments", "ExpertReport_ID");
            AddForeignKey("dbo.Attachments", "ExpertReport_ID", "dbo.ExpertReports", "Id");
            DropColumn("dbo.ExpertReports", "File_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ExpertReports", "File_ID", c => c.Int(nullable: false));
            DropForeignKey("dbo.Attachments", "ExpertReport_ID", "dbo.ExpertReports");
            DropIndex("dbo.Attachments", new[] { "ExpertReport_ID" });
            DropColumn("dbo.Attachments", "ExpertReport_ID");
            CreateIndex("dbo.ExpertReports", "File_ID");
            AddForeignKey("dbo.ExpertReports", "File_ID", "dbo.Attachments", "Id", cascadeDelete: true);
        }
    }
}
