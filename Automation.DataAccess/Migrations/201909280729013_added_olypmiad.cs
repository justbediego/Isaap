namespace Automation.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class added_olypmiad : DbMigration
    {
        //ALTER TABLE [dbo].[Documents] ADD [OlympiadRequestDate] [datetime]
        public override void Up()
        {
            AddColumn("dbo.Documents", "OlympiadRequestDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Documents", "OlympiadRequestDate");
        }
    }
}
