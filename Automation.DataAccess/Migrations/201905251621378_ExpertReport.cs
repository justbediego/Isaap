namespace Automation.DataAccess.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class ExpertReport : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ReportComments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsAccepted = c.Boolean(nullable: false),
                        Text = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        ExpertReport_Id = c.Int(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "Deleted" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ExpertReports", t => t.ExpertReport_Id)
                .Index(t => t.ExpertReport_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ReportComments", "ExpertReport_Id", "dbo.ExpertReports");
            DropIndex("dbo.ReportComments", new[] { "ExpertReport_Id" });
            DropTable("dbo.ReportComments",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "Deleted" },
                });
        }
    }
}
