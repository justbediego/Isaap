namespace Automation.DataAccess.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class deletedAdded : DbMigration
    {
        public override void Up()
        {
            AlterTableAnnotations(
                "dbo.AcademicCertifications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FieldOfStudy_ID = c.Int(nullable: false),
                        ScanFile_ID = c.Int(),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: null, newValue: "Deleted")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.FieldsOfStudy",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Code = c.String(),
                        OrginalityCode = c.String(),
                        Requires40Hours = c.Boolean(nullable: false),
                        Requires16Hours = c.Boolean(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: null, newValue: "Deleted")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Attachments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InsuranceRecord_ID = c.Int(),
                        NationalCard_ID = c.Int(),
                        Shenasname_ID = c.Int(),
                        Objection_ID = c.Int(),
                        TechnicalProtectionCommittee_ID = c.Int(),
                        Contract_ID = c.Int(),
                        Filename = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        AttachmentData_Id = c.Int(),
                        Flow_Id = c.Int(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: null, newValue: "Deleted")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.AttachmentDatas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FileData = c.Binary(),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: null, newValue: "Deleted")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Admin",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: null, newValue: "Deleted")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Analyzers",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: null, newValue: "Deleted")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Associations",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        County_ID = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: null, newValue: "Deleted")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.CountyInspectors",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        County_ID = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: null, newValue: "Deleted")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Clubs",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Province_ID = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: null, newValue: "Deleted")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.ProvinceInspectors",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Province_ID = c.Int(nullable: false),
                        HeadFullName = c.String(),
                        ChairmanFullName = c.String(),
                        SupervisorFullName = c.String(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: null, newValue: "Deleted")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.ProvinceManagers",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Province_ID = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: null, newValue: "Deleted")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Documents",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        BachelorsCertification_Id = c.Int(),
                        CurrentWorkShop_Id = c.Int(),
                        DiplomaCertification_Id = c.Int(),
                        HighSchoolCertification_Id = c.Int(),
                        MastersCertification_Id = c.Int(),
                        PhDCertification_Id = c.Int(),
                        DeactivationDate = c.DateTime(),
                        ReasonOfDeactivation = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        FatherName = c.String(),
                        ShenasnameNo = c.String(),
                        NationalCode = c.String(),
                        BirthDate = c.DateTime(),
                        PlaceOfBirth = c.String(),
                        Gender = c.Int(),
                        MaritalStatus = c.Int(),
                        MilitaryStatus = c.Int(),
                        ReasonOfExemption = c.String(),
                        AssociationMembershipNo = c.String(),
                        HomeContact_ID = c.Int(),
                        WorkContact_ID = c.Int(),
                        Mobile = c.String(),
                        PhotoScan_ID = c.Int(),
                        NezamCode = c.String(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: null, newValue: "Deleted")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Staff",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: null, newValue: "Deleted")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.InspectionOffices",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: null, newValue: "Deleted")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserName = c.String(maxLength: 100),
                        HashPassword = c.String(),
                        Type = c.Int(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: null, newValue: "Deleted")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Counties",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Province_ID = c.Int(nullable: false),
                        Name = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: null, newValue: "Deleted")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Provinces",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Code = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: null, newValue: "Deleted")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.ExpertContacts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        County_ID = c.Int(),
                        Address = c.String(),
                        PostalCode = c.String(),
                        Phone = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: null, newValue: "Deleted")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Workshops",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UnitName = c.String(),
                        EmployerName = c.String(),
                        EmployerGender = c.Int(nullable: false),
                        SocialSecurityCode = c.String(),
                        MaleWorkersNo = c.Int(nullable: false),
                        FemaleWorkersNo = c.Int(nullable: false),
                        WorkshopSize = c.Double(nullable: false),
                        MorningWorkersNo = c.Int(nullable: false),
                        EveningWorkersNo = c.Int(nullable: false),
                        NightWorkersNo = c.Int(nullable: false),
                        TechnicalProtectionCommittee = c.Boolean(nullable: false),
                        SafetyWorkersNo = c.Int(nullable: false),
                        WorkshopAddress = c.String(),
                        WorkshopPhone = c.String(),
                        WorkshopPostalCode = c.String(),
                        DateFrom = c.DateTime(nullable: false),
                        Email = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        FieldOfActivity_Id = c.Int(),
                        ProductionType_Id = c.Int(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: null, newValue: "Deleted")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.FieldsOfActivity",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DangerLevel = c.Int(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: null, newValue: "Deleted")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.ProductionTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: null, newValue: "Deleted")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Flows",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        Description = c.String(),
                        Staff_ID = c.Int(),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        Document_Id = c.Int(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: null, newValue: "Deleted")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.ExpertReports",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Description = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        File_Id = c.Int(),
                        Document_Id = c.Int(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: null, newValue: "Deleted")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.SafetyCertifications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CourseType = c.Int(nullable: false),
                        OtherCourseName = c.String(),
                        OtherDurationInHours = c.Double(nullable: false),
                        DateTaken = c.DateTime(nullable: false),
                        IssuerNumber = c.String(),
                        CertificationScan_ID = c.Int(),
                        Description = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        Document_Id = c.Int(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: null, newValue: "Deleted")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.WorkExperiences",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Document_ID = c.Int(nullable: false),
                        WorkshopName = c.String(),
                        EmployerName = c.String(),
                        DateFrom = c.DateTime(nullable: false),
                        DateTo = c.DateTime(nullable: false),
                        Description = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: null, newValue: "Deleted")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.RequirementRules",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DangerLevel = c.Int(nullable: false),
                        Hours = c.Int(nullable: false),
                        LevelOfEducation = c.Int(nullable: false),
                        SizeCategory = c.Int(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: null, newValue: "Deleted")
                    },
                });
            
            AddColumn("dbo.AcademicCertifications", "OwnerType", c => c.Int(nullable: false));
            AddColumn("dbo.AcademicCertifications", "Deleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.FieldsOfStudy", "OwnerType", c => c.Int(nullable: false));
            AddColumn("dbo.FieldsOfStudy", "Deleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Attachments", "OwnerType", c => c.Int(nullable: false));
            AddColumn("dbo.Attachments", "Deleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.AttachmentDatas", "OwnerType", c => c.Int(nullable: false));
            AddColumn("dbo.AttachmentDatas", "Deleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Users", "OwnerType", c => c.Int(nullable: false));
            AddColumn("dbo.Users", "Deleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Counties", "OwnerType", c => c.Int(nullable: false));
            AddColumn("dbo.Counties", "Deleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Provinces", "OwnerType", c => c.Int(nullable: false));
            AddColumn("dbo.Provinces", "Deleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.ExpertContacts", "OwnerType", c => c.Int(nullable: false));
            AddColumn("dbo.ExpertContacts", "Deleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Workshops", "OwnerType", c => c.Int(nullable: false));
            AddColumn("dbo.Workshops", "Deleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.FieldsOfActivity", "OwnerType", c => c.Int(nullable: false));
            AddColumn("dbo.FieldsOfActivity", "Deleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.ProductionTypes", "OwnerType", c => c.Int(nullable: false));
            AddColumn("dbo.ProductionTypes", "Deleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Flows", "OwnerType", c => c.Int(nullable: false));
            AddColumn("dbo.Flows", "Deleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.ExpertReports", "OwnerType", c => c.Int(nullable: false));
            AddColumn("dbo.ExpertReports", "Deleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.SafetyCertifications", "OwnerType", c => c.Int(nullable: false));
            AddColumn("dbo.SafetyCertifications", "Deleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.WorkExperiences", "OwnerType", c => c.Int(nullable: false));
            AddColumn("dbo.WorkExperiences", "Deleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.RequirementRules", "OwnerType", c => c.Int(nullable: false));
            AddColumn("dbo.RequirementRules", "Deleted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.RequirementRules", "Deleted");
            DropColumn("dbo.RequirementRules", "OwnerType");
            DropColumn("dbo.WorkExperiences", "Deleted");
            DropColumn("dbo.WorkExperiences", "OwnerType");
            DropColumn("dbo.SafetyCertifications", "Deleted");
            DropColumn("dbo.SafetyCertifications", "OwnerType");
            DropColumn("dbo.ExpertReports", "Deleted");
            DropColumn("dbo.ExpertReports", "OwnerType");
            DropColumn("dbo.Flows", "Deleted");
            DropColumn("dbo.Flows", "OwnerType");
            DropColumn("dbo.ProductionTypes", "Deleted");
            DropColumn("dbo.ProductionTypes", "OwnerType");
            DropColumn("dbo.FieldsOfActivity", "Deleted");
            DropColumn("dbo.FieldsOfActivity", "OwnerType");
            DropColumn("dbo.Workshops", "Deleted");
            DropColumn("dbo.Workshops", "OwnerType");
            DropColumn("dbo.ExpertContacts", "Deleted");
            DropColumn("dbo.ExpertContacts", "OwnerType");
            DropColumn("dbo.Provinces", "Deleted");
            DropColumn("dbo.Provinces", "OwnerType");
            DropColumn("dbo.Counties", "Deleted");
            DropColumn("dbo.Counties", "OwnerType");
            DropColumn("dbo.Users", "Deleted");
            DropColumn("dbo.Users", "OwnerType");
            DropColumn("dbo.AttachmentDatas", "Deleted");
            DropColumn("dbo.AttachmentDatas", "OwnerType");
            DropColumn("dbo.Attachments", "Deleted");
            DropColumn("dbo.Attachments", "OwnerType");
            DropColumn("dbo.FieldsOfStudy", "Deleted");
            DropColumn("dbo.FieldsOfStudy", "OwnerType");
            DropColumn("dbo.AcademicCertifications", "Deleted");
            DropColumn("dbo.AcademicCertifications", "OwnerType");
            AlterTableAnnotations(
                "dbo.RequirementRules",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DangerLevel = c.Int(nullable: false),
                        Hours = c.Int(nullable: false),
                        LevelOfEducation = c.Int(nullable: false),
                        SizeCategory = c.Int(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: "Deleted", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.WorkExperiences",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Document_ID = c.Int(nullable: false),
                        WorkshopName = c.String(),
                        EmployerName = c.String(),
                        DateFrom = c.DateTime(nullable: false),
                        DateTo = c.DateTime(nullable: false),
                        Description = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: "Deleted", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.SafetyCertifications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CourseType = c.Int(nullable: false),
                        OtherCourseName = c.String(),
                        OtherDurationInHours = c.Double(nullable: false),
                        DateTaken = c.DateTime(nullable: false),
                        IssuerNumber = c.String(),
                        CertificationScan_ID = c.Int(),
                        Description = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        Document_Id = c.Int(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: "Deleted", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.ExpertReports",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false),
                        Description = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        File_Id = c.Int(),
                        Document_Id = c.Int(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: "Deleted", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Flows",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        Description = c.String(),
                        Staff_ID = c.Int(),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        Document_Id = c.Int(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: "Deleted", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.ProductionTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: "Deleted", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.FieldsOfActivity",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        DangerLevel = c.Int(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: "Deleted", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Workshops",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UnitName = c.String(),
                        EmployerName = c.String(),
                        EmployerGender = c.Int(nullable: false),
                        SocialSecurityCode = c.String(),
                        MaleWorkersNo = c.Int(nullable: false),
                        FemaleWorkersNo = c.Int(nullable: false),
                        WorkshopSize = c.Double(nullable: false),
                        MorningWorkersNo = c.Int(nullable: false),
                        EveningWorkersNo = c.Int(nullable: false),
                        NightWorkersNo = c.Int(nullable: false),
                        TechnicalProtectionCommittee = c.Boolean(nullable: false),
                        SafetyWorkersNo = c.Int(nullable: false),
                        WorkshopAddress = c.String(),
                        WorkshopPhone = c.String(),
                        WorkshopPostalCode = c.String(),
                        DateFrom = c.DateTime(nullable: false),
                        Email = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        FieldOfActivity_Id = c.Int(),
                        ProductionType_Id = c.Int(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: "Deleted", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.ExpertContacts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        County_ID = c.Int(),
                        Address = c.String(),
                        PostalCode = c.String(),
                        Phone = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: "Deleted", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Provinces",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Code = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: "Deleted", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Counties",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Province_ID = c.Int(nullable: false),
                        Name = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: "Deleted", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserName = c.String(maxLength: 100),
                        HashPassword = c.String(),
                        Type = c.Int(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: "Deleted", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.InspectionOffices",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: "Deleted", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Staff",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: "Deleted", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Documents",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        BachelorsCertification_Id = c.Int(),
                        CurrentWorkShop_Id = c.Int(),
                        DiplomaCertification_Id = c.Int(),
                        HighSchoolCertification_Id = c.Int(),
                        MastersCertification_Id = c.Int(),
                        PhDCertification_Id = c.Int(),
                        DeactivationDate = c.DateTime(),
                        ReasonOfDeactivation = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        FatherName = c.String(),
                        ShenasnameNo = c.String(),
                        NationalCode = c.String(),
                        BirthDate = c.DateTime(),
                        PlaceOfBirth = c.String(),
                        Gender = c.Int(),
                        MaritalStatus = c.Int(),
                        MilitaryStatus = c.Int(),
                        ReasonOfExemption = c.String(),
                        AssociationMembershipNo = c.String(),
                        HomeContact_ID = c.Int(),
                        WorkContact_ID = c.Int(),
                        Mobile = c.String(),
                        PhotoScan_ID = c.Int(),
                        NezamCode = c.String(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: "Deleted", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.ProvinceManagers",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Province_ID = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: "Deleted", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.ProvinceInspectors",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Province_ID = c.Int(nullable: false),
                        HeadFullName = c.String(),
                        ChairmanFullName = c.String(),
                        SupervisorFullName = c.String(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: "Deleted", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Clubs",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Province_ID = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: "Deleted", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.CountyInspectors",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        County_ID = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: "Deleted", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Associations",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        County_ID = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: "Deleted", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Analyzers",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: "Deleted", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Admin",
                c => new
                    {
                        Id = c.Int(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: "Deleted", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.AttachmentDatas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FileData = c.Binary(),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: "Deleted", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Attachments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InsuranceRecord_ID = c.Int(),
                        NationalCard_ID = c.Int(),
                        Shenasname_ID = c.Int(),
                        Objection_ID = c.Int(),
                        TechnicalProtectionCommittee_ID = c.Int(),
                        Contract_ID = c.Int(),
                        Filename = c.String(),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                        AttachmentData_Id = c.Int(),
                        Flow_Id = c.Int(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: "Deleted", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.FieldsOfStudy",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Code = c.String(),
                        OrginalityCode = c.String(),
                        Requires40Hours = c.Boolean(nullable: false),
                        Requires16Hours = c.Boolean(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: "Deleted", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.AcademicCertifications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FieldOfStudy_ID = c.Int(nullable: false),
                        ScanFile_ID = c.Int(),
                        DateModified = c.DateTime(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "SoftDeleteColumnName",
                        new AnnotationValues(oldValue: "Deleted", newValue: null)
                    },
                });
            
        }
    }
}
