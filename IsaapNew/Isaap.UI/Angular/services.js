﻿angular.module('isaap').service('services', ['$http', '$uibModal', 'toaster', function ($http, $uibModal, toaster) {
    var vm = this;
    //************COMMON*********************
    vm.ShowAlertModal = function (title, text) {
        $uibModal.open(
            {
                templateUrl: '/Statics/Modals/alertModal.html',
                animation: true,
                controller: function ($scope) {
                    $scope.title = title;
                    $scope.text = text;
                    $scope.Close = function () {
                        this.$close();
                    }
                },
            });
    };
    vm.ShowError = function (title, text) {
        toaster.error(title, text);
    };
    vm.ShowSuccess = function (title, text) {
        toaster.success(title, text);
    }
    vm.WaitMessage = 'لطفاً صبر کنید';

    //************CALLER************
    function CallServer(method, location, data, success, fail) {
        switch (method) {
            case 'post':
                $http.post(location, data).then(function (result) {
                    if (success)
                        success(result);
                }, function (e) {
                    vm.ShowError('ایساپ', e.data.ExceptionMessage);
                    if (fail)
                        fail();
                });
                break;
            case 'get':
                $http.get(location, { params: data }).then(function (result) {
                    if (success)
                        success(result);
                }, function (e) {
                    vm.ShowError('ایساپ', e.data.ExceptionMessage);
                    if (fail)
                        fail();
                });
                break;
        }
    }

    //************USER************
    vm.Login = function (data, success, fail) {
        CallServer('post', '/api/Authentication/Login', data, success, fail);
    }
    vm.GetUserType = function (success, fail) {
        CallServer('get', '/api/Authentication/GetUserType', null, success, fail);
    }
    vm.IsLoggedIn = function (success, fail) {
        CallServer('get', '/api/Authentication/IsLoggedIn', null, success, fail);
    }
    vm.Logout = function (success, fail) {
        CallServer('post', '/api/Authentication/Logout', null, success, fail);
    }
    vm.ChangePassword = function (data, success, fail) {
        CallServer('post', '/api/Authentication/ChangePassword', data, success, fail);
    }

    //************EXPERT************
    vm.RegisterExpert = function (data, success, fail) {
        CallServer('post', '/api/Expert/Register', data, success, fail);
    };
    vm.ModifyExpertBasicInfo = function (data, success, fail) {
        CallServer('post', '/api/Expert/ModifyBasicInfo', data, success, fail);
    }
    vm.ModifyExpertContactInfo = function (data, success, fail) {
        CallServer('post', '/api/Expert/ModifyContactInfo', data, success, fail);
    }
    vm.GetExpertBasicInfo = function (success, fail) {
        CallServer('get', '/api/Expert/GetBasicInfo', null, success, fail);
    }
    vm.GetExpertContactInfo = function (success, fail) {
        CallServer('get', '/api/Expert/GetContactInfo', null, success, fail);
    }
    vm.AddOrEditEducation = function (data, success, fail) {
        CallServer('post', '/api/Expert/AddOrEditEducation', data, success, fail);
    }
    vm.DeleteEducation = function (data, success, fail) {
        CallServer('post', '/api/Expert/DeleteEducation', data, success, fail);
    }
    vm.AddOrEditSafetyCertification = function (data, success, fail) {
        CallServer('post', '/api/Expert/AddOrEditSafetyCertification', data, success, fail);
    }
    vm.GetEducation = function (data, success, fail) {
        CallServer('get', '/api/Expert/GetEducation', data, success, fail);
    }
    vm.DeleteCertification = function (data, success, fail) {
        CallServer('post', '/api/Expert/DeleteCertification', data, success, fail);
    }
    vm.AddOrEditWorkExperience = function (data, success, fail) {
        CallServer('post', '/api/Expert/AddOrEditWorkExperience', data, success, fail);
    }
    vm.DeleteWorkExperience = function (data, success, fail) {
        CallServer('post', '/api/Expert/DeleteWorkExperience', data, success, fail);
    }
    vm.GetAllEducations = function (success, fail) {
        CallServer('get', '/api/Expert/GetAllEducations', null, success, fail);
    }
    vm.GetExpertPictures = function (success, fail) {
        CallServer('get', '/api/Expert/GetPictures', null, success, fail);
    }
    vm.RemoveExpertPhotoScan = function (success, fail) {
        CallServer('post', '/api/Expert/RemovePhotoScan', null, success, fail);
    }
    vm.GetExpertDashboardData = function (success, fail) {
        CallServer('get', '/api/Expert/GetDashboardData', null, success, fail);
    }
    vm.RemoveExpertShenasnameScan = function (data, success, fail) {
        CallServer('post', '/api/Expert/RemoveShenasnameScan', data, success, fail);
    }
    vm.RemoveExpertNationalCardScan = function (data, success, fail) {
        CallServer('post', '/api/Expert/RemoveNationalCardScan', data, success, fail);
    }
    vm.RemoveExpertInsuranceScan = function (data, success, fail) {
        CallServer('post', '/api/Expert/RemoveInsuranceScan', data, success, fail);
    }
    vm.GetAllSafetyCertifications = function (success, fail) {
        CallServer('get', '/api/Expert/GetAllSafetyCertifications', null, success, fail);
    }
    vm.GetSafetyCertification = function (data, success, fail) {
        CallServer('get', '/api/Expert/GetSafetyCertification', data, success, fail);
    }
    vm.GetAllWorkExperiences = function (success, fail) {
        CallServer('get', '/api/Expert/GetAllWorkExperiences', null, success, fail);
    }
    vm.GetWorkExperience = function (data, success, fail) {
        CallServer('get', '/api/Expert/GetWorkExperience', data, success, fail);
    }
    vm.GetMyLicenses = function (success, fail) {
        CallServer('get', '/api/Expert/GetMyLicenses', null, success, fail);
    }
    vm.RemoveContractScan = function (data, success, fail) {
        CallServer('post', '/api/Expert/RemoveContractScan', data, success, fail);
    }
    vm.SendLicense = function (data, success, fail) {
        CallServer('post', '/api/Expert/SendLicense', data, success, fail);
    }
    vm.CheckExpertProfileProgress = function (success, fail) {
        CallServer('get', '/api/Expert/CheckProfileProgress', null, success, fail);
    }
    vm.GetLicenseFlows = function (data, success, fail) {
        CallServer('get', '/api/Expert/GetLicenseFlows', data, success, fail);
    };
    vm.RemoveObjectionAttachment = function (data, success, fail) {
        CallServer('post', '/api/Expert/RemoveObjectionAttachment', data, success, fail);
    }
    vm.GetTemporaryObjectionAttachments = function (data, success, fail) {
        CallServer('get', '/api/Expert/GetTemporaryObjectionAttachments', data, success, fail);
    };
    vm.Objectify = function (data, success, fail) {
        CallServer('post', '/api/Expert/Objectify', data, success, fail);
    }
    vm.CreateLicenseUser = function (data, success, fail) {
        CallServer('post', '/api/Expert/CreateLicenseUser', data, success, fail);
    }
    //************CEO************
    vm.RegisterCEO = function (data, success, fail) {
        CallServer('post', '/api/CEO/Register', data, success, fail);
    };
    vm.ModifyCEOBasicInfo = function (data, success, fail) {
        CallServer('post', '/api/CEO/ModifyBasicInfo', data, success, fail);
    }
    vm.ModifyCEOContactInfo = function (data, success, fail) {
        CallServer('post', '/api/CEO/ModifyContactInfo', data, success, fail);
    }
    vm.GetCEOBasicInfo = function (success, fail) {
        CallServer('get', '/api/CEO/GetBasicInfo', null, success, fail);
    }
    vm.GetCEOContactInfo = function (success, fail) {
        CallServer('get', '/api/CEO/GetContactInfo', null, success, fail);
    }
    vm.GetCEOPictures = function (success, fail) {
        CallServer('get', '/api/CEO/GetPictures', null, success, fail);
    }
    vm.RemoveCEOPhotoScan = function (success, fail) {
        CallServer('post', '/api/CEO/RemovePhotoScan', null, success, fail);
    }
    vm.GetCEODashboardData = function (success, fail) {
        CallServer('get', '/api/CEO/GetDashboardData', null, success, fail);
    }
    vm.RemoveCEOShenasnameScan = function (data, success, fail) {
        CallServer('post', '/api/CEO/RemoveShenasnameScan', data, success, fail);
    }
    vm.RemoveCEONationalCardScan = function (data, success, fail) {
        CallServer('post', '/api/CEO/RemoveNationalCardScan', data, success, fail);
    }
    vm.RemoveCEOInsuranceScan = function (data, success, fail) {
        CallServer('post', '/api/CEO/RemoveInsuranceScan', data, success, fail);
    }
    vm.GetWorkshopInfo = function (success, fail) {
        CallServer('get', '/api/CEO/GetWorkshopInfo', null, success, fail);
    }
    vm.ModifyWorkshopInfo = function (data, success, fail) {
        CallServer('post', '/api/CEO/ModifyWorkshopInfo', data, success, fail);
    }
    vm.RemoveTPCScan = function (data, success, fail) {
        CallServer('post', '/api/CEO/RemoveTPCScan', data, success, fail);
    }
    vm.FindExperts = function (data, success, fail) {
        CallServer('get', '/api/CEO/FindExperts', data, success, fail);
    }
    vm.GetWorkshopLocationInfo = function (success, fail) {
        CallServer('get', '/api/CEO/GetWorkshopLocationInfo', null, success, fail);
    }
    vm.ModifyWorkshopLocation = function (data, success, fail) {
        CallServer('post', '/api/CEO/ModifyWorkshopLocation', data, success, fail);
    }
    vm.HireAnExpert = function (data, success, fail) {
        CallServer('post', '/api/CEO/HireAnExpert', data, success, fail);
    }
    vm.CheckCEOProfileProgress = function (success, fail) {
        CallServer('get', '/api/CEO/CheckProfileProgress', null, success, fail);
    }
    //************STAFF************
    vm.GetStaffDashboardData = function (success, fail) {
        CallServer('get', '/api/Staff/GetDashboardData', null, success, fail);
    }
    vm.GetMyWaitingExperts = function (data, success, fail) {
        CallServer('get', '/api/Staff/GetMyWaitingExperts', data, success, fail);
    }
    vm.GetExpertData = function (data, success, fail) {
        CallServer('get', '/api/Staff/GetExpertData', data, success, fail);
    }
    vm.RecognizeLicense = function (data, success, fail) {
        CallServer('post', '/api/Staff/RecognizeLicense', data, success, fail);
    }
    vm.RejectLicenseRecognition = function (data, success, fail) {
        CallServer('post', '/api/Staff/RejectLicenseRecognition', data, success, fail);
    }
    vm.AuthenticateLicense = function (data, success, fail) {
        CallServer('post', '/api/Staff/AuthenticateLicense', data, success, fail);
    }
    vm.RejectLicenseAuthentication = function (data, success, fail) {
        CallServer('post', '/api/Staff/RejectLicenseAuthentication', data, success, fail);
    }
    vm.StaffGetLicenseFlows = function (data, success, fail) {
        CallServer('get', '/api/Staff/GetLicenseFlows', data, success, fail);
    }
    vm.GetLicensePrintData = function (data, success, fail) {
        CallServer('get', '/api/Staff/GetLicensePrintData', data, success, fail);
    }
    //************LICENSE***********
    vm.GetMyProfileData = function (success, fail) {
        CallServer('get', '/api/License/GetMyProfileData', null, success, fail);
    }

    //************SYSTEM************
    vm.GetAllProvinces = function (success, fail) {
        CallServer('get', '/api/System/GetAllProvinces', null, success, fail);
    }
    vm.GetAllCounties = function (data, success, fail) {
        CallServer('get', '/api/System/GetAllCounties', data, success, fail);
    }
    vm.GetAllFieldOfEducations = function (success, fail) {
        CallServer('get', '/api/System/GetAllFieldOfEducations', null, success, fail);
    }
    vm.GetAllFieldOfCertifications = function (success, fail) {
        CallServer('get', '/api/System/GetAllFieldOfCertifications', null, success, fail);
    }
    vm.GetAllProductionTypes = function (success, fail) {
        CallServer('get', '/api/System/GetAllProductionTypes', null, success, fail);
    }
    vm.GetAllFieldOfActivities = function (success, fail) {
        CallServer('get', '/api/System/GetAllFieldOfActivities', null, success, fail);
    }

}]);