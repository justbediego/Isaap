﻿angular.module("isaap").controller('safetyOfficerPicturesCtrl', function ($scope, services, $state) {
    var vm = $scope;
    vm.LoadPictures = function () {
        services.GetExpertPictures(function success(r) {
            vm.photoId = r.data.PhotoScanId;
            vm.shenasnameScanIds = r.data.ShenasnameScanIds;
            vm.nationalCardScanIds = r.data.NationalCardScanIds;
            vm.insuranceScanIds = r.data.InsuranceScanIds;
        });
    };
    vm.Init = function () {
        vm.LoadPictures();
    };
    vm.ReloadPage = function () {
        location.reload();
    }
    vm.DeletePhotoScan = function () {
        swal({
            title: "آیا مطمعن هستید",
            text: "بعد از حذف دیگر امکان بازگرداندن اطلاعات وجود نخواهد داشت",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                services.RemoveExpertPhotoScan(function success() {
                    vm.ReloadPage();
                    swal("با موفقیت حذف گردید", {
                        icon: "success",
                    });
                });
            }
        });
    };
    vm.DeleteShenasnameScan = function (pictureId) {
        swal({
            title: "آیا مطمعن هستید",
            text: "بعد از حذف دیگر امکان بازگرداندن اطلاعات وجود نخواهد داشت",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                services.RemoveExpertShenasnameScan({
                    ObjectID: pictureId,
                }, function success() {
                    vm.LoadPictures();
                    swal("با موفقیت حذف گردید", {
                        icon: "success",
                    });
                });
            }
        });
    };
    vm.DeleteNationalCardScan = function (pictureId) {
        swal({
            title: "آیا مطمعن هستید",
            text: "بعد از حذف دیگر امکان بازگرداندن اطلاعات وجود نخواهد داشت",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                services.RemoveExpertNationalCardScan({
                    ObjectID: pictureId,
                }, function success() {
                    vm.LoadPictures();
                    swal("با موفقیت حذف گردید", {
                        icon: "success",
                    });
                });
            }
        });
    };
    vm.DeleteInsuranceScan = function (pictureId) {
        swal({
            title: "آیا مطمعن هستید",
            text: "بعد از حذف دیگر امکان بازگرداندن اطلاعات وجود نخواهد داشت",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                services.RemoveExpertInsuranceScan({
                    ObjectID: pictureId,
                }, function success() {
                    vm.LoadPictures();
                    swal("با موفقیت حذف گردید", {
                        icon: "success",
                    });
                });
            }
        });
    };
    vm.Init();
});
