﻿angular.module("isaap").controller('safetyOfficerCertificationsCtrl', function ($scope, services, $state, $uibModal) {
    var vm = $scope;
    vm.isLoaded = false;
    vm.fieldOfCertifications = [];
    vm.certifications = [];
    vm.LoadCertifications = function () {
        services.GetAllSafetyCertifications(function (r) {
            vm.certifications = r.data;
            vm.isLoaded = true;
        });
    }
    vm.Init = function () {
        services.GetAllFieldOfCertifications(function (r) {
            vm.fieldOfCertifications = r.data;
        });
        vm.LoadCertifications();
    }
    vm.OpenAddModal = function () {
        if (vm.isLoaded)
            $uibModal.open(
                {
                    templateUrl: '/Statics/Dashboard/SafetyOfficer/Modals/certification.html',
                    animation: true,
                    controller: function ($scope, services) {
                        var modalvm = $scope;
                        modalvm.title = 'اضافه کردن دوره آموزشی';
                        modalvm.fieldOfCertifications = vm.fieldOfCertifications;
                        modalvm.fieldOfCertificationId = null;
                        modalvm.otherCourseName = null;
                        modalvm.otherDurationInHours = null;
                        modalvm.issuerName = null;
                        modalvm.description = null;
                        modalvm.persianDateTaken = null;
                        modalvm.Close = function () {
                            this.$close();
                        }
                        modalvm.AddOrEditSafetyCertification = function () {
                            services.AddOrEditSafetyCertification({
                                CertificationID: null,
                                FieldOfCertificationID: modalvm.fieldOfCertificationId,
                                PersianDateTaken: modalvm.persianDateTaken,
                                Description: modalvm.description,
                                IssuerName: modalvm.issuerName,
                                OtherCourseName: modalvm.otherCourseName,
                                OtherDurationInHours: modalvm.otherDurationInHours,
                            }, function () {
                                vm.LoadCertifications();
                                modalvm.Close();
                            });
                        }
                    },
                }
            );
    };
    vm.OpenEditModal = function (Id) {
        services.GetSafetyCertification({
            certificationId: Id,
        }, function (r) {
            $uibModal.open(
                {
                    templateUrl: '/Statics/Dashboard/SafetyOfficer/Modals/certification.html',
                    animation: true,
                    controller: function ($scope, services) {
                        var modalvm = $scope;
                        modalvm.title = 'ویرایش دوره آموزشی';
                        modalvm.fieldOfCertifications = vm.fieldOfCertifications;
                        modalvm.fieldOfCertificationId = r.data.FieldOfCertificationId;
                        modalvm.otherCourseName = r.data.OtherCourseName;
                        modalvm.otherDurationInHours = r.data.OtherDurationInHours;
                        modalvm.issuerName = r.data.IssuerName;
                        modalvm.description = r.data.Description;
                        modalvm.persianDateTaken = r.data.PersianDateTaken;
                        modalvm.certificationID = r.data.Id;
                        modalvm.Close = function () {
                            this.$close();
                        }
                        modalvm.AddOrEditSafetyCertification = function () {
                            services.AddOrEditSafetyCertification({
                                CertificationID: modalvm.certificationID,
                                FieldOfCertificationID: modalvm.fieldOfCertificationId,
                                PersianDateTaken: modalvm.persianDateTaken,
                                Description: modalvm.description,
                                IssuerName: modalvm.issuerName,
                                OtherCourseName: modalvm.otherCourseName,
                                OtherDurationInHours: modalvm.otherDurationInHours,
                            }, function () {
                                vm.LoadCertifications();
                                modalvm.Close();
                            });
                        }
                    },
                }
            );
        });
    };
    vm.ConfirmDelete = function (Id) {
        swal({
            title: "آیا مطمعن هستید",
            text: "بعد از حذف دیگر امکان بازگرداندن اطلاعات وجود نخواهد داشت",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                services.DeleteCertification({
                    ObjectID: Id,
                }, function success() {
                    vm.LoadCertifications();
                    swal("با موفقیت حذف گردید", {
                        icon: "success",
                    });
                });
            }
        });
    }
    vm.Init();
});