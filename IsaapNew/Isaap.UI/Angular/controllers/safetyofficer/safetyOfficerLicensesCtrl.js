﻿angular.module("isaap").controller('safetyOfficerLicensesCtrl', function ($scope, services, $state, $uibModal) {
    var vm = $scope;
    vm.licenses = [];
    vm.LoadLicenses = function () {
        services.GetMyLicenses(function (r) {
            vm.licenses = r.data;
        });
    };
    vm.Init = function () {
        vm.LoadLicenses();
    };
    vm.OpenLicenseFlow = function (licenseId) {
        services.GetLicenseFlows({
            licenseId: licenseId,
        }, function (r) {
            $uibModal.open({
                templateUrl: '/Statics/Dashboard/SafetyOfficer/Modals/licenseFlow.html',
                animation: true,
                controller: function ($scope) {
                    $scope.GetFlowType = vm.GetFlowType;
                    $scope.flows = r.data;
                    $scope.Close = function () {
                        this.$close();
                    }
                },
            });
        });
    };
    vm.SendMyLicense = function (licenseId) {
        services.SendLicense({
            LicenseId: licenseId,
        }, function success() {
            vm.LoadLicenses();
        })
    };
    vm.DeleteContractScan = function (pictureId) {
        swal({
            title: "آیا مطمعن هستید",
            text: "بعد از حذف دیگر امکان بازگرداندن اطلاعات وجود نخواهد داشت",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                services.RemoveContractScan({
                    ObjectID: pictureId,
                }, function success() {
                    vm.LoadLicenses();
                    swal("با موفقیت حذف گردید", {
                        icon: "success",
                    });
                });
            }
        });
    };
    vm.GetFlowType = function (flowType) {
        switch (flowType) {
            case 2:
                return 'در دست شخص';
            case 3:
                return 'در دست انجمن شهرستان';
            case 4:
                return 'در دست اداره بازرسی شهرستان';
            case 5:
                return 'در دست کانون استان';
            case 6:
                return 'در دست اداره بازرسی استان';
            case 7:
                return 'دارای صلاحیت';
            case 8:
                return 'رد صلاحیت شده بار اول';
            case 9:
                return 'در دست مدیریت بازرسی استان';
            case 10:
                return 'رد صلاحیت شده بار دوم';
            case 11:
                return 'در دست اداره کل بازرسی کشور';
            case 12:
                return 'غیر فعال شده';
        }
    };
    vm.Objectify = function (licenseId) {
        $uibModal.open({
            templateUrl: '/Statics/Dashboard/SafetyOfficer/Modals/objection.html',
            animation: true,
            controller: function ($scope) {
                var vm2 = $scope;
                vm2.licenseId = licenseId;
                vm2.attachmentIds = [];
                vm2.description = '';
                vm2.LoadAttachments = function () {
                    services.GetTemporaryObjectionAttachments({
                        licenseId: vm2.licenseId,
                    }, function (r) {
                        vm2.attachmentIds = r.data;
                    });
                }
                vm2.LoadAttachments();
                vm2.RemoveObjectionAttachment = function (pictureId) {
                    services.RemoveObjectionAttachment({
                        ObjectID: pictureId,
                    }, function success() {
                        vm2.LoadAttachments();
                    });
                }
                vm2.Objectify = function () {
                    services.Objectify({
                        Description: vm2.description,
                        LicenseID: vm2.licenseId,
                    }, function () {
                        vm.LoadLicenses();
                        vm2.Close();
                    });
                };
                vm2.Close = function () {
                    this.$close();
                }
            },
        });
    };
    vm.CreateUserModal = function (licenseId) {
        $uibModal.open({
            templateUrl: '/Statics/Dashboard/SafetyOfficer/Modals/createuser.html',
            animation: true,
            controller: function ($scope) {
                var vm2 = $scope;
                vm2.licenseId = licenseId;
                vm2.username = '';
                vm2.password = '';
                vm2.passwordAgain = '';
                vm2.CreateLicenseUser = function () {
                    if (vm2.password != vm2.passwordAgain)
                        services.ShowError('کلمه عبور', 'تکرار کلمه عبور یکی نمی باشد');
                    else {
                        swal({
                            title: "آیا مطمعن هستید",
                            text: "بعد از ایجاد این نام کاربری دیگر قادر به تغییر آن نخواهید بود",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        }).then((willCreate) => {
                            if (willCreate) {
                                services.CreateLicenseUser({
                                    Username: vm2.username,
                                    Password: vm2.password,
                                    LicenseId: vm2.licenseId,
                                }, function () {
                                    vm.LoadLicenses();
                                    vm2.Close();
                                    swal("با موفقیت ایجاد گردید", {
                                        icon: "success",
                                    });
                                });
                            }
                        });
                    }                    
                };
                vm2.Close = function () {
                    this.$close();
                };
            },
        });
    };
    vm.Init();
});
