﻿angular.module("isaap").controller('safetyOfficerExperiencesCtrl', function ($scope, services, $state, $uibModal) {
    var vm = $scope;
    vm.isLoaded = false;
    vm.experiences = [];
    vm.LoadExperiences = function () {
        services.GetAllWorkExperiences(function (r) {
            vm.experiences = r.data;
            vm.isLoaded = true;
        });
    }
    vm.Init = function () {
        vm.LoadExperiences();
    }
    vm.OpenAddModal = function () {
        if (vm.isLoaded)
            $uibModal.open(
                {
                    templateUrl: '/Statics/Dashboard/SafetyOfficer/Modals/experience.html',
                    animation: true,
                    controller: function ($scope, services) {
                        var modalvm = $scope;
                        modalvm.title = 'اضافه کردن سابقه کاری';
                        modalvm.experienceID = null;
                        modalvm.persianDateFrom = null;
                        modalvm.persianDateTo = null;
                        modalvm.workshopName = null;
                        modalvm.employerName = null;
                        modalvm.description = null;
                        modalvm.Close = function () {
                            this.$close();
                        }
                        modalvm.AddOrEditWorkExperience = function () {
                            services.AddOrEditWorkExperience({
                                ExperienceID: null,
                                PersianDateFrom: modalvm.persianDateFrom,
                                PersianDateTo: modalvm.persianDateTo,
                                WorkshopName: modalvm.workshopName,
                                EmployerName: modalvm.employerName,
                                Description: modalvm.description,
                            }, function () {
                                vm.LoadExperiences();
                                modalvm.Close();
                            });
                        }
                    },
                }
            );
    };
    vm.OpenEditModal = function (Id) {
        services.GetWorkExperience({
            experienceId: Id,
        }, function (r) {
            $uibModal.open(
                {
                    templateUrl: '/Statics/Dashboard/SafetyOfficer/Modals/experience.html',
                    animation: true,
                    controller: function ($scope, services) {
                        var modalvm = $scope;
                        modalvm.title = 'ویرایش کردن سابقه کاری';
                        modalvm.experienceID = r.data.Id;
                        modalvm.persianDateFrom = r.data.PersianDateFrom;
                        modalvm.persianDateTo = r.data.PersianDateTo;
                        modalvm.workshopName = r.data.WorkshopName;
                        modalvm.employerName = r.data.EmployerName;
                        modalvm.description = r.data.Description;
                        modalvm.Close = function () {
                            this.$close();
                        }
                        modalvm.AddOrEditWorkExperience = function () {
                            services.AddOrEditWorkExperience({
                                ExperienceID: modalvm.experienceID,
                                PersianDateFrom: modalvm.persianDateFrom,
                                PersianDateTo: modalvm.persianDateTo,
                                WorkshopName: modalvm.workshopName,
                                EmployerName: modalvm.employerName,
                                Description: modalvm.description,
                            }, function () {
                                vm.LoadExperiences();
                                modalvm.Close();
                            });
                        }
                    },
                }
            );
        });
    };
    vm.ConfirmDelete = function (Id) {
        swal({
            title: "آیا مطمعن هستید",
            text: "بعد از حذف دیگر امکان بازگرداندن اطلاعات وجود نخواهد داشت",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                services.DeleteWorkExperience({
                    ObjectID: Id,
                }, function success() {
                    vm.LoadExperiences();
                    swal("با موفقیت حذف گردید", {
                        icon: "success",
                    });
                });
            }
        });
    }
    vm.Init();
});