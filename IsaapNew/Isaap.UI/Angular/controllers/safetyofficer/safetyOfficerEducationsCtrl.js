﻿angular.module("isaap").controller('safetyOfficerEducationsCtrl', function ($scope, services, $state, $uibModal) {
    var vm = $scope;
    vm.isLoaded = false;
    vm.fieldOfEducations = [];
    vm.educations = [];
    vm.LoadEducations = function () {
        services.GetAllEducations(function (r) {
            vm.educations = r.data;
            vm.isLoaded = true;
        });
    }
    vm.Init = function () {
        services.GetAllFieldOfEducations(function (r) {
            vm.fieldOfEducations = r.data;
        });
        vm.LoadEducations();
    }
    vm.OpenAddModal = function () {
        if (vm.isLoaded)
            $uibModal.open(
                {
                    templateUrl: '/Statics/Dashboard/SafetyOfficer/Modals/education.html',
                    animation: true,
                    controller: function ($scope, services) {
                        var modalvm = $scope;
                        modalvm.title = 'اضافه کردن تحصیلات';
                        modalvm.fieldOfEducationID = null;
                        modalvm.degreeLevelID = null;
                        modalvm.fieldOfEducations = vm.fieldOfEducations;
                        modalvm.Close = function () {
                            this.$close();
                        }
                        modalvm.AddOrEditEducation = function () {
                            services.AddOrEditEducation({
                                EducationID: null,
                                DegreeLevel: modalvm.degreeLevelID,
                                FieldOfEducationID: modalvm.fieldOfEducationID,
                            }, function () {
                                vm.LoadEducations();
                                modalvm.Close();
                            });
                        }
                    },
                }
            );
    };
    vm.OpenEditModal = function (Id) {
        services.GetEducation({
            educationId: Id,
        }, function (r) {
            $uibModal.open(
                {
                    templateUrl: '/Statics/Dashboard/SafetyOfficer/Modals/education.html',
                    animation: true,
                    controller: function ($scope, services) {
                        var modalvm = $scope;
                        modalvm.title = 'ویرایش کردن تحصیلات';
                        modalvm.fieldOfEducationID = r.data.FieldOfEducationId;
                        modalvm.degreeLevelID = r.data.DegreeLevel;
                        modalvm.fieldOfEducations = vm.fieldOfEducations;
                        modalvm.Close = function () {
                            this.$close();
                        }
                        modalvm.AddOrEditEducation = function () {
                            services.AddOrEditEducation({
                                EducationID: r.data.Id,
                                DegreeLevel: modalvm.degreeLevelID,
                                FieldOfEducationID: modalvm.fieldOfEducationID,
                            }, function () {
                                vm.LoadEducations();
                                modalvm.Close();
                            });
                        }
                    },
                }
            );
        });
    };
    vm.ConfirmDelete = function (Id) {
        swal({
            title: "آیا مطمعن هستید",
            text: "بعد از حذف دیگر امکان بازگرداندن اطلاعات وجود نخواهد داشت",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                services.DeleteEducation({
                    ObjectID: Id,
                }, function success() {
                    vm.LoadEducations();
                    swal("با موفقیت حذف گردید", {
                        icon: "success",
                    });
                });
            }
        });
    };
    vm.Init();
});