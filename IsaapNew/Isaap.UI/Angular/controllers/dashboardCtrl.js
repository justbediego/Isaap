﻿angular.module("isaap").controller('dashboardCtrl', function ($scope, services, $uibModal, $state) {
    var vm = $scope;
    vm.userType;
    vm.fullName;
    vm.progressData;
    vm.photoLink = '/Resource/images/person.png';
    vm.Init = function () {
        services.IsLoggedIn(function (r1) {
            if (!r1.data) {
                $state.go('infopage.home');
            } else {
                services.GetUserType(function (r2) {
                    vm.userType = r2.data;
                    switch (r2.data) {
                        case 1:
                            services.GetExpertDashboardData(function (r3) {
                                vm.fullName = r3.data.FullName;
                                if (r3.data.PhotoId)
                                    vm.photoLink = '/Api/Expert/GetImage?ID=' + r3.data.PhotoId + '&height=60&width=60';
                            });
                            break;
                        case 9:
                            services.GetCEODashboardData(function (r3) {
                                vm.fullName = r3.data.FullName;
                                if (r3.data.PhotoId)
                                    vm.photoLink = '/Api/Expert/GetImage?ID=' + r3.data.PhotoId + '&height=60&width=60';
                            });
                            break;
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                            services.GetStaffDashboardData(function (r3) {
                                vm.fullName = r3.data.FullName;
                                if (r3.data.PhotoId)
                                    vm.photoLink = '/Api/Expert/GetImage?ID=' + r3.data.PhotoId + '&height=60&width=60';
                            });
                            break;
                    }
                    setTimeout('jQueryInit()', 100);
                });
            }
        });
    };
    vm.CheckProfileProgress = function () {
        vm.progressData = null;
        switch (vm.userType) {
            case 1:
                services.CheckExpertProfileProgress(function (r) {
                    vm.progressData = r.data;
                });
                break;
            case 9:
                services.CheckCEOProfileProgress(function (r) {
                    vm.progressData = r.data;
                });
                break;
        }
    }
    vm.Logout = function () {
        services.Logout(function success() {
            vm.Init();
        });
    };
    vm.OpenChangePassword = function () {
        $uibModal.open({
            templateUrl: '/Statics/Modals/changePassword.html',
            animation: true,
            controller: function ($scope) {
                var vm2 = $scope;
                vm2.ChangePassword = function () {
                    if (vm2.newPassword != vm2.newPasswordAgain)
                        services.ShowError('کلمه عبور', 'تکرار کلمه عبور یکی نمی باشد');
                    else {
                        services.ChangePassword({
                            OldPassword: vm2.oldPassword,
                            NewPassword: vm2.newPassword,
                        }, function () {
                            services.ShowSuccess('تغییر گذرواژه', 'با موفقیت انجام شد.');
                            vm2.Close();
                        });
                    }
                }
                vm2.Close = function () {
                    this.$close();
                }
            },
        });
    }
    vm.Init();
});