﻿angular.module("isaap").controller('licenseMyInfoCtrl', function ($scope, services, $state) {
    var vm = $scope;
    vm.data = [];
    vm.Init = function () {
        services.GetMyProfileData(function (r) {
            vm.data = r.data;
        });
    };
    vm.Init();
});