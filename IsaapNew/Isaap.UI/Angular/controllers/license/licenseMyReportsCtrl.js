﻿angular.module("isaap").controller('licenseMyReportsCtrl', function ($scope, services, $state, $uibModal) {
    var vm = $scope;
    vm.modalType = 1;
    
    vm.OpenAddModal = function () {
        $uibModal.open({
            windowClass: "modalCss",
            templateUrl: '/Statics/Dashboard/License/Modals/reportType' + vm.modalType + '.html',
            animation: true,
            controller: function ($scope) {
                var vm2 = $scope;              
                vm2.Close = function () {
                    this.$close();
                }
            },
        });
    };
});