﻿angular.module("isaap").controller('ceoRecruitmentCtrl', function ($scope, services, $state) {
    var vm = $scope;
    vm.searchBox = "";
    vm.result = [];
    vm.hasResult = false;
    vm.Search = function () {
        vm.hasResult = false;
        vm.result = [];
        services.FindExperts({
            filter: vm.searchBox
        }, function (r) {
            vm.result = r.data;
            vm.hasResult = true;
        });
    };
    vm.Hire = function (username) {
        swal({
            title: "آیا مطمعن هستید",
            text: "این پروانه بعنوان درخواست در کارتابل مسئول ایمنی ایجاد شده و پیگیری روند تایید صلاحیت از طریق وی صورت خواهد گرفت",
            icon: "warning",
            buttons: true,
            dangerMode: false,
        }).then((willHire) => {
            if (willHire) {
                services.HireAnExpert({
                    ExpertUsername: username,
                }, function success() {
                    swal("پروانه با موفقیت ایجاد گردید", {
                        icon: "success",
                    });
                });
            }
        });
    };
});