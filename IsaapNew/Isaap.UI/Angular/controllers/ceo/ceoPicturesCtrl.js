﻿angular.module("isaap").controller('ceoPicturesCtrl', function ($scope, services, $state) {
    var vm = $scope;
    vm.LoadPictures = function () {
        services.GetCEOPictures(function success(r) {
            vm.photoId = r.data.PhotoScanId;
            vm.shenasnameScanIds = r.data.ShenasnameScanIds;
            vm.nationalCardScanIds = r.data.NationalCardScanIds;
            vm.insuranceScanIds = r.data.InsuranceScanIds;
            vm.hasTPC = r.data.HasTPC;
            vm.tpcScanIds = r.data.TPCScanIds;
        });
    };
    vm.Init = function () {
        vm.LoadPictures();
    };
    vm.ReloadPage = function () {
        location.reload();
    }
    vm.DeletePhotoScan = function () {
        swal({
            title: "آیا مطمعن هستید",
            text: "بعد از حذف دیگر امکان بازگرداندن اطلاعات وجود نخواهد داشت",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                services.RemoveCEOPhotoScan(function success() {
                    vm.ReloadPage();
                    swal("با موفقیت حذف گردید", {
                        icon: "success",
                    });
                });
            }
        });
    };
    vm.DeleteShenasnameScan = function (pictureId) {
        swal({
            title: "آیا مطمعن هستید",
            text: "بعد از حذف دیگر امکان بازگرداندن اطلاعات وجود نخواهد داشت",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                services.RemoveCEOShenasnameScan({
                    ObjectID: pictureId,
                }, function success() {
                    vm.LoadPictures();
                    swal("با موفقیت حذف گردید", {
                        icon: "success",
                    });
                });
            }
        });
    };
    vm.DeleteNationalCardScan = function (pictureId) {
        swal({
            title: "آیا مطمعن هستید",
            text: "بعد از حذف دیگر امکان بازگرداندن اطلاعات وجود نخواهد داشت",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                services.RemoveCEONationalCardScan({
                    ObjectID: pictureId,
                }, function success() {
                    vm.LoadPictures();
                    swal("با موفقیت حذف گردید", {
                        icon: "success",
                    });
                });
            }
        });
    };
    vm.DeleteInsuranceScan = function (pictureId) {
        swal({
            title: "آیا مطمعن هستید",
            text: "بعد از حذف دیگر امکان بازگرداندن اطلاعات وجود نخواهد داشت",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                services.RemoveCEOInsuranceScan({
                    ObjectID: pictureId,
                }, function success() {
                    vm.LoadPictures();
                    swal("با موفقیت حذف گردید", {
                        icon: "success",
                    });
                });
            }
        });
    };
    vm.DeleteTPCScan = function (pictureId) {
        swal({
            title: "آیا مطمعن هستید",
            text: "بعد از حذف دیگر امکان بازگرداندن اطلاعات وجود نخواهد داشت",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                services.RemoveTPCScan({
                    ObjectID: pictureId,
                }, function success() {
                    vm.LoadPictures();
                    swal("با موفقیت حذف گردید", {
                        icon: "success",
                    });
                });
            }
        });
    };
    vm.Init();
});
