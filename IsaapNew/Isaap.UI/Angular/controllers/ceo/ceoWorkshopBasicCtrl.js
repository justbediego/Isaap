﻿angular.module("isaap").controller('ceoWorkshopBasicCtrl', function ($scope, services, $state) {
    var vm = $scope;
    vm.isLoaded = false;
    vm.productionTypes = [];
    vm.fieldOfActivities = [];
    vm.Init = function () {
        vm.unitName = services.WaitMessage;
        vm.workshopSize = services.WaitMessage;
        vm.companyCode = services.WaitMessage;
        vm.nightWorkersNo = services.WaitMessage;
        vm.morningWorkersNo = services.WaitMessage;
        vm.safetyWorkersNo = services.WaitMessage;
        vm.maleWorkersNo = services.WaitMessage;
        vm.eveningWorkersNo = services.WaitMessage;
        vm.femaleWorkersNo = services.WaitMessage;
        vm.socialSecurityCode = services.WaitMessage;
        vm.fieldOfActivityId = null;
        vm.productionTypeId = null;
        vm.technicalProtectionCommittee = null;
        services.GetAllProductionTypes(function (r) {
            vm.productionTypes = r.data;
        });
        services.GetAllFieldOfActivities(function (r) {
            vm.fieldOfActivities = r.data;
        });
        vm.LoadData();
    };
    vm.LoadData = function () {
        services.GetWorkshopInfo(function (r) {
            vm.unitName = r.data.UnitName;
            vm.workshopSize = r.data.WorkshopSize;
            vm.companyCode = r.data.CompanyCode;
            vm.nightWorkersNo = r.data.NightWorkersNo;
            vm.morningWorkersNo = r.data.MorningWorkersNo;
            vm.safetyWorkersNo = r.data.SafetyWorkersNo;
            vm.maleWorkersNo = r.data.MaleWorkersNo;
            vm.eveningWorkersNo = r.data.EveningWorkersNo;
            vm.femaleWorkersNo = r.data.FemaleWorkersNo;
            vm.fieldOfActivityId = r.data.FieldOfActivityId;
            vm.productionTypeId = r.data.ProductionTypeId;
            vm.technicalProtectionCommittee = r.data.TechnicalProtectionCommittee;
            vm.socialSecurityCode = r.data.SocialSecurityCode;
            vm.isLoaded = true;
        });
    }
    vm.ModifyWorkshopInfo = function () {
        if (vm.isLoaded = false)
            return;
        services.ModifyWorkshopInfo({
            UnitName: vm.unitName,
            WorkshopSize: vm.workshopSize,
            CompanyCode: vm.companyCode,
            NightWorkersNo: vm.nightWorkersNo,
            MorningWorkersNo: vm.morningWorkersNo,
            SafetyWorkersNo: vm.safetyWorkersNo,
            MaleWorkersNo: vm.maleWorkersNo,
            EveningWorkersNo: vm.eveningWorkersNo,
            FemaleWorkersNo: vm.femaleWorkersNo,
            FieldOfActivityId: vm.fieldOfActivityId,
            ProductionTypeId: vm.productionTypeId,
            TechnicalProtectionCommittee: vm.technicalProtectionCommittee,
            SocialSecurityCode: vm.socialSecurityCode,
        }, function () {
            vm.LoadData();
            services.ShowSuccess('تغییرات', 'با موفقیت انجام شد');
        });
    };
    vm.Init();
});