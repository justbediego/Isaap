﻿angular.module("isaap").controller('ceoWorkshopLocationCtrl', function ($scope, services, $state) {
    var vm = $scope;
    vm.isLoaded = false;
    vm.provinces = [];
    vm.counties = [];
    vm.provinceID;
    vm.countyID;
    vm.phone = services.WaitMessage;
    vm.postalCode = services.WaitMessage;
    vm.address = services.WaitMessage;
    vm.Init = function () {
        services.GetAllProvinces(function (r) {
            vm.provinces = r.data;
        });
        services.GetWorkshopLocationInfo(function (r) {
            vm.provinceID = r.data.ProvinceID;
            vm.countyID = r.data.CountyID;
            vm.phone = r.data.Phone;
            vm.postalCode = r.data.PostalCode;
            vm.address = r.data.Address;
            vm.isLoaded = true;
            vm.SelectProvince();
        });
    }
    vm.SelectProvince = function () {
        if (vm.provinceID > 0) {
            services.GetAllCounties({ provinceID: vm.provinceID }, function (r) {
                vm.counties = r.data;
            });
        }
    }
    vm.ModifyWorkshopLocation = function () {
        if (!vm.isLoaded)
            return;
        services.ModifyWorkshopLocation({
            CountyID: vm.countyID,
            Address: vm.address,
            PostalCode: vm.postalCode,
            Phone: vm.phone,
        }, function () {
            services.ShowSuccess('تغییرات', 'با موفقیت انجام شد');
        });
    };
    vm.Init();
});