﻿angular.module("isaap").controller('ceoBasicCtrl', function ($scope, services, $state) {
    var vm = $scope;
    vm.isLoaded = false;
    vm.Init = function () {
        vm.firstName = services.WaitMessage;
        vm.lastName = services.WaitMessage;
        vm.fatherName = services.WaitMessage;
        vm.gender;
        vm.shenasnameNo = services.WaitMessage;
        vm.nationalCode = services.WaitMessage;
        vm.placeOfBirth = services.WaitMessage;
        vm.maritalStatus;
        vm.militaryServiceStates;
        vm.reasonOfExemption = services.WaitMessage;
        vm.associationNumber = services.WaitMessage;
        vm.persianBirthDate = services.WaitMessage;
        services.GetCEOBasicInfo(function (r) {
            vm.firstName = r.data.FirstName;
            vm.lastName = r.data.LastName;
            vm.fatherName = r.data.FatherName;
            vm.gender = r.data.Gender;
            vm.shenasnameNo = r.data.ShenasnameNo;
            vm.nationalCode = r.data.NationalCode;
            vm.placeOfBirth = r.data.PlaceOfBirth;
            vm.maritalStatus = r.data.MaritalStatus;
            vm.militaryServiceStates = r.data.MilitaryServiceStates;
            vm.reasonOfExemption = r.data.ReasonOfExemption;
            vm.persianBirthDate = r.data.PersianBirthDate;
            vm.isLoaded = true;
        });
    };
    vm.ModifyCEOBasicInfo = function () {
        if (vm.isLoaded = false)
            return;
        services.ModifyCEOBasicInfo({
            FirstName: vm.firstName,
            LastName: vm.lastName,
            FatherName: vm.fatherName,
            Gender: vm.gender,
            ShenasnameNo: vm.shenasnameNo,
            NationalCode: vm.nationalCode,
            PlaceOfBirth: vm.placeOfBirth,
            MaritalStatus: vm.maritalStatus,
            MilitaryServiceStates: vm.militaryServiceStates,
            ReasonOfExemption: vm.reasonOfExemption,
            PersianBirthDate: vm.persianBirthDate,
        }, function () {
            location.reload();
            //services.ShowSuccess('تغییرات', 'با موفقیت انجام شد');
        });
    };
    vm.Init();
});