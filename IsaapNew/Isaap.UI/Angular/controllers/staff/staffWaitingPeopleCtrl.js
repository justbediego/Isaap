﻿angular.module("isaap").controller('staffWaitingPeopleCtrl', function ($scope, services, $state) {
    var vm = $scope;
    vm.waitingExperts = [];
    vm.filterBox = '';
    vm.Filter = function () {
        vm.LoadWaitingPeople();
    };
    vm.Init = function () {
        vm.LoadWaitingPeople();
    }
    vm.GetFlowType = function (flowType) {
        switch (flowType) {
            case 2:
                return 'در دست شخص';
            case 3:
                return 'در دست انجمن شهرستان';
            case 4:
                return 'در دست اداره بازرسی شهرستان';
            case 5:
                return 'در دست کانون استان';
            case 6:
                return 'در دست اداره بازرسی استان';
            case 7:
                return 'دارای صلاحیت';
            case 8:
                return 'رد صلاحیت شده بار اول';
            case 9:
                return 'در دست مدیریت بازرسی استان';
            case 10:
                return 'رد صلاحیت شده بار دوم';
            case 11:
                return 'در دست اداره کل بازرسی کشور';
            case 12:
                return 'غیر فعال شده';
        }
    };
    vm.LoadWaitingPeople = function () {
        services.GetMyWaitingExperts({
            filter: vm.filterBox,
        }, function (r) {
            vm.waitingExperts = r.data;
        });
    }
    vm.Init();
});