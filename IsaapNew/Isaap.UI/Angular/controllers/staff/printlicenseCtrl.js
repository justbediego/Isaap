﻿angular.module("isaap").controller('printlicenseCtrl', function ($scope, services, $state, $stateParams) {
    if (!$stateParams.LID)
        services.ShowError('خطا', 'صفحه بدرستی بارگذاری نشده است');
    else {
        var vm = $scope;
        vm.data = null;
        vm.Init = function () {
            services.GetLicensePrintData({
                licenseId: $stateParams.LID,
            }, function (r) {
                vm.data = r.data;
                vm.data.LicenseId = $stateParams.LID;
            });
        };
        vm.Init();
    }
});