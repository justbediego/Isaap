﻿angular.module("isaap").controller('staffViewPersonCtrl', function ($scope, services, $state, $stateParams, $uibModal) {
    if (!$stateParams.U)
        services.ShowError('خطا', 'صفحه بدرستی بارگذاری نشده است');
    else {
        var vm = $scope;
        vm.data = null;
        vm.Init = function () {
            vm.LoadExpert();
        };
        vm.LoadExpert = function () {
            vm.data = null;
            services.GetExpertData({
                username: $stateParams.U,
            }, function (r) {
                vm.data = r.data;
            })
        };
        vm.GetFlowType = function (flowType) {
            switch (flowType) {
                case 2:
                    return 'در دست شخص';
                case 3:
                    return 'در دست انجمن شهرستان';
                case 4:
                    return 'در دست اداره بازرسی شهرستان';
                case 5:
                    return 'در دست کانون استان';
                case 6:
                    return 'در دست اداره بازرسی استان';
                case 7:
                    return 'دارای صلاحیت';
                case 8:
                    return 'رد صلاحیت شده بار اول';
                case 9:
                    return 'در دست مدیریت بازرسی استان';
                case 10:
                    return 'رد صلاحیت شده بار دوم';
                case 11:
                    return 'در دست اداره کل بازرسی کشور';
                case 12:
                    return 'غیر فعال شده';
            }
        };
        vm.RejectLicenseAuthentication = function (licenseId, description) {
            services.RejectLicenseAuthentication({
                LicenseId: licenseId,
                Description: description,
            }, function (r) {
                services.ShowSuccess('تصمیم گیری', 'با موفقیت انجام شد');
                vm.LoadExpert();
            })
        };
        vm.RecognizeLicense = function (licenseId, description) {
            services.RecognizeLicense({
                LicenseId: licenseId,
                Description: description,
            }, function (r) {
                services.ShowSuccess('تصمیم گیری', 'با موفقیت انجام شد');
                vm.LoadExpert();
            })
        }
        vm.RejectLicenseRecognition = function (licenseId, description) {
            services.RejectLicenseRecognition({
                LicenseId: licenseId,
                Description: description,
            }, function (r) {
                services.ShowSuccess('تصمیم گیری', 'با موفقیت انجام شد');
                vm.LoadExpert();
            })
        }
        vm.AuthenticateLicense = function (licenseId, description) {
            services.AuthenticateLicense({
                LicenseId: licenseId,
                Description: description,
            }, function (r) {
                services.ShowSuccess('تصمیم گیری', 'با موفقیت انجام شد');
                vm.LoadExpert();
            })
        }
        vm.OpenLicenseFlow = function (licenseId) {
            services.StaffGetLicenseFlows({
                licenseId: licenseId,
            }, function (r) {
                $uibModal.open({
                    templateUrl: '/Statics/Dashboard/Staff/Modals/licenseFlow.html',
                    animation: true,
                    controller: function ($scope) {
                        $scope.GetFlowType = vm.GetFlowType;
                        $scope.flows = r.data;
                        $scope.Close = function () {
                            this.$close();
                        }
                    },
                });
            });
        };
        vm.Init();
    }
});