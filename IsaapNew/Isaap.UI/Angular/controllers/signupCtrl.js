﻿angular.module("isaap").controller('signupCtrl', function ($scope, services, $uibModal, $state, vcRecaptchaService) {
    var vm = $scope;
    vm.registerType = 1;
    vm.OpenAgreementModal = function () {
        $uibModal.open({
            templateUrl: '/Statics/Modals/agreement.html',
            animation: true,
            controller: function ($scope) {
                $scope.Close = function () {
                    this.$close();
                }
            },
        });
    };
    vm.Register = function () {
        if (vm.password != vm.cpassword) {
            services.ShowError('کلمه عبور', 'تکرار کلمه عبور یکی نمی باشد');
            return;
        }
        if (vm.agreement != true) {
            services.ShowError('قوانین سامانه', 'لطفاً با قوانین سامانه موافقت بفرمایید');
            return;
        }
        if (vm.registerType == 1) {
            services.RegisterExpert({
                Username: vm.username,
                Password: vm.password,
                Email: vm.email,
                Mobile: vm.mobile,
                Captcha: vm.recaptcha,
                FirstName: vm.firstname,
                LastName: vm.lastname,
                NationalCode: vm.nationalcode,
            }, function success() {
                services.ShowSuccess('ثبت نام', 'با موفقیت انجام شد. لطفاً وارد شوید');
                $state.go('infopage.home');
            }, function fail() {
                vcRecaptchaService.reload();
            });
        }
        if (vm.registerType == 2) {
            services.RegisterCEO({
                CompanyCode: vm.companyCode,
                Username: vm.username,
                Password: vm.password,
                Email: vm.email,
                Mobile: vm.mobile,
                Captcha: vm.recaptcha,
                FirstName: vm.firstname,
                LastName: vm.lastname,
                NationalCode: vm.nationalcode,
            }, function success() {
                services.ShowSuccess('ثبت نام', 'با موفقیت انجام شد. لطفاً وارد شوید');
                $state.go('infopage.home');
            }, function fail() {
                vcRecaptchaService.reload();
            });
        }
    }
});