﻿angular.module("isaap").controller('homeCtrl', function ($scope, services, $state) {
    var vm = $scope;
    vm.Init = function () {
        services.IsLoggedIn(function (r1) {
            if (r1.data) {
                services.GetUserType(function (r2) {
                    switch (r2.data) {
                        case 1:
                            $state.go('dashboard.safetyofficer.profile.basic');
                            break;
                        case 9:
                            $state.go('dashboard.ceo.profile.basic');
                            break;
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                            $state.go('dashboard.staff.waitingpeople');
                            break;
                        case 10:
                            $state.go('dashboard.license.myinfo');
                            break;
                    }
                });
            }
        });
    }
    vm.Login = function () {
        services.Login({
            Username: vm.username,
            Password: vm.password,
            IsPersistent: vm.ispersistent,
        }, function success() {
            vm.Init();
        });
    };
    vm.Init();
});