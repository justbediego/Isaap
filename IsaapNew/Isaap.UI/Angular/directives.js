﻿angular.module('isaap')
    .directive('input', function () {
        return function (scope, element, attrs) {
            element.bind('focus', function () {
                element.parent().addClass('focused');
            });
            function checkValue() {
                if (element.val() !== '') {
                    element.parents('.form-line').addClass('focused');
                } else {
                    element.parents('.form-line').removeClass('focused');
                }
            }
            element.bind('blur', function () {
                checkValue();
            });
            element.parents('.form-line').find('.form-label').bind('click', function () {
                element.focus();
            });
            scope.$watch(attrs.ngModel, function (value) {
                checkValue();
            });
        };
    })
    .directive('hosseinFileUpload', function (services) {
        return {
            scope: {
                actionUrl: '@',
                title: '@',
                success: "&"
            },
            restrict: 'E',
            template: '<div class="progress"><div class="progress-bar progress-bar-primary"></div></div><span class="btn btn-warning fileinput-button"><span>{{title}}</span><input class="fileupload" type="file" name="files"></span>',
            link: function (scope, el, attrs) {
                scope.title = attrs.title;
                el.find(".progress").hide();
                el.find(".progress .progress-bar").css('width', 0);
                el.find(".fileupload").fileupload({
                    url: attrs.actionUrl,
                    formData: {
                        Id: attrs.data,
                    },
                    done: function () {
                        el.find(".progress").hide();
                        el.find(".fileinput-button").show();
                        el.find(".progress .progress-bar").css('width', 0);
                        scope.success();
                    },
                    error: function (response) {
                        el.find(".progress").hide();
                        el.find(".fileinput-button").show();
                        el.find(".progress .progress-bar").css('width', 0);
                        services.ShowAlertModal('خطا', response.responseJSON.ExceptionMessage);
                    },
                    progressall: function (e, data) {
                        var progress = parseInt(data.loaded / data.total * 100, 10);
                        el.find(".progress").show();
                        el.find(".fileinput-button").hide();
                        el.find(".progress .progress-bar").css('width', progress + '%');
                    }
                });
            }
        }
    }).directive('viewerjs', function () {
        return {
            restrict: 'A',
            link: function (scope, el, attrs) {
                $(el).viewer({ navbar: false });
            }
        }
    });
    //.directive('input', function () {
    //    return function (scope, element, attrs) {
    //        if ($.fn.selectpicker) { $('select:not(.ms)').selectpicker(); }
    //    }
    //});