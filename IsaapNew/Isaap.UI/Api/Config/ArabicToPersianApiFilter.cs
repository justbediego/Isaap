﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using Newtonsoft.Json;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Net.Http;

namespace Isaap.UI.Api.Config
{
    public class ArabicToPersianApiFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext filterContext)
        {
            List<string> keys = filterContext.ActionArguments.Select(arg => arg.Key).ToList();
            keys.ForEach(k =>
            {
                var obj = filterContext.ActionArguments[k];
                if (obj != null)
                {
                    Type type = filterContext.ActionArguments[k].GetType();
                    filterContext.ActionArguments.Remove(k);
                    string jsonData = JsonConvert.SerializeObject(obj);
                    jsonData = jsonData.Replace("ك", "ک")
                                       .Replace("ي", "ی");
                    var newObj = JsonConvert.DeserializeObject(jsonData, type);
                    filterContext.ActionArguments.Add(k, newObj);
                }
            });
        }
    }
}