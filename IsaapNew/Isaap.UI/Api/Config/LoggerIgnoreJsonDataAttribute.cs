﻿using System;

namespace Isaap.UI.Api.Config
{
    internal class LoggerIgnoreJsonDataAttribute : Attribute
    {
        public string[] HiddenData;
        public LoggerIgnoreJsonDataAttribute(params string[] data)
        {
            HiddenData = data;
        }
    }
}