﻿using Isaap.Core.Business;
using Isaap.Core.DataAccess;
using Microsoft.Owin;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Isaap.UI.Api.Config
{
    public class ActionLoggerApiFilter : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Request.Method.Method == "POST" && actionExecutedContext.Exception == null)
            {
                try
                {
                    var httpContext = actionExecutedContext.Request.Properties.Where(rp => rp.Key == "MS_HttpContext").Select(rp => (HttpContextBase)rp.Value).FirstOrDefault();
                    if (httpContext != null)
                    {
                        string path = httpContext.Request.Path;
                        string ip = httpContext.Request.UserHostAddress;
                        string username = httpContext.User.Identity.Name;
                        var stream = httpContext.Request.InputStream;
                        byte[] data = new byte[stream.Length];
                        string stringData = null;
                        if (data.Length > 0)
                        {
                            stream.Read(data, 0, data.Length);
                            stringData = Encoding.UTF8.GetString(data);
                            try
                            {
                                var jsonData = (JToken)JsonConvert.DeserializeObject(stringData);
                                //can be seen as json
                                var actionDescriptor = ((ReflectedHttpActionDescriptor)actionExecutedContext.ActionContext.ActionDescriptor);
                                var ignoredFieldAttribute = actionDescriptor.GetCustomAttributes<LoggerIgnoreJsonDataAttribute>().FirstOrDefault();
                                string[] ignoredFields = ignoredFieldAttribute == null ? new string[] { } : ignoredFieldAttribute.HiddenData;
                                var jsonProperties = jsonData.Children<JProperty>().Where(c => ignoredFields.Any(igf => igf.ToLower() == c.Name.ToLower()));
                                for (int i = jsonProperties.Count() - 1; i >= 0; i--)
                                    jsonProperties.ElementAt(i).Remove();
                                stringData = JsonConvert.SerializeObject(jsonData);
                            }
                            catch { }
                        }
                        IsaapContext dbContext = new IsaapContext();
                        LogBusiness logBusiness = new LogBusiness(dbContext);
                        logBusiness.LogPostAction(path, ip, username, string.IsNullOrEmpty(stringData) ? null : stringData.Length > 1000 ? stringData.Substring(0, 1000) : stringData);
                    }
                }
                catch
                {

                }
            }
            base.OnActionExecuted(actionExecutedContext);
        }
    }
}