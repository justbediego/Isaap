﻿using Isaap.Core.Business;
using Isaap.Core.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Isaap.UI.Api
{
    [Authorize]
    public class LicenseController : ApiController
    {
        private IsaapContext dbContext;
        private LicenseBusiness licenseBusiness;
        private AuthenticationController authenticationController;
        public LicenseController()
        {
            dbContext = new IsaapContext();
            licenseBusiness = new LicenseBusiness(dbContext);
            authenticationController = new AuthenticationController(); 
        }
        [HttpGet]
        public object GetMyProfileData()
        {
            return licenseBusiness.GetMyProfileData(authenticationController.GetUsername());
        }
    }
}
