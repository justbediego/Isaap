﻿using Isaap.Core.Business;
using Isaap.Core.DataAccess;
using Isaap.UI.Api.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace Isaap.UI.Api
{
    [Authorize]
    public class StaffController : ApiController
    {
        private IsaapContext dbContext;
        private StaffBusiness staffBusiness;
        private AuthenticationController authenticationController;
        public StaffController()
        {
            dbContext = new IsaapContext();
            staffBusiness = new StaffBusiness(dbContext);
            authenticationController = new AuthenticationController();
        }
        [HttpGet]
        public object GetDashboardData()
        {
            return staffBusiness.GetDashboardData(authenticationController.GetUsername());
        }
        [HttpGet]
        public object GetMyWaitingExperts(string filter = null)
        {
            return staffBusiness.GetMyWaitingExperts(authenticationController.GetUsername(), filter);
        }
        [HttpGet]
        public object GetExpertData(string username)
        {
            return staffBusiness.GetExpertData(authenticationController.GetUsername(), username);
        }
        [HttpPost]
        public void RecognizeLicense(LicenseDecisionDTO model)
        {
            staffBusiness.RecognizeLicense(authenticationController.GetUsername(), model.LicenseId, model.Description);
        }
        [HttpPost]
        public void RejectLicenseRecognition(LicenseDecisionDTO model)
        {
            staffBusiness.RejectLicenseRecognition(authenticationController.GetUsername(), model.LicenseId, model.Description);
        }
        [HttpPost]
        public void AuthenticateLicense(LicenseDecisionDTO model)
        {
            staffBusiness.AuthenticateLicense(authenticationController.GetUsername(), model.LicenseId, model.Description);
        }
        [HttpPost]
        public void RejectLicenseAuthentication(LicenseDecisionDTO model)
        {
            staffBusiness.RejectLicenseAuthentication(authenticationController.GetUsername(), model.LicenseId, model.Description);
        }
        [HttpGet]
        public object GetLicenseFlows(int licenseId)
        {
            return staffBusiness.GetLicenseFlows(authenticationController.GetUsername(), licenseId);
        }
        [HttpGet]
        public object GetLicensePrintData(int licenseId)
        {
            return staffBusiness.GetLicensePrintData(authenticationController.GetUsername(), licenseId);
        }
    }
}
