﻿using Isaap.Core.Business;
using Isaap.Core.DataAccess;
using Isaap.UI.Api.Config;
using Isaap.UI.Api.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace Isaap.UI.Api
{
    public class CEOController : ApiController
    {
        private IsaapContext dbContext;
        private CEOBusiness ceoBusiness;
        private AuthenticationController authenticationController;
        public CEOController()
        {
            dbContext = new IsaapContext();
            ceoBusiness = new CEOBusiness(dbContext);
            authenticationController = new AuthenticationController();
        }
        [LoggerIgnoreJsonData("Password")]
        public void Register(UserRegisterDTO model)
        {
            HttpClient client = new HttpClient();
            Dictionary<string, string> postData = new Dictionary<string, string> {
                { "secret", "6LdVGCAUAAAAAP4HIDkp9xT6pBlkwYHz2eAFdXIK" },
                { "response", model.Captcha },
                //{"remoteip", HttpContext.Current.Request.UserHostAddress }
            };
            var postResponse = client.PostAsync("https://www.google.com/recaptcha/api/siteverify", new FormUrlEncodedContent(postData)).Result;
            var responseString = postResponse.Content.ReadAsStringAsync().Result;
            recaptchaResponse googleResponse = JsonConvert.DeserializeObject<recaptchaResponse>(responseString);
            if (!googleResponse.success)
                throw new Exception("کد امنیتی مورد تایید نیست");
            ceoBusiness.Register(model.CompanyCode, model.NationalCode, model.FirstName, model.LastName, model.Username, model.Password, model.Email, model.Mobile);
        }
        [Authorize]
        [HttpPost]
        public void ModifyBasicInfo(ChangePersonInfoDTO model)
        {
            ceoBusiness.ModifyBasicInfo(authenticationController.GetUsername(), model.FirstName, model.LastName, model.FatherName, (Core.Model.Person.Person.GenderType)model.Gender, model.ShenasnameNo, model.NationalCode, model.BirthDate, model.PlaceOfBirth, (Core.Model.Person.Person.MaritalStates)model.MaritalStatus, (Core.Model.Person.Person.MilitaryServiceStates)model.MilitaryServiceStates, model.ReasonOfExemption);
        }
        [Authorize]
        [HttpPost]
        public void ModifyContactInfo(ChangeContactInfoDTO model)
        {
            ceoBusiness.ModifyContactInfo(authenticationController.GetUsername(), model.Mobile, model.Email, model.CountyID, model.Address, model.PostalCode, model.Phone);
        }
        [Authorize]
        [HttpGet]
        public object GetBasicInfo()
        {
            return ceoBusiness.GetBasicInfo(authenticationController.GetUsername());
        }
        [Authorize]
        [HttpGet]
        public object GetContactInfo()
        {
            return ceoBusiness.GetContactInfo(authenticationController.GetUsername());
        }
        [Authorize]
        [HttpGet]
        public HttpResponseMessage GetImage(int ID, int width = -1, int height = -1)
        {
            var image = ceoBusiness.GetImage(authenticationController.GetUsername(), ID, height, width);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new ByteArrayContent(image.FileData);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpg");
            response.Content.Headers.Add("Content-Disposition", "attachment; filename=\"" + image.Filename.Replace("\"", "") + "\"");
            return response;
        }
        [Authorize]
        [HttpGet]
        public object GetPictures()
        {
            return ceoBusiness.GetPictures(authenticationController.GetUsername());
        }
        [Authorize]
        [HttpPost]
        public void RemovePhotoScan()
        {
            ceoBusiness.RemovePhotoScan(authenticationController.GetUsername());
        }
        [Authorize]
        [HttpGet]
        public object GetDashboardData()
        {
            return ceoBusiness.GetDashboardData(authenticationController.GetUsername());
        }
        [Authorize]
        [HttpPost]
        public void RemoveShenasnameScan(DeleteObjectDTO model)
        {
            ceoBusiness.RemoveShenasnameScan(authenticationController.GetUsername(), model.ObjectID);
        }
        [Authorize]
        [HttpPost]
        public void RemoveNationalCardScan(DeleteObjectDTO model)
        {
            ceoBusiness.RemoveNationalCardScan(authenticationController.GetUsername(), model.ObjectID);
        }
        [Authorize]
        [HttpPost]
        public void RemoveInsuranceScan(DeleteObjectDTO model)
        {
            ceoBusiness.RemoveInsuranceScan(authenticationController.GetUsername(), model.ObjectID);
        }
        [Authorize]
        [HttpPost]
        public async Task ChangePhotoScan()
        {
            var provider = await Request.Content.ReadAsMultipartAsync();
            var file = provider.Contents[0];
            ceoBusiness.ChangePhotoScan(authenticationController.GetUsername(), file.Headers.ContentDisposition.FileName, file.ReadAsByteArrayAsync().Result);
        }
        [Authorize]
        [HttpPost]
        public async Task AddShenasnameScan()
        {
            var provider = await Request.Content.ReadAsMultipartAsync();
            var file = provider.Contents[0];
            ceoBusiness.AddShenasnameScan(authenticationController.GetUsername(), file.Headers.ContentDisposition.FileName, file.ReadAsByteArrayAsync().Result);
        }
        [Authorize]
        [HttpPost]
        public async Task AddNationalCardScan()
        {
            var provider = await Request.Content.ReadAsMultipartAsync();
            var file = provider.Contents[0];
            ceoBusiness.AddNationalCardScan(authenticationController.GetUsername(), file.Headers.ContentDisposition.FileName, file.ReadAsByteArrayAsync().Result);
        }
        [Authorize]
        [HttpPost]
        public async Task AddInsuranceScan()
        {
            var provider = await Request.Content.ReadAsMultipartAsync();
            var file = provider.Contents[0];
            ceoBusiness.AddInsuranceScan(authenticationController.GetUsername(), file.Headers.ContentDisposition.FileName, file.ReadAsByteArrayAsync().Result);
        }
        [Authorize]
        [HttpPost]
        public async Task AddTPCScan()
        {
            var provider = await Request.Content.ReadAsMultipartAsync();
            var file = provider.Contents[0];
            ceoBusiness.AddTPCScan(authenticationController.GetUsername(), file.Headers.ContentDisposition.FileName, file.ReadAsByteArrayAsync().Result);
        }
        [Authorize]
        [HttpPost]
        public void RemoveTPCScan(DeleteObjectDTO model)
        {
            ceoBusiness.RemoveTPCScan(authenticationController.GetUsername(), model.ObjectID);
        }
        [Authorize]
        [HttpPost]
        public void ModifyWorkshopInfo(ChangeWorkshopInfo model)
        {
            ceoBusiness.ModifyWorkshopInfo(authenticationController.GetUsername(), model.UnitName, model.WorkshopSize, model.CompanyCode, model.NightWorkersNo, model.MorningWorkersNo, model.SafetyWorkersNo, model.MaleWorkersNo, model.EveningWorkersNo, model.FemaleWorkersNo, model.FieldOfActivityId, model.ProductionTypeId, model.TechnicalProtectionCommittee, model.SocialSecurityCode);
        }
        [Authorize]
        [HttpPost]
        public void ModifyWorkshopLocation(ChangeContactInfoDTO model)
        {
            ceoBusiness.ModifyWorkshopLocation(authenticationController.GetUsername(), model.Address, model.CountyID, model.Phone, model.PostalCode);
        }
        [Authorize]
        [HttpGet]
        public object GetWorkshopInfo()
        {
            return ceoBusiness.GetWorkshopInfo(authenticationController.GetUsername());
        }
        [Authorize]
        [HttpGet]
        public object GetWorkshopLocationInfo()
        {
            return ceoBusiness.GetWorkshopLocationInfo(authenticationController.GetUsername());
        }
        [Authorize]
        [HttpGet]
        public object FindExperts(string filter = null)
        {
            return ceoBusiness.FindExperts(authenticationController.GetUsername(), filter);
        }
        [Authorize]
        [HttpPost]
        public void HireAnExpert(HireAnExpertDTO model)
        {
            ceoBusiness.HireAnExpert(authenticationController.GetUsername(), model.ExpertUsername);
        }
        [Authorize]
        [HttpGet]
        public object CheckProfileProgress()
        {
            return ceoBusiness.CheckProfileProgress(authenticationController.GetUsername());
        }
    }
}
