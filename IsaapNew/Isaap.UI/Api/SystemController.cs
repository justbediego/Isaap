﻿using Isaap.Core.Business;
using Isaap.Core.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Isaap.UI.Api
{
    public class SystemController : ApiController
    {
        private IsaapContext dbContext;
        private SystemBusiness systemBusiness;
        private AuthenticationController authenticationController;
        public SystemController()
        {
            dbContext = new IsaapContext();
            systemBusiness = new SystemBusiness(dbContext);
            authenticationController = new AuthenticationController(); 
        }
        [HttpGet]
        public object GetAllProvinces()
        {
            return systemBusiness.GetAllProvinces();
        }
        [HttpGet]
        public object GetAllCounties(int provinceID)
        {
            return systemBusiness.GetAllCounties(provinceID);
        }
        [HttpGet]
        public object GetAllFieldOfEducations()
        {
            return systemBusiness.GetAllFieldOfEducations();
        }
        [HttpGet]
        public object GetAllFieldOfCertifications()
        {
            return systemBusiness.GetAllFieldOfCertifications();
        }
        [HttpGet]
        public object GetAllProductionTypes()
        {
            return systemBusiness.GetAllProductionTypes();
        }
        [HttpGet]
        public object GetAllFieldOfActivities()
        {
            return systemBusiness.GetAllFieldOfActivities();
        }
    }
}
