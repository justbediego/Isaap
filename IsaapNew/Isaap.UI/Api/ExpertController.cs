﻿using Isaap.Core.Business;
using Isaap.Core.DataAccess;
using Isaap.UI.Api.Config;
using Isaap.UI.Api.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace Isaap.UI.Api
{
    public class ExpertController : ApiController
    {
        private IsaapContext dbContext;
        private ExpertBusiness expertBusiness;
        private AuthenticationController authenticationController;
        public ExpertController()
        {
            dbContext = new IsaapContext();
            expertBusiness = new ExpertBusiness(dbContext);
            authenticationController = new AuthenticationController();
        }
        [HttpPost]
        [LoggerIgnoreJsonData("Password")]
        public void Register(UserRegisterDTO model)
        {
            HttpClient client = new HttpClient();
            Dictionary<string, string> postData = new Dictionary<string, string> {
                { "secret", "6LdVGCAUAAAAAP4HIDkp9xT6pBlkwYHz2eAFdXIK" },
                { "response", model.Captcha },
                //{"remoteip", HttpContext.Current.Request.UserHostAddress }
            };
            var postResponse = client.PostAsync("https://www.google.com/recaptcha/api/siteverify", new FormUrlEncodedContent(postData)).Result;
            var responseString = postResponse.Content.ReadAsStringAsync().Result;
            recaptchaResponse googleResponse = JsonConvert.DeserializeObject<recaptchaResponse>(responseString);
            if (!googleResponse.success)
                throw new Exception("کد امنیتی مورد تایید نیست");
            expertBusiness.Register(model.NationalCode, model.FirstName, model.LastName, model.Username, model.Password, model.Email, model.Mobile);
        }
        [Authorize]
        [HttpPost]
        public void ModifyBasicInfo(ChangePersonInfoDTO model)
        {
            expertBusiness.ModifyBasicInfo(authenticationController.GetUsername(), model.FirstName, model.LastName, model.FatherName, (Core.Model.Person.Person.GenderType)model.Gender, model.ShenasnameNo, model.NationalCode, model.BirthDate, model.PlaceOfBirth, (Core.Model.Person.Person.MaritalStates)model.MaritalStatus, (Core.Model.Person.Person.MilitaryServiceStates)model.MilitaryServiceStates, model.ReasonOfExemption, model.AssociationNumber);
        }
        [Authorize]
        [HttpPost]
        public void ModifyContactInfo(ChangeContactInfoDTO model)
        {
            expertBusiness.ModifyContactInfo(authenticationController.GetUsername(), model.Mobile, model.Email, model.CountyID, model.Address, model.PostalCode, model.Phone);
        }
        [Authorize]
        [HttpGet]
        public object GetBasicInfo()
        {
            return expertBusiness.GetBasicInfo(authenticationController.GetUsername());
        }
        [Authorize]
        [HttpGet]
        public object GetContactInfo()
        {
            return expertBusiness.GetContactInfo(authenticationController.GetUsername());
        }
        [Authorize]
        [HttpPost]
        public void AddOrEditEducation(AddEditEducationDTO model)
        {
            expertBusiness.AddOrEditEducation(authenticationController.GetUsername(), model.EducationID, model.DegreeLevel, model.FieldOfEducationID);
        }
        [Authorize]
        [HttpPost]
        public void DeleteEducation(DeleteObjectDTO model)
        {
            expertBusiness.DeleteEducation(authenticationController.GetUsername(), model.ObjectID);
        }
        [Authorize]
        [HttpPost]
        public void AddOrEditSafetyCertification(AddEditSafetyCertificationDTO model)
        {
            expertBusiness.AddOrEditSafetyCertification(authenticationController.GetUsername(), model.CertificationID, model.FieldOfCertificationID, model.DateTaken, model.Description, model.IssuerName, model.OtherCourseName, model.OtherDurationInHours);
        }
        [Authorize]
        [HttpPost]
        public void DeleteCertification(DeleteObjectDTO model)
        {
            expertBusiness.DeleteCertification(authenticationController.GetUsername(), model.ObjectID);
        }
        [Authorize]
        [HttpPost]
        public void AddOrEditWorkExperience(AddEditWorkExperienceDTO model)
        {
            expertBusiness.AddOrEditWorkExperience(authenticationController.GetUsername(), model.ExperienceID, model.DateFrom, model.DateTo, model.WorkshopName, model.EmployerName, model.Description);
        }
        [Authorize]
        [HttpPost]
        public void DeleteWorkExperience(DeleteObjectDTO model)
        {
            expertBusiness.DeleteWorkExperience(authenticationController.GetUsername(), model.ObjectID);
        }
        [Authorize]
        [HttpGet]
        public object GetAllEducations()
        {
            return expertBusiness.GetAllEducations(authenticationController.GetUsername());
        }
        [Authorize]
        [HttpGet]
        public object GetEducation(int educationId)
        {
            return expertBusiness.GetEducation(authenticationController.GetUsername(), educationId);
        }
        [Authorize]
        [HttpPost]
        public async Task ChangeEducationScan()
        {
            var provider = await Request.Content.ReadAsMultipartAsync();
            var file = provider.Contents[1];
            expertBusiness.ChangeEducationScan(authenticationController.GetUsername(), int.Parse(provider.Contents[0].ReadAsStringAsync().Result), file.Headers.ContentDisposition.FileName, file.ReadAsByteArrayAsync().Result);
        }
        [Authorize]
        [HttpPost]
        public async Task ChangeCertificationScan()
        {
            var provider = await Request.Content.ReadAsMultipartAsync();
            var file = provider.Contents[1];
            expertBusiness.ChangeCertificationScan(authenticationController.GetUsername(), int.Parse(provider.Contents[0].ReadAsStringAsync().Result), file.Headers.ContentDisposition.FileName, file.ReadAsByteArrayAsync().Result);
        }

        [Authorize]
        [HttpGet]
        public HttpResponseMessage GetImage(int ID, int width = -1, int height = -1)
        {
            var image = expertBusiness.GetImage(authenticationController.GetUsername(), ID, height, width);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new ByteArrayContent(image.FileData);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpg");
            response.Content.Headers.Add("Content-Disposition", "attachment; filename=\"" + image.Filename.Replace("\"", "") + "\"");
            return response;
        }
        [Authorize]
        [HttpPost]
        public async Task ChangePhotoScan()
        {
            var provider = await Request.Content.ReadAsMultipartAsync();
            var file = provider.Contents[0];
            expertBusiness.ChangePhotoScan(authenticationController.GetUsername(), file.Headers.ContentDisposition.FileName, file.ReadAsByteArrayAsync().Result);
        }
        [Authorize]
        [HttpPost]
        public async Task AddShenasnameScan()
        {
            var provider = await Request.Content.ReadAsMultipartAsync();
            var file = provider.Contents[0];
            expertBusiness.AddShenasnameScan(authenticationController.GetUsername(), file.Headers.ContentDisposition.FileName, file.ReadAsByteArrayAsync().Result);
        }
        [Authorize]
        [HttpPost]
        public async Task AddNationalCardScan()
        {
            var provider = await Request.Content.ReadAsMultipartAsync();
            var file = provider.Contents[0];
            expertBusiness.AddNationalCardScan(authenticationController.GetUsername(), file.Headers.ContentDisposition.FileName, file.ReadAsByteArrayAsync().Result);
        }
        [Authorize]
        [HttpPost]
        public async Task AddInsuranceScan()
        {
            var provider = await Request.Content.ReadAsMultipartAsync();
            var file = provider.Contents[0];
            expertBusiness.AddInsuranceScan(authenticationController.GetUsername(), file.Headers.ContentDisposition.FileName, file.ReadAsByteArrayAsync().Result);
        }
        [Authorize]
        [HttpGet]
        public object GetPictures()
        {
            return expertBusiness.GetPictures(authenticationController.GetUsername());
        }
        [Authorize]
        [HttpPost]
        public void RemovePhotoScan()
        {
            expertBusiness.RemovePhotoScan(authenticationController.GetUsername());
        }
        [Authorize]
        [HttpGet]
        public object GetDashboardData()
        {
            return expertBusiness.GetDashboardData(authenticationController.GetUsername());
        }
        [Authorize]
        [HttpPost]
        public void RemoveShenasnameScan(DeleteObjectDTO model)
        {
            expertBusiness.RemoveShenasnameScan(authenticationController.GetUsername(), model.ObjectID);
        }
        [Authorize]
        [HttpPost]
        public void RemoveNationalCardScan(DeleteObjectDTO model)
        {
            expertBusiness.RemoveNationalCardScan(authenticationController.GetUsername(), model.ObjectID);
        }
        [Authorize]
        [HttpPost]
        public void RemoveInsuranceScan(DeleteObjectDTO model)
        {
            expertBusiness.RemoveInsuranceScan(authenticationController.GetUsername(), model.ObjectID);
        }
        [Authorize]
        [HttpGet]
        public object GetAllSafetyCertifications()
        {
            return expertBusiness.GetAllSafetyCertifications(authenticationController.GetUsername());
        }
        [Authorize]
        [HttpGet]
        public object GetSafetyCertification(int certificationId)
        {
            return expertBusiness.GetSafetyCertification(authenticationController.GetUsername(), certificationId);
        }
        [Authorize]
        [HttpGet]
        public object GetAllWorkExperiences()
        {
            return expertBusiness.GetAllWorkExperiences(authenticationController.GetUsername());
        }
        [Authorize]
        [HttpGet]
        public object GetWorkExperience(int experienceId)
        {
            return expertBusiness.GetWorkExperience(authenticationController.GetUsername(), experienceId);
        }
        [Authorize]
        [HttpGet]
        public object GetMyLicenses()
        {
            return expertBusiness.GetMyLicenses(authenticationController.GetUsername());
        }
        [Authorize]
        [HttpGet]
        public object GetLicenseFlows(int licenseId)
        {
            return expertBusiness.GetLicenseFlows(authenticationController.GetUsername(), licenseId);
        }
        [Authorize]
        [HttpPost]
        public async Task AddContractScan()
        {
            var provider = await Request.Content.ReadAsMultipartAsync();
            var file = provider.Contents[1];
            expertBusiness.AddContractScan(authenticationController.GetUsername(), int.Parse(provider.Contents[0].ReadAsStringAsync().Result), file.Headers.ContentDisposition.FileName, file.ReadAsByteArrayAsync().Result);
        }
        [Authorize]
        [HttpPost]
        public void RemoveContractScan(DeleteObjectDTO model)
        {
            expertBusiness.RemoveContractScan(authenticationController.GetUsername(), model.ObjectID);
        }
        [Authorize]
        [HttpPost]
        public void SendLicense(SendLicenseDTO model)
        {
            expertBusiness.SendLicense(authenticationController.GetUsername(), model.LicenseId);
        }
        [Authorize]
        [HttpGet]
        public object CheckProfileProgress()
        {
            return expertBusiness.CheckProfileProgress(authenticationController.GetUsername());
        }
        [Authorize]
        [HttpPost]
        public async Task AddObjectionAttachment()
        {
            var provider = await Request.Content.ReadAsMultipartAsync();
            var file = provider.Contents[1];
            expertBusiness.AddObjectionAttachment(authenticationController.GetUsername(), int.Parse(provider.Contents[0].ReadAsStringAsync().Result), file.Headers.ContentDisposition.FileName, file.ReadAsByteArrayAsync().Result);
        }
        [Authorize]
        [HttpPost]
        public void RemoveObjectionAttachment(DeleteObjectDTO model)
        {
            expertBusiness.RemoveObjectionAttachment(authenticationController.GetUsername(), model.ObjectID);
        }
        [Authorize]
        [HttpGet]
        public object GetTemporaryObjectionAttachments(int licenseId)
        {
            return expertBusiness.GetTemporaryObjectionAttachments(authenticationController.GetUsername(), licenseId);
        }
        [Authorize]
        [HttpPost]
        public void Objectify(ObjectionDTO model)
        {
            expertBusiness.Objectify(authenticationController.GetUsername(), model.LicenseID, model.Description);
        }
        [Authorize]
        [HttpPost]
        public void CreateLicenseUser(CreateLicenseUserDTO model)
        {
            expertBusiness.CreateLicenseUser(authenticationController.GetUsername(), model.LicenseId, model.Username, model.Password);
        }
    }
}
