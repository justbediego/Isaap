﻿using Isaap.Core.Model.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Isaap.UI.Api.Models
{
    public class AddEditEducationDTO
    {
        public int? EducationID;
        public Education.DegreeLevels DegreeLevel;
        public int FieldOfEducationID;
    }
}