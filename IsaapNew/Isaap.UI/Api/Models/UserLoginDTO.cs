﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Isaap.UI.Api.Models
{
    public class UserLoginDTO
    {
        public bool IsPersistent;
        public string Username;
        public string Password;
    }
}