﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Isaap.UI.Api.Models
{
    public class ChangeContactInfoDTO
    {
        public string Mobile;
        public string Email;
        public int CountyID;
        public string Address;
        public string PostalCode;
        public string Phone;
    }
}