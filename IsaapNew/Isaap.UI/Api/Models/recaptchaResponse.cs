﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Isaap.UI.Api.Models
{
    public class recaptchaResponse
    {
        public bool success;
        public DateTime challenge_ts;
        public string hostname;
    }
}