﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Isaap.UI.Api.Models
{
    public class ChangePasswordDTO
    {
        public string OldPassword;
        public string NewPassword;
    }
}