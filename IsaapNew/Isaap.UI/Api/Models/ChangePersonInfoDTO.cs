﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Isaap.UI.Api.Models
{
    public class ChangePersonInfoDTO
    {
        public string FirstName;
        public string LastName;
        public string FatherName;
        public int Gender;
        public string ShenasnameNo;
        public string NationalCode;
        public DateTime BirthDate
        {
            get
            {
                return PersianDate.ConvertDate.ToEn(PersianBirthDate);
            }
        }
        public string PlaceOfBirth;
        public int MaritalStatus;
        public int MilitaryServiceStates;
        public string ReasonOfExemption;
        public string AssociationNumber;
        public string PersianBirthDate;
    }
}