﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Isaap.UI.Api.Models
{
    public class UserRegisterDTO
    {
        public string CompanyCode;
        public string FirstName;
        public string LastName;
        public string NationalCode;
        public string Username;
        public string Password;
        public string Email;
        public string Mobile;
        public string Captcha;
    }
}