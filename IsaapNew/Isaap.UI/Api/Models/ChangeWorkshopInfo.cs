﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Isaap.UI.Api.Models
{
    public class ChangeWorkshopInfo
    {
        public string UnitName;
        public double WorkshopSize;
        public string CompanyCode;
        public int NightWorkersNo;
        public int MorningWorkersNo;
        public int SafetyWorkersNo;
        public int MaleWorkersNo;
        public int EveningWorkersNo;
        public int FemaleWorkersNo;
        public int FieldOfActivityId;
        public int ProductionTypeId;
        public bool TechnicalProtectionCommittee;
        public string SocialSecurityCode;
    }
}