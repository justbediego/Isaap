﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Isaap.UI.Api.Models
{
    public class AddEditWorkExperienceDTO
    {
        public int? ExperienceID;
        public string PersianDateFrom;
        public string PersianDateTo;
        public DateTime DateFrom
        {
            get
            {
                return PersianDate.ConvertDate.ToEn(PersianDateFrom);
            }
        }
        public DateTime DateTo
        {
            get
            {
                return PersianDate.ConvertDate.ToEn(PersianDateTo);
            }
        }
        public string WorkshopName;
        public string EmployerName;
        public string Description;
    }
}