﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Isaap.UI.Api.Models
{
    public class ObjectionDTO
    {
        public int LicenseID;
        public string Description;
    }
}