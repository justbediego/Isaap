﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Isaap.UI.Api.Models
{
    public class AddEditSafetyCertificationDTO
    {
        public int? CertificationID;
        public int? FieldOfCertificationID;
        public string PersianDateTaken;
        public DateTime DateTaken
        {
            get
            {
                return PersianDate.ConvertDate.ToEn(PersianDateTaken);
            }
        }
        public string Description;
        public string IssuerName;
        public string OtherCourseName;
        public double OtherDurationInHours;
    }
}