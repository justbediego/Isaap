﻿using Isaap.Core.Business;
using Isaap.Core.DataAccess;
using Isaap.UI.Api.Config;
using Isaap.UI.Api.Models;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web;
using System.Web.Http;

namespace Isaap.UI.Api
{
    public class AuthenticationController : ApiController
    {
        private IsaapContext dbContext;
        private UserBusiness userBusiness;
        public AuthenticationController()
        {
            dbContext = new IsaapContext();
            userBusiness = new UserBusiness(dbContext);
        }        
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().Authentication;
            }
        }
        [HttpGet]
        public bool IsLoggedIn()
        {
            return !string.IsNullOrEmpty(User.Identity.Name);
        }
        [HttpGet]
        public string GetUsername()
        {
            return User.Identity.Name;
        }
        [HttpGet]
        public int GetUserType()
        {
            return int.Parse(((ClaimsPrincipal)User).FindFirst(ClaimTypes.Role).Value);
        }
        [HttpPost]
        [LoggerIgnoreJsonData("Password")]
        public void Login(UserLoginDTO model)
        {
            var user = userBusiness.GetUser(model.Username, model.Password);
            var identity = new ClaimsIdentity(DefaultAuthenticationTypes.ApplicationCookie);
            identity.AddClaim(new Claim(ClaimTypes.Name, user.UserName));
            identity.AddClaim(new Claim(ClaimTypes.Role, ((int)user.Type).ToString()));
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = model.IsPersistent }, identity);
        }
        [HttpPost]
        public void Logout()
        {
            AuthenticationManager.SignOut();
        }
        [Authorize]
        [HttpPost]
        [LoggerIgnoreJsonData("OldPassword", "NewPassword")]
        public void ChangePassword(ChangePasswordDTO model)
        {
            userBusiness.ChangePassword(GetUsername(), model.OldPassword, model.NewPassword);
        }
    }
}
