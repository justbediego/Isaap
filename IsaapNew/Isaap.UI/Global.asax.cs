﻿using Isaap.UI.Api.Config;
using Isaap.UI.Mvc.Config;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;


namespace Isaap.UI
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configuration.Filters.Add(new ArabicToPersianApiFilter());
            GlobalConfiguration.Configuration.Filters.Add(new ActionLoggerApiFilter());
            MvcConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}