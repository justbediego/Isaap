﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Isaap.Core.DataAccess;
using Isaap.Core.Model.User;
using Isaap.Core.Model.Person;
using System.Data.Entity;
using Isaap.Core.Model.VM;
using Isaap.Core.Model.Company;
using Isaap.Core.Model.License;

namespace Isaap.Core.Business
{
    public class CEOBusiness : UserBusiness
    {
        private SystemBusiness systemBusiness;
        private NotificationBusiness notificationBusiness;
        private ValidationBusiness validationBusiness;
        public CEOBusiness(IsaapContext dbContext) : base(dbContext)
        {
            systemBusiness = new SystemBusiness(dbContext);
            validationBusiness = new ValidationBusiness(dbContext);
            notificationBusiness = new NotificationBusiness(dbContext);
        }
        private bool CanModify(string username)
        {
            List<LicenseFlow.FlowType> restrictedTypes = new List<LicenseFlow.FlowType>()
            {
                LicenseFlow.FlowType.WithAssociation,
                LicenseFlow.FlowType.WithClub,
                LicenseFlow.FlowType.WithCountyInspector,
                LicenseFlow.FlowType.WithInspectionOffice,
                LicenseFlow.FlowType.WithProvinceInspector,
                LicenseFlow.FlowType.WithProvinceManager,
                LicenseFlow.FlowType.Deactivated,
            };
            return dbContext.CEOs.Any(c => c.User.UserName == username && c.Company.Licenses.All(l => !l.Flows.Any(lf => lf.IsCurrent && restrictedTypes.Contains(lf.Type))));
        }
        public void Register(string companyCode, string nationalcode, string firstname, string lastname, string username, string password, string email, string mobile)
        {
            validationBusiness.ValidateNationalCode(nationalcode);
            validationBusiness.ValidateCompanyCode(companyCode);
            validationBusiness.ValidateFirstName(firstname);
            validationBusiness.ValidateLastName(lastname);
            validationBusiness.ValidateUserName(username);
            validationBusiness.ValidatePassword(password);
            validationBusiness.ValidateMobilePhone(mobile);
            validationBusiness.ValidateEmail(email);
            Person person = dbContext.Persons.Where(p => p.NationalCode == nationalcode).Include(p => p.CEOs).FirstOrDefault();
            if (person == null)
                person = new Person()
                {
                    NationalCode = nationalcode,
                    FirstName = firstname,
                    LastName = lastname,
                    HomeLocation = new Model.Location.Location(),
                };
            if (person.CEOs.Any())
                throw new Model.Exception.DuplicatedException("شخص", "کد ملی قبلاً ثبت نام کرده است");
            Company company = dbContext.Companies.Where(c => c.CompanyCode == companyCode).Include(c => c.CEOs).FirstOrDefault();
            if (company == null)
                company = new Company()
                {
                    CompanyCode = companyCode,
                    Location = new Model.Location.Location(),
                };
            if (company.CEOs.Any(c => c.Person_ID == person.Id))
                throw new Model.Exception.DuplicatedException("مدیر عامل", "شما قبلاً به عنوان مدیر عامل این شرکت قبلاً ثبت نام نموده اید");
            if (company.CEOs.Any())
                throw new Model.Exception.DuplicatedException("مدیر عامل", "این شرکت دارای مدیر عامل دیگری می باشد");
            CEO ceo = new CEO()
            {
                Person = person,
                User = new User()
                {
                    HashPassword = systemBusiness.TextToMD5(password),
                    UserName = username,
                    Email = email,
                    Mobile = mobile,
                    Type = User.UserType.CEO,
                },
                Company = company,
            };
            dbContext.CEOs.Add(ceo);
            notificationBusiness.SendRegisterationMail(ceo.User.Email, ceo.User.UserName, person.FullName, ceo.User.EmailValidationKey);
            dbContext.SaveChanges();
        }
        public PersonBasicInfo GetBasicInfo(string username)
        {
            var result = dbContext.CEOs.Where(c => c.User.UserName == username).Select(c => new PersonBasicInfo()
            {
                //IsEditable = e.Person.Comments,
                BirthDate = c.Person.BirthDate,
                FatherName = c.Person.FatherName,
                FirstName = c.Person.FirstName,
                Gender = c.Person.Gender,
                LastName = c.Person.LastName,
                MaritalStatus = c.Person.MaritalStatus,
                MilitaryServiceStates = c.Person.MilitaryStatus,
                NationalCode = c.Person.NationalCode,
                PlaceOfBirth = c.Person.PlaceOfBirth,
                ReasonOfExemption = c.Person.ReasonOfExemption,
                ShenasnameNo = c.Person.ShenasnameNo,
            }).FirstOrDefault();
            if (result == null)
                throw new Model.Exception.NotFoundException("شخص", "");
            return result;
        }
        public void ModifyBasicInfo(string username, string firstName, string lastName, string fatherName, Person.GenderType gender, string shenasnameNo, string melliCode, DateTime birthDate, string placeOfBirth, Person.MaritalStates maritalStatus, Person.MilitaryServiceStates militaryServiceStates, string reasonOfExemption)
        {
            validationBusiness.ValidateFirstName(firstName);
            validationBusiness.ValidateLastName(lastName);
            validationBusiness.ValidateFirstName(fatherName);
            validationBusiness.ValidateShenasnameNo(shenasnameNo);
            validationBusiness.ValidateBirthDate(birthDate);
            validationBusiness.ValidatePlaceOfBirth(placeOfBirth);
            if (gender == Person.GenderType.Male)
                validationBusiness.ValidateMilitaryServiceStatus(militaryServiceStates);
            if (gender == Person.GenderType.Male && militaryServiceStates == Person.MilitaryServiceStates.Exempted)
                validationBusiness.ValidateReasonOfExemption(reasonOfExemption);
            CEO ceo = dbContext.CEOs.Include(c => c.Person).FirstOrDefault(e => e.User.UserName == username);
            if (ceo == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            if (ceo.Person.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "اطلاعات قبلاً تایید شده است");
            ceo.Person.FirstName = firstName;
            ceo.Person.LastName = lastName;
            ceo.Person.FatherName = fatherName;
            ceo.Person.ShenasnameNo = shenasnameNo;
            if (melliCode != ceo.Person.NationalCode)
            {
                validationBusiness.ValidateNationalCode(melliCode);
                if (dbContext.Persons.Any(p => p.NationalCode == melliCode))
                    throw new Model.Exception.DuplicatedException("شخص", "کد ملی قبلاً توسط شخص دیگری وارد شده است");
            }
            ceo.Person.NationalCode = melliCode;
            ceo.Person.BirthDate = birthDate;
            ceo.Person.PlaceOfBirth = placeOfBirth;
            ceo.Person.Gender = gender;
            ceo.Person.MilitaryStatus = militaryServiceStates;
            ceo.Person.MaritalStatus = maritalStatus;
            ceo.Person.ReasonOfExemption = reasonOfExemption;
            dbContext.SaveChanges();
        }
        public void ModifyContactInfo(string username, string mobilePhone, string email, int homeCountyID, string homeAddress, string homePostalCode, string homePhone)
        {
            validationBusiness.ValidateAddress(homeAddress);
            validationBusiness.ValidatePostalCode(homePostalCode);
            validationBusiness.ValidatePhone(homePhone);
            if (!dbContext.Counties.Any(c => c.Id == homeCountyID))
                throw new Model.Exception.NotFoundException("شهرستان", "");
            CEO ceo = dbContext.CEOs.Include(c => c.Person.HomeLocation).FirstOrDefault(e => e.User.UserName == username);
            if (ceo == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            if (ceo.Person.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "اطلاعات قبلاً تایید شده است");
            if (mobilePhone != ceo.User.Mobile)
                validationBusiness.ValidateMobilePhone(mobilePhone);
            if (email != ceo.User.Email)
                validationBusiness.ValidateEmail(email);
            ceo.User.Mobile = mobilePhone;
            ceo.User.Email = email;
            ceo.Person.HomeLocation.County_ID = homeCountyID;
            ceo.Person.HomeLocation.Address = homeAddress;
            ceo.Person.HomeLocation.PostalCode = homePostalCode;
            ceo.Person.HomeLocation.Phone = homePhone;
            dbContext.SaveChanges();
        }
        public PersonContactInfo GetContactInfo(string username)
        {
            var result = dbContext.CEOs.Where(c => c.User.UserName == username).Select(c => new PersonContactInfo()
            {
                //IsEditable = e.Person.IsEditable,
                Address = c.Person.HomeLocation.Address,
                CountyID = c.Person.HomeLocation.County_ID,
                ProvinceID = c.Person.HomeLocation.County_ID.HasValue ? (int?)c.Person.HomeLocation.County.Province_ID : null,
                Email = c.User.Email,
                Mobile = c.User.Mobile,
                Phone = c.Person.HomeLocation.Phone,
                PostalCode = c.Person.HomeLocation.PostalCode,
            }).FirstOrDefault();
            if (result == null)
                throw new Model.Exception.NotFoundException("شخص", "");
            return result;
        }
        public DashboardData GetDashboardData(string username)
        {
            var data = dbContext.CEOs.Where(c => c.User.UserName == username).Select(c => new DashboardData
            {
                FullName = c.Person.FirstName + " " + c.Person.LastName,
                PhotoId = c.Person.PhotoScan_ID,
            }).FirstOrDefault();
            if (data == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            return data;
        }
        public PersonPictures GetPictures(string username)
        {
            var personPicture = dbContext.CEOs.Where(c => c.User.UserName == username).Select(c => new PersonPictures
            {
                PhotoScanId = c.Person.PhotoScan_ID,
                InsuranceScanIds = c.Person.InsuranceRecordScans.Select(irs => irs.Id),
                NationalCardScanIds = c.Person.NationalCardScans.Select(ncs => ncs.Id),
                ShenasnameScanIds = c.Person.ShenasnameScans.Select(ss => ss.Id),
                HasTPC = c.Company.TechnicalProtectionCommittee,
                TPCScanIds = c.Company.TPCScans.Select(tpc => tpc.Id),
            }).FirstOrDefault();
            if (personPicture == null)
                throw new Model.Exception.NotFoundException("تصاویر", "");
            return personPicture;
        }
        public void ChangePhotoScan(string username, string filename, byte[] fileData)
        {
            validationBusiness.ValidatePhoto(fileData);
            CEO ceo = dbContext.CEOs.Include(c => c.Person.PhotoScan).FirstOrDefault(e => e.User.UserName == username);
            if (ceo == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            if (ceo.Person.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "اطلاعات قبلاً تایید شده است");
            if (ceo.Person.PhotoScan != null)
                dbContext.Attachments.Remove(ceo.Person.PhotoScan);
            ceo.Person.PhotoScan = new Model.Attachment()
            {
                FileData = fileData,
                Filename = filename,
            };
            dbContext.SaveChanges();
        }
        public void RemovePhotoScan(string username)
        {
            CEO ceo = dbContext.CEOs.Include(c => c.Person.PhotoScan).FirstOrDefault(e => e.User.UserName == username);
            if (ceo == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            if (ceo.Person.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "اطلاعات قبلاً تایید شده است");
            if (ceo.Person.PhotoScan != null)
                dbContext.Attachments.Remove(ceo.Person.PhotoScan);
            dbContext.SaveChanges();
        }
        public void AddShenasnameScan(string username, string filename, byte[] fileData)
        {
            validationBusiness.ValidatePhoto(fileData);
            CEO ceo = dbContext.CEOs.Include(c => c.Person.ShenasnameScans).FirstOrDefault(e => e.User.UserName == username);
            if (ceo == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            if (ceo.Person.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "اطلاعات قبلاً تایید شده است");
            ceo.Person.ShenasnameScans.Add(new Model.Attachment()
            {
                FileData = fileData,
                Filename = filename,
            });
            dbContext.SaveChanges();
        }
        public void RemoveShenasnameScan(string username, int shenasnameID)
        {
            CEO ceo = dbContext.CEOs.Include(c => c.Person.ShenasnameScans).FirstOrDefault(e => e.User.UserName == username);
            if (ceo == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            if (ceo.Person.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "اطلاعات قبلاً تایید شده است");
            Model.Attachment shenasnameScan = ceo.Person.ShenasnameScans.Where(sh => sh.Id == shenasnameID).FirstOrDefault();
            if (shenasnameScan == null)
                throw new Model.Exception.NotFoundException("تصویر شناسنامه", "");
            dbContext.Attachments.Remove(shenasnameScan);
            dbContext.SaveChanges();
        }
        public void AddNationalCardScan(string username, string filename, byte[] fileData)
        {
            validationBusiness.ValidatePhoto(fileData);
            CEO ceo = dbContext.CEOs.Include(c => c.Person.NationalCardScans).FirstOrDefault(e => e.User.UserName == username);
            if (ceo == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            if (ceo.Person.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "اطلاعات قبلاً تایید شده است");
            ceo.Person.NationalCardScans.Add(new Model.Attachment()
            {
                FileData = fileData,
                Filename = filename,
            });
            dbContext.SaveChanges();
        }
        public void RemoveNationalCardScan(string username, int nationalCardID)
        {
            CEO ceo = dbContext.CEOs.Include(c => c.Person.NationalCardScans).FirstOrDefault(e => e.User.UserName == username);
            if (ceo == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            if (ceo.Person.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "اطلاعات قبلاً تایید شده است");
            Model.Attachment nationalCardScan = ceo.Person.NationalCardScans.Where(ns => ns.Id == nationalCardID).FirstOrDefault();
            if (nationalCardScan == null)
                throw new Model.Exception.NotFoundException("تصویر کارت ملی", "");
            dbContext.Attachments.Remove(nationalCardScan);
            dbContext.SaveChanges();
        }
        public void AddInsuranceScan(string username, string filename, byte[] fileData)
        {
            validationBusiness.ValidatePhoto(fileData);
            CEO ceo = dbContext.CEOs.Include(c => c.Person.InsuranceRecordScans).FirstOrDefault(e => e.User.UserName == username);
            if (ceo == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            if (ceo.Person.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "اطلاعات قبلاً تایید شده است");
            ceo.Person.InsuranceRecordScans.Add(new Model.Attachment()
            {
                FileData = fileData,
                Filename = filename,
            });
            dbContext.SaveChanges();
        }
        public void RemoveInsuranceScan(string username, int insuranceScanID)
        {
            CEO ceo = dbContext.CEOs.Include(c => c.Person.InsuranceRecordScans).FirstOrDefault(e => e.User.UserName == username);
            if (ceo == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            if (ceo.Person.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "اطلاعات قبلاً تایید شده است");
            Model.Attachment insuranceScan = ceo.Person.InsuranceRecordScans.Where(ins => ins.Id == insuranceScanID).FirstOrDefault();
            if (insuranceScan == null)
                throw new Model.Exception.NotFoundException("تصویر بیمه", "");
            dbContext.Attachments.Remove(insuranceScan);
            dbContext.SaveChanges();
        }
        public void AddTPCScan(string username, string filename, byte[] fileData)
        {
            validationBusiness.ValidatePhoto(fileData);
            CEO ceo = dbContext.CEOs.Include(c => c.Company.TPCScans).FirstOrDefault(e => e.User.UserName == username);
            if (ceo == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            if (ceo.Company.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "اطلاعات قبلاً تایید شده است");
            ceo.Company.TPCScans.Add(new Model.Attachment()
            {
                FileData = fileData,
                Filename = filename,
            });
            dbContext.SaveChanges();
        }
        public void RemoveTPCScan(string username, int tpcScanID)
        {
            CEO ceo = dbContext.CEOs.Include(c => c.Company.TPCScans).FirstOrDefault(e => e.User.UserName == username);
            if (ceo == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            if (ceo.Company.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "اطلاعات قبلاً تایید شده است");
            Model.Attachment tpcScan = ceo.Company.TPCScans.Where(sc => sc.Id == tpcScanID).FirstOrDefault();
            if (tpcScan == null)
                throw new Model.Exception.NotFoundException("تصویر کمیته حفاظت فنی", "");
            dbContext.Attachments.Remove(tpcScan);
            dbContext.SaveChanges();
        }
        public Attachment GetImage(string username, int attachmentId, int height = 0, int width = 0)
        {
            Attachment attachment = dbContext.Attachments.Where(a => a.Id == attachmentId).Select(a => new Attachment()
            {
                FileData = a.FileData,
                Filename = a.Filename,
            }).FirstOrDefault();
            if (attachment == null)
                throw new Model.Exception.NotFoundException("تصویر", "کد اشتباه وارد شده است");
            return systemBusiness.ResizeImage(attachment, height, width);
        }
        public void ModifyWorkshopInfo(string username, string unitName, double workshopSize, string companyCode, int nightWorkersNo, int morningWorkersNo, int safetyWorkersNo, int maleWorkersNo, int eveningWorkersNo, int femaleWorkersNo, int fieldOfActivityId, int productionTypeId, bool technicalProtectionCommittee, string socialSecurityCode)
        {
            validationBusiness.ValidateUnitName(unitName);
            validationBusiness.ValidateWorkshopSize(workshopSize);
            validationBusiness.ValidateCompanyCode(companyCode);
            validationBusiness.ValidateAllNumberOfWorkers(maleWorkersNo, femaleWorkersNo, morningWorkersNo, eveningWorkersNo, nightWorkersNo, safetyWorkersNo);
            if (!string.IsNullOrEmpty(socialSecurityCode))
                validationBusiness.ValidateSocialSecurityCode(socialSecurityCode);
            
            CEO ceo = dbContext.CEOs.Include(c => c.Company).FirstOrDefault(e => e.User.UserName == username);
            if (ceo == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            if (ceo.Company.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "اطلاعات قبلاً تایید شده است");
            if (companyCode != ceo.Company.CompanyCode)
            {
                validationBusiness.ValidateCompanyCode(companyCode);
                if (dbContext.Companies.Any(c => c.CompanyCode == companyCode))
                    throw new Model.Exception.DuplicatedException("شناسه ملی شرکت", "شناسه ملی شرکت توسط شرکت دیگری وارد شده است");
                ceo.Company.CompanyCode = companyCode;
            }
            ceo.Company.EveningWorkersNo = eveningWorkersNo;
            ceo.Company.FemaleWorkersNo = femaleWorkersNo;
            FieldOfActivity fieldOfActivity = dbContext.FieldOfActivities.FirstOrDefault(fa => fa.Id == fieldOfActivityId);
            ceo.Company.FieldOfActivity = fieldOfActivity ?? throw new Model.Exception.NotFoundException("موضوع فعالیت", "");
            ceo.Company.MaleWorkersNo = maleWorkersNo;
            ceo.Company.MorningWorkersNo = morningWorkersNo;
            ceo.Company.NightWorkersNo = nightWorkersNo;
            ProductionType productionType = dbContext.ProductionTypes.FirstOrDefault(pt => pt.Id == productionTypeId);
            ceo.Company.ProductionType = productionType ?? throw new Model.Exception.NotFoundException("نوع تولید", "");
            ceo.Company.SafetyWorkersNo = safetyWorkersNo;
            ceo.Company.TechnicalProtectionCommittee = technicalProtectionCommittee;
            ceo.Company.UnitName = unitName;
            ceo.Company.WorkshopSize = workshopSize;
            ceo.Company.SocialSecurityCode = socialSecurityCode;
            dbContext.SaveChanges();
        }
        public WorkshopBasicInfo GetWorkshopInfo(string username)
        {
            var result = dbContext.CEOs.Where(c => c.User.UserName == username).Select(c => new WorkshopBasicInfo()
            {
                //IsEditable = e.Person.Comments,
                CompanyCode = c.Company.CompanyCode,
                EveningWorkersNo = c.Company.EveningWorkersNo,
                FemaleWorkersNo = c.Company.FemaleWorkersNo,
                FieldOfActivityId = c.Company.FieldOfActivity_ID,
                MaleWorkersNo = c.Company.MaleWorkersNo,
                MorningWorkersNo = c.Company.MorningWorkersNo,
                NightWorkersNo = c.Company.NightWorkersNo,
                ProductionTypeId = c.Company.ProductionType_ID,
                SafetyWorkersNo = c.Company.SafetyWorkersNo,
                TechnicalProtectionCommittee = c.Company.TechnicalProtectionCommittee,
                UnitName = c.Company.UnitName,
                WorkshopSize = c.Company.WorkshopSize,
                SocialSecurityCode = c.Company.SocialSecurityCode,
            }).FirstOrDefault();
            if (result == null)
                throw new Model.Exception.NotFoundException("شخص", "");
            return result;
        }
        public WorkshopLocationInfo GetWorkshopLocationInfo(string username)
        {
            var result = dbContext.CEOs.Where(c => c.User.UserName == username).Select(c => new WorkshopLocationInfo()
            {
                //IsEditable = e.Person.Comments,
                Address = c.Company.Location.Address,
                CountyID = c.Company.Location.County_ID,
                Phone = c.Company.Location.Phone,
                PostalCode = c.Company.Location.PostalCode,
                ProvinceID = c.Company.Location.County_ID == null ? null : (int?)c.Company.Location.County.Province_ID,
            }).FirstOrDefault();
            if (result == null)
                throw new Model.Exception.NotFoundException("شخص", "");
            return result;
        }
        public void ModifyWorkshopLocation(string username, string address, int countyID, string phone, string postalCode)
        {
            validationBusiness.ValidatePhone(phone);
            validationBusiness.ValidateAddress(address);
            validationBusiness.ValidatePostalCode(postalCode);
            if (!dbContext.Counties.Any(c => c.Id == countyID))
                throw new Model.Exception.NotFoundException("شهرستان", "");
            CEO ceo = dbContext.CEOs.Include(c => c.Company.Location).FirstOrDefault(e => e.User.UserName == username);
            if (ceo == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            if (ceo.Company.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "اطلاعات قبلاً تایید شده است");
            ceo.Company.Location.Address = address;
            ceo.Company.Location.County_ID = countyID;
            ceo.Company.Location.Phone = phone;
            ceo.Company.Location.PostalCode = postalCode;
            dbContext.SaveChanges();
        }
        public List<FoundExpert> FindExperts(string username, string filter)
        {
            CEO ceo = dbContext.CEOs.FirstOrDefault(c => c.User.UserName == username);
            if (ceo == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            //Just to make sure its completed enough
            var result = dbContext.Experts.Where(e => e.Person_ID != ceo.Person_ID && e.Person.BirthDate.HasValue && e.Person.HomeLocation_ID.HasValue && e.Person.HomeLocation.County_ID.HasValue);
            result = result.Where(e => !e.Licenses.Any(l => l.Company_ID == ceo.Company_ID && l.Flows.All(f => !f.IsCurrent || f.Type != LicenseFlow.FlowType.Deactivated) && (l.EndDate.HasValue ? l.EndDate >= DateTime.Now : true)));
            if (!string.IsNullOrEmpty(filter))
                foreach (var filterPart in filter.Split(' '))
                    result = result.Where(e => e.Person.FirstName.Contains(filterPart) ||
                                               e.Person.LastName.Contains(filterPart) ||
                                               e.Person.NationalCode.Contains(filterPart) ||
                                               e.User.Mobile.Contains(filterPart) ||
                                               e.User.Email.Contains(filterPart) ||
                                               e.User.UserName.Contains(filterPart) ||
                                               e.Person.HomeLocation.County.Name.Contains(filterPart) ||
                                               e.Person.HomeLocation.County.Province.Name.Contains(filterPart));
            if (result.Count() > 15)
                throw new Model.Exception.NotPossibleException("جستجو", "دقت معیار جستجو کافی نمی باشد");
            return result.Select(e => new FoundExpert()
            {
                BirthDate = e.Person.BirthDate.Value,
                County = e.Person.HomeLocation.County.Name,
                Province = e.Person.HomeLocation.County.Province.Name,
                Email = e.User.Email,
                FullName = e.Person.FirstName + " " + e.Person.LastName,
                MaxEducation = e.Person.Educations.OrderByDescending(ed => ed.DegreeLevel).Select(ed => new FoundExpert.Education()
                {
                    DegreeLevel = ed.DegreeLevel,
                    EducationField = ed.FieldOfEducation.Name,
                }).FirstOrDefault(),
                NationalCode = e.Person.NationalCode,
                PhotoScan_ID = e.Person.PhotoScan_ID,
                Username = e.User.UserName,
            }).ToList();
        }
        public void HireAnExpert(string username, string expertUsername)
        {
            CEO ceo = dbContext.CEOs.Include(c => c.Company.Licenses).FirstOrDefault(c => c.User.UserName == username);
            if (ceo == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            validationBusiness.ValidateCompletedCompanyData(ceo.Company_ID);
            Expert expert = dbContext.Experts.Where(e => e.User.UserName == expertUsername && e.Person_ID != ceo.Person_ID && e.Person.BirthDate.HasValue && e.Person.HomeLocation_ID.HasValue && e.Person.HomeLocation.County_ID.HasValue).FirstOrDefault();
            if (expert == null)
                throw new Model.Exception.NotFoundException("مسئول ایمنی", "");
            if (ceo.Company.Licenses.Any(l => l.Expert_ID == expert.Id && l.Flows.All(f => !f.IsCurrent || f.Type != LicenseFlow.FlowType.Deactivated) && (l.EndDate.HasValue ? l.EndDate >= DateTime.Now : true)))
                throw new Model.Exception.NotAllowedException("استخدام", "این مسئول ایمنی در حال حاضر استخدام شرکت شما می باشد");
            ceo.Company.Licenses.Add(new License()
            {
                Expert = expert,
                Flows = new List<LicenseFlow>()
                {
                    new LicenseFlow(username)
                    {
                        Type=LicenseFlow.FlowType.WithExpert,
                    }
                },
            });
            dbContext.SaveChanges();
        }
        public ProfileProgress CheckProfileProgress(string username)
        {
            CEO ceo = dbContext.CEOs.FirstOrDefault(c => c.User.UserName == username);
            if (ceo == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            return new ProfileProgress()
            {
                Messages = validationBusiness.GetCompanyIncompletedData(ceo.Company_ID),
            };
        }
    }
}
