﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Isaap.Core.DataAccess;
using Isaap.Core.Model.Log;

namespace Isaap.Core.Business
{
    public class LogBusiness : BaseBusiness
    {
        public LogBusiness(IsaapContext dbContext) : base(dbContext)
        {
        }
        public void LogPostAction(string actionName, string ip, string username, string data)
        {
            dbContext.ActionLogs.Add(new ActionLog()
            {
                ActionName = actionName,
                ActionType = "POST",
                IP = ip,
                Username = username,
                Data = data,
            });
            dbContext.SaveChanges();
        }
    }
}
