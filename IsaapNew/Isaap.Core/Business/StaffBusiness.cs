﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Isaap.Core.DataAccess;
using Isaap.Core.Model.VM;
using Isaap.Core.Model.User;
using Isaap.Core.Model.License;
using System.Data.Entity;

namespace Isaap.Core.Business
{
    public class StaffBusiness : UserBusiness
    {
        public StaffBusiness(IsaapContext dbContext) : base(dbContext)
        {
        }
        private User.UserType GetUserType(string username)
        {
            var user = dbContext.Users.Where(u => u.UserName == username && (u.Type == User.UserType.Association ||
                                                                             u.Type == User.UserType.Club ||
                                                                             u.Type == User.UserType.CountyInspector ||
                                                                             u.Type == User.UserType.InspectionOffice ||
                                                                             u.Type == User.UserType.ProvinceInspector ||
                                                                             u.Type == User.UserType.ProvinceManager)).FirstOrDefault();
            if (user == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            return user.Type;
        }
        public DashboardData GetDashboardData(string username)
        {
            var type = GetUserType(username);
            DashboardData result = null;
            switch (type)
            {
                case User.UserType.Association:
                    result = dbContext.Associations.Where(a => a.User.UserName == username).Select(s => new DashboardData()
                    {
                        FullName = "انجمن شهرستان " + s.County.Name
                    }).FirstOrDefault();
                    break;
                case User.UserType.Club:
                    result = dbContext.Clubs.Where(a => a.User.UserName == username).Select(s => new DashboardData()
                    {
                        FullName = "کانون استان " + s.Province.Name
                    }).FirstOrDefault();
                    break;
                case User.UserType.CountyInspector:
                    result = dbContext.CountyInspectors.Where(a => a.User.UserName == username).Select(s => new DashboardData()
                    {
                        FullName = "اداره بازرسی شهرستان " + s.County.Name
                    }).FirstOrDefault();
                    break;
                case User.UserType.InspectionOffice:
                    result = dbContext.InspectionOffices.Where(a => a.User.UserName == username).Select(s => new DashboardData()
                    {
                        FullName = "اداره کل بازرسی کشور"
                    }).FirstOrDefault();
                    break;
                case User.UserType.ProvinceInspector:
                    result = dbContext.ProvinceInspectors.Where(a => a.User.UserName == username).Select(s => new DashboardData()
                    {
                        FullName = "اداره بازرسی استان " + s.Province.Name
                    }).FirstOrDefault();
                    break;
                case User.UserType.ProvinceManager:
                    result = dbContext.ProvinceManagers.Where(a => a.User.UserName == username).Select(s => new DashboardData()
                    {
                        FullName = "مدیر بازرسی استان " + s.Province.Name
                    }).FirstOrDefault();
                    break;
            }
            if (result == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            return result;
        }
        private IQueryable<Expert> GetAllExperts(string username)
        {
            var myType = GetUserType(username);
            //from now, including incompleted experts
            var data = dbContext.Experts;
            switch (myType)
            {
                case User.UserType.Association:
                    return data.Where(e => e.Licenses.Any(l => l.Company.Location.County_ID.HasValue && l.Company.Location.County.Associations.Any(a => a.User.UserName == username)));
                case User.UserType.Club:
                    return data.Where(e => e.Licenses.Any(l => l.Company.Location.County_ID.HasValue && l.Company.Location.County.Province.Clubs.Any(c => c.User.UserName == username)));
                case User.UserType.CountyInspector:
                    return data.Where(e => e.Licenses.Any(l => l.Company.Location.County_ID.HasValue && l.Company.Location.County.CountyInspectors.Any(c => c.User.UserName == username)));
                case User.UserType.InspectionOffice:
                    return data.Where(e => e.Licenses.Any(l => l.Company.Location.County_ID.HasValue));
                case User.UserType.ProvinceInspector:
                    return data.Where(e => e.Licenses.Any(l => l.Company.Location.County_ID.HasValue && l.Company.Location.County.Province.ProvinceInspectors.Any(pi => pi.User.UserName == username)));
                case User.UserType.ProvinceManager:
                    return data.Where(e => e.Licenses.Any(l => l.Company.Location.County_ID.HasValue && l.Company.Location.County.Province.ProvinceManagers.Any(pm => pm.User.UserName == username)));
            }
            throw new Model.Exception.InternalException("نوع نامشخص کاربر");
        }
        private IQueryable<Expert> GetMyWaitingExpertsQuery(string username)
        {
            var myType = GetUserType(username);
            var data = GetAllExperts(username);
            switch (myType)
            {
                case User.UserType.Association:
                    return data.Where(e => e.Licenses.Any(l => l.Flows.Any(lf => lf.IsCurrent && lf.Type == Model.License.LicenseFlow.FlowType.WithAssociation)));
                case User.UserType.Club:
                    return data.Where(e => e.Licenses.Any(l => l.Flows.Any(lf => lf.IsCurrent && lf.Type == Model.License.LicenseFlow.FlowType.WithClub)));
                case User.UserType.CountyInspector:
                    return data.Where(e => e.Licenses.Any(l => l.Flows.Any(lf => lf.IsCurrent && lf.Type == Model.License.LicenseFlow.FlowType.WithCountyInspector)));
                case User.UserType.InspectionOffice:
                    return data.Where(e => e.Licenses.Any(l => l.Flows.Any(lf => lf.IsCurrent && lf.Type == Model.License.LicenseFlow.FlowType.WithInspectionOffice)));
                case User.UserType.ProvinceInspector:
                    return data.Where(e => e.Licenses.Any(l => l.Flows.Any(lf => lf.IsCurrent && lf.Type == Model.License.LicenseFlow.FlowType.WithProvinceInspector)));
                case User.UserType.ProvinceManager:
                    return data.Where(e => e.Licenses.Any(l => l.Flows.Any(lf => lf.IsCurrent && lf.Type == Model.License.LicenseFlow.FlowType.WithProvinceManager)));
            }
            throw new Model.Exception.InternalException("نوع نامشخص کاربر");
        }
        public List<WaitingExpertOverview> GetMyWaitingExperts(string username, string filter)
        {
            var result = GetMyWaitingExpertsQuery(username);
            if (!string.IsNullOrEmpty(filter))
                foreach (var filterPart in filter.Split(' '))
                    result = result.Where(e => e.Person.FirstName.Contains(filterPart) ||
                                               e.Person.LastName.Contains(filterPart) ||
                                               e.Person.NationalCode.Contains(filterPart) ||
                                               e.User.Mobile.Contains(filterPart) ||
                                               e.User.Email.Contains(filterPart) ||
                                               e.User.UserName.Contains(filterPart) ||
                                               e.Person.HomeLocation.County.Name.Contains(filterPart) ||
                                               e.Person.HomeLocation.County.Province.Name.Contains(filterPart));
            return result.Select(e => new WaitingExpertOverview()
            {
                BirthDate = e.Person.BirthDate.Value,
                FatherName = e.Person.FatherName,
                FullName = e.Person.FirstName + " " + e.Person.LastName,
                Gender = e.Person.Gender.Value,
                HomeCounty = e.Person.HomeLocation.County.Name,
                HomeProvince = e.Person.HomeLocation.County.Province.Name,
                Email = e.User.Email,
                MaxEducation = e.Person.Educations.OrderByDescending(ed => ed.DegreeLevel).Select(ed => new WaitingExpertOverview.Education()
                {
                    DegreeLevel = ed.DegreeLevel,
                    EducationField = ed.FieldOfEducation.Name,
                }).FirstOrDefault(),
                NationalCode = e.Person.NationalCode,
                PhotoScan_ID = e.Person.PhotoScan_ID.Value,
                ShenasnameNo = e.Person.ShenasnameNo,
                Username = e.User.UserName,
                Mobile = e.User.Mobile,
                NumberOfLicenses = e.Licenses.Count(),
                NumberOfAcceptedLicenses = e.Licenses.Count(l => l.Flows.Any(f => f.IsCurrent && f.Type == Model.License.LicenseFlow.FlowType.Authorized))
            }).ToList();
        }
        private bool IsLicenseAssignedToMe(string username, User.UserType userType, int licenseId)
        {
            switch (userType)
            {
                case User.UserType.Association:
                    return dbContext.Licenses.Any(l => l.Id == licenseId &&
                                                       l.Flows.Any(lf => lf.IsCurrent && lf.Type == LicenseFlow.FlowType.WithAssociation) &&
                                                       l.Company.Location.County_ID.HasValue &&
                                                       l.Company.Location.County.Associations.Any(s => s.User.UserName == username));
                case User.UserType.Club:
                    return dbContext.Licenses.Any(l => l.Id == licenseId &&
                                                       l.Flows.Any(lf => lf.IsCurrent && lf.Type == LicenseFlow.FlowType.WithClub) &&
                                                       l.Company.Location.County_ID.HasValue &&
                                                       l.Company.Location.County.Province.Clubs.Any(s => s.User.UserName == username));
                case User.UserType.CountyInspector:
                    return dbContext.Licenses.Any(l => l.Id == licenseId &&
                                                       l.Flows.Any(lf => lf.IsCurrent && lf.Type == LicenseFlow.FlowType.WithCountyInspector) &&
                                                       l.Company.Location.County_ID.HasValue &&
                                                       l.Company.Location.County.CountyInspectors.Any(s => s.User.UserName == username));
                case User.UserType.InspectionOffice:
                    return dbContext.Licenses.Any(l => l.Id == licenseId &&
                                                       l.Flows.Any(lf => lf.IsCurrent && lf.Type == LicenseFlow.FlowType.WithInspectionOffice) &&
                                                       l.Company.Location.County_ID.HasValue);
                case User.UserType.ProvinceInspector:
                    return dbContext.Licenses.Any(l => l.Id == licenseId &&
                                                       l.Flows.Any(lf => lf.IsCurrent && lf.Type == LicenseFlow.FlowType.WithProvinceInspector) &&
                                                       l.Company.Location.County_ID.HasValue &&
                                                       l.Company.Location.County.Province.ProvinceInspectors.Any(s => s.User.UserName == username));
                case User.UserType.ProvinceManager:
                    return dbContext.Licenses.Any(l => l.Id == licenseId &&
                                                       l.Flows.Any(lf => lf.IsCurrent && lf.Type == LicenseFlow.FlowType.WithProvinceManager) &&
                                                       l.Company.Location.County_ID.HasValue &&
                                                       l.Company.Location.County.Province.ProvinceManagers.Any(s => s.User.UserName == username));
            }
            throw new Model.Exception.InternalException("نوع نامشخص کاربر");
        }
        public WaitingExpert GetExpertData(string username, string expertUsername)
        {
            var myType = GetUserType(username);
            var result = GetAllExperts(username).Where(e => e.User.UserName == expertUsername)
                                                           .Select(e => new WaitingExpert()
                                                           {
                                                               Expert = new WaitingExpert.ExpertData()
                                                               {
                                                                   BirthDate = e.Person.BirthDate.Value,
                                                                   Email = e.User.Email,
                                                                   FatherName = e.Person.FatherName,
                                                                   FirstName = e.Person.FirstName,
                                                                   Gender = e.Person.Gender.Value,
                                                                   HomeAddress = e.Person.HomeLocation.Address,
                                                                   HomeCounty = e.Person.HomeLocation.County.Name,
                                                                   HomePhone = e.Person.HomeLocation.Phone,
                                                                   HomePostalCode = e.Person.HomeLocation.PostalCode,
                                                                   HomeProvince = e.Person.HomeLocation.County.Province.Name,
                                                                   LastName = e.Person.LastName,
                                                                   MaritalStatus = e.Person.MaritalStatus.Value,
                                                                   MilitaryStatus = e.Person.MilitaryStatus.Value,
                                                                   Mobile = e.User.Mobile,
                                                                   NationalCardScanIds = e.Person.NationalCardScans.Select(s => s.Id),
                                                                   Certifications = e.Person.SafetyCertifications.Select(sc => new WaitingExpert.ExpertData.CertificationData()
                                                                   {
                                                                       CertificationScan_ID = sc.CertificationScan_ID,
                                                                       DateTaken = sc.DateTaken,
                                                                       Description = sc.Description,
                                                                       DurationInHours = sc.FieldOfCertification_ID.HasValue ? sc.FieldOfCertification.DurationInHours : sc.OtherDurationInHours,
                                                                       FieldOfCertification = sc.FieldOfCertification_ID.HasValue ? sc.FieldOfCertification.Name : sc.OtherCourseName,
                                                                       IssuerName = sc.IssuerName,
                                                                   }),
                                                                   Educations = e.Person.Educations.Select(ed => new WaitingExpert.ExpertData.EducationData()
                                                                   {
                                                                       DegreeLevel = ed.DegreeLevel,
                                                                       FieldOfEducation = ed.FieldOfEducation.Name,
                                                                       ScanFile_ID = ed.ScanFile_ID.Value,
                                                                   }),
                                                                   NationalCode = e.Person.NationalCode,
                                                                   PhotoScan_ID = e.Person.PhotoScan_ID.Value,
                                                                   PlaceOfBirth = e.Person.PlaceOfBirth,
                                                                   ReasonOfExemption = e.Person.ReasonOfExemption,
                                                                   ShenasnameNo = e.Person.ShenasnameNo,
                                                                   ShenasnameScanIds = e.Person.ShenasnameScans.Select(ss => ss.Id),
                                                                   Username = e.User.UserName,
                                                                   WorkExperiences = e.Person.WorkExperiences.Select(we => new WaitingExpert.ExpertData.WorkExperienceData()
                                                                   {
                                                                       DateFrom = we.DateFrom,
                                                                       DateTo = we.DateTo,
                                                                       Description = we.Description,
                                                                       EmployerName = we.EmployerName,
                                                                       WorkshopName = we.WorkshopName,
                                                                   }),
                                                               },
                                                               Licenses = e.Licenses.Select(l => new WaitingExpert.LicenseData()
                                                               {
                                                                   Company = new WaitingExpert.LicenseData.CompanyData()
                                                                   {
                                                                       Address = l.Company.Location.Address,
                                                                       CEOFirstName = l.Company.CEOs.Select(ceo => ceo.Person.FirstName).FirstOrDefault(),
                                                                       CEOLastName = l.Company.CEOs.Select(ceo => ceo.Person.LastName).FirstOrDefault(),
                                                                       CompanyCode = l.Company.CompanyCode,
                                                                       County = l.Company.Location.County.Name,
                                                                       CountyId = l.Company.Location.County_ID.Value,
                                                                       EveningWorkersNo = l.Company.EveningWorkersNo,
                                                                       FemaleWorkersNo = l.Company.FemaleWorkersNo,
                                                                       FieldOfActivity = l.Company.FieldOfActivity.Name,
                                                                       FieldOfActivityDangerLevel = l.Company.FieldOfActivity.DangerLevel,
                                                                       MaleWorkersNo = l.Company.MaleWorkersNo,
                                                                       MorningWorkersNo = l.Company.MorningWorkersNo,
                                                                       NightWorkersNo = l.Company.NightWorkersNo,
                                                                       Phone = l.Company.Location.Phone,
                                                                       PostalCode = l.Company.Location.PostalCode,
                                                                       ProductionType = l.Company.ProductionType.Name,
                                                                       Province = l.Company.Location.County.Province.Name,
                                                                       ProvinceId = l.Company.Location.County.Province_ID,
                                                                       SafetyWorkersNo = l.Company.SafetyWorkersNo,
                                                                       SocialSecurityCode = l.Company.SocialSecurityCode,
                                                                       UnitName = l.Company.UnitName,
                                                                       WorkshopSize = l.Company.WorkshopSize,
                                                                   },
                                                                   LicenseId = l.Id,
                                                                   ContractScanIds = l.ContractScans.Select(cs => cs.Id),
                                                                   DateCreated = l.DateCreated,
                                                                   EndDate = l.EndDate,
                                                                   StartDate = l.StartDate,
                                                                   Username = l.User_ID.HasValue ? l.User.UserName : null,
                                                                   CurrentFlowType = l.Flows.Where(lf => lf.IsCurrent).Select(lf => lf.Type).FirstOrDefault(),
                                                               }),
                                                           }).FirstOrDefault();
            if (result == null)
                throw new Model.Exception.NotFoundException("مسئول ایمنی", "");
            foreach (var item in result.Licenses)
                item.IsAssignedToMe = IsLicenseAssignedToMe(username, myType, item.LicenseId);
            return result;
        }

        private LicenseFlow GetNextUpperLevel(string username, int countyID, int provinceID, LicenseFlow.FlowType currentFlow)
        {
            switch (currentFlow)
            {
                case LicenseFlow.FlowType.WithAssociation:
                    if (dbContext.Clubs.Any(c => c.Province_ID == provinceID))
                        return new LicenseFlow(username) { Type = LicenseFlow.FlowType.WithClub };
                    else
                        return GetNextUpperLevel(username, countyID, provinceID, LicenseFlow.FlowType.WithClub);
                case LicenseFlow.FlowType.WithClub:
                    if (dbContext.CountyInspectors.Any(ci => ci.County_ID == countyID))
                        return new LicenseFlow(username) { Type = LicenseFlow.FlowType.WithCountyInspector };
                    else
                        return GetNextUpperLevel(username, countyID, provinceID, LicenseFlow.FlowType.WithCountyInspector);
                case LicenseFlow.FlowType.WithCountyInspector:
                    if (dbContext.ProvinceInspectors.Any(pi => pi.Province_ID == provinceID))
                        return new LicenseFlow(username) { Type = LicenseFlow.FlowType.WithProvinceInspector };
                    else
                        throw new Model.Exception.InternalException("دفتر بازرسی استان پیدا نشد");
            }
            throw new Model.Exception.NotAllowedException("تغییر در پرونده", "مسیر بالا از اینجا آغاز نمیشود");
        }
        private LicenseFlow GetNextLowerLevel(string username, int countyID, int provinceID, LicenseFlow.FlowType currentFlow)
        {
            switch (currentFlow)
            {
                case LicenseFlow.FlowType.WithAssociation:
                    return new LicenseFlow(username) { Type = LicenseFlow.FlowType.WithExpert };
                case LicenseFlow.FlowType.WithClub:
                    if (dbContext.Associations.Any(a => a.County_ID == countyID))
                        return new LicenseFlow(username) { Type = LicenseFlow.FlowType.WithAssociation };
                    else
                        return GetNextLowerLevel(username, countyID, provinceID, LicenseFlow.FlowType.WithAssociation);
                case LicenseFlow.FlowType.WithCountyInspector:
                    if (dbContext.Clubs.Any(c => c.Province_ID == provinceID))
                        return new LicenseFlow(username) { Type = LicenseFlow.FlowType.WithClub };
                    else
                        return GetNextLowerLevel(username, countyID, provinceID, LicenseFlow.FlowType.WithClub);
                case LicenseFlow.FlowType.WithProvinceInspector:
                    if (dbContext.CountyInspectors.Any(ci => ci.County_ID == countyID))
                        return new LicenseFlow(username) { Type = LicenseFlow.FlowType.WithCountyInspector };
                    else
                        return GetNextLowerLevel(username, countyID, provinceID, LicenseFlow.FlowType.WithCountyInspector);
            }
            throw new Model.Exception.NotAllowedException("تغییر در پرونده", "مسیر پایین از اینجا آغاز نمیشود");
        }

        public void RejectLicenseRecognition(string username, int licenseId, string description)
        {
            var myType = GetUserType(username);
            if (!IsLicenseAssignedToMe(username, myType, licenseId))
                throw new Model.Exception.NotAllowedException("تصمیم گیری", "پروانه در دست شما نمی باشد");
            License license = dbContext.Licenses.Where(l => l.Id == licenseId).Include(l => l.Flows).First();
            var flow = license.Flows.Single(lf => lf.IsCurrent);
            flow.Description = description;
            flow.IsCurrent = false;
            var newFlow = GetNextLowerLevel(username, license.Company.Location.County_ID.Value, license.Company.Location.County.Province_ID, flow.Type);
            newFlow.IsCurrent = true;
            license.Flows.Add(newFlow);
            dbContext.SaveChanges();
        }
        public void RecognizeLicense(string username, int licenseId, string description)
        {
            var myType = GetUserType(username);
            if (!IsLicenseAssignedToMe(username, myType, licenseId))
                throw new Model.Exception.NotAllowedException("تصمیم گیری", "پروانه در دست شما نمی باشد");
            License license = dbContext.Licenses.Where(l => l.Id == licenseId).Include(l => l.Flows).First();
            var flow = license.Flows.Single(lf => lf.IsCurrent);
            flow.Description = description;
            flow.IsCurrent = false;
            var newFlow = GetNextUpperLevel(username, license.Company.Location.County_ID.Value, license.Company.Location.County.Province_ID, flow.Type);
            newFlow.IsCurrent = true;
            license.Flows.Add(newFlow);
            dbContext.SaveChanges();
        }
        public void AuthenticateLicense(string username, int licenseId, string description)
        {
            var myType = GetUserType(username);
            if (!IsLicenseAssignedToMe(username, myType, licenseId))
                throw new Model.Exception.NotAllowedException("تصمیم گیری", "پروانه در دست شما نمی باشد");
            License license = dbContext.Licenses.Where(l => l.Id == licenseId)
                                                .Include(l => l.Flows)
                                                .Include(l => l.Expert.Person.Educations)
                                                .Include(l => l.Expert.Person.SafetyCertifications)
                                                .Include(l => l.Expert.Person.WorkExperiences)
                                                .Include(l => l.Company)
                                                .First();
            var flow = license.Flows.Single(lf => lf.IsCurrent);
            flow.Description = description;
            flow.IsCurrent = false;
            var newFlow = new LicenseFlow(username) { IsCurrent = true };
            switch (flow.Type)
            {
                case LicenseFlow.FlowType.WithProvinceInspector:
                case LicenseFlow.FlowType.WithProvinceManager:
                case LicenseFlow.FlowType.WithInspectionOffice:
                    license.StartDate = DateTime.Now;
                    license.EndDate = DateTime.Now.AddYears(2);
                    newFlow.Type = LicenseFlow.FlowType.Authorized;
                    break;
                default:
                    throw new Model.Exception.NotAllowedException("تغییر در پرونده", "پرونده در مرحله درستی قرار ندارد");
            }
            license.Flows.Add(newFlow);
            //this is where we do bad things
            license.Company.IsAuthorized = true;
            license.Expert.Person.IsAuthorized = true;
            license.Expert.Person.Educations.ForEach(ed => ed.IsAuthorized = true);
            license.Expert.Person.SafetyCertifications.ForEach(sc => sc.IsAuthorized = true);
            license.Expert.Person.WorkExperiences.ForEach(we => we.IsAuthorized = true);
            //done
            dbContext.SaveChanges();
        }
        public void RejectLicenseAuthentication(string username, int licenseId, string description)
        {
            var myType = GetUserType(username);
            if (!IsLicenseAssignedToMe(username, myType, licenseId))
                throw new Model.Exception.NotAllowedException("تصمیم گیری", "پروانه در دست شما نمی باشد");
            License license = dbContext.Licenses.Where(l => l.Id == licenseId).Include(l => l.Flows).First();
            var flow = license.Flows.Single(lf => lf.IsCurrent);
            flow.Description = description;
            flow.IsCurrent = false;
            var newFlow = new LicenseFlow(username) { IsCurrent = true };
            switch (flow.Type)
            {
                case LicenseFlow.FlowType.WithProvinceInspector:
                    newFlow.Type = LicenseFlow.FlowType.RejectedOnce;
                    break;
                case LicenseFlow.FlowType.WithProvinceManager:
                    newFlow.Type = LicenseFlow.FlowType.RejectedTwice;
                    break;
                case LicenseFlow.FlowType.WithInspectionOffice:
                    newFlow.Type = LicenseFlow.FlowType.Deactivated;
                    newFlow.Description = "پس از 2 بار اعتراض مورد تایید قرار نگرفت";
                    break;
                default:
                    throw new Model.Exception.NotAllowedException("تغییر در پرونده", "پرونده در مرحله درستی قرار ندارد");
            }
            license.Flows.Add(newFlow);
            dbContext.SaveChanges();
        }
        public List<ExpertLicense.Flow> GetLicenseFlows(string username, int licenseId)
        {
            return GetAllExperts(username).Select(e => e.Licenses)
                                          .SelectMany(l => l)
                                          .Where(l => l.Id == licenseId)
                                          .Select(l => l.Flows)
                                          .SelectMany(lf => lf)
                                          .OrderBy(lf => lf.DateCreated)
                                          .Select(lf => new ExpertLicense.Flow
                                          {
                                              AttachmentIds = lf.Attachments.Select(a => a.Id),
                                              DateCreated = lf.DateCreated,
                                              Description = lf.Description,
                                              Type = lf.Type,
                                          }).ToList();
        }
        public LicensePrint GetLicensePrintData(string username, int licenseId)
        {
            var result = GetAllExperts(username).Select(e => e.Licenses)
                                          .SelectMany(l => l)
                                          .Where(l => l.Id == licenseId && l.Flows.Any(lf => lf.IsCurrent && lf.Type == LicenseFlow.FlowType.Authorized))
                                          .Select(l => new LicensePrint()
                                          {
                                              CompanyFullAddress = l.Company.Location.County.Province.Name + "، " +
                                                                   l.Company.Location.County.Name + "، " +
                                                                   l.Company.Location.Address + "، کدپستی " +
                                                                   l.Company.Location.PostalCode,
                                              CompanyName = l.Company.UnitName,
                                              FieldOfActivity = l.Company.FieldOfActivity.Name,
                                              FullName = l.Expert.Person.FirstName + " " + l.Expert.Person.LastName,
                                              Gender = l.Expert.Person.Gender.Value,
                                              MaxEducation = l.Expert.Person.Educations.OrderByDescending(ed => ed.DegreeLevel).Select(ed => new LicensePrint.Education
                                              {
                                                  DegreeLevel = ed.DegreeLevel,
                                                  EducationField = ed.FieldOfEducation.Name,
                                              }).FirstOrDefault(),
                                              CompanyProvince = l.Company.Location.County.Province.Name,
                                              PhotoId = l.Expert.Person.PhotoScan_ID.Value,
                                              ProvinceChiefFullName = l.Company.Location.County.Province.ProvinceInspectors.Select(pi => pi.ChiefPerson.FirstName + " " + pi.ChiefPerson.LastName).FirstOrDefault(),
                                              ProvinceOfficeAddress = l.Company.Location.County.Province.ProvinceInspectors.Select(pi => pi.Location.County.Province.Name + "، " + pi.Location.County.Name + "، " + pi.Location.Address).FirstOrDefault(),
                                              ProvinceOfficePhone = l.Company.Location.County.Province.ProvinceInspectors.Select(pi => pi.Location.Phone).FirstOrDefault(),
                                              TotalNumberOfWorkers = l.Company.FemaleWorkersNo + l.Company.MaleWorkersNo,
                                              DateAuthorized = l.Flows.Where(lf => lf.IsCurrent && lf.Type == LicenseFlow.FlowType.Authorized).Select(lf => lf.DateCreated).FirstOrDefault(),
                                          }).FirstOrDefault();
            if (result == null)
                throw new Model.Exception.NotFoundException("پروانه", "");
            return result;
        }
    }
}
