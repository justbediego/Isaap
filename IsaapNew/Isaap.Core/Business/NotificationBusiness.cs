﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Isaap.Core.DataAccess;
using System.Net.Mail;
using System.Net;
using System.Configuration;
using AegisImplicitMail;
using System.IO;

namespace Isaap.Core.Business
{
    public class NotificationBusiness : BaseBusiness
    {
        private MimeMailer smtpClient;
        public NotificationBusiness(IsaapContext dbContext) : base(dbContext)
        {
            smtpClient = new MimeMailer("isaap.ir", 465)
            {
                SslType = SslMode.Ssl,
                User = ConfigurationManager.AppSettings["NoReplyEmail"],
                Password = ConfigurationManager.AppSettings["NoReplyPassword"],
                AuthenticationMode = AuthenticationType.Base64
            };
        }
        private void SendMail(string toAddress, string subject, string body)
        {
            var mailMessage = new MimeMailMessage()
            {
                SubjectEncoding = Encoding.UTF8,
                BodyEncoding = Encoding.UTF8,
                IsBodyHtml = true,
                Subject = subject,
                Body = body,
                From = new MimeMailAddress(ConfigurationManager.AppSettings["NoReplyEmail"])
            };
            mailMessage.To.Add(new MimeMailAddress(toAddress));
            smtpClient.SendMail(mailMessage);
        }
        public void SendRegisterationMail(string toAddress, string username, string fullName, string validationKey)
        {
            SendMail(toAddress, "Isaap Core: Registeration", string.Format(Resource.RegisteryEmail, fullName, username, validationKey));
        }
    }
}
