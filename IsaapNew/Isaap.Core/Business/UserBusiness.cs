﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Isaap.Core.DataAccess;
using Isaap.Core.Model.User;

namespace Isaap.Core.Business
{
    public class UserBusiness : BaseBusiness
    {
        private SystemBusiness systemBusiness;
        private ValidationBusiness validationBusiness;
        public UserBusiness(IsaapContext dbContext) : base(dbContext)
        {
            systemBusiness = new SystemBusiness(dbContext);
            validationBusiness = new ValidationBusiness(dbContext);
        }
        public User GetUser(string username, string password)
        {
            string hashedPassword = systemBusiness.TextToMD5(password);
            var user = dbContext.Users.Where(u => u.UserName == username && u.HashPassword == hashedPassword).FirstOrDefault();
            if (user == null)
                throw new Model.Exception.NotFoundException("کاربر", "نام کاربری و یا کلمه عبور اشتباه می باشد");
            return user;
        }
        public void ChangePassword(string username, string oldPassword, string newPassword)
        {
            validationBusiness.ValidatePassword(newPassword);
            string hashedPassword = systemBusiness.TextToMD5(oldPassword);
            var user = dbContext.Users.Where(u => u.UserName == username && u.HashPassword == hashedPassword).FirstOrDefault();
            if (user == null)
                throw new Model.Exception.NotAllowedException("تغییر گذرواژه", "کلمه عبور فعلی اشتباه می باشد");
            user.HashPassword= systemBusiness.TextToMD5(newPassword);
            dbContext.SaveChanges();
        }
    }
}
