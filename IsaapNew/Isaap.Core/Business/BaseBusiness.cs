﻿using Isaap.Core.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Business
{
    public abstract class BaseBusiness
    {
        protected IsaapContext dbContext;
        public BaseBusiness(IsaapContext dbContext)
        {
            this.dbContext = dbContext;
        }
    }
}
