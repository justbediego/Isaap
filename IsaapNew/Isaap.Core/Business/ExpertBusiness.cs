﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Isaap.Core.DataAccess;
using Isaap.Core.Model.User;
using Isaap.Core.Model.Person;
using System.Data.Entity;
using Isaap.Core.Model.VM;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using Isaap.Core.Model.License;

namespace Isaap.Core.Business
{
    public class ExpertBusiness : UserBusiness
    {
        private SystemBusiness systemBusiness;
        private ValidationBusiness validationBusiness;
        private NotificationBusiness notificationBusiness;
        public ExpertBusiness(IsaapContext dbContext) : base(dbContext)
        {
            systemBusiness = new SystemBusiness(dbContext);
            validationBusiness = new ValidationBusiness(dbContext);
            notificationBusiness = new NotificationBusiness(dbContext);
        }
        private bool CanModify(string username)
        {
            List<LicenseFlow.FlowType> restrictedTypes = new List<LicenseFlow.FlowType>()
            {
                LicenseFlow.FlowType.WithAssociation,
                LicenseFlow.FlowType.WithClub,
                LicenseFlow.FlowType.WithCountyInspector,
                LicenseFlow.FlowType.WithInspectionOffice,
                LicenseFlow.FlowType.WithProvinceInspector,
                LicenseFlow.FlowType.WithProvinceManager,
                LicenseFlow.FlowType.Deactivated,
            };
            return dbContext.Experts.Any(e => e.User.UserName == username && e.Licenses.All(l => !l.Flows.Any(lf => lf.IsCurrent && restrictedTypes.Contains(lf.Type))));
        }
        private bool CanObjectify(string username, int licenseId)
        {
            List<LicenseFlow.FlowType> objectifyTypes = new List<LicenseFlow.FlowType>()
            {
                LicenseFlow.FlowType.RejectedOnce,
                LicenseFlow.FlowType.RejectedTwice,
            };
            return dbContext.Experts.Any(e => e.User.UserName == username && e.Licenses.Any(l => l.Id == licenseId && l.Flows.Any(lf => lf.IsCurrent && objectifyTypes.Contains(lf.Type))));
        }
        public void Register(string nationalcode, string firstname, string lastname, string username, string password, string email, string mobile)
        {
            validationBusiness.ValidateNationalCode(nationalcode);
            validationBusiness.ValidateFirstName(firstname);
            validationBusiness.ValidateLastName(lastname);
            validationBusiness.ValidateUserName(username);
            validationBusiness.ValidatePassword(password);
            validationBusiness.ValidateMobilePhone(mobile);
            validationBusiness.ValidateEmail(email);
            Person person = dbContext.Persons.Where(p => p.NationalCode == nationalcode).Include(p => p.Experts).FirstOrDefault();
            if (person == null)
                person = new Person()
                {
                    NationalCode = nationalcode,
                    FirstName = firstname,
                    LastName = lastname,
                    HomeLocation = new Model.Location.Location(),
                };
            if (person.Experts.Any())
                throw new Model.Exception.DuplicatedException("شخص", "کد ملی قبلاً توسط شخص دیگری وارد شده است");
            Expert expert = new Expert()
            {
                Person = person,
                User = new User()
                {
                    HashPassword = systemBusiness.TextToMD5(password),
                    UserName = username,
                    Email = email,
                    Mobile = mobile,
                    Type = User.UserType.Expert,
                },
            };
            dbContext.Experts.Add(expert);
            notificationBusiness.SendRegisterationMail(expert.User.Email, expert.User.UserName, person.FullName, expert.User.EmailValidationKey);
            dbContext.SaveChanges();
        }
        public PersonBasicInfo GetBasicInfo(string username)
        {
            var result = dbContext.Experts.Where(e => e.User.UserName == username).Select(e => new PersonBasicInfo()
            {
                //IsEditable = e.Person.Comments,
                AssociationNumber = e.Person.AssociationMembershipNo,
                BirthDate = e.Person.BirthDate,
                FatherName = e.Person.FatherName,
                FirstName = e.Person.FirstName,
                Gender = e.Person.Gender,
                LastName = e.Person.LastName,
                MaritalStatus = e.Person.MaritalStatus,
                MilitaryServiceStates = e.Person.MilitaryStatus,
                NationalCode = e.Person.NationalCode,
                PlaceOfBirth = e.Person.PlaceOfBirth,
                ReasonOfExemption = e.Person.ReasonOfExemption,
                ShenasnameNo = e.Person.ShenasnameNo,
            }).FirstOrDefault();
            if (result == null)
                throw new Model.Exception.NotFoundException("شخص", "");
            return result;
        }
        public void ModifyBasicInfo(string username, string firstName, string lastName, string fatherName, Person.GenderType gender, string shenasnameNo, string melliCode, DateTime birthDate, string placeOfBirth, Person.MaritalStates maritalStatus, Person.MilitaryServiceStates militaryServiceStates, string reasonOfExemption, string associationNumber)
        {
            validationBusiness.ValidateFirstName(firstName);
            validationBusiness.ValidateLastName(lastName);
            validationBusiness.ValidateFirstName(fatherName);
            validationBusiness.ValidateShenasnameNo(shenasnameNo);
            validationBusiness.ValidateBirthDate(birthDate);
            validationBusiness.ValidatePlaceOfBirth(placeOfBirth);
            if (gender == Person.GenderType.Male)
                validationBusiness.ValidateMilitaryServiceStatus(militaryServiceStates);
            if (gender == Person.GenderType.Male && militaryServiceStates == Person.MilitaryServiceStates.Exempted)
                validationBusiness.ValidateReasonOfExemption(reasonOfExemption);
            if (!string.IsNullOrEmpty(associationNumber))
                validationBusiness.ValidateAssociationMembershipNumber(associationNumber);
            Expert expert = dbContext.Experts.Include(e => e.Person).FirstOrDefault(e => e.User.UserName == username);
            if (expert == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            if (expert.Person.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "اطلاعات قبلاً تایید شده است");
            expert.Person.FirstName = firstName;
            expert.Person.LastName = lastName;
            expert.Person.FatherName = fatherName;
            expert.Person.ShenasnameNo = shenasnameNo;
            if (melliCode != expert.Person.NationalCode)
            {
                validationBusiness.ValidateNationalCode(melliCode);
                if (dbContext.Persons.Any(p => p.NationalCode == melliCode))
                    throw new Model.Exception.DuplicatedException("شخص", "کد ملی قبلاً توسط شخص دیگری وارد شده است");
            }
            expert.Person.NationalCode = melliCode;
            expert.Person.BirthDate = birthDate;
            expert.Person.PlaceOfBirth = placeOfBirth;
            expert.Person.Gender = gender;
            expert.Person.MilitaryStatus = militaryServiceStates;
            expert.Person.MaritalStatus = maritalStatus;
            expert.Person.ReasonOfExemption = reasonOfExemption;
            expert.Person.AssociationMembershipNo = associationNumber;
            dbContext.SaveChanges();
        }
        public PersonContactInfo GetContactInfo(string username)
        {
            var result = dbContext.Experts.Where(e => e.User.UserName == username).Select(e => new PersonContactInfo()
            {
                //IsEditable = e.Person.IsEditable,
                Address = e.Person.HomeLocation.Address,
                CountyID = e.Person.HomeLocation.County_ID,
                ProvinceID = e.Person.HomeLocation.County_ID.HasValue ? (int?)e.Person.HomeLocation.County.Province_ID : null,
                Email = e.User.Email,
                Mobile = e.User.Mobile,
                Phone = e.Person.HomeLocation.Phone,
                PostalCode = e.Person.HomeLocation.PostalCode,
            }).FirstOrDefault();
            if (result == null)
                throw new Model.Exception.NotFoundException("شخص", "");
            return result;
        }
        public void ModifyContactInfo(string username, string mobilePhone, string email, int homeCountyID, string homeAddress, string homePostalCode, string homePhone)
        {
            validationBusiness.ValidateAddress(homeAddress);
            validationBusiness.ValidatePostalCode(homePostalCode);
            validationBusiness.ValidatePhone(homePhone);
            if (!dbContext.Counties.Any(c => c.Id == homeCountyID))
                throw new Model.Exception.NotFoundException("شهرستان", "");
            Expert expert = dbContext.Experts.Include(e => e.User).Include(e => e.Person.HomeLocation).FirstOrDefault(e => e.User.UserName == username);
            if (expert == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            if (expert.Person.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "اطلاعات قبلاً تایید شده است");
            if (mobilePhone != expert.User.Mobile)
                validationBusiness.ValidateMobilePhone(mobilePhone);
            if (email != expert.User.Email)
                validationBusiness.ValidateEmail(email);
            expert.User.Mobile = mobilePhone;
            expert.User.Email = email;
            expert.Person.HomeLocation.County_ID = homeCountyID;
            expert.Person.HomeLocation.Address = homeAddress;
            expert.Person.HomeLocation.PostalCode = homePostalCode;
            expert.Person.HomeLocation.Phone = homePhone;
            dbContext.SaveChanges();
        }
        #region Education
        public void AddOrEditEducation(string username, int? educationID, Education.DegreeLevels degreeLevel, int fieldOfEducationID)
        {
            Expert expert = dbContext.Experts.Include(e => e.Person.Educations).FirstOrDefault(e => e.User.UserName == username);
            if (expert == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            Education education = null;
            if (educationID.HasValue)
            {
                education = expert.Person.Educations.FirstOrDefault(e => e.Id == educationID.Value);
                if (education == null)
                    throw new Model.Exception.NotFoundException("مدرک", "");
                if (education.IsAuthorized)
                    throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "این مدرک قبلاً تایید شده است");
            }
            else
            {
                education = new Education()
                {
                    Person_ID = expert.Person_ID,
                };
                dbContext.Educations.Add(education);
            }
            education.DegreeLevel = degreeLevel;
            if (!dbContext.FieldOfEducations.Any(fe => fe.Id == fieldOfEducationID))
                throw new Model.Exception.NotFoundException("رشته", "");
            education.FieldOfEducation_ID = fieldOfEducationID;
            dbContext.SaveChanges();
        }
        public void DeleteEducation(string username, int educationID)
        {
            Expert expert = dbContext.Experts.Include(e => e.Person.Educations).FirstOrDefault(e => e.User.UserName == username);
            if (expert == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            Education education = expert.Person.Educations.FirstOrDefault(e => e.Id == educationID);
            if (education == null)
                throw new Model.Exception.NotFoundException("مدرک", "");
            if (education.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "این مدرک قبلاً تایید شده است");
            dbContext.Educations.Remove(education);
            dbContext.SaveChanges();
        }
        #endregion
        #region Safety Certification
        public List<PersonEducationOverview> GetAllEducations(string username)
        {
            return dbContext.Educations.Where(ed => ed.Person.Experts.Any(e => e.User.UserName == username)).Select(e => new PersonEducationOverview
            {
                Id = e.Id,
                DegreeLevel = e.DegreeLevel,
                FieldOfEducation = e.FieldOfEducation.Name,
                ScanId = e.ScanFile_ID,
                //IsEditable = e.IsEditable,
            }).ToList();
        }
        public PersonEducation GetEducation(string username, int educationId)
        {
            var education = dbContext.Educations.Where(ed => ed.Id == educationId && ed.Person.Experts.Any(e => e.User.UserName == username)).Select(e => new PersonEducation
            {
                Id = e.Id,
                DegreeLevel = e.DegreeLevel,
                FieldOfEducationId = e.FieldOfEducation_ID,
                //IsEditable = e.IsEditable,
            }).FirstOrDefault();
            if (education == null)
                throw new Model.Exception.NotFoundException("مدرک", "");
            return education;
        }
        public List<PersonCertificationOverview> GetAllSafetyCertifications(string username)
        {
            return dbContext.SafetyCertifications.Where(c => c.Person.Experts.Any(e => e.User.UserName == username)).Select(c => new PersonCertificationOverview
            {
                Id = c.Id,
                CertificationScanId = c.CertificationScan_ID,
                Description = c.Description,
                IssuerName = c.IssuerName,
                DateTaken = c.DateTaken,
                CourseName = c.FieldOfCertification_ID.HasValue ? c.FieldOfCertification.Name : c.OtherCourseName,
                DurationInHours = c.FieldOfCertification_ID.HasValue ? c.FieldOfCertification.DurationInHours : c.OtherDurationInHours,
                //IsEditable = e.IsEditable,
            }).ToList();
        }
        public PersonCertification GetSafetyCertification(string username, int certificationId)
        {
            var certification = dbContext.SafetyCertifications.Where(c => c.Id == certificationId && c.Person.Experts.Any(e => e.User.UserName == username)).Select(c => new PersonCertification
            {
                Id = c.Id,
                Description = c.Description,
                FieldOfCertificationId = c.FieldOfCertification_ID,
                IssuerName = c.IssuerName,
                OtherCourseName = c.OtherCourseName,
                OtherDurationInHours = c.OtherDurationInHours,
                DateTaken = c.DateTaken,
                //IsEditable = e.IsEditable,
            }).FirstOrDefault();
            if (certification == null)
                throw new Model.Exception.NotFoundException("دوره آموزشی", "");
            return certification;
        }
        public List<PersonWorkExperienceOverview> GetAllWorkExperiences(string username)
        {
            return dbContext.WorkExperiences.Where(we => we.Person.Experts.Any(e => e.User.UserName == username)).Select(we => new PersonWorkExperienceOverview
            {
                Id = we.Id,
                DateFrom = we.DateFrom,
                DateTo = we.DateTo,
                Description = we.Description,
                EmployerName = we.EmployerName,
                WorkshopName = we.WorkshopName,
                //IsEditable = e.IsEditable,
            }).ToList();
        }
        public PersonWorkExperience GetWorkExperience(string username, int experienceId)
        {
            var experience = dbContext.WorkExperiences.Where(we => we.Id == experienceId && we.Person.Experts.Any(e => e.User.UserName == username)).Select(we => new PersonWorkExperience
            {
                Id = we.Id,
                DateFrom = we.DateFrom,
                DateTo = we.DateTo,
                Description = we.Description,
                EmployerName = we.EmployerName,
                WorkshopName = we.WorkshopName,
                //IsEditable = e.IsEditable,
            }).FirstOrDefault();
            if (experience == null)
                throw new Model.Exception.NotFoundException("سابقه کاری", "");
            return experience;
        }
        public void AddOrEditSafetyCertification(string username, int? certificationID, int? fieldOfCertificationID, DateTime dateTaken, string description, string issuerName, string otherCourseName, double otherDurationInHours)
        {
            validationBusiness.ValidateDateTaken(dateTaken);
            validationBusiness.ValidateIssuerName(issuerName);
            Expert expert = dbContext.Experts.Include(e => e.Person.SafetyCertifications).FirstOrDefault(e => e.User.UserName == username);
            if (expert == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            SafetyCertification certification = null;
            if (certificationID.HasValue)
            {
                certification = expert.Person.SafetyCertifications.FirstOrDefault(c => c.Id == certificationID.Value);
                if (certification == null)
                    throw new Model.Exception.NotFoundException("مدرک", "");
                if (certification.IsAuthorized)
                    throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "این مدرک قبلاً تایید شده است");
            }
            else
            {
                certification = new SafetyCertification()
                {
                    Person_ID = expert.Person_ID,
                };
                dbContext.SafetyCertifications.Add(certification);
            }
            certification.DateTaken = dateTaken;
            certification.Description = description;
            certification.IssuerName = issuerName;
            if (fieldOfCertificationID.HasValue)
            {
                if (!dbContext.FieldOfCertifications.Any(fc => fc.Id == fieldOfCertificationID.Value))
                    throw new Model.Exception.NotFoundException("رشته", "");
                certification.FieldOfCertification_ID = fieldOfCertificationID.Value;
            }
            else
            {
                validationBusiness.ValidateCourseName(otherCourseName);
                validationBusiness.ValidateDurationInHours(otherDurationInHours);
                certification.OtherDurationInHours = otherDurationInHours;
                certification.OtherCourseName = otherCourseName;
            }
            dbContext.SaveChanges();
        }
        public void DeleteCertification(string username, int certificationID)
        {
            Expert expert = dbContext.Experts.Include(e => e.Person.SafetyCertifications).FirstOrDefault(e => e.User.UserName == username);
            if (expert == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            SafetyCertification certification = expert.Person.SafetyCertifications.FirstOrDefault(e => e.Id == certificationID);
            if (certification == null)
                throw new Model.Exception.NotFoundException("مدرک", "");
            if (certification.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات در این مدرک", "این مدرک قبلاً تایید شده است");
            dbContext.SafetyCertifications.Remove(certification);
            dbContext.SaveChanges();
        }
        #endregion
        #region Work Experience
        public void AddOrEditWorkExperience(string username, int? experienceID, DateTime dateFrom, DateTime dateTo, string workshopName, string employerName, string description)
        {
            Expert expert = dbContext.Experts.Include(e => e.Person.WorkExperiences).FirstOrDefault(e => e.User.UserName == username);
            if (expert == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            validationBusiness.ValidateDateFromAndDateTo(dateFrom, dateTo);
            validationBusiness.ValidateFullName(employerName);
            validationBusiness.ValidateUnitName(workshopName);
            WorkExperience experience = null;
            if (experienceID.HasValue)
            {
                experience = expert.Person.WorkExperiences.FirstOrDefault(e => e.Id == experienceID.Value);
                if (experience == null)
                    throw new Model.Exception.NotFoundException("مدرک", "");
                if (experience.IsAuthorized)
                    throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "این مدرک قبلاً تایید شده است");
            }
            else
            {
                experience = new WorkExperience()
                {
                    Person_ID = expert.Person_ID,
                };
                dbContext.WorkExperiences.Add(experience);
            }
            experience.DateFrom = dateFrom;
            experience.DateTo = dateTo;
            experience.Description = description;
            experience.EmployerName = employerName;
            experience.WorkshopName = workshopName;
            dbContext.SaveChanges();
        }
        public void DeleteWorkExperience(string username, int experienceID)
        {
            Expert expert = dbContext.Experts.Include(e => e.Person.WorkExperiences).FirstOrDefault(e => e.User.UserName == username);
            if (expert == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            WorkExperience experience = expert.Person.WorkExperiences.FirstOrDefault(e => e.Id == experienceID);
            if (experience == null)
                throw new Model.Exception.NotFoundException("مدرک", "");
            if (experience.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "این مدرک قبلاً تایید شده است");
            dbContext.WorkExperiences.Remove(experience);
            dbContext.SaveChanges();
        }
        #endregion
        #region Scan
        public PersonPictures GetPictures(string username)
        {
            var personPicture = dbContext.Experts.Where(e => e.User.UserName == username).Select(e => new PersonPictures
            {
                PhotoScanId = e.Person.PhotoScan_ID,
                InsuranceScanIds = e.Person.InsuranceRecordScans.Select(irs => irs.Id),
                NationalCardScanIds = e.Person.NationalCardScans.Select(ncs => ncs.Id),
                ShenasnameScanIds = e.Person.ShenasnameScans.Select(ss => ss.Id)
            }).FirstOrDefault();
            if (personPicture == null)
                throw new Model.Exception.NotFoundException("تصاویر", "");
            return personPicture;
        }
        public Attachment GetImage(string username, int attachmentId, int height = 0, int width = 0)
        {
            Attachment attachment = dbContext.Attachments.Where(a => a.Id == attachmentId).Select(a => new Attachment()
            {
                FileData = a.FileData,
                Filename = a.Filename,
            }).FirstOrDefault();
            if (attachment == null)
                throw new Model.Exception.NotFoundException("تصویر", "کد اشتباه وارد شده است");
            return systemBusiness.ResizeImage(attachment, height, width);
        }
        public void ChangePhotoScan(string username, string filename, byte[] fileData)
        {
            validationBusiness.ValidatePhoto(fileData);
            Expert expert = dbContext.Experts.Include(e => e.Person.PhotoScan).FirstOrDefault(e => e.User.UserName == username);
            if (expert == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            if (expert.Person.IsAuthorized)
                throw new Model.Exception.NotAllowedException("تغییر در پروفایل", "پروفایل قبلاً تایید شده است");
            if (expert.Person.PhotoScan != null)
                dbContext.Attachments.Remove(expert.Person.PhotoScan);
            expert.Person.PhotoScan = new Model.Attachment()
            {
                FileData = fileData,
                Filename = filename,
            };
            dbContext.SaveChanges();
        }
        public void RemovePhotoScan(string username)
        {
            Expert expert = dbContext.Experts.Include(e => e.Person.PhotoScan).FirstOrDefault(e => e.User.UserName == username);
            if (expert == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            if (expert.Person.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پروفایل قبلاً تایید شده است");
            if (expert.Person.PhotoScan != null)
                dbContext.Attachments.Remove(expert.Person.PhotoScan);
            dbContext.SaveChanges();
        }
        public void AddShenasnameScan(string username, string filename, byte[] fileData)
        {
            validationBusiness.ValidatePhoto(fileData);
            Expert expert = dbContext.Experts.Include(e => e.Person.ShenasnameScans).FirstOrDefault(e => e.User.UserName == username);
            if (expert == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            if (expert.Person.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پروفایل قبلاً تایید شده است");
            expert.Person.ShenasnameScans.Add(new Model.Attachment()
            {
                FileData = fileData,
                Filename = filename,
            });
            dbContext.SaveChanges();
        }
        public void RemoveShenasnameScan(string username, int shenasnameID)
        {
            Expert expert = dbContext.Experts.Include(e => e.Person.ShenasnameScans).FirstOrDefault(e => e.User.UserName == username);
            if (expert == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            if (expert.Person.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پروفایل قبلاً تایید شده است");
            var shenasnameScan = expert.Person.ShenasnameScans.Where(sh => sh.Id == shenasnameID).FirstOrDefault();
            if (shenasnameScan == null)
                throw new Model.Exception.NotFoundException("تصویر شناسنامه", "");
            dbContext.Attachments.Remove(shenasnameScan);
            dbContext.SaveChanges();
        }
        public void AddNationalCardScan(string username, string filename, byte[] fileData)
        {
            validationBusiness.ValidatePhoto(fileData);
            Expert expert = dbContext.Experts.Include(e => e.Person.NationalCardScans).FirstOrDefault(e => e.User.UserName == username);
            if (expert == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            if (expert.Person.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پروفایل قبلاً تایید شده است");
            expert.Person.NationalCardScans.Add(new Model.Attachment()
            {
                FileData = fileData,
                Filename = filename,
            });
            dbContext.SaveChanges();
        }
        public void RemoveNationalCardScan(string username, int nationalCardID)
        {
            Expert expert = dbContext.Experts.Include(e => e.Person.NationalCardScans).FirstOrDefault(e => e.User.UserName == username);
            if (expert == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            if (expert.Person.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پروفایل قبلاً تایید شده است");
            var nationalCardScan = expert.Person.NationalCardScans.Where(ns => ns.Id == nationalCardID).FirstOrDefault();
            if (nationalCardScan == null)
                throw new Model.Exception.NotFoundException("تصویر کارت ملی", "");
            dbContext.Attachments.Remove(nationalCardScan);
            dbContext.SaveChanges();
        }
        public void AddInsuranceScan(string username, string filename, byte[] fileData)
        {
            validationBusiness.ValidatePhoto(fileData);
            Expert expert = dbContext.Experts.Include(e => e.Person.InsuranceRecordScans).FirstOrDefault(e => e.User.UserName == username);
            if (expert == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            if (expert.Person.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پروفایل قبلاً تایید شده است");
            expert.Person.InsuranceRecordScans.Add(new Model.Attachment()
            {
                FileData = fileData,
                Filename = filename,
            });
            dbContext.SaveChanges();
        }
        public void RemoveInsuranceScan(string username, int insuranceScanID)
        {
            Expert expert = dbContext.Experts.Include(e => e.Person.InsuranceRecordScans).FirstOrDefault(e => e.User.UserName == username);
            if (expert == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            if (expert.Person.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پروفایل قبلاً تایید شده است");
            var insuranceScan = expert.Person.InsuranceRecordScans.Where(ins => ins.Id == insuranceScanID).FirstOrDefault();
            if (insuranceScan == null)
                throw new Model.Exception.NotFoundException("تصویر بیمه", "");
            dbContext.Attachments.Remove(insuranceScan);
            dbContext.SaveChanges();
        }
        public void ChangeEducationScan(string username, int educationID, string filename, byte[] fileData)
        {
            validationBusiness.ValidatePhoto(fileData);
            Expert expert = dbContext.Experts.Include(e => e.Person.Educations.Select(ed => ed.ScanFile)).FirstOrDefault(e => e.User.UserName == username);
            if (expert == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            Education education = expert.Person.Educations.FirstOrDefault(e => e.Id == educationID);
            if (education == null)
                throw new Model.Exception.NotFoundException("مدرک", "");
            if (education.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "این مدرک قبلاً تایید شده است");
            if (education.ScanFile != null)
                dbContext.Attachments.Remove(education.ScanFile);
            education.ScanFile = new Model.Attachment()
            {
                FileData = fileData,
                Filename = filename,
            };
            dbContext.SaveChanges();
        }
        public void RemoveEducationScan(string username, int educationID)
        {
            Expert expert = dbContext.Experts.Include(e => e.Person.Educations.Select(ed => ed.ScanFile)).FirstOrDefault(e => e.User.UserName == username);
            if (expert == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            Education education = expert.Person.Educations.FirstOrDefault(e => e.Id == educationID);
            if (education == null)
                throw new Model.Exception.NotFoundException("مدرک", "");
            if (education.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "این مدرک قبلاً تایید شده است");
            if (education.ScanFile != null)
                dbContext.Attachments.Remove(education.ScanFile);
            dbContext.SaveChanges();
        }
        public void ChangeCertificationScan(string username, int certificationID, string filename, byte[] fileData)
        {
            validationBusiness.ValidatePhoto(fileData);
            Expert expert = dbContext.Experts.Include(e => e.Person.SafetyCertifications.Select(sc => sc.CertificationScan)).FirstOrDefault(e => e.User.UserName == username);
            if (expert == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            SafetyCertification certification = expert.Person.SafetyCertifications.FirstOrDefault(sc => sc.Id == certificationID);
            if (certification == null)
                throw new Model.Exception.NotFoundException("مدرک", "");
            if (certification.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "این مدرک قبلاً تایید شده است");
            if (certification.CertificationScan != null)
                dbContext.Attachments.Remove(certification.CertificationScan);
            certification.CertificationScan = new Model.Attachment()
            {
                FileData = fileData,
                Filename = filename,
            };
            dbContext.SaveChanges();
        }
        public void RemoveCertificationScan(string username, int certificationID)
        {
            Expert expert = dbContext.Experts.Include(e => e.Person.SafetyCertifications.Select(sc => sc.CertificationScan)).FirstOrDefault(e => e.User.UserName == username);
            if (expert == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            if (!CanModify(username))
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            SafetyCertification certification = expert.Person.SafetyCertifications.FirstOrDefault(sc => sc.Id == certificationID);
            if (certification == null)
                throw new Model.Exception.NotFoundException("مدرک", "");
            if (certification.IsAuthorized)
                throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "این مدرک قبلاً تایید شده است");
            if (certification.CertificationScan != null)
                dbContext.Attachments.Remove(certification.CertificationScan);
            dbContext.SaveChanges();
        }
        public DashboardData GetDashboardData(string username)
        {
            var data = dbContext.Experts.Where(e => e.User.UserName == username).Select(e => new DashboardData
            {
                FullName = e.Person.FirstName + " " + e.Person.LastName,
                PhotoId = e.Person.PhotoScan_ID,
            }).FirstOrDefault();
            if (data == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            return data;
        }
        public List<ExpertLicense> GetMyLicenses(string username)
        {
            return dbContext.Licenses.Where(l => l.Expert.User.UserName == username).Select(l => new ExpertLicense()
            {
                UnitName = l.Company.UnitName,
                CompanyCode = l.Company.CompanyCode,
                CEOFirstName = l.Company.CEOs.Select(ceo => ceo.Person.FirstName).FirstOrDefault(),
                CEOLastName = l.Company.CEOs.Select(ceo => ceo.Person.LastName).FirstOrDefault(),
                ContractScanIds = l.ContractScans.Select(cs => cs.Id),
                CurrentFlowType = l.Flows.Where(f => f.IsCurrent).Select(f => f.Type).FirstOrDefault(),
                Id = l.Id,
                StartDate = l.StartDate,
                EndDate = l.EndDate,
                DateCreated = l.DateCreated,
                Username = l.User_ID.HasValue ? l.User.UserName : null,
            }).ToList();
        }
        public List<int> GetTemporaryObjectionAttachments(string username, int licenseId)
        {
            return dbContext.Licenses.Where(l => l.Expert.User.UserName == username && l.Id == licenseId).Select(l => l.ObjectionAttachments.Select(o => o.Id)).SelectMany(id => id).ToList();
        }
        public List<ExpertLicense.Flow> GetLicenseFlows(string username, int licenseId)
        {
            return dbContext.LicenseFlows.Where(lf => lf.License.Expert.User.UserName == username && lf.License_ID == licenseId)
                                         .OrderBy(lf => lf.DateCreated)
                                         .Select(lf => new ExpertLicense.Flow
                                         {
                                             AttachmentIds = lf.Attachments.Select(a => a.Id),
                                             DateCreated = lf.DateCreated,
                                             Description = lf.Description,
                                             Type = lf.Type,
                                         }).ToList();
        }
        public void AddContractScan(string username, int licenseId, string filename, byte[] fileData)
        {
            validationBusiness.ValidatePhoto(fileData);
            Expert expert = dbContext.Experts.Include(e => e.Licenses.Select(l => l.ContractScans)).FirstOrDefault(e => e.User.UserName == username);
            if (expert == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            //doesnt matter if in proccess
            License license = expert.Licenses.Where(l => l.Id == licenseId && l.Flows.Where(f => f.IsCurrent).All(f => f.Type == LicenseFlow.FlowType.WithExpert || f.Type == LicenseFlow.FlowType.RejectedOnce || f.Type == LicenseFlow.FlowType.RejectedTwice)).FirstOrDefault();
            if (license == null)
                throw new Model.Exception.NotFoundException("پروانه", "");
            license.ContractScans.Add(new Model.Attachment()
            {
                FileData = fileData,
                Filename = filename,
            });
            dbContext.SaveChanges();
        }
        public void RemoveContractScan(string username, int contractScanId)
        {
            Expert expert = dbContext.Experts.Include(e => e.Licenses.Select(l => l.ContractScans)).FirstOrDefault(e => e.User.UserName == username);
            if (expert == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            //doesnt matter if in proccess
            var contractScan = expert.Licenses.Where(l => l.Flows.Where(f => f.IsCurrent).All(f => f.Type == LicenseFlow.FlowType.WithExpert || f.Type == LicenseFlow.FlowType.RejectedOnce || f.Type == LicenseFlow.FlowType.RejectedTwice)).Select(l => l.ContractScans).SelectMany(cs => cs).Where(cs => cs.Id == contractScanId).FirstOrDefault();
            if (contractScan == null)
                throw new Model.Exception.NotFoundException("تصویر", "");
            dbContext.Attachments.Remove(contractScan);
            dbContext.SaveChanges();
        }
        public void SendLicense(string username, int licenseId)
        {
            License license = dbContext.Licenses.Include(l => l.ContractScans)
                                                .Include(l => l.Company.Location.County)
                                                .Include(l => l.Flows)
                                                .Where(l => l.Expert.User.UserName == username && l.Id == licenseId && l.Flows.Where(f => f.IsCurrent).All(f => f.Type == LicenseFlow.FlowType.WithExpert))
                                                .FirstOrDefault();
            if (license == null)
                throw new Model.Exception.NotFoundException("پروانه", "");
            validationBusiness.ValidateCompletedExpertProfile(username);
            validationBusiness.ValidateCompletedCompanyData(license.Company_ID);
            if (!license.ContractScans.Any())
                throw new Model.Exception.NotAllowedException("ارسال", "تصویر قرارداد وارد نشده است");
            var newFlow = new LicenseFlow(username);
            if (dbContext.Associations.Any(a => a.County_ID == license.Company.Location.County_ID))
                newFlow.Type = LicenseFlow.FlowType.WithAssociation;
            else if (dbContext.Clubs.Any(c => c.Province_ID == license.Company.Location.County.Province_ID))
                newFlow.Type = LicenseFlow.FlowType.WithClub;
            else if (dbContext.CountyInspectors.Any(c => c.County_ID == license.Company.Location.County_ID))
                newFlow.Type = LicenseFlow.FlowType.WithCountyInspector;
            else if (dbContext.ProvinceInspectors.Any(p => p.Province_ID == license.Company.Location.County.Province_ID))
                newFlow.Type = LicenseFlow.FlowType.WithProvinceInspector;
            else
                throw new Model.Exception.InternalException("هیج اداره بازرسی برای استان پیدا نگردید");
            license.Flows.ForEach(f => f.IsCurrent = false);
            license.Flows.Add(newFlow);
            dbContext.SaveChanges();
            #endregion
        }
        public ProfileProgress CheckProfileProgress(string username)
        {
            return new ProfileProgress()
            {
                Messages = validationBusiness.GetExpertIncompletedData(username),
            };
        }
        public void AddObjectionAttachment(string username, int licenseId, string filename, byte[] fileData)
        {
            validationBusiness.ValidatePhoto(fileData);
            Expert expert = dbContext.Experts.Include(e => e.Licenses.Select(l => l.ObjectionAttachments)).FirstOrDefault(e => e.User.UserName == username);
            if (expert == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            //if (!CanModify(username))
            //    throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            if (!CanObjectify(username, licenseId))
                throw new Model.Exception.NotAllowedException("ثبت اعتراض", "پرونده در حالت اعتراض نمی باشد");
            var license = expert.Licenses.First(l => l.Id == licenseId);
            license.ObjectionAttachments.Add(new Model.Attachment()
            {
                FileData = fileData,
                Filename = filename,
            });
            dbContext.SaveChanges();
        }
        public void RemoveObjectionAttachment(string username, int objectionScanID)
        {
            Expert expert = dbContext.Experts.Include(e => e.Licenses.Select(l => l.ObjectionAttachments)).FirstOrDefault(e => e.User.UserName == username);
            if (expert == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            //if (!CanModify(username))
            //    throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            var objectionScan = expert.Licenses.Select(l => l.ObjectionAttachments).SelectMany(o => o).FirstOrDefault(o => o.Id == objectionScanID);
            if (objectionScan == null)
                throw new Model.Exception.NotFoundException("تصویر ضمیمه", "");
            if (!CanObjectify(username, objectionScan.Objection_ID.Value))
                throw new Model.Exception.NotAllowedException("ثبت اعتراض", "پرونده در حالت اعتراض نمی باشد");
            dbContext.Attachments.Remove(objectionScan);
            dbContext.SaveChanges();
        }
        public void Objectify(string username, int licenseId, string description)
        {
            Expert expert = dbContext.Experts.Include(e => e.Licenses.Select(l => l.Company.Location.County.Province.ProvinceManagers)).Include(e => e.Licenses.Select(l => l.Flows)).FirstOrDefault(e => e.User.UserName == username);
            if (expert == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            //if (!CanModify(username))
            //    throw new Model.Exception.NotAllowedException("ایجاد تغییرات", "پرونده در حال بررسی می باشد");
            if (!CanObjectify(username, licenseId))
                throw new Model.Exception.NotAllowedException("ثبت اعتراض", "پرونده در حالت اعتراض نمی باشد");
            var license = expert.Licenses.First(l => l.Id == licenseId);
            var currentFlow = license.Flows.First(lf => lf.IsCurrent);
            license.Flows.ForEach(lf => lf.IsCurrent = false);
            currentFlow.Description = description;
            license.ObjectionAttachments.ForEach(o =>
            {
                o.Objection_ID = null;
                o.LicenseFlow_ID = currentFlow.Id;
            });
            var newFlow = new LicenseFlow(username) { IsCurrent = true };
            license.Flows.Add(newFlow);
            switch (currentFlow.Type)
            {
                case LicenseFlow.FlowType.RejectedOnce:
                    if (!license.Company.Location.County.Province.ProvinceManagers.Any())
                        throw new Model.Exception.InternalException("مدیریت بازرسی استان پیدا نگردید");
                    newFlow.Type = LicenseFlow.FlowType.WithProvinceManager;
                    break;
                case LicenseFlow.FlowType.RejectedTwice:
                    if (!dbContext.InspectionOffices.Any())
                        throw new Model.Exception.InternalException("اداره کل بازرسی کشور پیدا نگردید");
                    newFlow.Type = LicenseFlow.FlowType.WithInspectionOffice;
                    break;
            }
            dbContext.SaveChanges();
        }
        public void CreateLicenseUser(string username, int licenseId, string newUsername, string password)
        {
            validationBusiness.ValidateUserName(newUsername);
            validationBusiness.ValidatePassword(password);
            Expert expert = dbContext.Experts.Include(e => e.Licenses.Select(l => l.User)).Include(e => e.Licenses.Select(l => l.Flows)).FirstOrDefault(e => e.User.UserName == username);
            if (expert == null)
                throw new Model.Exception.NotFoundException("کاربر", "");
            var license = expert.Licenses.FirstOrDefault(l => l.Id == licenseId);
            if (license == null)
                throw new Model.Exception.NotFoundException("پروانه", "");
            if (license.Flows.First(lf => lf.IsCurrent).Type != LicenseFlow.FlowType.Authorized)
                throw new Model.Exception.NotAllowedException("ایجاد نام کاربری","پروانه تایید نشده است");
            if (license.User_ID.HasValue)
                throw new Model.Exception.NotAllowedException("ایجاد نام کاربری", "نام کاربری قبلاً برای این پروانه ایجاد شده است");
            license.User = new User()
            {
                HashPassword = systemBusiness.TextToMD5(password),
                UserName = newUsername,
                Type = User.UserType.License,
            };
            dbContext.SaveChanges();
        }
    }
}