﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Isaap.Core.DataAccess;
using System.Security.Cryptography;
using Isaap.Core.Model.VM;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;

namespace Isaap.Core.Business
{
    public class SystemBusiness : BaseBusiness
    {
        public SystemBusiness(IsaapContext dbContext) : base(dbContext)
        {

        }
        public string TextToMD5(string text)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(text + "HosseinIsTheBest");
            byte[] hashBytes = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
                sb.Append(hashBytes[i].ToString("X2"));
            return sb.ToString();
        }
        public List<NameID> GetAllProvinces()
        {
            return dbContext.Provinces.Select(p => new NameID
            {
                ID = p.Id,
                Name = p.Name
            }).ToList();
        }
        public List<NameID> GetAllCounties(int provinceID)
        {
            return dbContext.Counties.Where(c => c.Province_ID == provinceID).Select(c => new NameID
            {
                ID = c.Id,
                Name = c.Name
            }).ToList();
        }
        public List<NameID> GetAllFieldOfEducations()
        {
            return dbContext.FieldOfEducations.Select(fe => new NameID
            {
                ID = fe.Id,
                Name = fe.Name,
            }).ToList();
        }
        public Attachment ResizeImage(Attachment attachment, int height = 0, int width = 0)
        {
            Bitmap image = null;
            MemoryStream InputBinary = new MemoryStream(attachment.FileData, false);
            try
            {
                image = new Bitmap(InputBinary);
            }
            catch (Exception ex)
            {
                throw new Model.Exception.InternalException("تبدیل به تصویر: " + ex.Message);
            }
            MemoryStream OutputBinary = new MemoryStream();
            //resize
            int newHeight = image.Height;
            int newWidth = image.Width;
            if (height > 0 && width <= 0)
            {
                newHeight = height;
                newWidth = (int)((float)height / image.Height * image.Width);
            }
            if (height <= 0 && width > 0)
            {
                newWidth = width;
                newHeight = (int)((float)width / image.Width * image.Height);
            }
            if (height > 0 && width > 0)
            {
                float fillRate = (float)height / width;
                float originalRate = (float)image.Height / image.Width;
                if (fillRate > originalRate)
                {
                    newHeight = height;
                    newWidth = (int)((float)height / image.Height * image.Width);
                }
                else
                {
                    newWidth = width;
                    newHeight = (int)((float)width / image.Width * image.Height);
                }
            }
            image = new Bitmap(image, newWidth, newHeight);
            //crop and fill
            if (height != -1 && width != -1)
            {
                Rectangle cropArea = new Rectangle()
                {
                    X = (newWidth - width) / 2,
                    Y = (newHeight - height) / 2,
                    Width = width,
                    Height = height,
                };
                image = image.Clone(cropArea, image.PixelFormat);
            }
            image.Save(OutputBinary, ImageFormat.Jpeg);
            return new Attachment()
            {
                Filename = attachment.Filename,
                FileData = OutputBinary.ToArray()
            };
        }
        public List<NameID> GetAllFieldOfCertifications()
        {
            return dbContext.FieldOfCertifications.Select(fc => new NameID
            {
                ID = fc.Id,
                Name = fc.Name,
            }).ToList();
        }
        public List<NameID> GetAllFieldOfActivities()
        {
            return dbContext.FieldOfActivities.Select(fa => new NameID()
            {
                ID = fa.Id,
                Name = fa.Name,
            }).ToList();
        }
        public List<NameID> GetAllProductionTypes()
        {
            return dbContext.ProductionTypes.Select(pt => new NameID()
            {
                ID = pt.Id,
                Name = pt.Name,
            }).ToList();
        }

    }
}
