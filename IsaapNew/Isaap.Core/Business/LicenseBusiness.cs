﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Isaap.Core.DataAccess;
using Isaap.Core.Model.VM;

namespace Isaap.Core.Business
{
    public class LicenseBusiness : UserBusiness
    {
        public LicenseBusiness(IsaapContext dbContext) : base(dbContext)
        {
        }
        public LicenseProfileOverview GetMyProfileData(string username)
        {
            var result = dbContext.Licenses.Where(l => l.User.UserName == username)
                                            .Select(l => new LicenseProfileOverview()
                                            {
                                                Expert = new LicenseProfileOverview.ExpertData()
                                                {
                                                    BirthDate = l.Expert.Person.BirthDate.Value,
                                                    Email = l.Expert.User.Email,
                                                    FatherName = l.Expert.Person.FatherName,
                                                    FirstName = l.Expert.Person.FirstName,
                                                    Gender = l.Expert.Person.Gender.Value,
                                                    HomeAddress = l.Expert.Person.HomeLocation.Address,
                                                    HomeCounty = l.Expert.Person.HomeLocation.County.Name,
                                                    HomePhone = l.Expert.Person.HomeLocation.Phone,
                                                    HomePostalCode = l.Expert.Person.HomeLocation.PostalCode,
                                                    HomeProvince = l.Expert.Person.HomeLocation.County.Province.Name,
                                                    LastName = l.Expert.Person.LastName,
                                                    MaritalStatus = l.Expert.Person.MaritalStatus.Value,
                                                    MilitaryStatus = l.Expert.Person.MilitaryStatus.Value,
                                                    Mobile = l.Expert.User.Mobile,
                                                    NationalCode = l.Expert.Person.NationalCode,
                                                    PlaceOfBirth = l.Expert.Person.PlaceOfBirth,
                                                    ReasonOfExemption = l.Expert.Person.ReasonOfExemption,
                                                    ShenasnameNo = l.Expert.Person.ShenasnameNo,
                                                    Username = l.Expert.User.UserName,
                                                },
                                                License = new LicenseProfileOverview.LicenseData()
                                                {
                                                    Company = new LicenseProfileOverview.LicenseData.CompanyData()
                                                    {
                                                        Address = l.Company.Location.Address,
                                                        CEOFirstName = l.Company.CEOs.Select(ceo => ceo.Person.FirstName).FirstOrDefault(),
                                                        CEOLastName = l.Company.CEOs.Select(ceo => ceo.Person.LastName).FirstOrDefault(),
                                                        CompanyCode = l.Company.CompanyCode,
                                                        County = l.Company.Location.County.Name,
                                                        CountyId = l.Company.Location.County_ID.Value,
                                                        EveningWorkersNo = l.Company.EveningWorkersNo,
                                                        FemaleWorkersNo = l.Company.FemaleWorkersNo,
                                                        FieldOfActivity = l.Company.FieldOfActivity.Name,
                                                        FieldOfActivityDangerLevel = l.Company.FieldOfActivity.DangerLevel,
                                                        MaleWorkersNo = l.Company.MaleWorkersNo,
                                                        MorningWorkersNo = l.Company.MorningWorkersNo,
                                                        NightWorkersNo = l.Company.NightWorkersNo,
                                                        Phone = l.Company.Location.Phone,
                                                        PostalCode = l.Company.Location.PostalCode,
                                                        ProductionType = l.Company.ProductionType.Name,
                                                        Province = l.Company.Location.County.Province.Name,
                                                        ProvinceId = l.Company.Location.County.Province_ID,
                                                        SafetyWorkersNo = l.Company.SafetyWorkersNo,
                                                        SocialSecurityCode = l.Company.SocialSecurityCode,
                                                        UnitName = l.Company.UnitName,
                                                        WorkshopSize = l.Company.WorkshopSize,
                                                    },
                                                    LicenseId = l.Id,
                                                    DateCreated = l.DateCreated,
                                                    EndDate = l.EndDate,
                                                    StartDate = l.StartDate,
                                                    Username = l.User_ID.HasValue ? l.User.UserName : null,
                                                },
                                            }).FirstOrDefault();
            if (result == null)
                throw new Model.Exception.NotFoundException("پروانه", "");
            return result;
        }
    }
}
