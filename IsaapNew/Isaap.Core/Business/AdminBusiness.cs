﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Isaap.Core.DataAccess;

namespace Isaap.Core.Business
{
    public class AdminBusiness : UserBusiness
    {
        public AdminBusiness(IsaapContext dbContext) : base(dbContext)
        {
        }
    }
}
