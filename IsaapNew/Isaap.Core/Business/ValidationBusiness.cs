﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Isaap.Core.DataAccess;
using System.Text.RegularExpressions;
using System.Configuration;
using System.IO;
using System.Drawing;
using Isaap.Core.Model.Person;
using Isaap.Core.Model.User;
using System.Data.Entity;

namespace Isaap.Core.Business
{
    public class ValidationBusiness : BaseBusiness
    {
        public ValidationBusiness(IsaapContext dbContext) : base(dbContext)
        {
        }
        public List<string> GetCompanyIncompletedData(int companyId)
        {
            var company = dbContext.Companies.Where(c => c.Id == companyId);
            if (!company.Any())
                throw new Model.Exception.NotFoundException("واحد/کارگاه", "");
            if (!company.Any(c => c.CEOs.Count() == 1))
                throw new Model.Exception.InternalException("واحد/کارگاه بدون کارفرما می باشد");
            List<string> result = new List<string>();
            if (!company.Any(c => c.CEOs.All(ceo => !string.IsNullOrEmpty(ceo.Person.FirstName))))
                result.Add("نام مدیر عامل وارد نشده است");
            if (!company.Any(c => c.CEOs.All(ceo => !string.IsNullOrEmpty(ceo.Person.LastName))))
                result.Add("نام خانوادگی مدیر عامل وارد نشده است");
            if (!company.Any(c => c.CEOs.All(ceo => ceo.Person.Gender.HasValue)))
                result.Add("جنسیت مدیر عامل وارد نشده است");
            if (!company.Any(c => !string.IsNullOrEmpty(c.UnitName)))
                result.Add("نام کارگاه وارد نشده است");
            if (!company.Any(c => c.ProductionType_ID.HasValue))
                result.Add("نوع تولید وارد نشده است");
            if (!company.Any(c => c.FieldOfActivity_ID.HasValue))
                result.Add("موضوع فعالیت وارد نشده است");
            if (!company.Any(c => (c.FemaleWorkersNo + c.MaleWorkersNo) > 0))
                result.Add("تعداد کارگران بر اساس جنسیت درست وارد نشده است");
            if (!company.Any(c => (c.EveningWorkersNo + c.MorningWorkersNo + c.NightWorkersNo) > 0))
                result.Add("تعداد کارگران بر شیفت کاری درست وارد نشده است");
            if (!company.Any(c => c.WorkshopSize > 0))
                result.Add("وسعت کارگاه وارد نشده است");
            if (!company.Any(c => c.Location.County_ID.HasValue))
                result.Add("شهرستان محل کارگاه وارد نشده است");
            if (!company.Any(c => !string.IsNullOrEmpty(c.Location.Phone)))
                result.Add("تلفن محل کارگاه وارد نشده است");
            if (!company.Any(c => !string.IsNullOrEmpty(c.Location.Address)))
                result.Add("آدرس محل کارگاه وارد نشده است");
            if (!company.Any(c => !string.IsNullOrEmpty(c.Location.PostalCode)))
                result.Add("کدپستی محل کارگاه وارد نشده است");
            if (!company.Any(c => c.TechnicalProtectionCommittee ? c.TPCScans.Any() : true))
                result.Add("تصویر کمیته حفاظت فنی وارد نشده است");
            return result;
        }
        public void ValidateCompletedCompanyData(int companyId)
        {
            var incompleteData = GetCompanyIncompletedData(companyId);
            if (incompleteData.Any())
                throw new Model.Exception.IncompleteException("کارگاه", incompleteData.First());
        }
        public List<string> GetExpertIncompletedData(string username)
        {
            var expert = dbContext.Experts.Where(e => e.User.UserName == username);
            if (!expert.Any())
                throw new Model.Exception.NotFoundException("مسئول ایمنی", "");
            List<string> result = new List<string>();
            if (!expert.Any(e => e.Person.PhotoScan_ID.HasValue))
                result.Add("تصویر شخص وارد نشده است");
            if (!expert.Any(e => e.Person.ShenasnameScans.Any()))
                result.Add("تصویر شناسنامه وارد نشده است");
            if (!expert.Any(e => e.Person.NationalCardScans.Any()))
                result.Add("تصویر کارت ملی وارد نشده است");
            if (!expert.Any(e => !string.IsNullOrEmpty(e.Person.FirstName)))
                result.Add("نام وارد نشده است");
            if (!expert.Any(e => !string.IsNullOrEmpty(e.Person.LastName)))
                result.Add("نام خانوادگی وارد نشده است");
            if (!expert.Any(e => !string.IsNullOrEmpty(e.Person.FatherName)))
                result.Add("نام پدر وارد نشده است");
            if (!expert.Any(e => !string.IsNullOrEmpty(e.Person.ShenasnameNo)))
                result.Add("شناسنامه وارد نشده است");
            if (!expert.Any(e => !string.IsNullOrEmpty(e.Person.NationalCode)))
                result.Add("کد ملی وارد نشده است");
            if (!expert.Any(e => e.Person.BirthDate.HasValue))
                result.Add("تاریخ تولد وارد نشده است");
            if (!expert.Any(e => !string.IsNullOrEmpty(e.Person.PlaceOfBirth)))
                result.Add("محل تولد وارد نشده است");
            if (!expert.Any(e => e.Person.MaritalStatus.HasValue))
                result.Add("وضعیت تاهل وارد نشده است");
            if (!expert.Any(e => e.Person.Gender.HasValue))
                result.Add("جنسیت وارد نشده است");
            if (!expert.Any(e => e.Person.Educations.Any()))
                result.Add("هیچ مدرک تحصیلی مشخص نشده است");
            if (!expert.Any(e => e.Person.Educations.All(ed => ed.ScanFile_ID.HasValue)))
                result.Add("تصویر تمامی مدارک تحصیلی وارد نشده است");
            if (!expert.Any(e => e.Person.SafetyCertifications.All(sc => sc.CertificationScan_ID.HasValue)))
                result.Add("تصویر تمامی مدارک ایمنی وارد نشده است");
            if (!expert.Any(e => !string.IsNullOrEmpty(e.User.Mobile)))
                result.Add("همراه وارد نشده است");
            if (!expert.Any(e => e.Person.HomeLocation != null))
                result.Add("محل سکونت وارد نشده است");
            if (!expert.Any(e => e.Person.HomeLocation.County_ID.HasValue))
                result.Add("شهرستان محل سکونت وارد نشده است");
            if (!expert.Any(e => !string.IsNullOrEmpty(e.Person.HomeLocation.Address)))
                result.Add("آدرس محل سکونت وارد نشده است");
            if (!expert.Any(e => !string.IsNullOrEmpty(e.Person.HomeLocation.PostalCode)))
                result.Add("کدپستی محل سکونت وارد نشده است");
            if (!expert.Any(e => !string.IsNullOrEmpty(e.Person.HomeLocation.Phone)))
                result.Add("تلفن محل سکونت وارد نشده است");
            var certificationsIncludedInEducations = expert.Select(e => e.Person.Educations.Select(ed => ed.FieldOfEducation.IncludedFieldOfCertifications.Select(fc => fc.Id)).SelectMany(fc => fc)).FirstOrDefault();
            var certificationsRecieved = expert.Select(e => e.Person.SafetyCertifications.Where(sc => sc.FieldOfCertification_ID.HasValue).Select(sc => sc.FieldOfCertification_ID.Value)).FirstOrDefault();
            var missingCertifications = dbContext.FieldOfCertifications.Where(fc => fc.IsRequiredForAuthorization && !certificationsRecieved.Contains(fc.Id) && !certificationsIncludedInEducations.Contains(fc.Id)).ToList();
            missingCertifications.ForEach(mc =>
            {
                result.Add("دوره آموزشی \"" + mc.Name + "\" گذرانده نشده است");
            });
            return result;
        }
        public void ValidateCompletedExpertProfile(string username)
        {
            var incompleteData = GetExpertIncompletedData(username);
            if (incompleteData.Any())
                throw new Model.Exception.IncompleteException("مسئول ایمنی", incompleteData.First());
        }
        public void ValidateFirstName(string name)
        {
            if (string.IsNullOrEmpty(name) || !Regex.IsMatch(name, @"^[آ-ی ]{2,30}$"))
                throw new Model.Exception.WrongInputException("نام", "فقط حروف فارسی بین 2 تا 30 حرف وارد شود");
        }
        public void ValidateMilitaryServiceStatus(Person.MilitaryServiceStates status)
        {
            if (status != Person.MilitaryServiceStates.Exempted && status != Person.MilitaryServiceStates.Passed)
                throw new Model.Exception.WrongInputException("خدمت سربازی", "موردی انتخاب نشده است");
        }
        public void ValidateLastName(string familyName)
        {
            if (string.IsNullOrEmpty(familyName) || !Regex.IsMatch(familyName, @"^[آ-ی ]{2,30}$"))
                throw new Model.Exception.WrongInputException("نام خانوادگی", "فقط حروف فارسی بین 2 تا 30 حرف وارد شود");
        }
        public void ValidateFullName(string fullName)
        {
            if (string.IsNullOrEmpty(fullName) || !Regex.IsMatch(fullName, @"^[آ-ی ]{2,100}$"))
                throw new Model.Exception.WrongInputException("نام و نام خانوادگی", "فقط حروف فارسی بین 2 تا 100 حرف وارد شود");
        }
        public void ValidateUserName(string userName)
        {
            if (string.IsNullOrEmpty(userName) || !Regex.IsMatch(userName.ToLower(), @"^[a-z][a-z0-9._]{5,30}$"))
                throw new Model.Exception.WrongInputException("نام کاربری", "فقط حروف انگلیسی، اعداد، _ و نقطه بین 5 تا 30 حرف وارد شود");
            if (dbContext.Users.Where(u => u.UserName.ToLower() == userName.ToLower()).Count() > 0)
                throw new Model.Exception.DuplicatedException("نام کاربری", "قبلاً ثبت شده است");
        }
        public void ValidatePassword(string password)
        {
            if (string.IsNullOrEmpty(password) || password.Length < 6 || password.Length > 50)
                throw new Model.Exception.WrongInputException("گذرواژه", "حتما بین 6 تا 50 حرف وارد شود");
        }
        public void ValidateProvinceName(string name)
        {
            if (string.IsNullOrEmpty(name) || !Regex.IsMatch(name, @"^[آ-ی ]{2,50}$"))
                throw new Model.Exception.WrongInputException("نام استان", "فقط حروف فارسی بین 2 تا 50 حرف وارد شود");
        }
        public void ValidateProvinceCode(string code)
        {
            if (string.IsNullOrEmpty(code) || !Regex.IsMatch(code, @"^[0-9]{2}$"))
                throw new Model.Exception.WrongInputException("کد استان", "فقط یک عدد 2 رقمی وارد شود");
        }
        public void ValidateCountyName(string name)
        {
            if (string.IsNullOrEmpty(name) || !Regex.IsMatch(name, @"^[آ-ی ()]{2,50}$"))
                throw new Model.Exception.WrongInputException("نام شهرستان", "فقط حروف فارسی بین 2 تا 50 حرف وارد شود");
        }
        public void ValidateFieldOfStudyName(string name)
        {
            if (string.IsNullOrEmpty(name) || !Regex.IsMatch(name.ToLower(), @"^[a-zآ-ی ]{10,100}$"))
                throw new Model.Exception.WrongInputException("نام رشته", "فقط حروف فارسی و انگلیسی بین 10 تا 100 حرف وارد شود");
        }
        public void ValidateFieldOfStudyCode(string code)
        {
            if (string.IsNullOrEmpty(code) || !Regex.IsMatch(code, @"^[0-9]{1}$"))
                throw new Model.Exception.WrongInputException("کد رشته", "فقط یک عددی تک رقمی وارد شود");
        }
        public void ValidateFieldOfStudyOrginalityCode(string code)
        {
            if (string.IsNullOrEmpty(code) || !Regex.IsMatch(code, @"^[0-9]{1}$"))
                throw new Model.Exception.WrongInputException("کد اصلی بودن رشته", "فقط یک عددی تک رقمی وارد شود");
        }
        public void ValidateFieldOfActivityName(string name)
        {
            if (string.IsNullOrEmpty(name) || !Regex.IsMatch(name, @"^[آ-ی ]{3,50}$"))
                throw new Model.Exception.WrongInputException("نام فعالیت", "فقط حروف فارسی بین 3 تا 50 حرف وارد شود");
        }
        public void ValidateProductionTypeName(string name)
        {
            if (string.IsNullOrEmpty(name) || !Regex.IsMatch(name, @"^[آ-ی ]{3,50}$"))
                throw new Model.Exception.WrongInputException("نام تولید", "فقط حروف فارسی بین 3 تا 50 حرف وارد شود");
        }
        public void ValidateShenasnameNo(string shenasnameNo)
        {
            if (string.IsNullOrEmpty(shenasnameNo) || !Regex.IsMatch(shenasnameNo, @"^[0-9]{1,10}$"))
                throw new Model.Exception.WrongInputException("شماره شناسنامه", "فقط یک عدد بین 1 تا 10 رقمی وارد شود");
        }
        public void ValidateNationalCode(string melliCode)
        {
            if (string.IsNullOrEmpty(melliCode) || !Regex.IsMatch(melliCode, @"^[0-9]{10}$"))
                throw new Model.Exception.WrongInputException("کد ملی", "فقط یک عدد 10 رقمی وارد شود");
            //if (dbContext.Persons.Any(p => p.NationalCode == melliCode))
            //    throw new Model.Exception.DuplicatedException("شخص", "کد ملی قبلاً وارد شده است");
        }
        public void ValidateBirthDate(DateTime birthDate)
        {
            if ((DateTime.Now - birthDate).TotalDays > (100 * 365) || (DateTime.Now - birthDate).TotalDays < (15 * 365))
                throw new Model.Exception.WrongInputException("تاریخ تولد", "بیش از 100 سال و یا کمتر از 15 سال وارد شده است");
        }
        public void ValidatePlaceOfBirth(string placeOfBirth)
        {
            if (string.IsNullOrEmpty(placeOfBirth) || string.IsNullOrEmpty(placeOfBirth))
                throw new Model.Exception.WrongInputException("محل تولد", "وارد نشده است");
        }
        public void ValidateReasonOfExemption(string reason)
        {
            if (string.IsNullOrEmpty(reason) || string.IsNullOrEmpty(reason))
                throw new Model.Exception.WrongInputException("علت معاقیت", "وارد نشده است");
        }
        public void ValidateAssociationMembershipNumber(string associationNumber)
        {
            if (string.IsNullOrEmpty(associationNumber) || string.IsNullOrEmpty(associationNumber))
                throw new Model.Exception.WrongInputException("شماره عضویت در انجمن", "وارد نشده است");
        }
        public void ValidateMobilePhone(string mobile)
        {
            if (string.IsNullOrEmpty(mobile) || !Regex.IsMatch(mobile, @"^09[0-9]{9}$"))
                throw new Model.Exception.WrongInputException("شماره همراه", "فقط با 09 شروع شده و یک عدد 11 رقمی وارد شود");
            if (dbContext.Users.Where(u => u.Mobile == mobile).Count() > 0)
                throw new Model.Exception.DuplicatedException("شماره همراه", "قبلاً ثبت شده است");
        }
        public void ValidateAddress(string address)
        {
            if (string.IsNullOrEmpty(address) || string.IsNullOrEmpty(address))
                throw new Model.Exception.WrongInputException("آدرس", "وارد نشده است");
        }
        public void ValidatePostalCode(string postalCode)
        {
            if (string.IsNullOrEmpty(postalCode) || !Regex.IsMatch(postalCode, @"^[0-9]{5,10}$"))
                throw new Model.Exception.WrongInputException("کدپستی", "فقط یک عدد بین 5 تا 10 رقمی وارد شود");
        }
        public void ValidatePhone(string phone)
        {
            if (string.IsNullOrEmpty(phone) || !Regex.IsMatch(phone, @"^[0-9]{6,11}$"))
                throw new Model.Exception.WrongInputException("تلفن", "فقط یک عدد بین 6 تا 11 رقمی وارد شود");
        }
        public void ValidatePhoto(byte[] fileData)
        {
            double uploadFileLimit = 0;
            try
            {
                uploadFileLimit = int.Parse(ConfigurationManager.AppSettings["UploadFileLimit"]);
            }
            catch
            {
                throw new Model.Exception.InternalException("تنظیمات حجم تصویر پیدا نگردید");
            }
            if (fileData.Length > uploadFileLimit)
                throw new Model.Exception.WrongInputException("حجم تصویر بارگزاری شده", "فقط کمتر از " + (uploadFileLimit / 1024 / 1024).ToString("F0") + " مگابایت آپلود شود");
            try
            {
                Image.FromStream(new MemoryStream(fileData));
            }
            catch
            {
                throw new Model.Exception.WrongInputException("فایل تصویر", "فایل بارگزاری شده تصویر نیست");
            }
        }
        public void ValidateUnitName(string unitName)
        {
            if (string.IsNullOrEmpty(unitName))
                throw new Model.Exception.WrongInputException("نام کارگاه/واحد", "وارد نشده است");
        }
        public void ValidateSocialSecurityCode(string code)
        {
            if (string.IsNullOrEmpty(code))
                throw new Model.Exception.WrongInputException("کد تامین اجتماعی", "وارد نشده است");
        }
        public void ValidateAllNumberOfWorkers(int male, int female, int morning, int evening, int night, int safety)
        {
            if (male < 0)
                throw new Model.Exception.WrongInputException("تعداد کارگران مرد", "کمتر از 0 وارد شده است");
            if (female < 0)
                throw new Model.Exception.WrongInputException("تعداد کارگران زن", "کمتر از 0 وارد شده است");
            if (morning < 0)
                throw new Model.Exception.WrongInputException("تعداد کارگران شیفت صبح", "کمتر از 0 وارد شده است");
            if (evening < 0)
                throw new Model.Exception.WrongInputException("تعداد کارگران شیفت عصر", "کمتر از 0 وارد شده است");
            if (night < 0)
                throw new Model.Exception.WrongInputException("تعداد کارگران شیفت شب", "کمتر از 0 وارد شده است");
            if (safety < 0)
                throw new Model.Exception.WrongInputException("تعداد مسئولین ایمنی", "کمتر از 0 وارد شده است");
            if ((male + female) == 0)
                throw new Model.Exception.WrongInputException("تعداد کارگران بر اساس جنسیت", "مجموع 0 وارد شده است");
            if ((morning + evening + night) == 0)
                throw new Model.Exception.WrongInputException("تعداد کارگران بر اساس شیفت کاری", "مجموع 0 وارد شده است");
            if ((morning + evening + night) != (male + female))
                throw new Model.Exception.WrongInputException("نعداد کارگران", "مجموع کارگران بر اساس جنسیت با مجموع کارگران بر اساس شیفت کاری یکی نمی باشد");
        }
        public void ValidateWorkshopSize(double size)
        {
            if (size <= 0)
                throw new Model.Exception.WrongInputException("وسعت کارگاه", "حتماً بزرگتر از 0 وارد شود");
        }
        public void ValidateCompanyCode(string companyCode)
        {
            if (string.IsNullOrEmpty(companyCode) || !Regex.IsMatch(companyCode, @"^[0-9]{11}$"))
                throw new Model.Exception.WrongInputException("شناسنامه ملی شرکت", "فقط یک عدد 11 رقمی");
        }
        public void ValidateCourseName(string courseName)
        {
            if (string.IsNullOrEmpty(courseName))
                throw new Model.Exception.WrongInputException("نام دوره", "وارد نشده است");
        }
        public void ValidateDurationInHours(double duration)
        {
            if (duration <= 0)
                throw new Model.Exception.WrongInputException("مدت دوره", "حتماً بیشتر از 0 وارد شود");
        }
        public void ValidateDateTaken(DateTime date)
        {
            if ((DateTime.Now - date).TotalDays < 1)
                throw new Model.Exception.WrongInputException("تاریخ برگزاری دوره", "حتماً تاریخ پیش از امروز وارد شود");
        }
        public void ValidateDateFromAndDateTo(DateTime from, DateTime to)
        {
            if ((to - from).TotalDays < 1)
                throw new Model.Exception.WrongInputException("تاریخ از، تا", "'تاریخ تا' نمی تواند کمتر از 'تاریخ از' باشد");
            if (to > DateTime.Now)
                throw new Model.Exception.WrongInputException("تاریخ تا", "'تاریخ تا' نمی تواند مربوط به آینده باشد");
        }
        public void ValidateIssuerName(string issuerName)
        {
            if (string.IsNullOrEmpty(issuerName))
                throw new Model.Exception.WrongInputException("شماره مرجع صدور", "وارد نشده است");
        }
        public void ValidateEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
            }
            catch
            {
                throw new Model.Exception.WrongInputException("ایمیل", "");
            }
            if (dbContext.Users.Where(u => u.Email == email).Count() > 0)
                throw new Model.Exception.DuplicatedException("ایمیل", "قبلاً ثبت شده است");
        }
    }
}
