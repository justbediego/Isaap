﻿using Isaap.Core.Model;
using Isaap.Core.Model.Company;
using Isaap.Core.Model.License;
using Isaap.Core.Model.Location;
using Isaap.Core.Model.Log;
using Isaap.Core.Model.Person;
using Isaap.Core.Model.User;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.DataAccess
{
    public class IsaapContext : DbContext
    {
        public DbSet<Attachment> Attachments { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<FieldOfActivity> FieldOfActivities { get; set; }
        public DbSet<ProductionType> ProductionTypes { get; set; }
        public DbSet<License> Licenses { get; set; }
        public DbSet<LicenseFlow> LicenseFlows { get; set; }
        public DbSet<County> Counties { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Province> Provinces { get; set; }
        public DbSet<Education> Educations { get; set; }
        public DbSet<FieldOfEducation> FieldOfEducations { get; set; }
        public DbSet<FieldOfCertification> FieldOfCertifications { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<SafetyCertification> SafetyCertifications { get; set; }
        public DbSet<WorkExperience> WorkExperiences { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public DbSet<Association> Associations { get; set; }
        public DbSet<CEO> CEOs { get; set; }
        public DbSet<Club> Clubs { get; set; }
        public DbSet<CountyInspector> CountyInspectors { get; set; }
        public DbSet<Expert> Experts { get; set; }
        public DbSet<InspectionOffice> InspectionOffices { get; set; }
        public DbSet<ProvinceInspector> ProvinceInspectors { get; set; }
        public DbSet<ProvinceManager> ProvinceManagers { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<ActionLog> ActionLogs { get; set; }
        public class EntityFrameworkConfiguration : DbConfiguration
        {
            public EntityFrameworkConfiguration()
            {
                AddInterceptor(new SoftDeleteInterceptor());
            }
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            var conv = new AttributeToTableAnnotationConvention<SoftDeleteAttribute, string>(
                "SoftDeleteColumnName",
                (type, attributes) => attributes.Single().ColumnName);
            modelBuilder.Conventions.Add(conv);
        }
        public override int SaveChanges()
        {
            var entries = ChangeTracker.Entries().Where(entry => entry.Entity is Base).ToList();
            //last updated
            foreach (var entry in entries)
                ((Base)entry.Entity).LastModified = DateTime.Now;
            return base.SaveChanges();
        }
    }

}
