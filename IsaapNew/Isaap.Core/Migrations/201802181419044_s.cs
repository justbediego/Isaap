namespace Isaap.Core.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class s : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Users", new[] { "Mobile" });
            DropIndex("dbo.Users", new[] { "Email" });
            CreateTable(
                "dbo.ActionLogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ActionType = c.String(maxLength: 10),
                        ActionName = c.String(maxLength: 500),
                        Username = c.String(maxLength: 100),
                        IP = c.String(maxLength: 20),
                        Data = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        LastModified = c.DateTime(nullable: false),
                        OwnerType = c.Int(nullable: false),
                        Deleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "Deleted" },
                })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.ProvinceManagers", "Person_ID", c => c.Int(nullable: false));
            AddColumn("dbo.LicenseFlows", "CreatedByUsername", c => c.String(nullable: false, maxLength: 100));
            CreateIndex("dbo.ProvinceManagers", "Person_ID");
            AddForeignKey("dbo.ProvinceManagers", "Person_ID", "dbo.People", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProvinceManagers", "Person_ID", "dbo.People");
            DropIndex("dbo.ProvinceManagers", new[] { "Person_ID" });
            DropColumn("dbo.LicenseFlows", "CreatedByUsername");
            DropColumn("dbo.ProvinceManagers", "Person_ID");
            DropTable("dbo.ActionLogs",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "SoftDeleteColumnName", "Deleted" },
                });
            CreateIndex("dbo.Users", "Email", unique: true);
            CreateIndex("dbo.Users", "Mobile", unique: true);
        }
    }
}
