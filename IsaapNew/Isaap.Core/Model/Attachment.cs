﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model
{
    public class Attachment:Base
    {
        public int? Objection_ID { get; set; }
        public int? LicenseFlow_ID { get; set; }
        public int? InsuranceRecord_ID { get; set; }
        public int? NationalCard_ID { get; set; }
        public int? Shenasname_ID { get; set; }
        public int? TPCScans_ID { get; set; }
        public int? Contract_ID { get; set; }
        [StringLength(200)]
        public string Filename { get; set; }
        public byte[] FileData { get; set; }
    }
}
