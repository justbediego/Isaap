﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.License
{
    public class LicenseFlow : Base
    {
        public int License_ID { get; set; }
        [ForeignKey("License_ID")]
        public virtual User.License License { get; set; }
        public LicenseFlow()
        {
            Attachments = new List<Attachment>();
            IsCurrent = true;
        }
        public LicenseFlow(string username) : this()
        {
            this.CreatedByUsername = username;
        }
        public enum FlowType
        {
            WithExpert = 2,
            WithAssociation = 3,
            WithCountyInspector = 4,
            WithClub = 5,
            WithProvinceInspector = 6,
            Authorized = 7,
            RejectedOnce = 8,
            WithProvinceManager = 9,
            RejectedTwice = 10,
            WithInspectionOffice = 11,
            Deactivated = 12,
        }
        [Required]
        [StringLength(100)]
        public string CreatedByUsername { get; private set; }
        public FlowType Type { get; set; }
        [StringLength(10000)]
        public string Description { get; set; }
        [ForeignKey("LicenseFlow_ID")]
        public virtual List<Attachment> Attachments { get; set; }
        public bool IsCurrent { get; set; }
    }
}
