﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.Log
{
    public class ActionLog:Base
    {
        [StringLength(10)]
        public string ActionType { get; set; }
        [StringLength(500)]
        public string ActionName { get; set; }
        [StringLength(100)]
        public string Username { get; set; }
        [StringLength(20)]
        public string IP { get; set; }
        [StringLength(1000)]
        public string Data { get; set; }
    }
}