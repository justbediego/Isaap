﻿using Isaap.Core.Model.Location;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.User
{
    public class CountyInspector : Base
    {
        public int User_ID { get; set; }
        [ForeignKey("User_ID")]
        public virtual User User { get; set; }
        public int County_ID { get; set; }
        [ForeignKey("County_ID")]
        public virtual County County { get; set; }
    }
}
