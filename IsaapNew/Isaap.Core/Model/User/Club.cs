﻿using Isaap.Core.Model.Location;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.User
{
    public class Club : Base
    {
        public int User_ID { get; set; }
        [ForeignKey("User_ID")]
        public virtual User User { get; set; }
        public int Province_ID { get; set; }
        [ForeignKey("Province_ID")]
        public virtual Province Province { get; set; }
    }
}
