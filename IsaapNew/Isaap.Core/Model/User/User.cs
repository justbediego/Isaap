﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.User
{
    public class User : Base
    {
        public User()
        {
            Random rand = new Random();
            EmailValidationKey = string.Empty;
            for(int i = 0; i < 50; i++)
                EmailValidationKey += rand.Next(10).ToString();
        }
        [Index(IsUnique = true)]
        [MaxLength(100)]
        public string UserName { get; set; }
        [NotMapped]
        public string Password { get; set; }
        [StringLength(32)]
        public string HashPassword { get; set; }
        public enum UserType
        {
            Expert = 1,
            Association = 2,
            Club = 3,
            CountyInspector = 4,
            ProvinceInspector = 5,
            ProvinceManager = 6,
            InspectionOffice = 7,
            Admin = 8,
            CEO = 9,
            License = 10,
        }
        public UserType Type { get; set; }
        //[Index(IsUnique = true)]
        [StringLength(20)]
        public string Mobile { get; set; }
        //[Index(IsUnique = true)]
        [StringLength(200)]
        public string Email { get; set; }
        [StringLength(50)]
        public string EmailValidationKey { get; set; }
    }
}
