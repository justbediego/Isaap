﻿using Isaap.Core.Model.License;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.User
{
    public class Expert : Base
    {
        public Expert()
        {
            Licenses = new List<License>();
        }
        public int User_ID { get; set; }
        [ForeignKey("User_ID")]
        public virtual User User { get; set; }
        public int Person_ID { get; set; }
        [ForeignKey("Person_ID")]
        public virtual Person.Person Person { get; set; }
        public virtual List<License> Licenses { get; set; }
    }
}
