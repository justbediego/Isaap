﻿using Isaap.Core.Model.Location;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.User
{
    public class ProvinceInspector : Base
    {
        public int Location_ID { get; set; }
        [ForeignKey("Location_ID")]
        public virtual Location.Location Location { get; set; }
        public int ChiefPerson_ID { get; set; }
        [ForeignKey("ChiefPerson_ID")]
        public virtual Person.Person ChiefPerson { get; set; }
        public int User_ID { get; set; }
        [ForeignKey("User_ID")]
        public virtual User User { get; set; }
        public int Province_ID { get; set; }
        [ForeignKey("Province_ID")]
        public virtual Province Province { get; set; }
    }
}
