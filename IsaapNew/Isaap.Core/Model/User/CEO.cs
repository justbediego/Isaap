﻿using Isaap.Core.Model.License;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.User
{
    public class CEO : Base
    {
        public int Person_ID { get; set; }
        [ForeignKey("Person_ID")]
        public virtual Person.Person Person { get; set; }
        //Ceo will be removed from
        public int Company_ID { get; set; }
        [ForeignKey("Company_ID")]
        public virtual Company.Company Company { get; set; }
        public int User_ID { get; set; }
        [ForeignKey("User_ID")]
        public virtual User User { get; set; }
    }
}
