﻿using Isaap.Core.Model.License;
using Isaap.Core.Model.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.User
{
    public class License : Base
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public virtual List<LicenseFlow> Flows { get; set; }
        public int Company_ID { get; set; }
        [ForeignKey("Company_ID")]
        public virtual Company.Company Company { get; set; }
        public int Expert_ID { get; set; }
        [ForeignKey("Expert_ID")]
        public virtual Expert Expert { get; set; }
        [ForeignKey("Contract_ID")]
        public virtual List<Attachment> ContractScans { get; set; }
        public int? User_ID { get; set; }
        [ForeignKey("User_ID")]
        public virtual User User { get; set; }

        [ForeignKey("Objection_ID")]
        public virtual List<Attachment> ObjectionAttachments { get; set; }
        public License()
        {
            Flows = new List<LicenseFlow>();
            ContractScans = new List<Attachment>();
        }
    }
}
