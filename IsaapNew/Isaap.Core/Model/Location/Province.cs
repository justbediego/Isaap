﻿using Isaap.Core.Model.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.Location
{
    public class Province : Base
    {
        public Province()
        {
            Counties = new List<County>();
            Clubs = new List<Club>();
            ProvinceInspectors = new List<ProvinceInspector>();
            ProvinceManagers = new List<ProvinceManager>();
        }
        [StringLength(100)]
        public string Name { get; set; }
        [StringLength(10)]
        public string Code { get; set; }
        public virtual List<County> Counties { get; set; }
        public virtual List<Club> Clubs { get; set; }
        public virtual List<ProvinceInspector> ProvinceInspectors { get; set; }
        public virtual List<ProvinceManager> ProvinceManagers { get; set; }
    }
}