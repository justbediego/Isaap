﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.Location
{
    public class Location : Base
    {
        public int? County_ID { get; set; }
        [ForeignKey("County_ID")]
        public virtual County County { get; set; }
        [StringLength(1000)]
        public string Address { get; set; }
        [StringLength(20)]
        public string PostalCode { get; set; }
        [StringLength(20)]
        public string Phone { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
    }
}
