﻿using Isaap.Core.Model.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.Location
{
    public class County:Base
    {
        public int Province_ID { get; set; }
        [ForeignKey("Province_ID")]
        public virtual Province Province { get; set; }
        public virtual List<Association> Associations { get; set; }
        public virtual List<CountyInspector> CountyInspectors { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        public County()
        {
            Associations = new List<Association>();
            CountyInspectors = new List<CountyInspector>();
        }
    }
}
