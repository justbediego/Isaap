﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.Exception
{
    public class DuplicatedException : System.Exception
    {
        public DuplicatedException(string duplicatedObject, string description)
            : base(string.Format("اطلاعات '{0}' تکراری می باشد. {1}", duplicatedObject, description))
        {
        }
    }
}
