﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.Exception
{
    public class NotFoundException : System.Exception
    {
        public NotFoundException(string obj, string description)
            : base(string.Format("'{0}' پیدا نگردید. {1}", obj, description))
        {
        }
    }
}
