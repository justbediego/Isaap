﻿using Isaap.Core.Model.License;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.Analyzable
{
    public abstract class Analyzable : Base
    {
        public Analyzable()
        {
            IsAuthorized = false;
        }
        public bool IsAuthorized { get; set; }
    }
}
