﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.Company
{
    public class FieldOfActivity : Base
    {
        [StringLength(100)]
        public string Name { get; set; }
        public enum DangerLevels
        {
            Unknown = 0,
            Low = 1,
            Moderate = 2,
            High = 3,
        }
        public DangerLevels DangerLevel { get; set; }
    }
}
