﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.Company
{
    public class ProductionType:Base
    {
        [StringLength(100)]
        public string Name { get; set; }
    }
}
