﻿using Isaap.Core.Model.License;
using Isaap.Core.Model.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.Company
{
    public class Company : Analyzable.Analyzable
    {
        public Company()
        {
            TPCScans = new List<Attachment>();
            CEOs = new List<CEO>();
            Licenses = new List<User.License>();
        }
        [Index(IsUnique = true)]
        [StringLength(20)]
        public string CompanyCode { get; set; }
        [StringLength(200)]
        public string UnitName { get; set; }
        [StringLength(100)]
        public string SocialSecurityCode { get; set; }
        public int? FieldOfActivity_ID { get; set; }
        [ForeignKey("FieldOfActivity_ID")]
        public virtual FieldOfActivity FieldOfActivity { get; set; }
        public int? ProductionType_ID { get; set; }
        [ForeignKey("ProductionType_ID")]
        public virtual ProductionType ProductionType { get; set; }
        public int MaleWorkersNo { get; set; }
        public int FemaleWorkersNo { get; set; }
        public double WorkshopSize { get; set; }
        public int MorningWorkersNo { get; set; }
        public int EveningWorkersNo { get; set; }
        public int NightWorkersNo { get; set; }
        public bool TechnicalProtectionCommittee { get; set; }
        [ForeignKey("TPCScans_ID")]
        public virtual List<Attachment> TPCScans { get; set; }
        public int SafetyWorkersNo { get; set; }
        public int Location_ID { get; set; }
        [ForeignKey("Location_ID")]
        public virtual Location.Location Location { get; set; }
        public virtual List<CEO> CEOs { get; set; }
        public virtual List<User.License> Licenses { get; set; }
    }
}
