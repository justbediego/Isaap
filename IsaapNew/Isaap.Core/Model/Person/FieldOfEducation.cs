﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.Person
{
    public class FieldOfEducation : Base
    {
        [StringLength(500)]
        public string Name { get; set; }
        [StringLength(10)]
        public string Code { get; set; }
        [StringLength(10)]
        public string OrginalityCode { get; set; }
        public List<FieldOfCertification> IncludedFieldOfCertifications { get; set; }
        public FieldOfEducation()
        {
            IncludedFieldOfCertifications = new List<FieldOfCertification>();
        }
    }
}
