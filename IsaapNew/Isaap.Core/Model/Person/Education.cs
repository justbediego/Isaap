﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.Person
{
    public class Education: Analyzable.Analyzable
    {
        public enum DegreeLevels
        {
            HighSchool = 1,
            Diploma = 2,
            Bachelor = 3,
            Masters = 4,
            PhD = 5,
        }
        public DegreeLevels DegreeLevel { get; set; }
        public int FieldOfEducation_ID { get; set; }
        [ForeignKey("FieldOfEducation_ID")]
        public virtual FieldOfEducation FieldOfEducation { get; set; }
        public int? ScanFile_ID { get; set; }
        [ForeignKey("ScanFile_ID")]
        public virtual Attachment ScanFile { get; set; }
        public int Person_ID { get; set; }
        [ForeignKey("Person_ID")]
        public virtual Person Person { get; set; }
    }
}
