﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.Person
{
    public class SafetyCertification : Analyzable.Analyzable
    {
        public int? FieldOfCertification_ID { get; set; }
        [ForeignKey("FieldOfCertification_ID")]
        public virtual FieldOfCertification FieldOfCertification { get; set; }
        [StringLength(200)]
        public string OtherCourseName { get; set; }
        public double OtherDurationInHours { get; set; }
        public DateTime DateTaken { get; set; }
        [StringLength(20)]
        public string IssuerName { get; set; }
        public int? CertificationScan_ID { get; set; }
        [ForeignKey("CertificationScan_ID")]
        public virtual Attachment CertificationScan { get; set; }
        [StringLength(1000)]
        public string Description { get; set; }
        public int Person_ID { get; set; }
        [ForeignKey("Person_ID")]
        public virtual Person Person { get; set; }
    }
}
