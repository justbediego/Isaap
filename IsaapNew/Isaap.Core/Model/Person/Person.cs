﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.Person
{
    public class Person : Analyzable.Analyzable
    {
        public Person()
        {
            Educations = new List<Education>();
            SafetyCertifications = new List<SafetyCertification>();
            WorkExperiences = new List<WorkExperience>();
            ShenasnameScans = new List<Attachment>();
            NationalCardScans = new List<Attachment>();
            InsuranceRecordScans = new List<Attachment>();
            Experts = new List<User.Expert>();
            CEOs = new List<User.CEO>();
        }
        [StringLength(100)]
        public string FirstName { get; set; }
        [StringLength(100)]
        public string LastName { get; set; }
        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
        [StringLength(100)]
        public string FatherName { get; set; }
        [StringLength(20)]
        public string ShenasnameNo { get; set; }
        [Index(IsUnique = true)]
        [StringLength(10)]
        public string NationalCode { get; set; }
        public DateTime? BirthDate { get; set; }
        [StringLength(100)]
        public string PlaceOfBirth { get; set; }
        public enum GenderType
        {
            Male = 1,
            Female = 2,
        }
        public GenderType? Gender { get; set; }
        public enum MilitaryServiceStates
        {
            Passed = 1,
            Exempted = 2,
        }
        public MilitaryServiceStates? MilitaryStatus { get; set; }
        [StringLength(1000)]
        public string ReasonOfExemption { get; set; }
        public enum MaritalStates
        {
            Single = 1,
            Married = 2,
        }
        public MaritalStates? MaritalStatus { get; set; }
        public int? HomeLocation_ID { get; set; }
        [ForeignKey("HomeLocation_ID")]
        public virtual Location.Location HomeLocation { get; set; }
        public int? PhotoScan_ID { get; set; }
        [ForeignKey("PhotoScan_ID")]
        public virtual Attachment PhotoScan { get; set; }
        [ForeignKey("Shenasname_ID")]
        public virtual List<Attachment> ShenasnameScans { get; set; }
        [ForeignKey("NationalCard_ID")]
        public virtual List<Attachment> NationalCardScans { get; set; }
        [ForeignKey("InsuranceRecord_ID")]
        public virtual List<Attachment> InsuranceRecordScans { get; set; }
        public virtual List<Education> Educations { get; set; }
        public virtual List<SafetyCertification> SafetyCertifications { get; set; }
        public virtual List<WorkExperience> WorkExperiences { get; set; }
        public virtual List<User.Expert> Experts { get; set; }
        public virtual List<User.CEO> CEOs { get; set; }
        [StringLength(50)]
        public string AssociationMembershipNo { get; set; }
    }
}
