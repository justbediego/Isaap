﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.Person
{
    public class WorkExperience : Analyzable.Analyzable
    {
        [StringLength(1000)]
        public string WorkshopName { get; set; }
        [StringLength(200)]
        public string EmployerName { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        [StringLength(1000)]
        public string Description { get; set; }
        public int Person_ID { get; set; }
        [ForeignKey("Person_ID")]
        public virtual Person Person { get; set; }
    }
}
