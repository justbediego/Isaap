﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.Person
{
    public class FieldOfCertification : Base
    {
        [StringLength(1000)]
        public string Name { get; set; }
        public double DurationInHours { get; set; }
        public bool IsRequiredForAuthorization { get; set; }
        public List<FieldOfEducation> IncludedInFieldOfEducations { get; set; }
        public FieldOfCertification()
        {
            IncludedInFieldOfEducations = new List<FieldOfEducation>();
        }
    }
}
