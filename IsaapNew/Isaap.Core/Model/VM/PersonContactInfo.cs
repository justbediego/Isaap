﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.VM
{
    public class PersonContactInfo
    {
        public bool IsEditable;
        public string Mobile;
        public string Email;
        public int? CountyID;
        public int? ProvinceID;
        public string Address;
        public string PostalCode;
        public string Phone;
    }
}
