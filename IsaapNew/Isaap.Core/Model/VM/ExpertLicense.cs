﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.VM
{
    public class ExpertLicense
    {
        public class Flow
        {
            public License.LicenseFlow.FlowType Type;
            public IEnumerable<int> AttachmentIds;
            public DateTime DateCreated { set; private get; }
            public string Description;
            public string PersianDateCreated
            {
                get
                {
                    return PersianDate.ConvertDate.ToFa(DateCreated) + " " + DateCreated.ToString("HH:mm");
                }
            }
        }
        public int Id;
        public DateTime DateCreated { private get; set; }
        public string PersianDateCreated
        {
            get
            {
                return PersianDate.ConvertDate.ToFa(DateCreated);
            }
        }
        public string UnitName;
        public string CompanyCode;
        public string CEOFirstName;
        public string CEOLastName;
        public IEnumerable<int> ContractScanIds;
        public License.LicenseFlow.FlowType CurrentFlowType;
        public DateTime? StartDate;
        public DateTime? EndDate;
        public string PersianStartDate
        {
            get
            {
                return StartDate.HasValue ? PersianDate.ConvertDate.ToFa(StartDate.Value) : null;
            }
        }
        public string PersianEndDate
        {
            get
            {
                return EndDate.HasValue ? PersianDate.ConvertDate.ToFa(EndDate.Value) : null;
            }
        }
        public string Username;
    }
}
