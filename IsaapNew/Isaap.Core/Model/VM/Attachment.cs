﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.VM
{
    public class Attachment
    {
        public string Filename;
        public byte[] FileData;
    }
}
