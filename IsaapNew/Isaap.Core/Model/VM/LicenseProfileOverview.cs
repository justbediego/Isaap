﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.VM
{
    public class LicenseProfileOverview
    {
        public class ExpertData
        {
            public string Username;
            public string NationalCode;
            public DateTime BirthDate;
            public string PersianBirthDate
            {
                get
                {
                    return PersianDate.ConvertDate.ToFa(BirthDate);
                }
            }
            public string FirstName;
            public string LastName;
            public string FatherName;
            public string Mobile;
            public string Email;
            public Person.Person.GenderType Gender;
            public Person.Person.MaritalStates MaritalStatus;
            public Person.Person.MilitaryServiceStates MilitaryStatus;
            public string ReasonOfExemption;
            public string ShenasnameNo;
            public string PlaceOfBirth;
            public string HomeCounty;
            public string HomeProvince;
            public string HomeAddress;
            public string HomePostalCode;
            public string HomePhone;
        }
        public class LicenseData
        {
            public class CompanyData
            {
                public string CompanyCode;
                public string UnitName;
                public string SocialSecurityCode;
                public string CEOFirstName;
                public string CEOLastName;
                public string ProductionType;
                public string FieldOfActivity;
                public Company.FieldOfActivity.DangerLevels FieldOfActivityDangerLevel;
                public int MaleWorkersNo;
                public int FemaleWorkersNo;
                public int MorningWorkersNo;
                public int EveningWorkersNo;
                public int NightWorkersNo;
                public int SafetyWorkersNo;
                public double WorkshopSize;
                public string Address;
                public string PostalCode;
                public string Phone;
                public string County;
                public int CountyId;
                public string Province;
                public int ProvinceId;
            }
            public int LicenseId;
            public CompanyData Company;
            public DateTime DateCreated;
            public string PersianDateCreated
            {
                get
                {
                    return PersianDate.ConvertDate.ToFa(DateCreated);
                }
            }
            public DateTime? StartDate;
            public string PersianStartDate
            {
                get
                {
                    return StartDate.HasValue ? PersianDate.ConvertDate.ToFa(StartDate.Value) : null;
                }
            }
            public DateTime? EndDate;
            public string PersianEndDate
            {
                get
                {
                    return EndDate.HasValue ? PersianDate.ConvertDate.ToFa(EndDate.Value) : null;
                }
            }
            public string Username;
        }
        public ExpertData Expert;
        public LicenseData License;
    }
}
