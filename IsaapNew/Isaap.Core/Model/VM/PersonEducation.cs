﻿using Isaap.Core.Model.Person;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.VM
{
    public class PersonEducation
    {
        public int Id;
        public bool IsEditable;
        public Education.DegreeLevels DegreeLevel;
        public int FieldOfEducationId;
    }
}
