﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.VM
{
    public class LicensePrint
    {
        public Person.Person.GenderType Gender;
        public string FullName;
        public class Education
        {
            public Person.Education.DegreeLevels DegreeLevel;
            public string EducationField;
        }
        public DateTime DateAuthorized;
        public string PersianDateAuthorized
        {
            get
            {
                return PersianDate.ConvertDate.ToFa(DateAuthorized);
            }
        }
        public string CompanyProvince;
        public Education MaxEducation;
        public string CompanyName;
        public string FieldOfActivity;
        public int TotalNumberOfWorkers;
        public string CompanyFullAddress;
        public string ProvinceChiefFullName;
        public string ProvinceOfficeAddress;
        public string ProvinceOfficePhone;
        public int PhotoId;
    }
}
