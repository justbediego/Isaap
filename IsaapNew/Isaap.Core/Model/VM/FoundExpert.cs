﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.VM
{
    public class FoundExpert
    {
        public class Education
        {
            public Person.Education.DegreeLevels DegreeLevel;
            public string EducationField;
        }
        public int? PhotoScan_ID;
        public string FullName;
        public string Username;
        public string NationalCode;
        public string Province;
        public string County;
        public DateTime BirthDate { set; private get; }
        public Education MaxEducation;
        public string Email;
        public int Age
        {
            get
            {
                return (int)(DateTime.Now - BirthDate).TotalDays / 365;
            }
        }
    }
}
