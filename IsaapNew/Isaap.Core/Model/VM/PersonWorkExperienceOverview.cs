﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.VM
{
    public class PersonWorkExperienceOverview
    {
        public int Id;
        public string WorkshopName;
        public string EmployerName;
        public DateTime DateFrom;
        public string PersianDateFrom
        {
            get
            {
                return PersianDate.ConvertDate.ToFa(DateFrom);
            }
        }
        public string PersianDateTo
        {
            get
            {
                return PersianDate.ConvertDate.ToFa(DateTo);
            }
        }
        public DateTime DateTo;
        public string Description;
    }
}
