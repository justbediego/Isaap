﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.VM
{
    public class DashboardData
    {
        public string FullName;
        public int? PhotoId;
    }
}
