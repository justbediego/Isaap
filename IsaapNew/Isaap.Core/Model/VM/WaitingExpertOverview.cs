﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.VM
{
    public class WaitingExpertOverview
    {
        public Person.Person.GenderType Gender;
        public string Username;
        public DateTime BirthDate { private get; set; }
        public string FatherName;
        public string FullName;
        public string HomeCounty;
        public string HomeProvince;
        public string NationalCode;
        public int PhotoScan_ID;
        public string ShenasnameNo;
        public int NumberOfAcceptedLicenses;
        public int NumberOfLicenses;
        public string Mobile;
        public string PersianBirthDate
        {
            get
            {
                return PersianDate.ConvertDate.ToFa(BirthDate);
            }
        }
        public class Education
        {
            public Person.Education.DegreeLevels DegreeLevel;
            public string EducationField;
        }
        public Education MaxEducation;
        public string Email;
        public int Age
        {
            get
            {
                return (int)(DateTime.Now - BirthDate).TotalDays / 365;
            }
        }
    }
}
