﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.VM
{
    public class WorkshopLocationInfo
    {
        public bool IsEditable;
        public int? CountyID;
        public int? ProvinceID;
        public string Address;
        public string PostalCode;
        public string Phone;
    }
}
