﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.VM
{
    public class PersonCertification
    {
        public int Id;
        public int? FieldOfCertificationId;
        public string OtherCourseName;
        public double OtherDurationInHours;
        public string IssuerName;
        public string Description;
        public DateTime DateTaken;
        public string PersianDateTaken
        {
            get
            {
                return PersianDate.ConvertDate.ToFa(DateTaken);
            }
        }
    }
}
