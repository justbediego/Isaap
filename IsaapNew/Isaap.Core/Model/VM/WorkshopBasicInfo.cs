﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.VM
{
    public class WorkshopBasicInfo
    {
        public string UnitName;
        public double WorkshopSize;
        public string CompanyCode;
        public int NightWorkersNo;
        public int MorningWorkersNo;
        public int SafetyWorkersNo;
        public int MaleWorkersNo;
        public int EveningWorkersNo;
        public int FemaleWorkersNo;
        public int? FieldOfActivityId;
        public int? ProductionTypeId;
        public bool TechnicalProtectionCommittee;
        public string SocialSecurityCode;
    }
}
