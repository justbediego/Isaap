﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.VM
{
    public class PersonCertificationOverview
    {
        public int Id;
        public string CourseName;
        public double DurationInHours;
        public string IssuerName;
        public int? CertificationScanId;
        public string Description;
        public DateTime DateTaken;
        public string PersianDateTaken
        {
            get
            {
                return PersianDate.ConvertDate.ToFa(DateTaken);
            }
        }
    }
}
