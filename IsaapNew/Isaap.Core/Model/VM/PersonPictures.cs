﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.VM
{
    public class PersonPictures
    {
        public int? PhotoScanId;
        public IEnumerable<int> ShenasnameScanIds;
        public IEnumerable<int> NationalCardScanIds;
        public IEnumerable<int> InsuranceScanIds;
        public IEnumerable<int> TPCScanIds;
        public bool HasTPC;
    }
}
