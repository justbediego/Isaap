﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.VM
{
    public class ProfileProgress
    {
        public List<string> Messages = new List<string>();
        public int Percentage
        {
            get
            {
                return (int)((35 - (Messages.Count > 30 ? 30 : Messages.Count)) / .35);
            }
        }
    }
}
