﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Isaap.Core.Model.VM
{
    public class PersonBasicInfo
    {
        public bool IsEditable;
        public string FirstName;
        public string LastName;
        public string FatherName;
        public Person.Person.GenderType? Gender;
        public string ShenasnameNo;
        public string NationalCode;
        public DateTime? BirthDate;
        public string PlaceOfBirth;
        public Person.Person.MaritalStates? MaritalStatus;
        public Person.Person.MilitaryServiceStates? MilitaryServiceStates;
        public string ReasonOfExemption;
        public string AssociationNumber;
        public string PersianBirthDate
        {
            get
            {
                if (!BirthDate.HasValue)
                    return PersianDate.ConvertDate.ToFa(DateTime.Now);
                return PersianDate.ConvertDate.ToFa(BirthDate);
            }
        }
    }
}
