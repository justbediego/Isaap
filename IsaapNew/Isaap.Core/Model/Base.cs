﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Isaap.Core.DataAccess;

namespace Isaap.Core.Model
{
    [SoftDelete("Deleted")]
    public class Base
    {
        [Key]
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime LastModified { get; set; }
        public int OwnerType { get; set; }
        public bool Deleted { get; set; }
        public Base()
        {
            DateCreated = DateTime.Now;
            LastModified = DateTime.Now;
            OwnerType = 1;
        }
    }
}
