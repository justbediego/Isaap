﻿using Automation.DataAccess;
using Automation.Model;
using Automation.Model.Exception;
using System;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Automation.Business.Inner
{
    public class Validation
    {
        private AutomationContext dbContext;
        public Validation(AutomationContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void ValidateEditableDocument(Document document)
        {
            if (document.CurrentFlow.Type != Flow.FlowType.NotCompleted)
                throw new NotPossibleException("تغییر در پرونده", "پرونده در حالت غیرقابل تغییر می باشد");
            if (document.DeactivationDate != null)
                throw new DeactivatedException("پرونده", document.ReasonOfDeactivation);
        }
        public void ValidateFirstName(string name)
        {
            if (string.IsNullOrEmpty(name) || !Regex.IsMatch(name, @"^[آ-ی ]{2,30}$"))
                throw new WrongInputException("نام", "فقط حروف فارسی بین 2 تا 30 حرف وارد شود");
        }
        public void ValidateFullName(string fullname)
        {
            if (string.IsNullOrEmpty(fullname) || !Regex.IsMatch(fullname, @"^[آ-ی ]{5,60}$"))
                throw new WrongInputException("نام و نام خانوادگی", "فقط حروف فارسی بین 5 تا 60 حرف وارد شود");
        }
        public void ValidateLastName(string familyName)
        {
            if (string.IsNullOrEmpty(familyName) || !Regex.IsMatch(familyName, @"^[آ-ی ]{2,30}$"))
                throw new WrongInputException("نام خانوادگی", "فقط حروف فارسی بین 2 تا 30 حرف وارد شود");
        }
        public void ValidateUserName(string userName)
        {
            if (string.IsNullOrEmpty(userName) || !Regex.IsMatch(userName.ToLower(), @"^[a-z][a-z0-9._]{5,29}$"))
                throw new WrongInputException("نام کاربری", "فقط حروف انگلیسی، اعداد، _ و نقطه بین 6 تا 30 حرف وارد شود");
            AutomationContextPhysical dbContextPhysical = new AutomationContextPhysical();
            if (dbContextPhysical.Users.Where(u => u.UserName.ToLower() == userName.ToLower()).Count() > 0)
                throw new DuplicatedException("نام کاربری", "قبلاً ثبت شده است");
        }
        public void ValidatePassword(string password)
        {
            if (string.IsNullOrEmpty(password) || password.Length < 6 || password.Length > 50)
                throw new WrongInputException("گذرواژه", "حتما بین 6 تا 50 حرف وارد شود");
        }
        public void ValidateProvinceName(string name)
        {
            if (string.IsNullOrEmpty(name) || !Regex.IsMatch(name, @"^[آ-ی ]{2,50}$"))
                throw new WrongInputException("نام استان", "فقط حروف فارسی بین 2 تا 50 حرف وارد شود");
        }
        public void ValidateProvinceCode(string code)
        {
            if (string.IsNullOrEmpty(code) || !Regex.IsMatch(code, @"^[0-9]{2}$"))
                throw new WrongInputException("کد استان", "فقط یک عدد 2 رقمی وارد شود");
        }
        public void ValidateCountyName(string name)
        {
            if (string.IsNullOrEmpty(name) || !Regex.IsMatch(name, @"^[آ-ی ()]{2,50}$"))
                throw new WrongInputException("نام شهرستان", "فقط حروف فارسی بین 2 تا 50 حرف وارد شود");
        }
        public void ValidateFieldOfStudyName(string name)
        {
            if (string.IsNullOrEmpty(name) || !Regex.IsMatch(name.ToLower(), @"^[a-zآ-ی ]{10,100}$"))
                throw new WrongInputException("نام رشته", "فقط حروف فارسی و انگلیسی بین 10 تا 100 حرف وارد شود");
        }
        public void ValidateFieldOfStudyCode(string code)
        {
            if (string.IsNullOrEmpty(code) || !Regex.IsMatch(code, @"^[0-9]{1}$"))
                throw new WrongInputException("کد رشته", "فقط یک عددی تک رقمی وارد شود");
        }
        public void ValidateFieldOfStudyOrginalityCode(string code)
        {
            if (string.IsNullOrEmpty(code) || !Regex.IsMatch(code, @"^[0-9]{1}$"))
                throw new WrongInputException("کد اصلی بودن رشته", "فقط یک عددی تک رقمی وارد شود");
        }
        public void ValidateFieldOfActivityName(string name)
        {
            if (string.IsNullOrEmpty(name) || !Regex.IsMatch(name, @"^[آ-ی ]{3,50}$"))
                throw new WrongInputException("نام فعالیت", "فقط حروف فارسی بین 3 تا 50 حرف وارد شود");
        }
        public void ValidateProductionTypeName(string name)
        {
            if (string.IsNullOrEmpty(name) || !Regex.IsMatch(name, @"^[آ-ی ]{3,50}$"))
                throw new WrongInputException("نام تولید", "فقط حروف فارسی بین 3 تا 50 حرف وارد شود");
        }
        public void ValidateShenasnameNo(string shenasnameNo)
        {
            if (string.IsNullOrEmpty(shenasnameNo) || !Regex.IsMatch(shenasnameNo, @"^[0-9]{1,10}$"))
                throw new WrongInputException("شماره شناسنامه", "فقط یک عدد بین 1 تا 10 رقمی وارد شود");
        }
        public void ValidateNationalCode(string melliCode)
        {
            if (string.IsNullOrEmpty(melliCode) || !Regex.IsMatch(melliCode, @"^[0-9]{10}$"))
                throw new WrongInputException("کد ملی", "فقط یک عدد 10 رقمی وارد شود");
        }
        public void ValidateBirthDate(DateTime birthDate)
        {
            if ((DateTime.Now - birthDate).TotalDays > (100 * 365) || (DateTime.Now - birthDate).TotalDays < (15 * 365))
                throw new WrongInputException("تاریخ تولد", "بیش از 100 سال و یا کمتر از 15 سال وارد شده است");
        }
        public void ValidatePlaceOfBirth(string placeOfBirth)
        {
            if (string.IsNullOrEmpty(placeOfBirth) || string.IsNullOrEmpty(placeOfBirth))
                throw new WrongInputException("محل تولد", "وارد نشده است");
        }
        public void ValidateReasonOfExemption(string reason)
        {
            if (string.IsNullOrEmpty(reason) || string.IsNullOrEmpty(reason))
                throw new WrongInputException("علت معاقیت", "وارد نشده است");
        }
        public void ValidateAssociationMembershipNumber(string associationNumber)
        {
            if (string.IsNullOrEmpty(associationNumber) || string.IsNullOrEmpty(associationNumber))
                throw new WrongInputException("شماره عضویت در انجمن", "وارد نشده است");
        }
        public void ValidateMobilePhone(string mobile)
        {
            if (string.IsNullOrEmpty(mobile) || !Regex.IsMatch(mobile, @"^09[0-9]{9}$"))
                throw new WrongInputException("شماره همراه", "فقط با 09 شروع شده و یک عدد 11 رقمی وارد شود");
        }
        public void ValidateAddress(string address)
        {
            if (string.IsNullOrEmpty(address) || string.IsNullOrEmpty(address))
                throw new WrongInputException("آدرس", "وارد نشده است");
        }
        public void ValidatePostalCode(string postalCode)
        {
            if (string.IsNullOrEmpty(postalCode) || !Regex.IsMatch(postalCode, @"^[0-9]{5,10}$"))
                throw new WrongInputException("کدپستی", "فقط یک عدد بین 5 تا 10 رقمی وارد شود");
        }
        public void ValidatePhone(string phone)
        {
            if (string.IsNullOrEmpty(phone) || !Regex.IsMatch(phone, @"^[0-9]{6,11}$"))
                throw new WrongInputException("تلفن", "فقط یک عدد بین 6 تا 11 رقمی وارد شود");
        }
        public void ValidatePhoto(byte[] fileData)
        {
            long fileLimit = 0;
            try
            {
                fileLimit = long.Parse(ConfigurationManager.AppSettings["FileLimit"]);
            }
            catch
            {
                throw new InternalException("تنظیمات حجم تصویر پیدا نگردید");
            }
            if (fileData.Length > fileLimit)
                throw new WrongInputException("حجم تصویر بارگزاری شده", "فقط کمتر از " + (fileLimit / 1024 / 1024).ToString("F0") + " مگابایت آپلود شود");
            try
            {
                Image.FromStream(new MemoryStream(fileData));
            }
            catch
            {
                throw new WrongInputException("فایل تصویر", "فایل بارگزاری شده تصویر نیست");
            }
        }
        public void ValidateUnitName(string unitName)
        {
            if (string.IsNullOrEmpty(unitName))
                throw new WrongInputException("نام کارگاه/واحد", "وارد نشده است");
        }
        public void ValidateSocialSecurityCode(string code)
        {
            if (string.IsNullOrEmpty(code))
                throw new WrongInputException("کد تامین اجتماعی", "وارد نشده است");
        }
        public void ValidateAllNumberOfWorkers(int male, int female, int morning, int evening, int night, int safety)
        {
            if (male < 0)
                throw new WrongInputException("تعداد کارگران مرد", "کمتر از 0 وارد شده است");
            if (female < 0)
                throw new WrongInputException("تعداد کارگران زن", "کمتر از 0 وارد شده است");
            if (morning < 0)
                throw new WrongInputException("تعداد کارگران شیفت صبح", "کمتر از 0 وارد شده است");
            if (evening < 0)
                throw new WrongInputException("تعداد کارگران شیفت عصر", "کمتر از 0 وارد شده است");
            if (night < 0)
                throw new WrongInputException("تعداد کارگران شیفت شب", "کمتر از 0 وارد شده است");
            if (safety < 0)
                throw new WrongInputException("تعداد کارشناسان ایمنی و حفاظت فنی", "کمتر از 0 وارد شده است");
            if ((male + female) == 0)
                throw new WrongInputException("تعداد کارگران بر اساس جنسیت", "مجموع 0 وارد شده است");
            if ((morning + evening + night) == 0)
                throw new WrongInputException("تعداد کارگران بر اساس شیفت کاری", "مجموع 0 وارد شده است");
            if ((morning + evening + night) != (male + female))
                throw new WrongInputException("نعداد کارگران", "مجموع کارگران بر اساس جنسیت با مجموع کارگران بر اساس شیفت کاری یکی نمی باشد");
        }
        public void ValidateWorkshopSize(double size)
        {
            if (size <= 0)
                throw new WrongInputException("وسعت کارگاه", "حتماً بزرگتر از 0 وارد شود");
        }
        public void ValidateCourseName(string courseName)
        {
            if (string.IsNullOrEmpty(courseName))
                throw new WrongInputException("نام دوره", "وارد نشده است");
        }
        public void ValidateDurationInHours(double? duration)
        {
            if (!duration.HasValue)
                throw new WrongInputException("مدت دوره", "وارد نشده است");
            if (duration <= 0)
                throw new WrongInputException("مدت دوره", "حتماً بیشتر از 0 وارد شود");
        }
        public void ValidateDateTaken(DateTime date)
        {
            if ((DateTime.Now - date).TotalDays < 1)
                throw new WrongInputException("تاریخ برگزاری دوره", "حتماً تاریخ پیش از امروز وارد شود");
        }
        public void ValidateIssuerName(string issuerName)
        {
            if (string.IsNullOrEmpty(issuerName))
                throw new WrongInputException("شماره مرجع صدور", "وارد نشده است");
        }
        public void ValidateDateFromAndDateTo(DateTime from, DateTime to)
        {
            if (to <= from)
                throw new WrongInputException("تاریخ شروع و پایان", "تاریخ شروع بعد از تاریخ پایان می باشد");
        }
        public void ValidateEmailIsUnique(string username, string email)
        {
            AutomationContextPhysical dbContextPhysical = new AutomationContextPhysical();
            if (dbContextPhysical.Users.Where(u => u.UserName.ToLower() != username && u.Email == email).Count() > 0)
                throw new DuplicatedException("ایمیل", "برای کاربر دیگری ثبت شده است");
        }
        public void ValidateEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
            }
            catch
            {
                throw new WrongInputException("ایمیل", "ایمیل ساختار درستی ندارد");
            }
        }
    }
}
