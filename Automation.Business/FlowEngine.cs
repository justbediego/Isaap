﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automation.DataAccess;
using Automation.Model;
using Automation.Business.Inner;
using System.Data.Entity;
using Automation.Model.Exception;

namespace Automation.Business
{
    public class FlowEngine
    {
        AutomationContext dbContext;
        Validation validation;
        public FlowEngine(AutomationContext dbContext, Validation validation)
        {
            this.dbContext = dbContext;
            this.validation = validation;
        }
        public Flow SendUpToProvinceInspector(int documentId, string description)
        {
            Document document = dbContext.Documents.Where(d => d.Id == documentId).Include(d => d.WorkContact.County.Province.ProvinceInspectors).FirstOrDefault();
            Province province = document.WorkContact.County.Province;
            if (province.ProvinceInspectors.Count(pi => !pi.IsDeactivated) > 0)
            {
                Flow flow = new Flow()
                {
                    Type = Flow.FlowType.SentToProvinceInspector,
                    Staff = province.ProvinceInspectors.Single(),
                    Description = description,
                };
                return flow;
            }
            else
                throw new InternalException("عدم پیدا کردن مقصد");
        }
        public Flow SendUpToCountyInspector(int documentId, string description)
        {
            Document document = dbContext.Documents.Where(d => d.Id == documentId).Include(d => d.WorkContact.County.CountyInspectors).FirstOrDefault();
            County county = document.WorkContact.County;
            if (county.CountyInspectors.Count(ci => !ci.IsDeactivated) > 0)
            {
                Flow flow = new Flow()
                {
                    Type = Flow.FlowType.SentToCountyInspector,
                    Staff = county.CountyInspectors.Single(),
                    Description = description,
                };
                return flow;
            }
            else
                return SendUpToProvinceInspector(documentId, description);
        }
        public Flow SendUpToClub(int documentId, string description)
        {
            Document document = dbContext.Documents.Where(d => d.Id == documentId).Include(d => d.WorkContact.County.Province.Clubs).FirstOrDefault();
            Province province = document.WorkContact.County.Province;
            if (province.Clubs.Count(cl => !cl.IsDeactivated) > 0)
            {
                Flow flow = new Flow()
                {
                    Type = Flow.FlowType.SentToClub,
                    Staff = province.Clubs.Single(),
                    Description = description,
                };
                return flow;
            }
            else
                return SendUpToCountyInspector(documentId, description);
        }
        public Flow SendUpToAssociation(int documentId, string description)
        {
            Document document = dbContext.Documents.Where(d => d.Id == documentId).Include(d => d.WorkContact.County.Associations).FirstOrDefault();
            County county = document.WorkContact.County;
            if (county.Associations.Count(a => !a.IsDeactivated) > 0)
            {
                Flow flow = new Flow()
                {
                    Type = Flow.FlowType.SentToAssociation,
                    Staff = county.Associations.Single(),
                    Description = description,
                };
                return flow;
            }
            else
                return SendUpToClub(documentId, description);
        }
        public Flow SendDownToExpert(int documentId, string description)
        {
            Flow flow = new Flow()
            {
                Type = Flow.FlowType.NotCompleted,
                Description = description,
            };
            return flow;
        }
        public Flow SendDownToAssociation(int documentId, string description)
        {
            Document document = dbContext.Documents.Where(d => d.Id == documentId).Include(d => d.WorkContact.County.Associations).FirstOrDefault();
            County county = document.WorkContact.County;
            if (county.Associations.Count(a => !a.IsDeactivated) > 0)
            {
                Flow flow = new Flow()
                {
                    Type = Flow.FlowType.SentToAssociation,
                    Staff = county.Associations.Single(),
                    Description = description,
                };
                return flow;
            }
            else
                return SendDownToExpert(documentId, description);
        }
        public Flow SendDownToClub(int documentId, string description)
        {
            Document document = dbContext.Documents.Where(d => d.Id == documentId).Include(d => d.WorkContact.County.Province.Clubs).FirstOrDefault();
            Province province = document.WorkContact.County.Province;
            if (province.Clubs.Count(cl => !cl.IsDeactivated) > 0)
            {
                Flow flow = new Flow()
                {
                    Type = Flow.FlowType.SentToClub,
                    Staff = province.Clubs.Single(),
                    Description = description,
                };
                return flow;
            }
            else
                return SendDownToAssociation(documentId, description);
        }
        public Flow SendDownToCountyInspector(int documentId, string description)
        {
            Document document = dbContext.Documents.Where(d => d.Id == documentId).Include(d => d.WorkContact.County.CountyInspectors).FirstOrDefault();
            County county = document.WorkContact.County;
            if (county.CountyInspectors.Count(ci => !ci.IsDeactivated) > 0)
            {
                Flow flow = new Flow()
                {
                    Type = Flow.FlowType.SentToCountyInspector,
                    Staff = county.CountyInspectors.Single(),
                    Description = description,
                };
                return flow;
            }
            else
                return SendDownToClub(documentId, description);
        }
        public Flow AuthorizeDocument(int documentId, string description)
        {
            Random rnd = new Random();
            string nezamCode = string.Empty;
            string nezamSerialCode = string.Empty;
            Document document = dbContext.Documents.Where(d => d.Id == documentId)
                                                   .Include(d => d.WorkContact.County.Province)
                                                   .Include(d => d.PhDCertification.FieldOfStudy)
                                                   .Include(d => d.MastersCertification.FieldOfStudy)
                                                   .Include(d => d.BachelorsCertification.FieldOfStudy)
                                                   .Include(d => d.DiplomaCertification.FieldOfStudy)
                                                   .Include(d => d.HighSchoolCertification.FieldOfStudy)
                                                   .FirstOrDefault();
            var otherDocuments = dbContext.Documents.Where(d => d.NationalCode == document.NationalCode && d.Id != document.Id && d.NezamCode != null).ToList();
            if (otherDocuments.Count == 0)
            {
                nezamCode = document.WorkContact.County.Province.Code;
                if (document.PhDCertification != null)
                    nezamCode += document.PhDCertification.FieldOfStudy.Code + document.PhDCertification.FieldOfStudy.OrginalityCode;
                else if (document.MastersCertification != null)
                    nezamCode += document.MastersCertification.FieldOfStudy.Code + document.MastersCertification.FieldOfStudy.OrginalityCode;
                else if (document.BachelorsCertification != null)
                    nezamCode += document.BachelorsCertification.FieldOfStudy.Code + document.BachelorsCertification.FieldOfStudy.OrginalityCode;
                else if (document.DiplomaCertification != null)
                    nezamCode += document.DiplomaCertification.FieldOfStudy.Code + document.DiplomaCertification.FieldOfStudy.OrginalityCode;
                else if (document.HighSchoolCertification != null)
                    nezamCode += document.HighSchoolCertification.FieldOfStudy.Code + document.HighSchoolCertification.FieldOfStudy.OrginalityCode;
                else
                    throw new NotFoundException("کد رشته");
                do
                {
                    nezamSerialCode = string.Empty;
                    for (int i = 0; i < 6; i++)
                        nezamSerialCode += rnd.Next(0, 10).ToString();
                } while (dbContext.Documents.Where(d => d.NezamCode == nezamCode + nezamSerialCode).Count() != 0);
                document.NezamCode = nezamCode + nezamSerialCode;
            }
            else
            {
                //has registered before
                document.NezamCode = otherDocuments.First().NezamCode;
            }
            Flow flow = new Flow()
            {
                Type = Flow.FlowType.Authorized,
                Description = description,
            };
            return flow;
        }
        public Flow RejectOnce(int documentId, string description)
        {
            Flow flow = new Flow()
            {
                Type = Flow.FlowType.RejectedOnce,
                Description = description,
            };
            return flow;
        }
        public Flow RejectTwice(int documentId, string description)
        {
            Flow flow = new Flow()
            {
                Type = Flow.FlowType.RejectedTwice,
                Description = description,
            };
            return flow;
        }
        public Flow RejectCompletely(int documentId, string description)
        {
            Flow flow = new Flow()
            {
                Type = Flow.FlowType.NotCompleted,
                Description = description,
            };
            return flow;
        }
        public Flow ObjectifyOnce(int documentId, string description)
        {
            Document document = dbContext.Documents.Where(d => d.Id == documentId).Include(d => d.WorkContact.County.Province.ProvinceManagers).FirstOrDefault();
            Flow flow = new Flow()
            {
                Type = Flow.FlowType.SentToProvinceManager,
                Staff = document.WorkContact.County.Province.ProvinceManagers.Single(),
                Description = description,
            };
            return flow;
        }
        public Flow ObjectifyTwice(int documentId, string description)
        {
            Flow flow = new Flow()
            {
                Type = Flow.FlowType.SentToInspectionOffice,
                Staff = dbContext.InspectionOffices.Single(),
                Description = description,
            };
            return flow;
        }
    }
}
