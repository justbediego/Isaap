﻿using Automation.Business.Inner;
using Automation.DataAccess;
using Automation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Automation.Model.Report;
using Automation.Model.Exception;

namespace Automation.Business
{
    public class StaffBusiness
    {
        AutomationContext dbContext;
        Validation validation;
        FlowEngine flowEngine;

        public StaffBusiness(AutomationContext dbContext, Validation validation, FlowEngine flowEngine)
        {
            this.dbContext = dbContext;
            this.validation = validation;
            this.flowEngine = flowEngine;
        }

        public Staff GetStaff(string username)
        {
            Staff staff = dbContext.Staff.Where(s => s.UserName == username).FirstOrDefault();
            if (staff == null)
                throw new NotFoundException("کارمند");
            if (staff.IsDeactivated)
                throw new DeactivatedException("کارمند", "دسترسی امکان پذیر نمی باشد");
            return staff;
        }

        private IQueryable<Document> GetAllDocuments(string username)
        {
            Staff staff = dbContext.Staff.Where(s => s.UserName == username).FirstOrDefault();
            if (staff == null)
                throw new NotFoundException("کارمند");

            List<int> underStaff = new List<int>() { staff.Id };
            switch (staff.Type)
            {
                case User.UserType.Club:
                    Club club = dbContext.Clubs.First(c => c.Id == staff.Id);
                    underStaff.AddRange(dbContext.Associations.Where(a => a.County.Province_ID == club.Province_ID).Select(a => a.Id).ToList());
                    break;
                case User.UserType.CountyInspector:
                    CountyInspector countyInspector = dbContext.CountyInspectors.Include(c => c.County).First(c => c.Id == staff.Id);
                    underStaff.AddRange(dbContext.Associations.Where(a => a.County_ID == countyInspector.County_ID).Select(a => a.Id).ToList());
                    underStaff.AddRange(dbContext.Clubs.Where(c => c.Province_ID == countyInspector.County.Province_ID).Select(c => c.Id).ToList());
                    break;
                case User.UserType.ProvinceInspector:
                    ProvinceInspector provinceInspector = dbContext.ProvinceInspectors.First(p => p.Id == staff.Id);
                    underStaff.AddRange(dbContext.Associations.Where(a => a.County.Province_ID == provinceInspector.Province_ID).Select(a => a.Id).ToList());
                    underStaff.AddRange(dbContext.Clubs.Where(c => c.Province_ID == provinceInspector.Province_ID).Select(c => c.Id).ToList());
                    underStaff.AddRange(dbContext.CountyInspectors.Where(c => c.County.Province_ID == provinceInspector.Province_ID).Select(c => c.Id).ToList());
                    break;
                case User.UserType.ProvinceManager:
                    ProvinceManager provinceManager = dbContext.ProvinceManagers.First(p => p.Id == staff.Id);
                    underStaff.AddRange(dbContext.Associations.Where(a => a.County.Province_ID == provinceManager.Province_ID).Select(a => a.Id).ToList());
                    underStaff.AddRange(dbContext.Clubs.Where(c => c.Province_ID == provinceManager.Province_ID).Select(c => c.Id).ToList());
                    underStaff.AddRange(dbContext.CountyInspectors.Where(c => c.County.Province_ID == provinceManager.Province_ID).Select(c => c.Id).ToList());
                    underStaff.AddRange(dbContext.ProvinceInspectors.Where(c => c.Province_ID == provinceManager.Province_ID).Select(c => c.Id).ToList());
                    break;
                case User.UserType.InspectionOffice:
                    underStaff.AddRange(dbContext.Staff.Select(s => s.Id).ToList());
                    break;
            }
            //has it ever passed through anyone below me
            return dbContext.Documents.Where(d => d.DeactivationDate == null && d.Flows.Any(f => underStaff.Contains(f.Staff.Id))).OrderByDescending(d => d.DateCreated);
            //List<int> indexes = dbContext.Documents.Where(d => d.Flows.Any(f => underStaff.Contains(f.Staff.Id))).Select(d => d.Id).ToList();
            //return dbContext.Documents.Where(d => indexes.Contains(d.Id));
        }
        public List<Document> GetAllMyDocuments(string username, out int count, int? skip = null, int? take = null, string phrase = null)
        {
            List<Flow.FlowType> waitingTypes = new List<Flow.FlowType>()
            {
                Flow.FlowType.SentToAssociation,
                Flow.FlowType.SentToCountyInspector,
                Flow.FlowType.SentToClub,
                Flow.FlowType.SentToProvinceInspector,
                Flow.FlowType.SentToProvinceManager,
                Flow.FlowType.SentToInspectionOffice,
            };
            var list = GetAllDocuments(username).Where(d => d.Flows.OrderByDescending(x => x.DateCreated).FirstOrDefault().Staff.UserName == username)
                                                .Include(d => d.HomeContact.County.Province)
                                                .Include(d => d.WorkContact.County.Province)
                                                .Include(d => d.CurrentWorkShop.ContractScans)
                                                .Include(d => d.CurrentWorkShop.ProductionType)
                                                .Include(d => d.CurrentWorkShop.FieldOfActivity)
                                                .Include(d => d.CurrentWorkShop.TechnicalProtectionCommitteeScans)
                                                .Include(d => d.Flows);
            if (phrase != null)
                list = list.Where(d => (d.FirstName + " " + d.LastName).Contains(phrase) || d.NationalCode.Contains(phrase) || d.UserName.Contains(phrase));

            count = list.Count();
            if (skip.HasValue)
                list = list.Skip(skip.Value);
            if (take.HasValue)
                list = list.Take(take.Value);
            return list.ToList();
        }
        public List<Document> GetAllWorkingDocuments(string username, out int count, int? skip = null, int? take = null, string phrase = null)
        {
            List<Flow.FlowType> waitingTypes = new List<Flow.FlowType>()
            {
                Flow.FlowType.SentToAssociation,
                Flow.FlowType.SentToCountyInspector,
                Flow.FlowType.SentToClub,
                Flow.FlowType.SentToProvinceInspector,
                Flow.FlowType.SentToProvinceManager,
                Flow.FlowType.SentToInspectionOffice,
            };
            var list = GetAllDocuments(username).Where(d => waitingTypes.Contains(d.Flows.OrderByDescending(x => x.DateCreated).FirstOrDefault().Type))
                                                .Include(d => d.HomeContact.County.Province)
                                                .Include(d => d.WorkContact.County.Province)
                                                .Include(d => d.CurrentWorkShop.ContractScans)
                                                .Include(d => d.CurrentWorkShop.ProductionType)
                                                .Include(d => d.CurrentWorkShop.FieldOfActivity)
                                                .Include(d => d.CurrentWorkShop.TechnicalProtectionCommitteeScans)
                                                .Include(d => d.Flows);
            if (phrase != null)
                list = list.Where(d => (d.FirstName + " " + d.LastName).Contains(phrase) || d.NationalCode.Contains(phrase) || d.UserName.Contains(phrase));

            count = list.Count();
            if (skip.HasValue)
                list = list.Skip(skip.Value);
            if (take.HasValue)
                list = list.Take(take.Value);
            return list.ToList();
        }
        public List<Document> GetAllWaitingDocuments(string username, out int count, int? skip = null, int? take = null, string phrase = null)
        {
            List<Flow.FlowType> waitingTypes = new List<Flow.FlowType>()
            {
                Flow.FlowType.Created,
                Flow.FlowType.RejectedOnce,
                Flow.FlowType.RejectedTwice,
                Flow.FlowType.NotCompleted
            };
            var list = GetAllDocuments(username).Where(d => waitingTypes.Contains(d.Flows.OrderByDescending(x => x.DateCreated).FirstOrDefault().Type))
                                                .Include(d => d.HomeContact.County.Province)
                                                .Include(d => d.WorkContact.County.Province)
                                                .Include(d => d.CurrentWorkShop.ContractScans)
                                                .Include(d => d.CurrentWorkShop.ProductionType)
                                                .Include(d => d.CurrentWorkShop.FieldOfActivity)
                                                .Include(d => d.CurrentWorkShop.TechnicalProtectionCommitteeScans)
                                                .Include(d => d.Flows);
            if (phrase != null)
                list = list.Where(d => (d.FirstName + " " + d.LastName).Contains(phrase) || d.NationalCode.Contains(phrase) || d.UserName.Contains(phrase));

            count = list.Count();
            if (skip.HasValue)
                list = list.Skip(skip.Value);
            if (take.HasValue)
                list = list.Take(take.Value);
            return list.ToList();
        }
        public List<Document> GetAllDoneDocuments(string username, out int count, int? skip = null, int? take = null, string phrase = null)
        {
            List<Flow.FlowType> waitingTypes = new List<Flow.FlowType>()
            {
                Flow.FlowType.Authorized
            };
            var list = GetAllDocuments(username).Where(d => waitingTypes.Contains(d.Flows.OrderByDescending(x => x.DateCreated).FirstOrDefault().Type))
                                                .Include(d => d.HomeContact.County.Province)
                                                .Include(d => d.WorkContact.County.Province)
                                                .Include(d => d.CurrentWorkShop.ContractScans)
                                                .Include(d => d.CurrentWorkShop.ProductionType)
                                                .Include(d => d.CurrentWorkShop.FieldOfActivity)
                                                .Include(d => d.CurrentWorkShop.TechnicalProtectionCommitteeScans)
                                                .Include(d => d.Flows);
            if (phrase != null)
                list = list.Where(d => (d.FirstName + " " + d.LastName).Contains(phrase) || d.NationalCode.Contains(phrase) || d.UserName.Contains(phrase));

            count = list.Count();
            if (skip.HasValue)
                list = list.Skip(skip.Value);
            if (take.HasValue)
                list = list.Take(take.Value);
            return list.ToList();
        }
        public List<Document> GetDocumentsForDataExtract(string username)
        {
            return GetAllDocuments(username).Include(d => d.HighSchoolCertification.FieldOfStudy)
                                            .Include(d => d.DiplomaCertification.FieldOfStudy)
                                            .Include(d => d.BachelorsCertification.FieldOfStudy)
                                            .Include(d => d.MastersCertification.FieldOfStudy)
                                            .Include(d => d.PhDCertification.FieldOfStudy)
                                            .Include(d => d.HomeContact.County.Province)
                                            .Include(d => d.CurrentWorkShop.FieldOfActivity)
                                            .Include(d => d.CurrentWorkShop.ProductionType)
                                            .Include(d => d.WorkContact.County.Province)
                                            .Include(d => d.Flows)
                                            .ToList();
        }
        
        public void AcceptDocument(string username, string documentUsername, string description)
        {
            Staff staff = dbContext.Staff.Where(s => s.UserName == username).FirstOrDefault();
            if (staff == null)
                throw new NotFoundException("کارمند");

            Document document = dbContext.Documents.Where(d => d.UserName == documentUsername).Include(d => d.Flows).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            if (document.DeactivationDate != null)
                throw new DeactivatedException("پرونده", document.ReasonOfDeactivation);
            if (document.CurrentFlow.Staff_ID != staff.Id)
                throw new NotAllowedException("تایید", "پرونده در دست شما نمی باشد");
            switch (document.CurrentFlow.Type)
            {
                case Flow.FlowType.SentToAssociation:
                    document.Flows.Add(flowEngine.SendUpToClub(document.Id, description));
                    break;
                case Flow.FlowType.SentToClub:
                    document.Flows.Add(flowEngine.SendUpToCountyInspector(document.Id, description));
                    break;
                case Flow.FlowType.SentToCountyInspector:
                    document.Flows.Add(flowEngine.SendUpToProvinceInspector(document.Id, description));
                    break;
                default:
                    throw new NotAllowedException("تایید مدارک پرونده", "پرونده در حالت مربوطه نمی باشد");
            }
            dbContext.SaveChanges();
        }

        public void RejectDocument(string username, string documentUsername, string description)
        {
            Staff staff = dbContext.Staff.Where(s => s.UserName == username).FirstOrDefault();
            if (staff == null)
                throw new NotFoundException("کارمند");

            Document document = dbContext.Documents.Where(d => d.UserName == documentUsername).Include(d => d.Flows).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            if (document.DeactivationDate != null)
                throw new DeactivatedException("پرونده", document.ReasonOfDeactivation);
            if (document.CurrentFlow.Staff_ID != staff.Id)
                throw new NotAllowedException("تایید", "پرونده در دست شما نمی باشد");
            switch (document.CurrentFlow.Type)
            {
                case Flow.FlowType.SentToAssociation:
                    document.Flows.Add(flowEngine.SendDownToExpert(document.Id, description));
                    break;
                case Flow.FlowType.SentToClub:
                    document.Flows.Add(flowEngine.SendDownToAssociation(document.Id, description));
                    break;
                case Flow.FlowType.SentToCountyInspector:
                    document.Flows.Add(flowEngine.SendDownToClub(document.Id, description));
                    break;
                case Flow.FlowType.SentToProvinceInspector:
                    document.Flows.Add(flowEngine.SendDownToCountyInspector(document.Id, description));
                    break;
                default:
                    throw new NotAllowedException("رد مدارک پرونده", "پرونده در حالت مربوطه نمی باشد");
            }
            dbContext.SaveChanges();
        }

        public void AuthorizeDocument(string username, string documentUsername, string description)
        {
            Staff staff = dbContext.Staff.Where(s => s.UserName == username).FirstOrDefault();
            if (staff == null)
                throw new NotFoundException("کارمند");

            Document document = dbContext.Documents.Where(d => d.UserName == documentUsername).Include(d => d.Flows).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            if (document.DeactivationDate != null)
                throw new DeactivatedException("پرونده", document.ReasonOfDeactivation);
            if (document.CurrentFlow.Staff_ID != staff.Id)
                throw new NotAllowedException("تایید", "پرونده در دست شما نمی باشد");

            switch (document.CurrentFlow.Type)
            {
                case Flow.FlowType.SentToInspectionOffice:
                case Flow.FlowType.SentToProvinceInspector:
                case Flow.FlowType.SentToProvinceManager:
                    document.Flows.Add(flowEngine.AuthorizeDocument(document.Id, description));
                    break;
                default:
                    throw new NotAllowedException("تایید صلاحیت پرونده", "پرونده در حالت مربوطه نمی باشد");
            }
            dbContext.SaveChanges();
        }

        public void UnauthorizeDocument(string username, string documentUsername, string description)
        {
            Staff staff = dbContext.Staff.Where(s => s.UserName == username).FirstOrDefault();
            if (staff == null)
                throw new NotFoundException("کارمند");

            Document document = dbContext.Documents.Where(d => d.UserName == documentUsername).Include(d => d.Flows).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            if (document.DeactivationDate != null)
                throw new DeactivatedException("پرونده", document.ReasonOfDeactivation);
            if (document.CurrentFlow.Staff_ID != staff.Id)
                throw new NotAllowedException("تایید", "پرونده در دست شما نمی باشد");

            switch (document.CurrentFlow.Type)
            {
                case Flow.FlowType.SentToInspectionOffice:
                    document.Flows.Add(flowEngine.RejectCompletely(document.Id, description));
                    break;
                case Flow.FlowType.SentToProvinceInspector:
                    document.Flows.Add(flowEngine.RejectOnce(document.Id, description));
                    break;
                case Flow.FlowType.SentToProvinceManager:
                    document.Flows.Add(flowEngine.RejectTwice(document.Id, description));
                    break;
                default:
                    throw new NotAllowedException("رد صلاحیت پرونده", "پرونده در حالت مربوطه نمی باشد");
            }
            dbContext.SaveChanges();
        }

        public Document GetLicense(string username, string documentUsername)
        {
            if (username != documentUsername && GetAllDocuments(username).Count(d => d.UserName == documentUsername) != 1)
                throw new NotFoundException("پرونده");

            Document document = dbContext.Documents.Where(d => d.UserName == documentUsername)
                                                   .Include(d => d.WorkContact.County.Province.ProvinceInspectors)
                                                   //.Include(d => d.Flows)
                                                   //.Include(d => d.CurrentWorkShop)
                                                   .FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");

            if (document.DeactivationDate != null)
                throw new DeactivatedException("پرونده", document.ReasonOfDeactivation);
            if (document.CurrentFlow.Type != Flow.FlowType.Authorized)
                throw new NotAllowedException("دریافت گواهینامه", "پرونده دارای صلاحیت نیست");
            return document;
        }
        public CurrentFlowReport GetCurrentFlowReport(string username)
        {
            var documents = GetAllDocuments(username).Include(d => d.Flows).Select(d => new
            {
                CurrentType = d.Flows.OrderByDescending(x => x.DateCreated).FirstOrDefault().Type,
                FlowCount = d.Flows.Count(),
            }).ToList();
            return new CurrentFlowReport()
            {
                AuthorizedCount = documents.Count(d => d.CurrentType == Flow.FlowType.Authorized),
                InAssociationCount = documents.Count(d => d.CurrentType == Flow.FlowType.SentToAssociation),
                InClubCount = documents.Count(d => d.CurrentType == Flow.FlowType.SentToClub),
                InCountyInspectorCount = documents.Count(d => d.CurrentType == Flow.FlowType.SentToCountyInspector),
                InInspectionOfficeCount = documents.Count(d => d.CurrentType == Flow.FlowType.SentToInspectionOffice),
                InProvinceInspectorCount = documents.Count(d => d.CurrentType == Flow.FlowType.SentToProvinceInspector),
                InProvinceManagerCount = documents.Count(d => d.CurrentType == Flow.FlowType.SentToProvinceManager),
                RejectedCount = documents.Count(d => d.FlowCount > 2 && (d.CurrentType == Flow.FlowType.NotCompleted || d.CurrentType == Flow.FlowType.RejectedOnce || d.CurrentType == Flow.FlowType.RejectedTwice)),
                NotSentCount = documents.Count(d => d.FlowCount <= 2 && d.CurrentType == Flow.FlowType.NotCompleted),
                NotRequestedCount = documents.Count(d => d.CurrentType == Flow.FlowType.Created),
            };
        }
        public AuthorizationByNumberReport GetAuthorizationByNumber(string username)
        {
            var documents = GetAllDocuments(username).Where(d => d.Flows.Any(f => f.Type == Flow.FlowType.Authorized)).Select(d => new
            {
                Type = (d.CurrentWorkShop.FemaleWorkersNo + d.CurrentWorkShop.MaleWorkersNo) > 25,
            }).ToList();

            return new AuthorizationByNumberReport()
            {
                Below25 = documents.Count(d => d.Type == false),
                Over25 = documents.Count(d => d.Type == true)
            };
        }
        public SafetyCertificationReport GetSafetyCertificationReport(string username)
        {
            var documents = GetAllDocuments(username).Select(d => new
            {
                Count16Hours = d.SafetyCertifications.Count(s => s.CourseType == Document.SafetyCertification.Type.SixteenHours),
                Count40Hours = d.SafetyCertifications.Count(s => s.CourseType == Document.SafetyCertification.Type.ForthyHours),
                CountOthers = d.SafetyCertifications.Count(s => s.CourseType == Document.SafetyCertification.Type.Others)
            }).ToList();

            return new SafetyCertificationReport()
            {
                SixteenHourse = documents.Sum(d => d.Count16Hours),
                ForthyHours = documents.Sum(d => d.Count40Hours),
                Others = documents.Sum(d => d.CountOthers)
            };
        }
        public RegisterHistogramReport GetRegisterHistogramReport(string username)
        {
            var documents = dbContext.Documents.OrderBy(d => d.DateCreated).Select(d => new
            {
                Month = d.DateCreated.Month,
                Year = d.DateCreated.Year,
            }).ToList();

            RegisterHistogramReport result = new RegisterHistogramReport();
            documents.Distinct().ToList().ForEach(m =>
            {
                result.Months.Add(new RegisterHistogramReport.Month()
                {
                    Count = documents.Count(d => d.Month == m.Month && d.Year == m.Year),
                    Sum = result.Months.Sum(s => s.Count),
                    Name = PersianDate.ConvertDate.ToFa(new DateTime(m.Year, m.Month, 1)),
                });
            });
            return result;
        }
        public List<Document> GetDocumentsByFilter(string username, out int count, string fullName, string mobile, Flow.FlowType? currentFlowType, DateTime? birthdayFrom, DateTime? birthdayTo, DateTime? registerDateFrom, DateTime? registerDateTo, DateTime? currentFlowDateFrom, DateTime? currentFlowDateTo, string nationalCode, string shenasname, string placeOfBirth, Document.GenderType? gender, Document.MaritalStates? maritalStatus, bool? olympiadRequest, Document.MilitaryServiceStates? militaryStatus, int? fieldOfStudyId, int? fieldOfActivityId, int? productionTypeId, int? homeProvinceId, int? homeCountyId, int? workProvinceId, int? workCountyId, string unitName, string employerFullName, string associationNumber, int? skip = null, int? take = null)
        {
            var result = GetAllDocuments(username);
            //fullname
            if (!string.IsNullOrEmpty(fullName))
                result = result.Where(d => (d.FirstName + " " + d.LastName).Contains(fullName));

            //mobile
            if (!string.IsNullOrEmpty(mobile))
                result = result.Where(d => d.Mobile.Contains(mobile));

            //currentFlowType
            if (currentFlowType.HasValue)
                result = result.Where(d => d.Flows.OrderByDescending(x => x.DateCreated).FirstOrDefault().Type == currentFlowType);

            //birthdayFrom
            if (birthdayFrom.HasValue)
                result = result.Where(d => d.BirthDate >= birthdayFrom);

            //birthdayTo
            if (birthdayTo.HasValue)
                result = result.Where(d => d.BirthDate <= birthdayTo);

            //registerDateFrom
            if (registerDateFrom.HasValue)
                result = result.Where(d => d.DateCreated >= registerDateFrom);

            //registerDateTo
            if (registerDateTo.HasValue)
                result = result.Where(d => d.DateCreated <= registerDateTo);

            //currentFlowDateFrom
            if (currentFlowDateFrom.HasValue)
                result = result.Where(d => d.Flows.OrderByDescending(x => x.DateCreated).FirstOrDefault().DateCreated >= currentFlowDateFrom);

            //currentFlowDateTo
            if (currentFlowDateTo.HasValue)
                result = result.Where(d => d.Flows.OrderByDescending(x => x.DateCreated).FirstOrDefault().DateCreated <= currentFlowDateTo);

            //nationalCode
            if (!string.IsNullOrEmpty(nationalCode))
                result = result.Where(d => d.NationalCode.Contains(nationalCode));

            //shenasname
            if (!string.IsNullOrEmpty(shenasname))
                result = result.Where(d => d.ShenasnameNo.Contains(shenasname));

            //placeOfBirth
            if (!string.IsNullOrEmpty(placeOfBirth))
                result = result.Where(d => d.PlaceOfBirth.Contains(placeOfBirth));

            //gender
            if (gender.HasValue)
                result = result.Where(d => d.Gender == gender);

            //maritalStatus
            if (maritalStatus.HasValue)
                result = result.Where(d => d.MaritalStatus == maritalStatus);

            //militaryStatus
            if (militaryStatus.HasValue)
                result = result.Where(d => d.MilitaryStatus == militaryStatus);

            //olympiadRequest
            if (olympiadRequest.HasValue)
                result = result.Where(d => olympiadRequest.Value == d.OlympiadRequestDate.HasValue);

            //fieldOfStudyId
            if (fieldOfStudyId.HasValue)
                result = result.Where(d => (d.HighSchoolCertification != null && d.HighSchoolCertification.FieldOfStudy_ID == fieldOfStudyId) ||
                                           (d.DiplomaCertification != null && d.DiplomaCertification.FieldOfStudy_ID == fieldOfStudyId) ||
                                           (d.BachelorsCertification != null && d.BachelorsCertification.FieldOfStudy_ID == fieldOfStudyId) ||
                                           (d.MastersCertification != null && d.MastersCertification.FieldOfStudy_ID == fieldOfStudyId) ||
                                           (d.PhDCertification != null && d.PhDCertification.FieldOfStudy_ID == fieldOfStudyId));

            //fieldOfActivityId
            if (fieldOfActivityId.HasValue)
                result = result.Where(d => d.CurrentWorkShop.FieldOfActivity.Id == fieldOfActivityId);

            //productionTypeId
            if (productionTypeId.HasValue)
                result = result.Where(d => d.CurrentWorkShop.ProductionType.Id == productionTypeId);

            //homeProvinceId
            if (homeProvinceId.HasValue)
                result = result.Where(d => d.HomeContact.County != null && d.HomeContact.County.Province_ID == homeProvinceId);

            //homeCountyId
            if (homeCountyId.HasValue)
                result = result.Where(d => d.HomeContact.County_ID == homeCountyId);

            //workProvinceId
            if (workProvinceId.HasValue)
                result = result.Where(d => d.WorkContact.County != null && d.WorkContact.County.Province_ID == workProvinceId);

            //workCountyId
            if (workCountyId.HasValue)
                result = result.Where(d => d.WorkContact.County_ID == workCountyId);

            //unitName
            if (!string.IsNullOrEmpty(unitName))
                result = result.Where(d => d.CurrentWorkShop.UnitName.Contains(unitName));

            //employerFullName
            if (!string.IsNullOrEmpty(employerFullName))
                result = result.Where(d => d.CurrentWorkShop.EmployerName.Contains(employerFullName));

            //associationNumber
            if (!string.IsNullOrEmpty(associationNumber))
                result = result.Where(d => d.AssociationMembershipNo.Contains(associationNumber));

            //RESULT
            count = result.Count();
            if (skip.HasValue)
                result = result.Skip(skip.Value);
            if (take.HasValue)
                result = result.Take(take.Value);

            //includes
            result = result.Include(d => d.CurrentWorkShop.TechnicalProtectionCommitteeScans)
                           .Include(d => d.CurrentWorkShop.ContractScans)
                           .Include(d => d.CurrentWorkShop.ProductionType)
                           .Include(d => d.CurrentWorkShop.FieldOfActivity);

            return result.ToList();
        }
    }
}
