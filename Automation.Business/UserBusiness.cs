﻿using Automation.Business.Inner;
using Automation.DataAccess;
using Automation.Model;
using Automation.Model.Exception;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Automation.Business
{
    public class UserBusiness
    {
        AutomationContext dbContext;
        Validation validation;
        SystemInfoBusiness systemInfo;
        public UserBusiness(AutomationContext dbContext, Validation validation, SystemInfoBusiness systemInfo)
        {
            this.dbContext = dbContext;
            this.validation = validation;
            this.systemInfo = systemInfo;
        }
        public User GetUser(string username, string password)
        {
            User user = GetUser(username);
            password = Utilities.TextToMD5(password);
            if (user.HashPassword.ToLower() != password.ToLower())
                throw new WrongInputException("نام کاربری یا گذرواژه", "");
            return user;
        }
        public User GetUser(string username)
        {
            User user = dbContext.Users.Where(u => u.UserName.ToLower() == username.ToLower()).FirstOrDefault();
            if (user == null)
                throw new WrongInputException("نام کاربری یا گذرواژه", "");
            if (user.Type != User.UserType.Admin && systemInfo.GetIsLocked())
                throw new NotAllowedException("ورود به سامانه", "سامانه در حالت غیرفعال می باشد");
            return user;
        }
        public String ResetPassword(string usernameOrEmail)
        {
            Document document = dbContext.Documents.Where(d => d.UserName.ToLower() == usernameOrEmail.ToLower() || d.Email.ToLower() == usernameOrEmail.ToLower()).FirstOrDefault();
            Thread.Sleep(1000);
            if (document == null)
                throw new WrongInputException("نام کاربری یا گذرواژه", "پرونده ای با این مشخصات در سامانه وجود ندارد");
            if (document.Email == null)
                throw new NotPossibleException("بازیابی اتوماتیک", "ایمیل برای این پرونده ثبت نشده است");
            return "KIRMAMA@ASD.com";
        }
        public void ChangePassword(string username, string oldPassword, string newPassword)
        {
            validation.ValidatePassword(newPassword);
            User user = GetUser(username, oldPassword);
            user.HashPassword = Utilities.TextToMD5(newPassword);
            dbContext.SaveChanges();
        }
        public void ModifyRecoveryEmail(string username, string email)
        {
            validation.ValidateEmail(email);
            validation.ValidateEmailIsUnique(username, email);
            User user = GetUser(username);
            user.Email = email;
            dbContext.SaveChanges();
        }
    }
}
