﻿using Automation.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Business
{
    public class LogBusiness
    {
        AutomationContext dbContext;
        public LogBusiness(AutomationContext dbContext)
        {
            this.dbContext = dbContext;
        }
        public void LogPostAction(string actionName, string ip, string username, string data)
        {
            var user = dbContext.Users.FirstOrDefault(u => u.UserName == username);
            dbContext.ActionLogs.Add(new Model.ActionLog()
            {
                ActionName = actionName,
                ActionType = "POST",
                IP = ip,
                User = user,
                Identity = username,
                Data = data,
            });
            dbContext.SaveChanges();
        }
    }
}
