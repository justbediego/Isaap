﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Business.CustomExceptions
{
    public class WrongInputException : Exception
    {
        public WrongInputException(string sourceName)
            : base(string.Format("اطلاعات وارد شده برای '{0}' اشتباه می باشد", sourceName)) { }
    }
    public class NotPossibleException : Exception
    {
        public NotPossibleException(string sourceName) : base(string.Format("تغییر در '{0}' امکان پذیر نمیباشد", sourceName)) { }
    }
}
