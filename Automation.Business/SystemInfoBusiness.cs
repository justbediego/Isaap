﻿using Automation.Business.Inner;
using Automation.DataAccess;
using Automation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using static System.Net.WebRequestMethods;
using System.Linq.Expressions;
using Automation.Model.Exception;

namespace Automation.Business
{
    public class SystemInfoBusiness
    {
        AutomationContext dbContext;
        Validation validation;
        public SystemInfoBusiness(AutomationContext dbContext, Validation validation)
        {
            this.dbContext = dbContext;
            this.validation = validation;
        }
        public List<Province> GetAllProvinces()
        {
            return dbContext.Provinces.OrderBy(o => o.Name).ToList();
        }
        public List<County> GetAllCounties()
        {
            return dbContext.Counties.OrderBy(o => o.Name).ToList();
        }
        public List<RequirementRule> GetAllRequirementRules()
        {
            return dbContext.RequirementRules.ToList();
        }
        public List<County> GetAllCounties(int provinceID)
        {
            return dbContext.Counties.Where(c => c.Province_ID == provinceID).OrderBy(o => o.Name).ToList();
        }
        public List<Association> GetAllAssociations()
        {
            return dbContext.Associations.ToList();
        }
        public List<Club> GetAllClubs()
        {
            return dbContext.Clubs.ToList();
        }
        public List<CountyInspector> GetAllCountyInspectors()
        {
            return dbContext.CountyInspectors.ToList();
        }
        public List<FieldOfStudy> GetAllFieldsOfStudy()
        {
            return dbContext.FieldsOfStudy.OrderBy(o => o.Name).OrderBy(f => f.Code).OrderBy(f => f.OrginalityCode).ToList();
        }
        public List<FieldOfActivity> GetAllFieldsOfActivity()
        {
            return dbContext.FieldsOfActivity.OrderBy(f => f.Name).ToList();
        }
        public List<ProductionType> GetAllProductionTypes()
        {
            return dbContext.ProductionTypes.OrderBy(p => p.Name).ToList();
        }
        public bool GetIsLocked()
        {
            Setting setting;
            try
            {
                setting = dbContext.Settings.Where(s => s.Key == Setting.KeyType.Lock).Single();
            }
            catch
            {
                throw new InternalException("تنظیمات قفل پیدا نگردید");
            }
            return setting.Value == Setting.ValueType.On;
        }
        public List<User> GetAllUsers()
        {
            return dbContext.Users.ToList();
        }
    }
}
