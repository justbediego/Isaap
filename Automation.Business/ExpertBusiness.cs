﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automation.DataAccess;
using Automation.Model;
using Automation.Business.Inner;
using System.Data.Entity;
using Automation.Model.Exception;

namespace Automation.Business
{
    public class ExpertBusiness
    {
        AutomationContext dbContext;
        Validation validation;
        FlowEngine flowEngine;
        public ExpertBusiness(AutomationContext dbContext, Validation validation, FlowEngine flowEngine)
        {
            this.dbContext = dbContext;
            this.validation = validation;
            this.flowEngine = flowEngine;
        }
        private Document GetDocument(string username)
        {
            Document document = dbContext.Documents.Where(d => d.UserName == username).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده", "علت نامشخص");
            if (document.DeactivationDate != null)
                throw new DeactivatedException("پرونده", document.ReasonOfDeactivation);
            return document;
        }
        public int CreateDocument(string username, string password)
        {
            validation.ValidateUserName(username);
            validation.ValidatePassword(password);
            Document document = new Document()
            {
                UserName = username,
                HashPassword = Utilities.TextToMD5(password),
                HomeContact = new Document.ExpertContact(),
                WorkContact = new Document.ExpertContact(),
            };
            document.Flows.Add(new Flow()
            {
                Type = Flow.FlowType.Created,
            });
            document.CurrentWorkShop = new Document.Workshop()
            {
                DateFrom = DateTime.Now,
            };
            dbContext.Documents.Add(document);
            dbContext.SaveChanges();
            return document.Id;
        }
        public void SendRequest(string username, Document.GenderType gender, string firstName, string lastName, string unitName, string employerName, Document.GenderType employerGender, string socialSecurityCode, int fieldOfActivityID, int productionTypeID, int maleWorkersNo, int femaleWorkersNo, int morningWorkersNo, int eveningWorkersNo, int nightWorkersNo, int safetyWorkersNo, bool technicalProtectionCommittee, string workshopAddress, string workshopPostalCode, string workshopPhone, double workshopSize, DateTime dateFrom, string email)
        {
            validation.ValidateFirstName(firstName);
            validation.ValidateLastName(lastName);
            validation.ValidateFullName(employerName);
            validation.ValidateUnitName(unitName);
            if (socialSecurityCode != null)
                validation.ValidateSocialSecurityCode(socialSecurityCode);
            validation.ValidatePostalCode(workshopPostalCode);
            validation.ValidateWorkshopSize(workshopSize);
            validation.ValidatePhone(workshopPhone);
            validation.ValidateAddress(workshopAddress);
            validation.ValidateAllNumberOfWorkers(maleWorkersNo, femaleWorkersNo, morningWorkersNo, eveningWorkersNo, nightWorkersNo, safetyWorkersNo);
            validation.ValidateEmail(email);
            validation.ValidateEmailIsUnique(username, email);
            //validateEmail
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(f => f.Flows).Include(d => d.CurrentWorkShop).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            if (document.CurrentFlow.Type != Flow.FlowType.Created)
                throw new NotPossibleException("ارسال پرونده", "حالت پرونده درست نمی باشد");
            if (document.DeactivationDate != null)
                throw new DeactivatedException("پرونده", document.ReasonOfDeactivation);
            document.Gender = gender;
            document.FirstName = firstName;
            document.LastName = lastName;
            document.CurrentWorkShop.UnitName = unitName;
            document.CurrentWorkShop.EmployerName = employerName;
            document.CurrentWorkShop.EmployerGender = employerGender;
            document.CurrentWorkShop.SocialSecurityCode = socialSecurityCode;
            document.CurrentWorkShop.FieldOfActivity = dbContext.FieldsOfActivity.Where(f => f.Id == fieldOfActivityID).SingleOrDefault();
            if (document.CurrentWorkShop.FieldOfActivity == null)
                throw new NotFoundException("نوع فعالیت");
            document.CurrentWorkShop.ProductionType = dbContext.ProductionTypes.Where(p => p.Id == productionTypeID).SingleOrDefault();
            if (document.CurrentWorkShop.ProductionType == null)
                throw new NotFoundException("نوع تولید");
            document.CurrentWorkShop.MaleWorkersNo = maleWorkersNo;
            document.CurrentWorkShop.FemaleWorkersNo = femaleWorkersNo;
            document.CurrentWorkShop.MorningWorkersNo = morningWorkersNo;
            document.CurrentWorkShop.EveningWorkersNo = eveningWorkersNo;
            document.CurrentWorkShop.NightWorkersNo = nightWorkersNo;
            document.CurrentWorkShop.SafetyWorkersNo = safetyWorkersNo;
            document.CurrentWorkShop.TechnicalProtectionCommittee = technicalProtectionCommittee;
            document.CurrentWorkShop.WorkshopAddress = workshopAddress;
            document.CurrentWorkShop.WorkshopPostalCode = workshopPostalCode;
            document.CurrentWorkShop.WorkshopPhone = workshopPhone;
            document.CurrentWorkShop.WorkshopSize = workshopSize;
            document.CurrentWorkShop.DateFrom = dateFrom;
            document.Email = email;
            document.Flows.Add(new Flow()
            {
                Type = Flow.FlowType.NotCompleted,
            });
            dbContext.SaveChanges();
        }     
        public void ModifyBasicInfo(string username, string firstName, string lastName, string fatherName, Document.GenderType gender, string shenasnameNo, string melliCode, DateTime birthDate, string placeOfBirth, Document.MaritalStates maritalStatus, Document.MilitaryServiceStates militaryServiceStates, string reasonOfExemption, string associationNumber)
        {
            validation.ValidateFirstName(firstName);
            validation.ValidateLastName(lastName);
            validation.ValidateFirstName(fatherName);
            validation.ValidateShenasnameNo(shenasnameNo);
            validation.ValidateNationalCode(melliCode);
            validation.ValidateBirthDate(birthDate);
            validation.ValidatePlaceOfBirth(placeOfBirth);
            if (!string.IsNullOrEmpty(associationNumber))
            {
                validation.ValidateAssociationMembershipNumber(associationNumber);
            }
            if (militaryServiceStates == Document.MilitaryServiceStates.Exempted)
            {
                validation.ValidateReasonOfExemption(reasonOfExemption);
            }
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            document.FirstName = firstName;
            document.LastName = lastName;
            document.FatherName = fatherName;
            document.ShenasnameNo = shenasnameNo;
            document.NationalCode = melliCode;
            document.BirthDate = birthDate;
            document.PlaceOfBirth = placeOfBirth;
            document.MaritalStatus = maritalStatus;
            document.MilitaryStatus = militaryServiceStates;
            document.AssociationMembershipNo = associationNumber;
            document.Gender = gender;
            if (militaryServiceStates == Document.MilitaryServiceStates.Exempted)
                document.ReasonOfExemption = reasonOfExemption;
            dbContext.SaveChanges();
        }
        public void ModifyContactInfo(string username, string mobilePhone, int homeCountyID, string homeAddress, string homePostalCode, string homePhone, int workCountyID, string workAddress, string workPostalCode, string workPhone)
        {
            validation.ValidateMobilePhone(mobilePhone);
            validation.ValidateAddress(homeAddress);
            validation.ValidatePostalCode(homePostalCode);
            validation.ValidatePhone(homePhone);
            validation.ValidateAddress(workAddress);
            validation.ValidatePostalCode(workPostalCode);
            validation.ValidatePhone(workPhone);
            County homeCounty = dbContext.Counties.Where(c => c.Id == homeCountyID).FirstOrDefault();
            if (homeCounty == null)
            {
                throw new NotFoundException("شهرستان محل سکونت");
            }
            County workCounty = dbContext.Counties.Where(c => c.Id == workCountyID).FirstOrDefault();
            if (workCounty == null)
            {
                throw new NotFoundException("شهرستان محل اشتغال");
            }
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            document.Mobile = mobilePhone;
            if (document.HomeContact == null)
            {
                document.HomeContact = new Document.ExpertContact();
            }
            if (document.WorkContact == null)
            {
                document.WorkContact = new Document.ExpertContact();
            }
            document.HomeContact.County = homeCounty;
            document.WorkContact.County = workCounty;
            document.HomeContact.Address = homeAddress;
            document.HomeContact.PostalCode = homePostalCode;
            document.HomeContact.Phone = homePhone;
            document.WorkContact.Address = workAddress;
            document.WorkContact.PostalCode = workPostalCode;
            document.WorkContact.Phone = workPhone;
            dbContext.SaveChanges();
        }
        public void ModifyAcademicInfo(string username, int? fieldOfHighSchool, int? fieldOfDiploma, int? fieldOfBachelor, int? fieldOfMaster, int? fieldOfPhD)
        {
            Document document = dbContext.Documents.Where(d => d.UserName == username)
                                        .Include(d => d.Flows)
                                        .Include(d => d.HighSchoolCertification.ScanFile)
                                        .Include(d => d.DiplomaCertification.ScanFile)
                                        .Include(d => d.BachelorsCertification.ScanFile)
                                        .Include(d => d.MastersCertification.ScanFile)
                                        .Include(d => d.PhDCertification.ScanFile)
                                        .FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            //highSchool
            if (fieldOfHighSchool.HasValue)
            {
                FieldOfStudy field = dbContext.FieldsOfStudy.Where(f => f.Id == fieldOfHighSchool).FirstOrDefault();
                if (field == null)
                    throw new NotFoundException("رشته تحصیلی");
                if (document.HighSchoolCertification == null)
                    document.HighSchoolCertification = new Document.AcademicCertification();
                document.HighSchoolCertification.FieldOfStudy = field;
            }
            else if (document.HighSchoolCertification != null)
            {
                if (document.HighSchoolCertification.ScanFile != null)
                    dbContext.Attachments.Remove(document.HighSchoolCertification.ScanFile);
                dbContext.AcademicCertifications.Remove(document.HighSchoolCertification);
            }
            //diploma
            if (fieldOfDiploma.HasValue)
            {
                FieldOfStudy field = dbContext.FieldsOfStudy.Where(f => f.Id == fieldOfDiploma).FirstOrDefault();
                if (field == null)
                    throw new NotFoundException("رشته تحصیلی");
                if (document.DiplomaCertification == null)
                    document.DiplomaCertification = new Document.AcademicCertification();
                document.DiplomaCertification.FieldOfStudy = field;
            }
            else if (document.DiplomaCertification != null)
            {
                if (document.DiplomaCertification.ScanFile != null)
                    dbContext.Attachments.Remove(document.DiplomaCertification.ScanFile);
                dbContext.AcademicCertifications.Remove(document.DiplomaCertification);
            }
            //bachelor
            if (fieldOfBachelor.HasValue)
            {
                FieldOfStudy field = dbContext.FieldsOfStudy.Where(f => f.Id == fieldOfBachelor).FirstOrDefault();
                if (field == null)
                    throw new NotFoundException("رشته تحصیلی");
                if (document.BachelorsCertification == null)
                    document.BachelorsCertification = new Document.AcademicCertification();
                document.BachelorsCertification.FieldOfStudy = field;
            }
            else if (document.BachelorsCertification != null)
            {
                if (document.BachelorsCertification.ScanFile != null)
                    dbContext.Attachments.Remove(document.BachelorsCertification.ScanFile);
                dbContext.AcademicCertifications.Remove(document.BachelorsCertification);
            }
            //master
            if (fieldOfMaster.HasValue)
            {
                FieldOfStudy field = dbContext.FieldsOfStudy.Where(f => f.Id == fieldOfMaster).FirstOrDefault();
                if (field == null)
                    throw new NotFoundException("رشته تحصیلی");
                if (document.MastersCertification == null)
                    document.MastersCertification = new Document.AcademicCertification();
                document.MastersCertification.FieldOfStudy = field;
            }
            else if (document.MastersCertification != null)
            {
                if (document.MastersCertification.ScanFile != null)
                    dbContext.Attachments.Remove(document.MastersCertification.ScanFile);
                dbContext.AcademicCertifications.Remove(document.MastersCertification);
            }
            //phd
            if (fieldOfPhD.HasValue)
            {
                FieldOfStudy field = dbContext.FieldsOfStudy.Where(f => f.Id == fieldOfPhD).FirstOrDefault();
                if (field == null)
                    throw new NotFoundException("رشته تحصیلی");
                if (document.PhDCertification == null)
                    document.PhDCertification = new Document.AcademicCertification();
                document.PhDCertification.FieldOfStudy = field;
            }
            else if (document.PhDCertification != null)
            {
                if (document.PhDCertification.ScanFile != null)
                    dbContext.Attachments.Remove(document.PhDCertification.ScanFile);
                dbContext.AcademicCertifications.Remove(document.PhDCertification);
            }
            dbContext.SaveChanges();
        }
        public void AddOrEditWorkExperience(string username, int workExperienceID, string unitName, string employerName, DateTime dateFrom, DateTime dateTo, string description)
        {
            validation.ValidateUnitName(unitName);
            validation.ValidateFullName(employerName);
            validation.ValidateDateFromAndDateTo(dateFrom, dateTo);
            Document document = dbContext.Documents
                                         .Where(d => d.UserName == username)
                                         .Include(d => d.Flows)
                                         .Include(d => d.WorkExperiences)
                                         .FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            Document.WorkExperience experience = null;
            if (workExperienceID == -1)
            {
                experience = new Document.WorkExperience();
                document.WorkExperiences.Add(experience);
            }
            else
            {
                experience = document.WorkExperiences.Where(e => e.Id == workExperienceID).FirstOrDefault();
                if (experience == null)
                    throw new NotFoundException("سابقه کاری");
            }
            experience.EmployerName = employerName;
            experience.WorkshopName = unitName;
            experience.Description = description;
            experience.DateTo = dateTo;
            experience.DateFrom = dateFrom;
            dbContext.SaveChanges();
        }
        public void RemoveExperience(string username, int workExperienceID)
        {
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.WorkExperiences).Include(d => d.Flows).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            Document.WorkExperience experience = document.WorkExperiences.Where(e => e.Id == workExperienceID).FirstOrDefault();
            if (experience == null)
                throw new NotFoundException("سابقه کاری");
            dbContext.WorkExperiences.Remove(experience);
            dbContext.SaveChanges();
        }
        public void AddOrEditCertification(string username, int certificationID, Document.SafetyCertification.Type courseType, string otherCourseName, int? otherDurationInHours, DateTime dateTaken, string issuerNumber, string description)
        {
            if (courseType == Document.SafetyCertification.Type.Others)
            {
                validation.ValidateCourseName(otherCourseName);
                validation.ValidateDurationInHours(otherDurationInHours);
            }
            validation.ValidateDateTaken(dateTaken);
            validation.ValidateIssuerName(issuerNumber);
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.SafetyCertifications).Include(d => d.Flows).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            Document.SafetyCertification certification = null;
            if (certificationID == -1)
            {
                certification = new Document.SafetyCertification();
                document.SafetyCertifications.Add(certification);
            }
            else
            {
                certification = document.SafetyCertifications.Where(c => c.Id == certificationID).FirstOrDefault();
                if (certification == null)
                    throw new NotFoundException("دوره آموزشی");
            }
            certification.CourseType = courseType;
            if (courseType == Document.SafetyCertification.Type.Others)
            {
                certification.OtherCourseName = otherCourseName;
                certification.OtherDurationInHours = otherDurationInHours.Value;
            }
            certification.DateTaken = dateTaken;
            certification.IssuerNumber = issuerNumber;
            certification.Description = description;
            dbContext.SaveChanges();
        }
        public void RemoveCertification(string username, int certificationID)
        {
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).Include(d => d.SafetyCertifications).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            Document.SafetyCertification safetyCertification = document.SafetyCertifications.Where(c => c.Id == certificationID).FirstOrDefault();
            if (safetyCertification == null)
                throw new NotFoundException("دوره آموزشی");
            if (safetyCertification.CertificationScan != null)
                dbContext.Attachments.Remove(safetyCertification.CertificationScan);
            dbContext.SafetyCertifications.Remove(safetyCertification);
            dbContext.SaveChanges();
        }

        #region PhotoScan

        public void ChangePhotoScan(string username, string filename, byte[] fileData)
        {
            validation.ValidatePhoto(fileData);
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).Include(d => d.PhotoScan.AttachmentData).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            if (document.PhotoScan != null)
                dbContext.Attachments.Remove(document.PhotoScan);
            document.PhotoScan = new Attachment()
            {
                AttachmentData = new AttachmentData()
                {
                    FileData = fileData,
                },
                Filename = filename,
            };
            dbContext.SaveChanges();
        }
        public void RemovePhotoScan(string username)
        {
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).Include(d => d.PhotoScan).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            if (document.PhotoScan != null)
            {
                dbContext.Attachments.Remove(document.PhotoScan);
            }
            dbContext.SaveChanges();
        }

        #endregion

        #region Shenasname

        public void AddShenasnameScan(string username, string filename, byte[] fileData)
        {
            validation.ValidatePhoto(fileData);
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).Include(d => d.ShenasnameScans).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            document.ShenasnameScans.Add(new Attachment()
            {
                AttachmentData = new AttachmentData()
                {
                    FileData = fileData,
                },
                Filename = filename,
            });
            dbContext.SaveChanges();
        }
        public void RemoveShenasnameScan(string username, int shenasnameID)
        {
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).Include(d => d.ShenasnameScans).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            if (document.ShenasnameScans == null)
                throw new NotFoundException("تصویر شناسنامه");
            Attachment shenasnameScan = document.ShenasnameScans.Where(sh => sh.Id == shenasnameID).FirstOrDefault();
            if (shenasnameScan == null)
                throw new NotFoundException("تصویر شناسنامه");
            dbContext.Attachments.Remove(shenasnameScan);
            dbContext.SaveChanges();
        }


        #endregion

        #region NationalCard

        public void AddNationalCardScan(string username, string filename, byte[] fileData)
        {
            validation.ValidatePhoto(fileData);
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).Include(d => d.NationalCardScans).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            document.NationalCardScans.Add(new Attachment()
            {
                AttachmentData = new AttachmentData()
                {
                    FileData = fileData,
                },
                Filename = filename,
            });
            dbContext.SaveChanges();
        }

        public void RemoveNationalCardScan(string username, int nationalCardID)
        {
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).Include(d => d.NationalCardScans).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            if (document.NationalCardScans == null)
                throw new NotFoundException("تصویر کارت ملی");
            Attachment nationalCardScan = document.NationalCardScans.Where(n => n.Id == nationalCardID).FirstOrDefault();
            if (nationalCardScan == null)
                throw new NotFoundException("تصویر کارت ملی");
            dbContext.Attachments.Remove(nationalCardScan);
            dbContext.SaveChanges();
        }

        #endregion

        #region HighSchool

        public void ChangeHighSchoolScan(string username, string filename, byte[] fileData)
        {
            validation.ValidatePhoto(fileData);
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).Include(d => d.HighSchoolCertification.ScanFile).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            if (document.HighSchoolCertification == null)
                throw new NotPossibleException("تغییر تصویر مدرک دیپلم", "مدرک دیپلم وجود ندارد");
            if (document.HighSchoolCertification.ScanFile != null)
                dbContext.Attachments.Remove(document.HighSchoolCertification.ScanFile);
            document.HighSchoolCertification.ScanFile = new Attachment()
            {
                AttachmentData = new AttachmentData()
                {
                    FileData = fileData,
                },
                Filename = filename,
            };
            dbContext.SaveChanges();
        }

        public void RemoveHighSchoolScan(string username)
        {
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).Include(d => d.HighSchoolCertification.ScanFile).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            if (document.HighSchoolCertification == null)
            {
                throw new NotPossibleException("حذف مدرک دیپلم", "مدرک دیپلم وجود ندارد");
            }
            if (document.HighSchoolCertification.ScanFile != null)
            {
                dbContext.Attachments.Remove(document.HighSchoolCertification.ScanFile);
            }
            dbContext.SaveChanges();
        }

        #endregion

        #region Diploma

        public void ChangeDiplomaScan(string username, string filename, byte[] fileData)
        {
            validation.ValidatePhoto(fileData);
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).Include(d => d.DiplomaCertification.ScanFile).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            if (document.DiplomaCertification == null)
                throw new NotPossibleException("تغییر تصویر مدرک کاردانی", "مدرک کاردانی وجود ندارد");
            if (document.DiplomaCertification.ScanFile != null)
                dbContext.Attachments.Remove(document.DiplomaCertification.ScanFile);
            document.DiplomaCertification.ScanFile = new Attachment()
            {
                AttachmentData = new AttachmentData()
                {
                    FileData = fileData,
                },
                Filename = filename,
            };
            dbContext.SaveChanges();
        }

        public void RemoveDiplomaScan(string username)
        {
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).Include(d => d.DiplomaCertification.ScanFile).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            if (document.DiplomaCertification == null)
            {
                throw new NotPossibleException("حذف مدرک کاردانی", "مدرک کاردانی وجود ندارد");
            }
            if (document.DiplomaCertification.ScanFile != null)
            {
                dbContext.Attachments.Remove(document.DiplomaCertification.ScanFile);
            }
            dbContext.SaveChanges();
        }

        #endregion

        #region Bachelor

        public void ChangeBachelorScan(string username, string filename, byte[] fileData)
        {
            validation.ValidatePhoto(fileData);
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).Include(d => d.BachelorsCertification.ScanFile).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            if (document.BachelorsCertification == null)
                throw new NotPossibleException("تغییر تصویر مدرک کارشناسی", "مدرک کارشناسی وجود ندارد");
            if (document.BachelorsCertification.ScanFile != null)
                dbContext.Attachments.Remove(document.BachelorsCertification.ScanFile);
            document.BachelorsCertification.ScanFile = new Attachment()
            {
                AttachmentData = new AttachmentData()
                {
                    FileData = fileData,
                },
                Filename = filename,
            };
            dbContext.SaveChanges();
        }

        public void RemoveBachelorScan(string username)
        {
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).Include(d => d.BachelorsCertification.ScanFile).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            if (document.BachelorsCertification == null)
            {
                throw new NotPossibleException("حذف مدرک کارشناسی", "مدرک کارشناسی وجود ندارد");
            }
            if (document.BachelorsCertification.ScanFile != null)
            {
                dbContext.Attachments.Remove(document.BachelorsCertification.ScanFile);
            }
            dbContext.SaveChanges();
        }

        #endregion

        #region Master

        public void ChangeMasterScan(string username, string filename, byte[] fileData)
        {
            validation.ValidatePhoto(fileData);
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).Include(d => d.MastersCertification.ScanFile).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            if (document.MastersCertification == null)
                throw new NotPossibleException("تغییر تصویر مدرک کارشناسی ارشد", "مدرک کارشناسی ارشد وجود ندارد");
            if (document.MastersCertification.ScanFile != null)
                dbContext.Attachments.Remove(document.MastersCertification.ScanFile);
            document.MastersCertification.ScanFile = new Attachment()
            {
                AttachmentData = new AttachmentData()
                {
                    FileData = fileData,
                },
                Filename = filename,
            };
            dbContext.SaveChanges();
        }

        public void RemoveMasterScan(string username)
        {
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).Include(d => d.MastersCertification.ScanFile).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            if (document.MastersCertification == null)
            {
                throw new NotPossibleException("حذف مدرک کارشناسی ارشد", "مدرک کارشناسی ارشد وجود ندارد");
            }
            if (document.MastersCertification.ScanFile != null)
            {
                dbContext.Attachments.Remove(document.MastersCertification.ScanFile);
            }
            dbContext.SaveChanges();
        }

        #endregion

        #region Phd

        public void ChangePhDScan(string username, string filename, byte[] fileData)
        {
            validation.ValidatePhoto(fileData);
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).Include(d => d.PhDCertification.ScanFile).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            if (document.PhDCertification == null)
                throw new NotPossibleException("تغییر تصویر مدرک دکترا", "مدرک دکترا وجود ندارد");
            if (document.PhDCertification.ScanFile != null)
                dbContext.Attachments.Remove(document.PhDCertification.ScanFile);
            document.PhDCertification.ScanFile = new Attachment()
            {
                AttachmentData = new AttachmentData()
                {
                    FileData = fileData,
                },
                Filename = filename,
            };
            dbContext.SaveChanges();
        }

        public void RemovePhdScan(string username)
        {
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).Include(d => d.PhDCertification.ScanFile).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            if (document.PhDCertification == null)
            {
                throw new NotPossibleException("حذف مدرک دکترا", "مدرک دکترا وجود ندارد");
            }
            if (document.PhDCertification.ScanFile != null)
            {
                dbContext.Attachments.Remove(document.PhDCertification.ScanFile);
            }
            dbContext.SaveChanges();
        }

        #endregion

        #region Certification

        public void ChangeCertificationScan(string username, int certificationID, string filename, byte[] fileData)
        {
            validation.ValidatePhoto(fileData);
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).Include(d => d.SafetyCertifications.Select(sc => sc.CertificationScan)).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            Document.SafetyCertification certification = document.SafetyCertifications.Where(c => c.Id == certificationID).FirstOrDefault();
            if (certification == null)
                throw new NotFoundException("مدرک ایمنی");
            if (certification.CertificationScan != null)
                dbContext.Attachments.Remove(certification.CertificationScan);
            certification.CertificationScan = new Attachment()
            {
                AttachmentData = new AttachmentData()
                {
                    FileData = fileData,
                },
                Filename = filename,
            };
            dbContext.SaveChanges();
        }
        public void RemoveCertificationScan(string username, int certificationID)
        {
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).Include(d => d.SafetyCertifications.Select(c => c.CertificationScan)).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            Document.SafetyCertification certification = document.SafetyCertifications.Where(c => c.Id == certificationID).FirstOrDefault();
            if (certification == null)
                throw new NotFoundException("مدرک ایمنی");
            if (certification.CertificationScan != null)
            {
                dbContext.Attachments.Remove(certification.CertificationScan);
            }
            dbContext.SaveChanges();
        }

        #endregion

        #region Insurance

        public void AddInsuranceScan(string username, string filename, byte[] fileData)
        {
            validation.ValidatePhoto(fileData);
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).Include(d => d.InsuranceRecordScans).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            document.InsuranceRecordScans.Add(new Attachment()
            {
                AttachmentData = new AttachmentData()
                {
                    FileData = fileData,
                },
                Filename = filename,
            });
            dbContext.SaveChanges();
        }

        public void RemoveInsuranceScan(string username, int scanID)
        {
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).Include(d => d.InsuranceRecordScans).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            Attachment insuranceScan = document.InsuranceRecordScans.Where(s => s.Id == scanID).FirstOrDefault();
            if (insuranceScan == null)
                throw new NotFoundException("تصویر بیمه");
            dbContext.Attachments.Remove(insuranceScan);
            dbContext.SaveChanges();
        }

        #endregion

        #region Contract

        public void AddContractScan(string username, string filename, byte[] fileData)
        {
            validation.ValidatePhoto(fileData);
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).Include(d => d.CurrentWorkShop.ContractScans).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            document.CurrentWorkShop.ContractScans.Add(new Attachment()
            {
                AttachmentData = new AttachmentData()
                {
                    FileData = fileData,
                },
                Filename = filename,
            });
            dbContext.SaveChanges();
        }

        public void RemoveContractScan(string username, int scanID)
        {
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).Include(d => d.CurrentWorkShop.ContractScans).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            Attachment contractScan = document.CurrentWorkShop.ContractScans.Where(s => s.Id == scanID).FirstOrDefault();
            if (contractScan == null)
                throw new NotFoundException("تصویر قرارداد");
            dbContext.Attachments.Remove(contractScan);
            dbContext.SaveChanges();
        }

        #endregion

        #region TPC

        public void AddTPCScan(string username, string filename, byte[] fileData)
        {
            validation.ValidatePhoto(fileData);
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).Include(d => d.CurrentWorkShop.TechnicalProtectionCommitteeScans).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            document.CurrentWorkShop.TechnicalProtectionCommitteeScans.Add(new Attachment()
            {
                AttachmentData = new AttachmentData()
                {
                    FileData = fileData,
                },
                Filename = filename,
            });
            dbContext.SaveChanges();
        }
        public void RemoveTPCScan(string username, int scanID)
        {
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).Include(d => d.CurrentWorkShop.TechnicalProtectionCommitteeScans).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            Attachment TPCScan = document.CurrentWorkShop.TechnicalProtectionCommitteeScans.Where(s => s.Id == scanID).FirstOrDefault();
            if (TPCScan == null)
                throw new NotFoundException("تصویر کمیته فنی");
            dbContext.Attachments.Remove(TPCScan);
            dbContext.SaveChanges();
        }

        #endregion

        public void SignAgreement(string username)
        {
            Document document = GetDocument(username);// dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateEditableDocument(document);
            if (!document.AcademicInfoCompleted.IsComplete)
                throw new IncompleteException("پرونده", document.AcademicInfoCompleted.Message);
            if (!document.BasicAttachmentsCompleted.IsComplete)
                throw new IncompleteException("پرونده", document.BasicAttachmentsCompleted.Message);
            if (!document.BasicInfoCompleted.IsComplete)
                throw new IncompleteException("پرونده", document.BasicInfoCompleted.Message);
            if (!document.CertificationAttachmentsCompleted.IsComplete)
                throw new IncompleteException("پرونده", document.CertificationAttachmentsCompleted.Message);
            if (!document.ContactInfoCompleted.IsComplete)
                throw new IncompleteException("پرونده", document.ContactInfoCompleted.Message);
            if (!document.WorkshopAttachmentsCompleted.IsComplete)
                throw new IncompleteException("پرونده", document.WorkshopAttachmentsCompleted.Message);
            if (!document.CertificationCompleted.IsComplete)
                throw new IncompleteException("پرونده", document.CertificationCompleted.Message);

            //List<Document> doneDocuments = dbContext.Documents.Where(d => d.NationalCode == document.NationalCode)
            //                                                  .ToList()
            //                                                  .Where(d => d.CurrentFlow.Type == Flow.FlowType.Authorized || d.Id == document.Id)
            //                                                  .ToList();
            //int sumHours = 0;
            //doneDocuments.ForEach(doneDocument =>
            //{
            //    RequirementRule rule = dbContext.RequirementRules.Where(r => r.SizeCategory == doneDocument.CurrentWorkShop.SizeCategory &&
            //                                                                 r.DangerLevel == doneDocument.CurrentWorkShop.FieldOfActivity.DangerLevel &&
            //                                                                 r.LevelOfEducation == doneDocument.LevelOfEducation)
            //                                                     .FirstOrDefault();
            //    if (rule != null)
            //        sumHours += rule.Hours;
            //    else
            //    {
            //        throw new NotAllowedToWork();
            //    }
            //});
            //if (sumHours > 176)
            //    throw new NoHoursAvailable(document.NationalCode);
            document.Flows.Add(flowEngine.SendUpToAssociation(document.Id, null));
            dbContext.SaveChanges();
        }
        public void ObjectifyOnce(string username, string objection)
        {
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).Include(d => d.ObjectionAttachments).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            if (document.CurrentFlow.Type != Flow.FlowType.RejectedOnce)
                throw new NotPossibleException("درخواست اعتراض بار اول", "پرونده در حالت درست نمی باشد");
            if (document.DeactivationDate != null)
                throw new DeactivatedException("پرونده", document.ReasonOfDeactivation);
            Flow flow = flowEngine.ObjectifyOnce(document.Id, objection);
            flow.Attachments.AddRange(document.ObjectionAttachments);
            document.ObjectionAttachments.Clear();
            document.Flows.Add(flow);
            dbContext.SaveChanges();
        }
        public void ObjectifyTwice(string username, string objection)
        {
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).Include(d => d.ObjectionAttachments).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            if (document.CurrentFlow.Type != Flow.FlowType.RejectedTwice)
                throw new NotPossibleException("درخواست اعتراض بار دوم", "پرونده در حالت درست نمی باشد");
            if (document.DeactivationDate != null)
                throw new DeactivatedException("پرونده", document.ReasonOfDeactivation);
            Flow flow = flowEngine.ObjectifyTwice(document.Id, objection);
            flow.Attachments.AddRange(document.ObjectionAttachments);
            document.ObjectionAttachments.Clear();
            document.Flows.Add(flow);
            dbContext.SaveChanges();
        }
        public void AddObjectionScan(string username, string filename, byte[] fileData)
        {
            validation.ValidatePhoto(fileData);
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.Flows).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            if (document.DeactivationDate != null)
                throw new DeactivatedException("پرونده", document.ReasonOfDeactivation);
            if (document.CurrentFlow.Type != Flow.FlowType.RejectedOnce &&
               document.CurrentFlow.Type != Flow.FlowType.RejectedTwice)
            {
                throw new NotAllowedException("بارگذاری ضمیمه", "پرونده در حالت درست نمی باشد");
            }
            document.ObjectionAttachments.Add(new Attachment()
            {
                AttachmentData = new AttachmentData()
                {
                    FileData = fileData,
                },
                Filename = filename,
            });
            dbContext.SaveChanges();
        }
        public void RemoveObjectionScan(string username, int scanID)
        {
            Document document = dbContext.Documents.Where(d => d.UserName == username).Include(d => d.ObjectionAttachments).Include(d => d.Flows).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            if (document.CurrentFlow.Type != Flow.FlowType.RejectedOnce &&
               document.CurrentFlow.Type != Flow.FlowType.RejectedTwice)
            {
                throw new NotAllowedException("حذف ضمیمه", "پرونده در حالت درست نمی باشد");
            }
            if (document.DeactivationDate != null)
                throw new DeactivatedException("پرونده", document.ReasonOfDeactivation);
            Attachment objectionScan = document.ObjectionAttachments.Where(s => s.Id == scanID).FirstOrDefault();
            if (objectionScan == null)
                throw new NotFoundException("تصویر ضمیمه");
            dbContext.Attachments.Remove(objectionScan);
            dbContext.SaveChanges();
        }
    }
}
