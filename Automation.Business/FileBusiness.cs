﻿using Automation.DataAccess;
using Automation.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Automation.Model.Exception;

namespace Automation.Business
{
    public class FileBusiness
    {
        private AutomationContext dbContext;
        public FileBusiness(AutomationContext dbContext)
        {
            this.dbContext = dbContext;
        }
        public Attachment GetReportFile(Guid reportKey)
        {
            Attachment attachment = dbContext.Attachments.Where(a => a.Key == reportKey && a.ExpertReport_ID != null).Include(a => a.AttachmentData).FirstOrDefault();
            if (attachment == null)
            {
                throw new NotFoundException("فایل", "کد فایل اشتباه می باشد");
            }
            return attachment;
        }
        public Attachment GetImage(Guid imageKey, int width, int height)
        {
            Attachment attachment = dbContext.Attachments.Where(a => a.Key == imageKey).Include(a => a.AttachmentData).FirstOrDefault();
            if (attachment == null)
            {
                throw new NotFoundException("فایل", "کد تصویر اشتباه می باشد");
            }
            Bitmap image = null;
            MemoryStream InputBinary = new MemoryStream(attachment.AttachmentData.FileData, false);
            try
            {
                image = new Bitmap(InputBinary);
            }
            catch (Exception ex)
            {
                throw new InternalBufferOverflowException("تبدیل به تصویر: " + ex.Message);
            }
            MemoryStream OutputBinary = new MemoryStream();
            //resize
            int newHeight = image.Height;
            int newWidth = image.Width;
            if (height != -1 && width == -1)
            {
                newHeight = height;
                newWidth = (int)((float)height / image.Height * image.Width);
            }
            if (height == -1 && width != -1)
            {
                newWidth = width;
                newHeight = (int)((float)width / image.Width * image.Height);
            }
            if (height != -1 && width != -1)
            {
                float fillRate = (float)height / width;
                float originalRate = (float)image.Height / image.Width;
                if (fillRate > originalRate)
                {
                    newHeight = height;
                    newWidth = (int)((float)height / image.Height * image.Width);
                }
                else
                {
                    newWidth = width;
                    newHeight = (int)((float)width / image.Width * image.Height);
                }
            }
            image = new Bitmap(image, newWidth, newHeight);
            //crop and fill
            if (height != -1 && width != -1)
            {
                Rectangle cropArea = new Rectangle()
                {
                    X = (newWidth - width) / 2,
                    Y = (newHeight - height) / 2,
                    Width = width,
                    Height = height,
                };
                image = image.Clone(cropArea, image.PixelFormat);
            }
            image.Save(OutputBinary, ImageFormat.Jpeg);
            return new Attachment()
            {
                DateCreated = attachment.DateCreated,
                DateModified = attachment.DateModified,
                Filename = attachment.Filename,
                Id = attachment.Id,
                AttachmentData = new AttachmentData()
                {
                    FileData = OutputBinary.ToArray(),
                },
            };
        }
    }
}
