﻿using Automation.DataAccess;
using Automation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Automation.Model.Exception;
using Automation.BusinessNew.DTO;
using Automation.BusinessNew.DTO.Document;

namespace Automation.BusinessNew
{
    public class ExpertBusiness
    {
        AutomationContext dbContext;
        ValidationBusiness validation;

        public ExpertBusiness(AutomationContext dbContext, ValidationBusiness validation)
        {
            this.dbContext = dbContext;
            this.validation = validation;
        }

        public void UpdateSeasonalReport(string username, DateTime forDate, string filename, byte[] fileData)
        {
            validation.ValidateReportFile(fileData);
            var availableSeasons = GetAvailableSeasonalReports(username);
            var seasonInfo = UtilitiesBusiness.GetSeasonFromDate(forDate);
            var matched = availableSeasons.FirstOrDefault(a => a.Season == seasonInfo.Season && a.Year == seasonInfo.Year);
            if (matched == null)
                throw new WrongInputException("زمان گزارش", "ارسال گزارش برای این تاریخ امکان پذیر نمی باشد");
            if (matched.IsAccepted)
                throw new NotAllowedException("ایجاد تغییرات", "گزارشات تایید شده قابل تغییر نمی باشند");
            if (matched.dayDifference > 120)
                throw new NotAllowedException("ایجاد تغییرات", "بیشتر از 30 روز از زمان آپلود گذشته است");
            if (matched.dayDifference < 90)
                throw new NotAllowedException("ایجاد تغییرات", "آپلود زودتر از زمان مقرر امکان پذیر نمی باشد");
            Document document = dbContext.Documents.Where(d => d.UserName == username).FirstOrDefault();
            ExpertReport report = null;
            if (matched.ReportID.HasValue)
            {
                report = dbContext.ExpertReports.Where(r => r.Id == matched.ReportID).Include(r => r.Files).FirstOrDefault();
                while (report.Files.Any())
                    dbContext.Attachments.Remove(report.Files[0]);
            }
            else
            {
                report = new ExpertReport()
                {
                    ForDate = forDate,
                    ReportType = ExpertReport.ReportTypes.Seasonal,
                };
                document.Reports.Add(report);
            }
            report.Files.Add(new Attachment()
            {
                AttachmentData = new AttachmentData()
                {
                    FileData = fileData,
                },
                Filename = filename,
            });
            dbContext.SaveChanges();
        }
        public List<SeasonalReportDTO> GetAvailableSeasonalReports(string username)
        {
            Document document = dbContext.Documents
                                         .Where(d => d.UserName == username)
                                         .Include(d => d.Reports.Select(r => r.Files))
                                         .Include(d => d.Reports.Select(r => r.Comments))
                                         .FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            validation.ValidateAuthorizedDocument(document);
            DateTime dateOfAuthorization = document.CurrentFlow.DateCreated;
            List<SeasonalReportDTO> result = new List<SeasonalReportDTO>();
            while (dateOfAuthorization < DateTime.Now)
            {
                var season = UtilitiesBusiness.GetSeasonFromDate(dateOfAuthorization);
                var report = document.Reports.Where(r => r.ReportType == ExpertReport.ReportTypes.Seasonal)
                    .Where(r =>
                    {
                        var reportSeason = UtilitiesBusiness.GetSeasonFromDate(r.ForDate);
                        return season.Year == reportSeason.Year && season.Season == reportSeason.Season;
                    }).SingleOrDefault();
                result.Add(new SeasonalReportDTO()
                {
                    Year = season.Year,
                    Season = season.Season,
                    dayDifference = (int)(DateTime.Now - UtilitiesBusiness.GetDateFromSeason(season.Year, season.Season)).TotalDays,
                    ReportID = report == null ? null : (int?)report.Id,
                    IsVisible = report == null ? false : report.IsVisible,
                    IsAccepted = report == null ? false : report.Comments.Any(c => c.IsAccepted),
                    ReportFileName = report == null ? null : report.Files.Any() ? report.Files[0].Filename : null,
                    ReportFileKey = report == null ? null : report.Files.Any() ? (Guid?)report.Files[0].Key : null,
                    Comments = report == null ? null : report.Comments.Select(c => new ExpertReportComment()
                    {
                        Text = c.Text,
                        DateCreated = c.DateCreated,
                        IsAccepted = c.IsAccepted
                    }).ToList()
                });
                dateOfAuthorization = dateOfAuthorization.AddMonths(3);
            }
            return result.OrderByDescending(r => r.Year).ThenByDescending(r => r.Season).ToList();
        }
        public void RemoveReport(string username, int reportID)
        {
            var availableSeasons = GetAvailableSeasonalReports(username);
            var matched = availableSeasons.FirstOrDefault(a => a.ReportID == reportID);
            if (matched == null)
                throw new NotFoundException("گزارش");
            if (matched.IsAccepted)
                throw new NotAllowedException("ایجاد تغییرات", "گزارشات تایید شده قابل تغییر نمی باشند");
            ExpertReport report = dbContext.ExpertReports.Include(r => r.Files).FirstOrDefault(r => r.Id == reportID);
            while (report.Files.Any())
                dbContext.Attachments.Remove(report.Files[0]);
            dbContext.SaveChanges();
        }
        public void ToggleReportVisibility(string username, int reportID)
        {
            var availableSeasons = GetAvailableSeasonalReports(username);
            var matched = availableSeasons.FirstOrDefault(a => a.ReportID == reportID);
            if (matched == null)
                throw new NotFoundException("گزارش");
            if (matched.IsAccepted)
                throw new NotAllowedException("ایجاد تغییرات", "گزارشات تایید شده قابل تغییر نمی باشند");
            ExpertReport report = dbContext.ExpertReports.FirstOrDefault(r => r.Id == reportID);
            report.IsVisible = !report.IsVisible;
            dbContext.SaveChanges();
        }
        public DocumentDTO GetDocumentForOverview(string username)
        {
            Document document = dbContext.Documents.Where(d => d.UserName == username).FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده", "علت نامشخص");
            if (document.DeactivationDate != null)
                throw new DeactivatedException("پرونده", document.ReasonOfDeactivation);
            List<Document> otherDocuments = dbContext.Documents.Where(d => document.NationalCode.Length == 10 && d.NationalCode == document.NationalCode && d.Id != document.Id).ToList();
            return DocumentDTO.LoadOverviewData(document, otherDocuments, new List<int>());
        }
    }
}
