﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.BusinessNew.DTO
{
    public class ExpertReportComment
    {
        public DateTime DateCreated;
        public bool IsAccepted;
        public string Text;
        public string PersianDateCreated
        {
            get
            {
                return PersianDate.ConvertDate.ToFa(DateCreated);
            }
        }
        public string TimeCreated
        {
            get
            {
                return DateCreated.ToString("HH:mm");
            }
        }
    }
}
