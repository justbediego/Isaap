﻿using Automation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.BusinessNew.DTO
{
    public class ExpertReportOverview
    {
        public int Id;
        public bool IsAccepted;
        public DateTime ForDate;
        public UtilitiesBusiness.PersianSeason SeasonData
        {
            get
            {
                return UtilitiesBusiness.GetSeasonFromDate(ForDate);
            }
        }
        public string PersianForDate { get { return PersianDate.ConvertDate.ToFa(ForDate); } }
        public ExpertReport.ReportTypes ReportType;
        public DateTime DateCreated;
        public string PersianDateCreated { get { return PersianDate.ConvertDate.ToFa(DateCreated); } }
        public DateTime DateModified;
        public string PersianDateModified { get { return PersianDate.ConvertDate.ToFa(DateModified); } }
        public Guid PhotoScan_Key;
        public string FullName;
        public string UnitName;
        public string NezamCode;
        public string WorkProvince;
        public string WorkCounty;
        public Guid File_Key;
        public string FileName;
    }
}
