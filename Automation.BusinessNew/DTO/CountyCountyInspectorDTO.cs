﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.BusinessNew.DTO
{
    public class CountyCountyInspectorDTO
    {
        public int ID;
        public string ProvinceName;
        public string Name;
        public CountyInspectorDTO CountyInspector;
        public class CountyInspectorDTO
        {
            public int ID;
            public string Username;
            public string Password;
            public bool Activated;
            public int CurrentDocuments;
        }
    }
}
