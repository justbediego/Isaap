﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.BusinessNew.DTO
{
    public class ProvinceClubDTO
    {
        public int ID;
        public string ChairmanFullName;
        public string HeadFullName;
        public string Name;
        public ClubDTO Club;
        public class ClubDTO
        {
            public int ID;
            public string Username;
            public string Password;
            public bool Activated;
            public int CurrentDocuments;
        }        
    }
}
