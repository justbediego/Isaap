﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.BusinessNew.DTO
{
    public class SeasonalReportDTO
    {
        public int Season;
        public int Year;
        public List<ExpertReportComment> Comments = new List<ExpertReportComment>();
        public int? ReportID;
        public string ReportFileName;
        public Guid? ReportFileKey;
        public bool IsVisible;
        public bool IsAccepted;
        public int dayDifference;
    }
}
