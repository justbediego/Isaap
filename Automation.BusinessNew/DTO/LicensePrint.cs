﻿using Automation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.BusinessNew.DTO
{
    public class LicensePrint
    {
        public Model.Document.GenderType Gender;
        public string FullName;
        public class Education
        {
            public Model.Document.LevelOfEducations DegreeLevel;
            public string EducationField;
        }
        public DateTime DateAuthorized;
        public string PersianDateAuthorized
        {
            get
            {
                return PersianDate.ConvertDate.ToFa(DateAuthorized);
            }
        }
        public string NezamCode;
        public string CompanyProvince;
        public Education MaxEducation;
        public string CompanyName;
        public string FieldOfActivity;
        public int TotalNumberOfWorkers;
        public string CompanyFullAddress;
        public string ProvinceChiefFullName;
        public string ProvinceOfficeAddress;
        public string ProvinceOfficePhone;
        public Guid PhotoId;
    }
}
