﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.BusinessNew.DTO
{
    public class AddExpertReportComment
    {
        public int ReportID;
        public string Text;
        public bool IsAccepted;
    }
}
