﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Automation.BusinessNew.DTO.Document
{
    public class WorkExperienceDTO : BaseDTO
    {
        public string WorkshopName;
        public string EmployerName;
        public DateTime DateFrom;
        public string PersianDateFrom
        {
            get
            {
                return PersianDate.ConvertDate.ToFa(DateFrom);
            }
            set
            {
                DateFrom = PersianDate.ConvertDate.ToEn(value);
            }
        }
        public DateTime DateTo;
        public string PersianDateTo
        {
            get
            {
                return PersianDate.ConvertDate.ToFa(DateTo);
            }
            set
            {
                DateTo = PersianDate.ConvertDate.ToEn(value);
            }
        }
        public string Description;
        public static explicit operator WorkExperienceDTO(Model.Document.WorkExperience model)
        {
            if (model == null)
                return null;
            return new WorkExperienceDTO()
            {
                Id = model.Id,
                DateCreated = model.DateCreated,
                DateModified = model.DateModified,
                DateFrom = model.DateFrom,
                DateTo = model.DateTo,
                Description = model.Description,
                EmployerName = model.EmployerName,
                WorkshopName = model.WorkshopName,
            };
        }
    }
}