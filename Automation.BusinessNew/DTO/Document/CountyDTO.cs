﻿using Automation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Automation.BusinessNew.DTO.Document
{
    public class CountyDTO : BaseDTO
    {
        public ProvinceDTO Province;
        public int Province_ID;
        public string Name;
        public static explicit operator CountyDTO(County model)
        {
            if (model == null)
                return null;
            return new CountyDTO()
            {
                Id = model.Id,
                DateCreated = model.DateModified,
                DateModified = model.DateModified,
                Province = (ProvinceDTO)model.Province,
                Province_ID = model.Province_ID,
                Name = model.Name,
            };
        }
    }
}