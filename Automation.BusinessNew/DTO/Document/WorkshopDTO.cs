﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Automation.Model.Document;

namespace Automation.BusinessNew.DTO.Document
{
    public class WorkshopDTO : BaseDTO
    {
        public string UnitName;
        public string EmployerName;
        public int EmployerGender;
        public string SocialSecurityCode;
        public FieldOfActivityDTO FieldOfActivity;
        public ProductionTypeDTO ProductionType;
        public int MaleWorkersNo;
        public int FemaleWorkersNo;
        public double WorkshopSize;
        public int MorningWorkersNo;
        public int EveningWorkersNo;
        public int NightWorkersNo;
        public bool TechnicalProtectionCommittee;
        public List<AttachmentDTO> TechnicalProtectionCommitteeScans;
        public int SafetyWorkersNo;
        public string WorkshopAddress;
        public string WorkshopPhone;
        public string WorkshopPostalCode;
        public List<AttachmentDTO> ContractScans;
        public DateTime DateFrom;
        public string PersianDateFrom
        {
            get
            {
                return PersianDate.ConvertDate.ToFa(DateFrom);
            }
            set
            {
                DateFrom = PersianDate.ConvertDate.ToEn(value);
            }
        }
        public int SizeCategory;

        public static explicit operator WorkshopDTO(Workshop model)
        {
            if (model == null)
                return null;
            return new WorkshopDTO()
            {
                Id = model.Id,
                DateCreated = model.DateCreated,
                DateModified = model.DateModified,
                ContractScans = model.ContractScans.Select(a => (AttachmentDTO)a).ToList(),
                DateFrom = model.DateFrom,
                EmployerGender = (int)model.EmployerGender,
                EmployerName = model.EmployerName,
                EveningWorkersNo = model.EveningWorkersNo,
                FemaleWorkersNo = model.FemaleWorkersNo,
                FieldOfActivity = (FieldOfActivityDTO)model.FieldOfActivity,
                MaleWorkersNo = model.MaleWorkersNo,
                MorningWorkersNo = model.MorningWorkersNo,
                NightWorkersNo = model.NightWorkersNo,
                ProductionType = (ProductionTypeDTO)model.ProductionType,
                SafetyWorkersNo = model.SafetyWorkersNo,
                SizeCategory = (int)model.SizeCategory,
                SocialSecurityCode = model.SocialSecurityCode,
                TechnicalProtectionCommittee = model.TechnicalProtectionCommittee,
                TechnicalProtectionCommitteeScans = model.TechnicalProtectionCommitteeScans.Select(a => (AttachmentDTO)a).ToList(),
                UnitName = model.UnitName,
                WorkshopAddress = model.WorkshopAddress,
                WorkshopPhone = model.WorkshopPhone,
                WorkshopPostalCode = model.WorkshopPostalCode,
                WorkshopSize = model.WorkshopSize,
            };
        }
    }
}