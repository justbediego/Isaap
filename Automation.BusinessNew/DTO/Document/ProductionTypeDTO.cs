﻿using Automation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Automation.BusinessNew.DTO.Document
{
    public class ProductionTypeDTO : BaseDTO
    {
        public string Name;
        public static explicit operator ProductionTypeDTO(ProductionType model)
        {
            if (model == null)
                return null;
            return new ProductionTypeDTO()
            {
                Id = model.Id,
                DateCreated = model.DateCreated,
                DateModified = model.DateModified,
                Name = model.Name,
            };
        }
    }
}