﻿using Automation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Automation.BusinessNew.DTO.Document
{
    public class ProvinceDTO : BaseDTO
    {
        public string Name;
        public string Code;
        public List<ProvinceInspectorDTO> ProvinceInspectors;
        public static explicit operator ProvinceDTO(Province model)
        {
            if (model == null)
                return null;
            return new ProvinceDTO()
            {
                Id = model.Id,
                DateCreated = model.DateCreated,
                DateModified = model.DateModified,
                Name = model.Name,
                Code = model.Code,
                ProvinceInspectors = model.ProvinceInspectors.Select(pi => (ProvinceInspectorDTO)pi).ToList(),
            };
        }
    }
}