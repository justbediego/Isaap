﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Automation.BusinessNew.DTO.Document
{
    public class SafetyCertificationDTO : BaseDTO
    {
        public int CourseType;
        public string OtherCourseName;
        public double OtherDurationInHours;
        public DateTime DateTaken;
        public string PersianDateTaken
        {
            get
            {
                return PersianDate.ConvertDate.ToFa(DateTaken);
            }
            set
            {
                DateTaken = PersianDate.ConvertDate.ToEn(value);
            }
        }
        public string IssuerNumber;
        public string CertificationScan_Key;
        //public AttachmentDTO CertificationScan;
        public string Description;
        public static explicit operator SafetyCertificationDTO(Model.Document.SafetyCertification model)
        {
            if (model == null)
                return null;
            return new SafetyCertificationDTO()
            {
                Id = model.Id,
                DateCreated = model.DateCreated,
                DateModified = model.DateModified,
                //CertificationScan = (AttachmentDTO)model.CertificationScan,
                CertificationScan_Key = model.CertificationScan == null ? null : model.CertificationScan.Key.ToString(),
                CourseType = (int)model.CourseType,
                DateTaken = model.DateTaken,
                Description = model.Description,
                IssuerNumber = model.IssuerNumber,
                OtherCourseName = model.OtherCourseName,
                OtherDurationInHours = model.OtherDurationInHours,
            };
        }
    }
}