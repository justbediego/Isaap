﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Automation.Model.Document;

namespace Automation.BusinessNew.DTO.Document
{
    public class ExpertContactDTO : BaseDTO
    {
        public CountyDTO County;
        public int? County_ID;
        public string Address;
        public string PostalCode;
        public string Phone;
        public static explicit operator ExpertContactDTO(ExpertContact model)
        {
            if (model == null)
                return null;
            return new ExpertContactDTO()
            {
                Id = model.Id,
                DateCreated = model.DateCreated,
                DateModified = model.DateModified,
                County = (CountyDTO)model.County,
                County_ID = model.County_ID,
                Address = model.Address,
                Phone = model.Phone,
                PostalCode = model.PostalCode,
            };
        }
    }
}