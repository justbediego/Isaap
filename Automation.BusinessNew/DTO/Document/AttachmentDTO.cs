﻿using Automation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Automation.BusinessNew.DTO.Document
{
    public class AttachmentDTO : BaseDTO
    {
        public string Filename;
        public string Key;
        public static explicit operator AttachmentDTO(Attachment model)
        {
            if (model == null)
                return null;
            return new AttachmentDTO()
            {
                Id = model.Id,
                Key = model.Key.ToString(),
                DateCreated = model.DateCreated,
                DateModified = model.DateModified,
                Filename = model.Filename,
            };
        }
    }
}