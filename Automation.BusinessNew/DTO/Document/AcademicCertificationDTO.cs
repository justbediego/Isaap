﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Automation.Model.Document;

namespace Automation.BusinessNew.DTO.Document
{
    public class AcademicCertificationDTO : BaseDTO
    {
        public FieldOfStudyDTO FieldOfStudy;
        public int FieldOfStudy_ID;
        public string ScanFile_Key;
        public static explicit operator AcademicCertificationDTO(AcademicCertification model)
        {
            if (model == null)
                return null;
            return new AcademicCertificationDTO()
            {
                Id = model.Id,
                DateCreated = model.DateCreated,
                DateModified = model.DateModified,
                FieldOfStudy = (FieldOfStudyDTO)model.FieldOfStudy,
                FieldOfStudy_ID = model.FieldOfStudy_ID,
                ScanFile_Key = model.ScanFile == null ? null : model.ScanFile.Key.ToString(),
            };
        }
    }
}