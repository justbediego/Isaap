﻿using Automation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Automation.BusinessNew.DTO.Document
{
    public class FieldOfActivityDTO : BaseDTO
    {
        public string Name;
        public int DangerLevel;
        public static explicit operator FieldOfActivityDTO(FieldOfActivity model)
        {
            if (model == null)
                return null;
            return new FieldOfActivityDTO()
            {
                Id = model.Id,
                DateCreated = model.DateCreated,
                DateModified = model.DateModified,
                DangerLevel = (int)model.DangerLevel,
                Name = model.Name,
            };
        }
    }
}