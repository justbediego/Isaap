﻿using Automation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Automation.BusinessNew.DTO.Document
{
    public class FlowDTO : BaseDTO
    {
        public int Type;
        public string Description;
        public int? Staff_ID;
        public List<AttachmentDTO> Attachments;
        public static explicit operator FlowDTO(Flow model)
        {
            if (model == null)
                return null;
            return new FlowDTO()
            {
                Id = model.Id,
                DateCreated = model.DateCreated,
                DateModified = model.DateModified,
                Type = (int)model.Type,
                Attachments = model.Attachments.Select(a => (AttachmentDTO)a).ToList(),
                Description = model.Description,
                Staff_ID = model.Staff_ID,
            };
        }
    }
}