﻿using Automation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Automation.BusinessNew.DTO.Document
{
    public class DocumentDTO : BaseDTO
    {
        public string PersianDeactivationDate
        {
            get
            {
                if (DeactivationDate.HasValue)
                {
                    return PersianDate.ConvertDate.ToFa(DeactivationDate.Value);
                }
                else
                {
                    return null;
                }
            }
        }
        public DateTime? DeactivationDate;
        public string FirstName;
        public string LastName;
        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
        public string FatherName;
        public string ShenasnameNo;
        public string NationalCode;
        public string Email;
        public DateTime? BirthDate;
        public string PersianBirthDate
        {
            get
            {
                if (!BirthDate.HasValue)
                    return null;
                return PersianDate.ConvertDate.ToFa(BirthDate.Value);
            }
            set
            {
                if (value != null)
                    BirthDate = PersianDate.ConvertDate.ToEn(value);
            }
        }
        public string PlaceOfBirth;
        public int? Gender;
        public int? MaritalStatus;
        public int? MilitaryStatus;
        public string ReasonOfExemption;
        public bool Requires16HourseSafetyCertification;
        public bool Requires40HourseSafetyCertification;
        public string NezamCode;
        public string AssociationMembershipNo;
        public string Mobile;
        public int LevelOfEducation;
        public string PhotoScan_Key;
        public string UserName;
        public AcademicCertificationDTO HighSchoolCertification;
        public AcademicCertificationDTO DiplomaCertification;
        public AcademicCertificationDTO BachelorsCertification;
        public AcademicCertificationDTO MastersCertification;
        public AcademicCertificationDTO PhDCertification;
        public ExpertContactDTO HomeContact;
        public ExpertContactDTO WorkContact;
        //public AttachmentDTO PhotoScan;
        public List<AttachmentDTO> ShenasnameScans;
        public List<AttachmentDTO> NationalCardScans;
        public List<AttachmentDTO> InsuranceRecordScans;
        public WorkshopDTO CurrentWorkShop;
        public List<WorkExperienceDTO> WorkExperiences;
        public List<SafetyCertificationDTO> SafetyCertifications;
        public CompletionStatusDTO BasicInfoCompleted;
        public CompletionStatusDTO ContactInfoCompleted;
        public CompletionStatusDTO BasicAttachmentsCompleted;
        public CompletionStatusDTO CertificationAttachmentsCompleted;
        public CompletionStatusDTO WorkshopAttachmentsCompleted;
        public CompletionStatusDTO AcademicInfoCompleted;
        public CompletionStatusDTO CertificationCompleted;
        public List<FlowDTO> Flows;
        public List<AttachmentDTO> ObjectionAttachments;
        public FlowDTO CurrentFlow;
        public bool HasOlympiadRequest;
        public List<DocumentDTO> OtherDocuments = new List<DocumentDTO>();
        public static DocumentDTO LoadOverviewData(Model.Document model, List<Model.Document> otherDocuments, List<int> documentsWithPermission)
        {
            if (model == null)
                return null;
            return new DocumentDTO()
            {
                DeactivationDate = model.DeactivationDate,
                UserName = model.UserName,
                Id = model.Id,
                DateCreated = model.DateCreated,
                DateModified = model.DateModified,
                AssociationMembershipNo = model.AssociationMembershipNo,
                BirthDate = model.BirthDate,
                FatherName = model.FatherName,
                FirstName = model.FirstName,
                Gender = (int?)model.Gender,
                LastName = model.LastName,
                Email = model.Email,
                LevelOfEducation = (int)model.LevelOfEducation,
                MaritalStatus = (int?)model.MaritalStatus,
                MilitaryStatus = (int?)model.MilitaryStatus,
                Mobile = model.Mobile,
                NationalCode = model.NationalCode,
                NezamCode = model.NezamCode,
                PlaceOfBirth = model.PlaceOfBirth,
                ReasonOfExemption = model.ReasonOfExemption,
                Requires16HourseSafetyCertification = model.Requires16HourseSafetyCertification,
                Requires40HourseSafetyCertification = model.Requires40HourseSafetyCertification,
                ShenasnameNo = model.ShenasnameNo,
                PhotoScan_Key = model.PhotoScan == null ? null : model.PhotoScan.Key.ToString(),
                //Academic
                HighSchoolCertification = (AcademicCertificationDTO)model.HighSchoolCertification,
                DiplomaCertification = (AcademicCertificationDTO)model.DiplomaCertification,
                BachelorsCertification = (AcademicCertificationDTO)model.BachelorsCertification,
                MastersCertification = (AcademicCertificationDTO)model.MastersCertification,
                PhDCertification = (AcademicCertificationDTO)model.PhDCertification,
                //Completion
                AcademicInfoCompleted = (CompletionStatusDTO)model.AcademicInfoCompleted,
                BasicAttachmentsCompleted = (CompletionStatusDTO)model.BasicAttachmentsCompleted,
                BasicInfoCompleted = (CompletionStatusDTO)model.BasicInfoCompleted,
                CertificationAttachmentsCompleted = (CompletionStatusDTO)model.CertificationAttachmentsCompleted,
                CertificationCompleted = (CompletionStatusDTO)model.CertificationCompleted,
                ContactInfoCompleted = (CompletionStatusDTO)model.ContactInfoCompleted,
                WorkshopAttachmentsCompleted = (CompletionStatusDTO)model.WorkshopAttachmentsCompleted,
                //Contact
                HomeContact = (ExpertContactDTO)model.HomeContact,
                WorkContact = (ExpertContactDTO)model.WorkContact,
                //Attachments
                InsuranceRecordScans = model.InsuranceRecordScans.Select(a => (AttachmentDTO)a).ToList(),
                NationalCardScans = model.NationalCardScans.Select(a => (AttachmentDTO)a).ToList(),
                //PhotoScan = (AttachmentDTO)model.PhotoScan,
                ShenasnameScans = model.ShenasnameScans.Select(a => (AttachmentDTO)a).ToList(),
                ObjectionAttachments = model.ObjectionAttachments.Select(a => (AttachmentDTO)a).ToList(),
                //Flows
                Flows = model.Flows.Select(f => (FlowDTO)f).OrderByDescending(f => f.DateCreated).ToList(),
                CurrentFlow = (FlowDTO)model.CurrentFlow,
                //Workshop
                CurrentWorkShop = (WorkshopDTO)model.CurrentWorkShop,
                //WorkExperience
                WorkExperiences = model.WorkExperiences.Select(w => (WorkExperienceDTO)w).ToList(),
                //SafetyCertification
                SafetyCertifications = model.SafetyCertifications.Select(c => (SafetyCertificationDTO)c).ToList(),
                HasOlympiadRequest = model.OlympiadRequestDate.HasValue,
                OtherDocuments = otherDocuments.Select(other => new DocumentDTO
                {
                    DeactivationDate = other.DeactivationDate,
                    UserName = documentsWithPermission.Contains(other.Id) ? other.UserName : null,
                    PhotoScan_Key = other.PhotoScan?.Key.ToString(),
                    FirstName = other.FirstName,
                    LastName = other.LastName,
                    NationalCode = other.NationalCode,
                    BirthDate = other.BirthDate,
                    CurrentWorkShop = (WorkshopDTO)other.CurrentWorkShop,
                    CurrentFlow = (FlowDTO)other.CurrentFlow,
                }).ToList()
            };
        }
        public static DocumentDTO LoadListData(Model.Document model)
        {
            if (model == null)
                return null;
            return new DocumentDTO()
            {
                UserName = model.UserName,
                Id = model.Id,
                DateCreated = model.DateCreated,
                DateModified = model.DateModified,
                AssociationMembershipNo = model.AssociationMembershipNo,
                BirthDate = model.BirthDate,
                FatherName = model.FatherName,
                FirstName = model.FirstName,
                Gender = (int?)model.Gender,
                LastName = model.LastName,
                Email = model.Email,
                MaritalStatus = (int?)model.MaritalStatus,
                MilitaryStatus = (int?)model.MilitaryStatus,
                Mobile = model.Mobile,
                NationalCode = model.NationalCode,
                NezamCode = model.NezamCode,
                PlaceOfBirth = model.PlaceOfBirth,
                ReasonOfExemption = model.ReasonOfExemption,
                ShenasnameNo = model.ShenasnameNo,
                PhotoScan_Key = model.PhotoScan == null ? null : model.PhotoScan.Key.ToString(),
                //Contact
                HomeContact = (ExpertContactDTO)model.HomeContact,
                WorkContact = (ExpertContactDTO)model.WorkContact,
                //Workshop
                CurrentWorkShop = (WorkshopDTO)model.CurrentWorkShop,
                //Flow
                CurrentFlow = (FlowDTO)model.CurrentFlow,
            };
        }
        public static DocumentDTO LoadXMLData(Model.Document model)
        {
            if (model == null)
                return null;
            return new DocumentDTO()
            {
                UserName = model.UserName,
                Id = model.Id,
                DateCreated = model.DateCreated,
                DateModified = model.DateModified,
                AssociationMembershipNo = model.AssociationMembershipNo,
                BirthDate = model.BirthDate,
                FatherName = model.FatherName,
                FirstName = model.FirstName,
                Gender = (int?)model.Gender,
                LastName = model.LastName,
                Email = model.Email,
                MaritalStatus = (int?)model.MaritalStatus,
                MilitaryStatus = (int?)model.MilitaryStatus,
                Mobile = model.Mobile,
                NationalCode = model.NationalCode,
                NezamCode = model.NezamCode,
                PlaceOfBirth = model.PlaceOfBirth,
                ReasonOfExemption = model.ReasonOfExemption,
                ShenasnameNo = model.ShenasnameNo,
                PhotoScan_Key = model.PhotoScan == null ? null : model.PhotoScan.Key.ToString(),
                //Workshop
                CurrentWorkShop = (WorkshopDTO)model.CurrentWorkShop,
            };
        }
        public static DocumentDTO LoadLicenseData(Model.Document model)
        {
            if (model == null)
                return null;
            return new DocumentDTO()
            {
                UserName = model.UserName,
                Id = model.Id,
                DateCreated = model.DateCreated,
                DateModified = model.DateModified,
                AssociationMembershipNo = model.AssociationMembershipNo,
                BirthDate = model.BirthDate,
                FatherName = model.FatherName,
                FirstName = model.FirstName,
                Gender = (int?)model.Gender,
                LastName = model.LastName,
                LevelOfEducation = (int)model.LevelOfEducation,
                MaritalStatus = (int?)model.MaritalStatus,
                MilitaryStatus = (int?)model.MilitaryStatus,
                Mobile = model.Mobile,
                NationalCode = model.NationalCode,
                NezamCode = model.NezamCode,
                PlaceOfBirth = model.PlaceOfBirth,
                ReasonOfExemption = model.ReasonOfExemption,
                Requires16HourseSafetyCertification = model.Requires16HourseSafetyCertification,
                Requires40HourseSafetyCertification = model.Requires40HourseSafetyCertification,
                ShenasnameNo = model.ShenasnameNo,
                PhotoScan_Key = model.PhotoScan == null ? null : model.PhotoScan.Key.ToString(),
                //Workshop
                CurrentWorkShop = (WorkshopDTO)model.CurrentWorkShop,
                //Flows
                Flows = model.Flows.Select(f => (FlowDTO)f).ToList(),
                CurrentFlow = (FlowDTO)model.CurrentFlow,
                //Contact
                WorkContact = (ExpertContactDTO)model.WorkContact,
            };
        }
    }
}