﻿using Automation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Automation.BusinessNew.DTO.Document
{
    public class BaseDTO
    {
        public BaseDTO()
        {
            //defaults
            DateCreated = DateTime.Now;
            DateModified = DateTime.Now;
        }
        public int Id;
        public DateTime DateCreated;
        public DateTime DateModified;
        public string PersianDateCreated
        {
            get
            {
                return PersianDate.ConvertDate.ToFa(DateCreated);
            }
            set
            {
                DateCreated = PersianDate.ConvertDate.ToEn(value);
            }
        }
        public string PersianDateModified
        {
            get
            {
                return PersianDate.ConvertDate.ToFa(DateModified);
            }
            set
            {
                DateModified = PersianDate.ConvertDate.ToEn(value);
            }
        }
        public string TimeCreated
        {
            get
            {
                return DateCreated.ToString("HH:mm");
            }
        }
    }
}