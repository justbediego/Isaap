﻿using Automation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Automation.BusinessNew.DTO.Document
{
    public class ProvinceInspectorDTO : BaseDTO
    {
        public string ChairmanFullName;
        public string SupervisorFullName;
        public static explicit operator ProvinceInspectorDTO(ProvinceInspector model)
        {
            if (model == null)
                return null;
            return new ProvinceInspectorDTO()
            {
                Id = model.Id,
                DateCreated = model.DateCreated,
                DateModified = model.DateModified,
                SupervisorFullName = model.SupervisorFullName,
                ChairmanFullName = model.ChairmanFullName,
            };
        }
    }
}