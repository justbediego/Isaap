﻿using Automation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Automation.BusinessNew.DTO.Document
{
    public class FieldOfStudyDTO : BaseDTO
    {
        public string Name;
        public string Code;
        public string OrginalityCode;
        public bool Requires40Hours;
        public bool Requires16Hours;
        public static explicit operator FieldOfStudyDTO(FieldOfStudy model)
        {
            if (model == null)
                return null;
            return new FieldOfStudyDTO()
            {
                Id = model.Id,
                DateCreated = model.DateCreated,
                DateModified = model.DateModified,
                Name = model.Name,
                Code = model.Code,
                OrginalityCode = model.OrginalityCode,
                Requires16Hours = model.Requires16Hours,
                Requires40Hours = model.Requires40Hours,
            };
        }
    }
}