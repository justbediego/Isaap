﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Automation.BusinessNew.DTO.Document
{
    public class CompletionStatusDTO
    {
        public string Message;
        public bool IsComplete;
        public static explicit operator CompletionStatusDTO(Model.Document.CompletionStatus model) 
        {
            if (model == null)
                return null;
            return new CompletionStatusDTO()
            {
                Message = model.Message,
                IsComplete = model.IsComplete,
            };
        }
    }
}