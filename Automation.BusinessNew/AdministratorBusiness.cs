﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automation.BusinessNew.DTO;
using Automation.DataAccess;
using Automation.Model;
using Automation.Model.Exception;

namespace Automation.BusinessNew
{
    public class AdministratorBusiness
    {
        AutomationContext dbContext;
        ValidationBusiness validation;
        FlowEngine flowEngine;
        public AdministratorBusiness(AutomationContext dbContext, ValidationBusiness validation, FlowEngine flowEngine)
        {
            this.dbContext = dbContext;
            this.validation = validation;
            this.flowEngine = flowEngine;
        }
        private void CheckAdmin(string adminUsername)
        {
            if (!dbContext.Admins.Any(a => a.UserName == adminUsername && !a.IsDeactivated))
                throw new NotAllowedException("ایجاد تغییرات", "کاربر دارای این اجازه نمی باشد");
        }
        private void CheckAdminPassword(string adminUsername, string adminPassword)
        {
            var adminUser = dbContext.Admins.FirstOrDefault(a => a.UserName == adminUsername && !a.IsDeactivated);
            if (adminUser == null || adminUser.HashPassword != UtilitiesBusiness.TextToMD5(adminPassword))
                throw new NotAllowedException("ایجاد تغییرات", "گذرواژه صحیح نمی باشد");
        }

        public void AddCounty(string adminUsername, int provinceID, string countyName)
        {
            CheckAdmin(adminUsername);
            validation.ValidateCountyName(countyName);
            Province province = dbContext.Provinces.FirstOrDefault(p => p.Id == provinceID);
            if (province == null)
                throw new NotFoundException("استان");
            County county = new County()
            {
                Name = countyName,
                Province_ID = province.Id
            };
            dbContext.Counties.Add(county);
            dbContext.SaveChanges();
        }

        public void ChangeCountyName(string adminUsername, int countyID, string newCountyName)
        {
            CheckAdmin(adminUsername);
            validation.ValidateCountyName(newCountyName);
            County county = dbContext.Counties.FirstOrDefault(c => c.Id == countyID);
            if (county == null)
                throw new NotFoundException("شهرستان");
            county.Name = newCountyName;
            dbContext.SaveChanges();
        }

        public void AddAssociation(string adminUsername, int countyID, string username, string password)
        {
            CheckAdmin(adminUsername);
            validation.ValidateUserName(username);
            validation.ValidatePassword(password);
            County county = dbContext.Counties.Where(c => c.Id == countyID).FirstOrDefault();
            if (county == null)
                throw new NotFoundException("شهرستان");
            Association association = null;
            try
            {
                association = county.Associations.SingleOrDefault();
            }
            catch
            {
                throw new InternalException("تعداد انجمن بیش از یک");
            }
            if (association != null)
                throw new NotPossibleException("اضافه کردن انجمن", "قبلاً اضافه شده است");
            association = new Association()
            {
                UserName = username,
                HashPassword = UtilitiesBusiness.TextToMD5(password),
            };
            county.Associations.Add(association);
            dbContext.SaveChanges();
        }
        public void ActivateAssociation(string adminUsername, int associationID)
        {
            CheckAdmin(adminUsername);
            Association association = dbContext.Associations.Where(a => a.Id == associationID).FirstOrDefault();
            if (association == null)
                throw new NotFoundException("انجمن");
            association.IsDeactivated = false;
            dbContext.SaveChanges();
        }
        public void DeactiveAssociation(string adminUsername, int associationID, string description)
        {
            CheckAdmin(adminUsername);
            Association association = dbContext.Associations.Where(a => a.Id == associationID).FirstOrDefault();
            if (association == null)
                throw new NotFoundException("انجمن");
            association.IsDeactivated = true;
            var pending = dbContext.Documents.Where(d => d.Flows.Any() && d.Flows.OrderByDescending(f => f.DateCreated).FirstOrDefault().Staff_ID == associationID).ToList();
            pending.ForEach(doc =>
            {
                doc.Flows.Add(flowEngine.SendUpToClub(doc.Id, "انجمن غیرفعال گردید: " + description));
            });
            dbContext.SaveChanges();
        }

        public void AddClub(string adminUsername, int provinceID, string username, string password)
        {
            CheckAdmin(adminUsername);
            validation.ValidateUserName(username);
            validation.ValidatePassword(password);
            Province province = dbContext.Provinces.Where(p => p.Id == provinceID).FirstOrDefault();
            if (province == null)
                throw new NotFoundException("استان");
            Club club = null;
            try
            {
                club = province.Clubs.SingleOrDefault();
            }
            catch
            {
                throw new InternalException("تعداد کانون بیش از یک");
            }
            if (club != null)
                throw new NotPossibleException("اضافه کردن کانون", "قبلاً اضافه شده است");
            club = new Club()
            {
                UserName = username,
                HashPassword = UtilitiesBusiness.TextToMD5(password),
            };
            province.Clubs.Add(club);
            dbContext.SaveChanges();
        }
        public void ActivateClub(string adminUsername, int clubID)
        {
            CheckAdmin(adminUsername);
            Club club = dbContext.Clubs.Where(c => c.Id == clubID).FirstOrDefault();
            if (club == null)
                throw new NotFoundException("کانون");
            club.IsDeactivated = false;
            dbContext.SaveChanges();
        }
        public void DeactivateClub(string adminUsername, int clubID, string description)
        {
            CheckAdmin(adminUsername);
            Club club = dbContext.Clubs.Where(c => c.Id == clubID).FirstOrDefault();
            if (club == null)
                throw new NotFoundException("کانون");
            club.IsDeactivated = true;
            var pending = dbContext.Documents.Where(d => d.Flows.Any() && d.Flows.OrderByDescending(f => f.DateCreated).FirstOrDefault().Staff_ID == clubID).ToList();
            pending.ForEach(doc =>
            {
                doc.Flows.Add(flowEngine.SendUpToCountyInspector(doc.Id, "کانون غیرفعال گردید: " + description));
            });
            dbContext.SaveChanges();
        }

        public void AddCountyInspector(string adminUsername, int countyID, string username, string password)
        {
            CheckAdmin(adminUsername);
            validation.ValidateUserName(username);
            validation.ValidatePassword(password);
            County county = dbContext.Counties.Where(c => c.Id == countyID).FirstOrDefault();
            if (county == null)
                throw new NotFoundException("شهرستان");
            CountyInspector countyInspector = null;
            try
            {
                countyInspector = county.CountyInspectors.SingleOrDefault();
            }
            catch
            {
                throw new InternalException("تعداد بازرس شهرستان بیش از یک");
            }
            if (countyInspector != null)
                throw new NotPossibleException("بازرس شهرستان", "");
            countyInspector = new CountyInspector()
            {
                UserName = username,
                HashPassword = UtilitiesBusiness.TextToMD5(password),
            };
            county.CountyInspectors.Add(countyInspector);
            dbContext.SaveChanges();
        }
        public void ActivateCountyInspector(string adminUsername, int countyInspectorID)
        {
            CheckAdmin(adminUsername);
            CountyInspector countyInspector = dbContext.CountyInspectors.Where(c => c.Id == countyInspectorID).FirstOrDefault();
            if (countyInspector == null)
                throw new NotFoundException("بازرس شهرستان");
            countyInspector.IsDeactivated = false;
            dbContext.SaveChanges();
        }
        public void DeactivateCountyInspector(string adminUsername, int countyInspectorID, string description)
        {
            CheckAdmin(adminUsername);
            CountyInspector countyInspector = dbContext.CountyInspectors.Where(c => c.Id == countyInspectorID).FirstOrDefault();
            if (countyInspector == null)
                throw new NotFoundException("بازرس شهرستان");
            countyInspector.IsDeactivated = true;
            var pending = dbContext.Documents.Where(d => d.Flows.Any() && d.Flows.OrderByDescending(f => f.DateCreated).FirstOrDefault().Staff_ID == countyInspectorID).ToList();
            pending.ForEach(doc =>
            {
                doc.Flows.Add(flowEngine.SendUpToProvinceInspector(doc.Id, "بازرس شهرستان غیرفعال گردید: " + description));
            });
            dbContext.SaveChanges();
        }

        public void ChangeOthersPassword(string adminUsername, string adminPassword, string username, string password)
        {
            CheckAdmin(adminUsername);
            CheckAdminPassword(adminUsername, adminPassword);
            validation.ValidatePassword(password);
            User user = dbContext.Users.Where(u => u.UserName == username).FirstOrDefault();
            if (user == null)
                throw new NotFoundException("کاربر");
            if (user is Admin)
                throw new NotAllowedException("تغییر گذرواژه", "کاربر مورد نظر ادمین می باشد");
            user.HashPassword = UtilitiesBusiness.TextToMD5(password);
            dbContext.SaveChanges();
        }

        public void ChangeProvinceHeadName(string adminUsername, int provinceID, string newName)
        {
            CheckAdmin(adminUsername);
            validation.ValidateFullName(newName);
            ProvinceInspector provinceInspector = dbContext.ProvinceInspectors.Where(pi => pi.Province_ID == provinceID).FirstOrDefault();
            if (provinceInspector == null)
                throw new NotFoundException("استان");
            provinceInspector.HeadFullName = newName;
            dbContext.SaveChanges();
        }

        public void ChangeProvinceChairmanName(string adminUsername, int provinceID, string newName)
        {
            CheckAdmin(adminUsername);
            validation.ValidateFullName(newName);
            ProvinceInspector provinceInspector = dbContext.ProvinceInspectors.Where(pi => pi.Province_ID == provinceID).FirstOrDefault();
            if (provinceInspector == null)
                throw new NotFoundException("استان");
            provinceInspector.ChairmanFullName = newName;
            dbContext.SaveChanges();
        }

        public void SetSystemLock(string adminUsername, bool isLocked)
        {
            CheckAdmin(adminUsername);
            Setting setting;
            try
            {
                setting = dbContext.Settings.Where(s => s.Key == Setting.KeyType.Lock).Single();
            }
            catch
            {
                throw new InternalException("تنظیمات قفل پیدا نشد");
            }
            if (isLocked)
                setting.Value = Setting.ValueType.On;
            else
                setting.Value = Setting.ValueType.Off;
            dbContext.SaveChanges();
        }

        public List<ProvinceClubDTO> GetAllProvinceClubs(string adminUsername)
        {
            CheckAdmin(adminUsername);
            return dbContext.Provinces.Select(pr => new ProvinceClubDTO()
            {
                ID = pr.Id,
                HeadFullName = pr.ProvinceInspectors.Select(pi => pi.HeadFullName).FirstOrDefault(),
                ChairmanFullName = pr.ProvinceInspectors.Select(pi => pi.ChairmanFullName).FirstOrDefault(),
                Name = pr.Name,
                Club = pr.Clubs.Select(cl => new ProvinceClubDTO.ClubDTO()
                {
                    ID = cl.Id,
                    Activated = !cl.IsDeactivated,
                    Username = cl.UserName,
                    Password = (cl.HashPassword == "DA8B701A78522E5795C75DF4844F8A7F") ? "123456" : (cl.HashPassword == "5E7D36FE8A3F14D5D802DF62D738F816") ? "isaap123" : "unknown",
                    CurrentDocuments = dbContext.Documents.Count(d => d.Flows.Any() && d.Flows.OrderByDescending(f => f.DateCreated).FirstOrDefault().Staff_ID == cl.Id)
                }).FirstOrDefault()
            }).ToList();
        }
        public List<CountyCountyInspectorDTO> GetAllCountyCountyInspectors(string adminUsername)
        {
            CheckAdmin(adminUsername);
            return dbContext.Counties.Select(pr => new CountyCountyInspectorDTO()
            {
                ID = pr.Id,
                ProvinceName = pr.Province.Name,
                Name = pr.Name,
                CountyInspector = pr.CountyInspectors.Select(cl => new CountyCountyInspectorDTO.CountyInspectorDTO()
                {
                    ID = cl.Id,
                    Activated = !cl.IsDeactivated,
                    Username = cl.UserName,
                    Password = (cl.HashPassword == "DA8B701A78522E5795C75DF4844F8A7F") ? "123456" : (cl.HashPassword == "5E7D36FE8A3F14D5D802DF62D738F816") ? "isaap123" : "unknown",
                    CurrentDocuments = dbContext.Documents.Count(d => d.Flows.Any() && d.Flows.OrderByDescending(f => f.DateCreated).FirstOrDefault().Staff_ID == cl.Id)
                }).FirstOrDefault()
            }).ToList();
        }
    }
}
