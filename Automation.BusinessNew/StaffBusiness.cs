﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automation.BusinessNew.DTO;
using Automation.DataAccess;
using Automation.Model;
using Automation.Model.Exception;
using System.Data.Entity;
using Automation.BusinessNew.DTO.Document;

namespace Automation.BusinessNew
{
    public class StaffBusiness
    {
        AutomationContext dbContext;
        ValidationBusiness validation;
        UtilitiesBusiness utilitiesBusiness;
        FlowEngine flowEngine;
        public StaffBusiness(AutomationContext dbContext, ValidationBusiness validation, FlowEngine flowEngine, UtilitiesBusiness utilitiesBusiness)
        {
            this.dbContext = dbContext;
            this.validation = validation;
            this.utilitiesBusiness = utilitiesBusiness;
            this.flowEngine = flowEngine;
        }

        private IQueryable<Document> GetAllDocuments(string username)
        {
            Staff staff = dbContext.Staff.Where(s => s.UserName == username).FirstOrDefault();
            if (staff == null)
                throw new NotFoundException("کارمند");

            List<int> underStaff = new List<int>() { staff.Id };
            switch (staff.Type)
            {
                case User.UserType.Club:
                    Club club = dbContext.Clubs.First(c => c.Id == staff.Id);
                    underStaff.AddRange(dbContext.Associations.Where(a => a.County.Province_ID == club.Province_ID).Select(a => a.Id).ToList());
                    break;
                case User.UserType.CountyInspector:
                    CountyInspector countyInspector = dbContext.CountyInspectors.Include(c => c.County).First(c => c.Id == staff.Id);
                    underStaff.AddRange(dbContext.Associations.Where(a => a.County_ID == countyInspector.County_ID).Select(a => a.Id).ToList());
                    underStaff.AddRange(dbContext.Clubs.Where(c => c.Province_ID == countyInspector.County.Province_ID).Select(c => c.Id).ToList());
                    break;
                case User.UserType.ProvinceInspector:
                    ProvinceInspector provinceInspector = dbContext.ProvinceInspectors.First(p => p.Id == staff.Id);
                    underStaff.AddRange(dbContext.Associations.Where(a => a.County.Province_ID == provinceInspector.Province_ID).Select(a => a.Id).ToList());
                    underStaff.AddRange(dbContext.Clubs.Where(c => c.Province_ID == provinceInspector.Province_ID).Select(c => c.Id).ToList());
                    underStaff.AddRange(dbContext.CountyInspectors.Where(c => c.County.Province_ID == provinceInspector.Province_ID).Select(c => c.Id).ToList());
                    break;
                case User.UserType.ProvinceManager:
                    ProvinceManager provinceManager = dbContext.ProvinceManagers.First(p => p.Id == staff.Id);
                    underStaff.AddRange(dbContext.Associations.Where(a => a.County.Province_ID == provinceManager.Province_ID).Select(a => a.Id).ToList());
                    underStaff.AddRange(dbContext.Clubs.Where(c => c.Province_ID == provinceManager.Province_ID).Select(c => c.Id).ToList());
                    underStaff.AddRange(dbContext.CountyInspectors.Where(c => c.County.Province_ID == provinceManager.Province_ID).Select(c => c.Id).ToList());
                    underStaff.AddRange(dbContext.ProvinceInspectors.Where(c => c.Province_ID == provinceManager.Province_ID).Select(c => c.Id).ToList());
                    break;
                case User.UserType.InspectionOffice:
                    underStaff.AddRange(dbContext.Staff.Select(s => s.Id).ToList());
                    break;
            }
            //has it ever passed through anyone below me
            return dbContext.Documents.Where(d => d.DeactivationDate == null && d.Flows.Any(f => underStaff.Contains(f.Staff.Id))).OrderByDescending(d => d.DateCreated);
            //List<int> indexes = dbContext.Documents.Where(d => d.Flows.Any(f => underStaff.Contains(f.Staff.Id))).Select(d => d.Id).ToList();
            //return dbContext.Documents.Where(d => indexes.Contains(d.Id));
        }

        public DocumentDTO GetDocumentForOverview(string username, string documentUsername)
        {
            Document document = GetAllDocuments(username).Where(d => d.UserName == documentUsername)
                                                        .FirstOrDefault();
            if (document == null)
                throw new NotFoundException("پرونده");
            document.Flows = document.Flows.OrderByDescending(f => f.DateCreated).ToList();
            List<Document> otherDocuments = dbContext.Documents.Where(d => document.NationalCode.Length == 10 && d.NationalCode == document.NationalCode && d.Id != document.Id).ToList();
            List<int> otherDocumentIds = otherDocuments.Select(od => od.Id).ToList();
            List<int> permittedDocuments = GetAllDocuments(username).Select(d => d.Id).Where(dId => otherDocumentIds.Contains(dId)).ToList();
            return DocumentDTO.LoadOverviewData(document, otherDocuments, permittedDocuments);
        }

        public LicensePrint GetLicensePrintData(string username, string documentUsername)
        {
            var result = GetAllDocuments(username)
                                          .Where(l => l.UserName == documentUsername)
                                          .Where(l => l.Flows.OrderByDescending(lf => lf.DateCreated).Select(lf => lf.Type).FirstOrDefault() == Flow.FlowType.Authorized)
                                          .Select(l => new LicensePrint()
                                          {
                                              CompanyFullAddress = l.WorkContact.County.Province.Name + "، " +
                                                                   l.WorkContact.County.Name + "، " +
                                                                   l.WorkContact.Address + "، کدپستی " +
                                                                   l.WorkContact.PostalCode,
                                              CompanyName = l.CurrentWorkShop.UnitName,
                                              FieldOfActivity = l.CurrentWorkShop.FieldOfActivity.Name,
                                              FullName = l.FirstName + " " + l.LastName,
                                              Gender = l.Gender.Value,
                                              MaxEducation = new LicensePrint.Education
                                              {
                                                  DegreeLevel = l.PhDCertification != null ? Document.LevelOfEducations.PhD :
                                                    l.MastersCertification != null ? Document.LevelOfEducations.Master :
                                                    l.BachelorsCertification != null ? Document.LevelOfEducations.Bachelor :
                                                    l.DiplomaCertification != null ? Document.LevelOfEducations.Diploma :
                                                    l.HighSchoolCertification != null ? Document.LevelOfEducations.HighSchool : Document.LevelOfEducations.NoEducation,
                                                  EducationField = l.PhDCertification != null ? l.PhDCertification.FieldOfStudy.Name :
                                                    l.MastersCertification != null ? l.MastersCertification.FieldOfStudy.Name :
                                                    l.BachelorsCertification != null ? l.BachelorsCertification.FieldOfStudy.Name :
                                                    l.DiplomaCertification != null ? l.DiplomaCertification.FieldOfStudy.Name :
                                                    l.HighSchoolCertification != null ? l.HighSchoolCertification.FieldOfStudy.Name : ""
                                              },
                                              CompanyProvince = l.WorkContact.County.Province.Name,
                                              PhotoId = l.PhotoScan.Key,
                                              NezamCode = l.NezamCode,
                                              ProvinceChiefFullName = l.WorkContact.County.Province.ProvinceInspectors.Select(pi => pi.ChairmanFullName).FirstOrDefault(),
                                              //ProvinceOfficeAddress = l.WorkContact.County.Province.ProvinceInspectors.Select(pi => pi.Province.Name + "، " + pi.Name).FirstOrDefault(),
                                              //ProvinceOfficePhone = l.WorkContact.County.Province.ProvinceInspectors.Select(pi => pi.Location.Phone).FirstOrDefault(),
                                              TotalNumberOfWorkers = l.CurrentWorkShop.FemaleWorkersNo + l.CurrentWorkShop.MaleWorkersNo,
                                              DateAuthorized = l.Flows.OrderByDescending(lf => lf.DateCreated).Where(lf => lf.Type == Flow.FlowType.Authorized).Select(lf => lf.DateCreated).FirstOrDefault(),
                                          }).FirstOrDefault();
            if (result == null)
                throw new Model.Exception.NotFoundException("پروانه", "");
            return result;
        }
        public List<ExpertReportOverview> GetAllExportReports(string username, out int count, int? skip = null, int? take = null, string phrase = null)
        {
            var list = GetAllDocuments(username).Select(d => d.Reports)
                .SelectMany(r => r)
                .Where(r => r.IsVisible)
                .Select(r => new ExpertReportOverview
                {
                    Id = r.Id,
                    IsAccepted = r.Comments.Any(c => c.IsAccepted),
                    ForDate = r.ForDate,
                    ReportType = r.ReportType,
                    DateCreated = r.DateCreated,
                    DateModified = r.DateModified,
                    FullName = r.Document.FirstName + " " + r.Document.LastName,
                    NezamCode = r.Document.NezamCode,
                    PhotoScan_Key = r.Document.PhotoScan.Key,
                    UnitName = r.Document.CurrentWorkShop.UnitName,
                    WorkCounty = r.Document.WorkContact.County.Name,
                    WorkProvince = r.Document.WorkContact.County.Province.Name,
                    File_Key = r.Files.Select(f => f.Key).FirstOrDefault(),
                    FileName = r.Files.Select(f => f.Filename).FirstOrDefault(),
                });

            if (phrase != null)
                list = list.Where(r => r.FullName.Contains(phrase) || r.UnitName.Contains(phrase) || r.NezamCode.Contains(phrase));

            count = list.Count();
            list = list
                .OrderByDescending(r => r.IsAccepted)
                .ThenByDescending(r => r.DateModified);

            if (skip.HasValue)
                list = list.Skip(skip.Value);
            if (take.HasValue)
                list = list.Take(take.Value);

            return list.ToList();
        }
        public List<ExpertReportComment> GetReportComments(string username, int reportID)
        {
            var report = GetAllDocuments(username).Select(d => d.Reports).SelectMany(r => r).Where(r => r.Id == reportID).Include(r => r.Comments).FirstOrDefault();
            if (report == null)
                throw new NotFoundException("گزارش");
            return report.Comments.Select(c => new ExpertReportComment
            {
                DateCreated = c.DateCreated,
                IsAccepted = c.IsAccepted,
                Text = c.Text
            }).ToList();
        }
        public void AddExpertReportComment(string username, int reportID, string text, bool isAccepted)
        {
            var report = GetAllDocuments(username).Select(d => d.Reports).SelectMany(r => r).Where(r => r.Id == reportID).Include(r => r.Comments).FirstOrDefault();
            if (report == null)
                throw new NotFoundException("گزارش");
            if (report.Comments.Any(c => c.IsAccepted))
                throw new NotAllowedException("ثبت نظر", "گزارش قبلا تایید شده است");
            report.Comments.Add(new ReportComment()
            {
                Text = text,
                IsAccepted = isAccepted,
            });
            dbContext.SaveChanges();
        }
    }
}
