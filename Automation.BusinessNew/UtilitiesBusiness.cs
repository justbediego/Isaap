﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Automation.BusinessNew
{
    public class UtilitiesBusiness
    {
        public static string TextToMD5(string text)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(text + "HosseinIsTheBest");
            byte[] hashBytes = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
                sb.Append(hashBytes[i].ToString("X2"));
            return sb.ToString();
        }
        public class PersianSeason
        {
            public int Year;
            public int Season;
        }
        public static DateTime GetDateFromSeason(int year, int season)
        {
            var persianDate = string.Format("{0}/{1}/1", year, (season - 1) * 3 + 1);
            return PersianDate.ConvertDate.ToEn(persianDate);
        }
        public static PersianSeason GetSeasonFromDate(DateTime date)
        {
            var persianDate = PersianDate.ConvertDate.ToFa(date).Replace('\\', '-').Replace('/', '-').Split('-');
            return new PersianSeason()
            {
                Year = int.Parse(persianDate[0]),
                Season = (int)Math.Ceiling(double.Parse(persianDate[1]) / 3)
            };
        }

        public static void SendHtmlEmail(string toEmail, string subject, string body)
        {
            try
            {
                string username = ConfigurationManager.AppSettings["MailServerUsername"];
                string password = ConfigurationManager.AppSettings["MailServerPassword"];
                string host = ConfigurationManager.AppSettings["MailServerHost"];
                int port = int.Parse(ConfigurationManager.AppSettings["MailServerPort"]);
                bool EnableSsl = bool.Parse(ConfigurationManager.AppSettings["MailServerSsl"]);
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(host);
                mail.From = new MailAddress(username);
                mail.To.Add(toEmail);
                mail.Subject = username;
                mail.Body = body;
                mail.IsBodyHtml = true;
                SmtpServer.Port = port;
                SmtpServer.Credentials = new System.Net.NetworkCredential(username, password);
                SmtpServer.EnableSsl = EnableSsl;
                SmtpServer.Send(mail);
            }
            catch
            {
                throw new Model.Exception.InternalException("Mail server error");
            }
        }
    }
}
