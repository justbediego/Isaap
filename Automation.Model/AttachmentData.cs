﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model
{
    public class AttachmentData : Base
    {
        public byte[] FileData { get; set; }
    }
}
