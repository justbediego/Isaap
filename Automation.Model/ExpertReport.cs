﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model
{
    public class ExpertReport : Base
    {
        public int Document_ID { get; set; }
        [ForeignKey("Document_ID")]
        public Document Document { get; set; }
        public enum ReportTypes
        {
            Seasonal = 1,
        }
        public ReportTypes ReportType { get; set; }
        [ForeignKey("ExpertReport_ID")]
        public List<Attachment> Files { get; set; }
        public DateTime ForDate { get; set; }
        public string Description { get; set; }
        public List<ReportComment> Comments { get; set; }
        public bool IsVisible { get; set; }
        public ExpertReport()
        {
            Files = new List<Attachment>();
            Comments = new List<ReportComment>();
            IsVisible = false;
        }
    }
}
