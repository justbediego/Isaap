﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model
{
    public class Setting
    {
        [Key]
        public KeyType Key { get; set; }
        public ValueType Value { get; set; }
        public long Amount { get; set; }
        public enum KeyType
        {
            Lock = 1,// "Lock"
        }
        public enum ValueType
        {
            On = 1,//"On"
            Off = 2,//"Off"
        }
    }
}
