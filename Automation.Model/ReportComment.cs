﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model
{
    public class ReportComment:Base
    {
        public bool IsAccepted { get; set; }
        public string Text { get; set; }
    }
}
