﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model
{
    [Table("Flows")]
    public class Flow:Base
    {
        public Flow()
        {
            Attachments = new List<Attachment>();
        }
        public enum FlowType
        {
            Created = 1,
            NotCompleted = 2, //rejected by time, rejected three times
            SentToAssociation = 3,
            SentToCountyInspector = 4,
            SentToClub = 5,
            SentToProvinceInspector = 6,
            Authorized = 7,
            RejectedOnce = 8,
            SentToProvinceManager = 9,
            RejectedTwice = 10,
            SentToInspectionOffice = 11,
        }
        [JsonIgnore]
        public Document Document { get; set; }
        public FlowType Type { get; set; }
        public string Description { get; set; }
        public int? Staff_ID { get; set; }
        [ForeignKey("Staff_ID")]
        public Staff Staff { get; set; }
        public virtual List<Attachment> Attachments { get; set; }
    }
}
