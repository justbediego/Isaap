﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model
{
    [SoftDelete("Deleted")]
    public class Base
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        public DateTime DateModified { get; set; }
        public DateTime DateCreated { get; set; }
        public int OwnerType { get; set; }
        public bool Deleted { get; set; }
        public Base()
        {
            DateCreated = DateTime.Now;
            DateModified = DateTime.Now;
            OwnerType = 0;
            Deleted = false;
        }
    }
}
