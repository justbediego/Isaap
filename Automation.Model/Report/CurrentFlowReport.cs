﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model.Report
{
    public class CurrentFlowReport
    {
        public int NotRequestedCount;
        public int NotSentCount;
        public int RejectedCount;
        public int InAssociationCount;
        public int InClubCount;
        public int InCountyInspectorCount;
        public int InProvinceInspectorCount;
        public int AuthorizedCount;
        public int InProvinceManagerCount;
        public int InInspectionOfficeCount;
    }
}
