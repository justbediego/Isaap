﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model.Report
{
    public class AuthorizationByNumberReport
    {
        public int Over25;
        public int Below25;
    }
}
