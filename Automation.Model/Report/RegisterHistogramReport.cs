﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model.Report
{
    public class RegisterHistogramReport
    {
        public class Month
        {
            public string Name;
            public int Count;
            public int Sum;
        }
        public List<Month> Months;
        public RegisterHistogramReport()
        {
            Months = new List<Month>();
        }
    }
}
