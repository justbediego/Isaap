﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model.Exception
{
    public class WrongInputException : System.Exception
    {
        public WrongInputException(string inputName, string description)
            : base(string.Format("اطلاعات وارد شده برای '{0}' اشتباه می باشد. {1}", inputName, description))
        {
        }
    }
}
