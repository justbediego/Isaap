﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model.Exception
{
    public class InternalException : System.Exception
    {
        public InternalException(string description)
            : base(string.Format("مشکل درونی در سامانه. لطفاً مدیریت سامانه را در جریان بگذارید. {0}", description))
        {
        }
    }
}
