﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model.Exception
{
    public class IncompleteException : System.Exception
    {
        public IncompleteException(string incompleteObj, string description)
            : base(string.Format("اطلاعات '{0}' کامل نمی باشد. {1}", incompleteObj, description))
        {
        }
    }
}
