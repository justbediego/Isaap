﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model.Exception
{
    public class NotAllowedException : System.Exception
    {
        public NotAllowedException(string unallowedprocedure, string description)
            : base(string.Format("عدم دسترسی به انجام فرایند {0}. {1}", unallowedprocedure, description))
        {
        }
    }
}
