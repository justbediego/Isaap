﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model.Exception
{
    public class NotPossibleException : System.Exception
    {
        public NotPossibleException(string impossibleprocedure, string description)
            : base(string.Format("عملیات '{0}' امکان پذیر نمیباشد. {1}", impossibleprocedure, description))
        {
        }
    }
}
