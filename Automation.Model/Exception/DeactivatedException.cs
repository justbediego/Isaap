﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model.Exception
{
    public class DeactivatedException : System.Exception
    {
        public DeactivatedException(string deactivatedObject, string description)
            : base(string.Format("{0} غیرفعال گردیده است. {1}: ", deactivatedObject, description))
        {

        }
    }
}
