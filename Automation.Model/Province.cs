﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model
{
    [Table("Provinces")]
    public class Province : Base
    {
        public Province()
        {
            Counties = new List<County>();
            Clubs = new List<Club>();
            ProvinceInspectors = new List<ProvinceInspector>();
            ProvinceManagers = new List<ProvinceManager>();
        }
        public string Name { get; set; }
        public string Code { get; set; }
        [JsonIgnore]
        public List<County> Counties { get; set; }
        [JsonIgnore]
        public List<Club> Clubs { get; set; }
        //[JsonIgnore]
        public List<ProvinceInspector> ProvinceInspectors { get; set; }
        [JsonIgnore]
        public List<ProvinceManager> ProvinceManagers { get; set; }
    }
}
