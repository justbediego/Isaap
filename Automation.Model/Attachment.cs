﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model
{
    [Table("Attachments")]
    public class Attachment:Base
    {
        public Attachment()
        {
            Key = Guid.NewGuid();
        }
        public Guid Key { get; set; }
        public int? InsuranceRecord_ID { get; set; }
        public int? NationalCard_ID { get; set; }
        public int? Shenasname_ID { get; set; }
        public int? Objection_ID { get; set; }
        public int? TechnicalProtectionCommittee_ID { get; set; }
        public int? Contract_ID { get; set; }
        public int? ExpertReport_ID { get; set; }
        public string Filename { get; set; }
        public AttachmentData AttachmentData { get; set; }
    }
}
