﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model
{
    [Table("Analyzers")]
    public class Analyzer : Staff
    {
        public Analyzer()
        {
            Type = UserType.Analyzer;
        }
    }
}
