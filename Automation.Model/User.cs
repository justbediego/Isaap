﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model
{
    [Table("Users")]
    public class User:Base
    {
        [Index(IsUnique=true)]
        [MaxLength(100)]
        public string UserName { get; set; }
        [NotMapped]
        public string Password { get; set; }
        [MaxLength(1000)]
        public string Email { get; set; }
        [MaxLength(200)]
        public string ForgotPasswordKey { get; set; }
        public DateTime? ForgotPasswordDate { get; set; }
        public string HashPassword { get; set; }
        public enum UserType
        {
            Expert = 1,
            Association = 2,
            Club = 3,
            CountyInspector = 4,
            ProvinceInspector = 5,
            ProvinceManager = 6,
            InspectionOffice = 7,
            Admin = 8,
            Analyzer=9,
        }
        public UserType Type { get; set; }
        public List<ActionLog> ActionLogs { get; set; }
    }
}
