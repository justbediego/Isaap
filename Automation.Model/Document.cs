﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model
{
    [Table("Documents")]
    public class Document : User
    {
        public class CompletionStatus
        {
            public string Message;
            public bool IsComplete;
        }
        public class Workshop : Base
        {
            public Workshop()
            {
                TechnicalProtectionCommitteeScans = new List<Attachment>();
                ContractScans = new List<Attachment>();
            }
            public string UnitName { get; set; }
            public string EmployerName { get; set; }
            public GenderType EmployerGender { get; set; }
            public string SocialSecurityCode { get; set; }
            public virtual FieldOfActivity FieldOfActivity { get; set; }
            public virtual ProductionType ProductionType { get; set; }
            public int MaleWorkersNo { get; set; }
            public int FemaleWorkersNo { get; set; }
            public double WorkshopSize { get; set; }
            public int MorningWorkersNo { get; set; }
            public int EveningWorkersNo { get; set; }
            public int NightWorkersNo { get; set; }
            public bool TechnicalProtectionCommittee { get; set; }
            [ForeignKey("TechnicalProtectionCommittee_ID")]
            public virtual List<Attachment> TechnicalProtectionCommitteeScans { get; set; }
            public int SafetyWorkersNo { get; set; }
            public string WorkshopAddress { get; set; }
            public string WorkshopPhone { get; set; }
            public string WorkshopPostalCode { get; set; }
            [ForeignKey("Contract_ID")]
            public virtual List<Attachment> ContractScans { get; set; }
            public DateTime DateFrom { get; set; }
            public string Email { get; set; }
            public enum SizeCategories
            {
                Below25 = 1,
                Below50Over25 = 2,
                Below100Over50 = 3,
                Below200Over100 = 4,
                Below300Over200 = 5,
                Over300 = 6,
            }
            public SizeCategories SizeCategory
            {
                get
                {
                    var totalNumer = MaleWorkersNo + FemaleWorkersNo;
                    if (totalNumer >= 300)
                        return SizeCategories.Over300;
                    if (totalNumer >= 200)
                        return SizeCategories.Below300Over200;
                    if (totalNumer >= 100)
                        return SizeCategories.Below200Over100;
                    if (totalNumer >= 50)
                        return SizeCategories.Below100Over50;
                    if (totalNumer > 25)
                        return SizeCategories.Below50Over25;
                    return SizeCategories.Below25;
                }
            }
        }
        public class AcademicCertification : Base
        {
            public int FieldOfStudy_ID { get; set; }
            [ForeignKey("FieldOfStudy_ID")]
            public virtual FieldOfStudy FieldOfStudy { get; set; }
            public int? ScanFile_ID { get; set; }
            [ForeignKey("ScanFile_ID")]
            public virtual Attachment ScanFile { get; set; }
        }
        public class ExpertContact : Base
        {
            public int? County_ID { get; set; }
            [ForeignKey("County_ID")]
            public virtual County County { get; set; }
            public string Address { get; set; }
            public string PostalCode { get; set; }
            public string Phone { get; set; }
        }
        public class WorkExperience : Base
        {
            public int Document_ID { get; set; }
            [ForeignKey("Document_ID")]
            [JsonIgnore]
            public Document Document { get; set; }
            public string WorkshopName { get; set; }
            public string EmployerName { get; set; }
            public DateTime DateFrom { get; set; }
            public DateTime DateTo { get; set; }
            public string Description { get; set; }
        }
        public class SafetyCertification : Base
        {
            public enum Type
            {
                ForthyHours = 1,
                SixteenHours = 2,
                Others = 3,
            }
            public Type CourseType { get; set; }
            public string OtherCourseName { get; set; }
            public double OtherDurationInHours { get; set; }
            public DateTime DateTaken { get; set; }
            public string IssuerNumber { get; set; }
            public int? CertificationScan_ID { get; set; }
            [ForeignKey("CertificationScan_ID")]
            public virtual Attachment CertificationScan { get; set; }
            public string Description { get; set; }
        }
        public enum GenderType
        {
            Male = 1,
            Female = 2,
        }
        public enum MilitaryServiceStates
        {
            Passed = 1,
            Exempted = 2,
        }
        public enum MaritalStates
        {
            Single = 1,
            Married = 2,
        }
        public DateTime? DeactivationDate { get; set; }
        public string ReasonOfDeactivation { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
        public string FatherName { get; set; }
        public string ShenasnameNo { get; set; }
        public string NationalCode { get; set; }
        public DateTime? BirthDate { get; set; }
        public string PlaceOfBirth { get; set; }
        public GenderType? Gender { get; set; }
        public MaritalStates? MaritalStatus { get; set; }
        public MilitaryServiceStates? MilitaryStatus { get; set; }
        public string ReasonOfExemption { get; set; }
        public virtual AcademicCertification HighSchoolCertification { get; set; }
        public virtual AcademicCertification DiplomaCertification { get; set; }
        public virtual AcademicCertification BachelorsCertification { get; set; }
        public virtual AcademicCertification MastersCertification { get; set; }
        public virtual AcademicCertification PhDCertification { get; set; }
        public string AssociationMembershipNo { get; set; }
        public int? HomeContact_ID { get; set; }
        [ForeignKey("HomeContact_ID")]
        public virtual ExpertContact HomeContact { get; set; }
        public int? WorkContact_ID { get; set; }
        [ForeignKey("WorkContact_ID")]
        public virtual ExpertContact WorkContact { get; set; }
        public string Mobile { get; set; }
        public int? PhotoScan_ID { get; set; }
        [ForeignKey("PhotoScan_ID")]
        public virtual Attachment PhotoScan { get; set; }
        [ForeignKey("Shenasname_ID")]
        public virtual List<Attachment> ShenasnameScans { get; set; }
        [ForeignKey("NationalCard_ID")]
        public virtual List<Attachment> NationalCardScans { get; set; }
        public virtual Workshop CurrentWorkShop { get; set; }
        public virtual List<WorkExperience> WorkExperiences { get; set; }
        [ForeignKey("InsuranceRecord_ID")]
        public virtual List<Attachment> InsuranceRecordScans { get; set; }
        public virtual List<SafetyCertification> SafetyCertifications { get; set; }
        public List<ExpertReport> Reports { get; set; }
        public enum LevelOfEducations
        {
            NoEducation = 0,
            HighSchool = 1,
            Diploma = 2,
            Bachelor = 3,
            Master = 4,
            PhD = 5,
        }
        public LevelOfEducations LevelOfEducation
        {
            get
            {
                if (PhDCertification != null)
                    return LevelOfEducations.PhD;
                if (MastersCertification != null)
                    return LevelOfEducations.Master;
                if (BachelorsCertification != null)
                    return LevelOfEducations.Bachelor;
                if (DiplomaCertification != null)
                    return LevelOfEducations.Diploma;
                if (HighSchoolCertification != null)
                    return LevelOfEducations.HighSchool;
                return LevelOfEducations.NoEducation;
            }
        }
        public CompletionStatus BasicInfoCompleted
        {
            get
            {
                if (FirstName == null)
                {
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "نام وارد نشده است",
                    };
                }
                if (LastName == null)
                {
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "نام خانوادگی وارد نشده است",
                    };
                }
                if (FatherName == null)
                {
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "نام پدر وارد نشده است",
                    };
                }
                if (ShenasnameNo == null)
                {
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "شناسنامه وارد نشده است",
                    };
                }
                if (NationalCode == null)
                {
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "کد ملی وارد نشده است",
                    };
                }
                if (BirthDate == null)
                {
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "تاریخ تولد وارد نشده است",
                    };
                }
                if (PlaceOfBirth == null)
                {
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "محل تولد وارد نشده است",
                    };
                }
                if (MaritalStatus == null)
                {
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "وضعیت تاهل وارد نشده است",
                    };
                }
                if (Gender == null)
                {
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "جنسیت وارد نشده است",
                    };
                }
                return new CompletionStatus()
                {
                    IsComplete = true,
                };
            }
        }
        public CompletionStatus ContactInfoCompleted
        {
            get
            {
                if (Mobile == null)
                {
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "همراه وارد نشده است",
                    };
                }
                if (HomeContact == null)
                {
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "محل سکونت وارد نشده است",
                    };
                }
                if (WorkContact == null)
                {
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "محل کار وارد نشده است",
                    };
                }
                if (HomeContact.County == null)
                {
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "شعرستان سکونت وارد نشده است",
                    };
                }
                if (WorkContact.County == null)
                {
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "شهرستان محل کار وارد نشده است",
                    };
                }
                if (HomeContact.Address == null)
                {
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "آدرس محل سکونت وارد نشده است",
                    };
                }
                if (HomeContact.PostalCode == null)
                {
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "کدپستی محل سکونت وارد نشده است",
                    };
                }
                if (HomeContact.Phone == null)
                {
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "تلفن محل سکونت وارد نشده است",
                    };
                }
                if (WorkContact.Address == null)
                {
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "آدرس محل کار وارد نشده است",
                    };
                }
                if (WorkContact.PostalCode == null)
                {
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "کد پستی محل کار وارد نشده است",
                    };
                }
                if (WorkContact.Phone == null)
                {
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "تلفن محل کار وارد نشده است",
                    };
                }
                return new CompletionStatus()
                {
                    IsComplete = true,
                };
            }
        }
        public CompletionStatus BasicAttachmentsCompleted
        {
            get
            {
                if (PhotoScan_ID == null)
                {
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "تصویر شخص وارد نشده است",
                    };
                }
                if (ShenasnameScans.Count == 0)
                {
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "تصویر شناسنامه وارد نشده است",
                    };
                }
                if (NationalCardScans.Count == 0)
                {
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "تصویر کارت ملی وارد نشده است",
                    };
                }
                return new CompletionStatus()
                {
                    IsComplete = true,
                };
            }
        }
        public CompletionStatus CertificationAttachmentsCompleted
        {
            get
            {
                if (HighSchoolCertification != null && HighSchoolCertification.ScanFile_ID == null)
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "تصویر مدرک دیپلم وارد نشده است"
                    };
                if (DiplomaCertification != null && DiplomaCertification.ScanFile_ID == null)
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "تصویر مدرک کاردانی وارد نشده است"
                    };
                if (BachelorsCertification != null && BachelorsCertification.ScanFile_ID == null)
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "تصویر مدرک کارشناسی وارد نشده است"
                    };
                if (MastersCertification != null && MastersCertification.ScanFile_ID == null)
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "تصویر مدرک کارشناسی ارشد وارد نشده است"
                    };
                if (PhDCertification != null && PhDCertification.ScanFile_ID == null)
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "تصویر مدرک دکترا وارد نشده است"
                    };
                foreach (var certification in SafetyCertifications)
                    if (certification.CertificationScan_ID == null)
                        return new CompletionStatus()
                        {
                            IsComplete = false,
                            Message = "تصویر مدارک ایمنی وارد نشده است"
                        };
                return new CompletionStatus()
                {
                    IsComplete = true,
                };
            }
        }
        public CompletionStatus WorkshopAttachmentsCompleted
        {
            get
            {
                if (CurrentWorkShop == null)
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "CurrentWorkShop وجود ندارد",
                    };
                if (CurrentWorkShop.ContractScans.Count == 0)
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "تصویر قرارداد وارد نشده است",
                    };
                if (CurrentWorkShop.TechnicalProtectionCommittee && CurrentWorkShop.TechnicalProtectionCommitteeScans.Count == 0)
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "تصویر کمیته حفاظت فنی وارد نشده است",
                    };
                return new CompletionStatus()
                {
                    IsComplete = true,
                };
            }
        }
        public CompletionStatus AcademicInfoCompleted
        {
            get
            {
                if (HighSchoolCertification == null &&
                        DiplomaCertification == null &&
                        BachelorsCertification == null &&
                        MastersCertification == null &&
                        PhDCertification == null)
                {
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "هیچ مدرک تحصیلی وارد نشده است",
                    };
                }
                return new CompletionStatus()
                {
                    IsComplete = true,
                };
            }
        }
        public CompletionStatus CertificationCompleted
        {
            get
            {
                if (Requires16HourseSafetyCertification)
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "دوره شناسایی خطر و ارزیابی ریسک 16 ساعته وارد نشده است",
                    };
                if (Requires40HourseSafetyCertification)
                    return new CompletionStatus()
                    {
                        IsComplete = false,
                        Message = "دوره آموزش عمومی ایمنی 40 ساعته یا گواهی آزمون ادواری وارد نشده است",
                    };
                return new CompletionStatus()
                {
                    IsComplete = true,
                };
            }
        }
        public bool Requires16HourseSafetyCertification
        {
            get
            {
                return !((HighSchoolCertification != null && HighSchoolCertification.FieldOfStudy != null && !HighSchoolCertification.FieldOfStudy.Requires16Hours) ||
                    (DiplomaCertification != null && DiplomaCertification.FieldOfStudy != null && !DiplomaCertification.FieldOfStudy.Requires16Hours) ||
                    (BachelorsCertification != null && BachelorsCertification.FieldOfStudy != null && !BachelorsCertification.FieldOfStudy.Requires16Hours) ||
                    (MastersCertification != null && MastersCertification.FieldOfStudy != null && !MastersCertification.FieldOfStudy.Requires16Hours) ||
                    (PhDCertification != null && PhDCertification.FieldOfStudy != null && !PhDCertification.FieldOfStudy.Requires16Hours))
                    && SafetyCertifications.Count(c => c.CourseType == SafetyCertification.Type.SixteenHours) == 0;
            }
        }
        public bool Requires40HourseSafetyCertification
        {
            get
            {
                return !((HighSchoolCertification != null && HighSchoolCertification.FieldOfStudy != null && !HighSchoolCertification.FieldOfStudy.Requires40Hours) ||
                    (DiplomaCertification != null && DiplomaCertification.FieldOfStudy != null && !DiplomaCertification.FieldOfStudy.Requires40Hours) ||
                    (BachelorsCertification != null && BachelorsCertification.FieldOfStudy != null && !BachelorsCertification.FieldOfStudy.Requires40Hours) ||
                    (MastersCertification != null && MastersCertification.FieldOfStudy != null && !MastersCertification.FieldOfStudy.Requires40Hours) ||
                    (PhDCertification != null && PhDCertification.FieldOfStudy != null && !PhDCertification.FieldOfStudy.Requires40Hours))
                    && SafetyCertifications.Count(c => c.CourseType == SafetyCertification.Type.ForthyHours) == 0;
            }
        }
        public virtual List<Flow> Flows { get; set; }
        [ForeignKey("Objection_ID")]
        public virtual List<Attachment> ObjectionAttachments { get; set; }
        public Flow CurrentFlow
        {
            get
            {
                return Flows.OrderBy(f => f.DateCreated).LastOrDefault();
            }
        }
        public string NezamCode { get; set; }
        public DateTime? OlympiadRequestDate { get; set; }
        public Document()
        {
            Type = UserType.Expert;
            ShenasnameScans = new List<Attachment>();
            NationalCardScans = new List<Attachment>();
            WorkExperiences = new List<WorkExperience>();
            InsuranceRecordScans = new List<Attachment>();
            Flows = new List<Flow>();
            ObjectionAttachments = new List<Attachment>();
            Reports = new List<ExpertReport>();
            SafetyCertifications = new List<SafetyCertification>();
        }
    }
}
