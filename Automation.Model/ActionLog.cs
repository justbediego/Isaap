﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model
{
    public class ActionLog : Base
    {
        public int? User_ID { get; set; }
        [ForeignKey("User_ID")]
        public User User { get; set; }
        [StringLength(10)]
        public string ActionType { get; set; }
        [StringLength(500)]
        public string ActionName { get; set; }
        [StringLength(20)]
        public string IP { get; set; }
        [StringLength(1000)]
        public string Data { get; set; }
        [MaxLength(100)]
        public string Identity { get; set; }
    }
}