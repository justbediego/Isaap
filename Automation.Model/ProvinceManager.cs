﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model
{
    [Table("ProvinceManagers")]
    public class ProvinceManager:Staff
    {
        public int Province_ID { get; set; }
        [ForeignKey("Province_ID")]
        public Province Province { get; set; }
        public ProvinceManager()
        {
            Type = UserType.ProvinceManager;
        }
    }
}
