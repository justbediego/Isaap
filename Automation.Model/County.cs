﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model
{
    [Table("Counties")]
    public class County:Base
    {
        public int Province_ID { get; set; }
        [ForeignKey("Province_ID")]
        public virtual Province Province { get; set; }
        public List<Association> Associations { get; set; }
        public List<CountyInspector> CountyInspectors { get; set; }
        public string Name { get; set; }
        public County()
        {
            Associations = new List<Association>();
            CountyInspectors = new List<CountyInspector>();
        }
    }
}
