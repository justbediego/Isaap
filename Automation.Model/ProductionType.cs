﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model
{
    [Table("ProductionTypes")]
    public class ProductionType : Base
    {
        public string Name { get; set; }
    }
}
