﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model
{
    [Table("ProvinceInspectors")]
    public class ProvinceInspector : Staff
    {
        public int Province_ID { get; set; }
        [ForeignKey("Province_ID")]
        [JsonIgnore]
        public Province Province { get; set; }
        public string HeadFullName { get; set; }
        public string ChairmanFullName { get; set; }
        public string SupervisorFullName { get; set; }
        public ProvinceInspector()
        {
            Type = UserType.ProvinceInspector;
        }
    }
}
