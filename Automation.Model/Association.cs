﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model
{
    [Table("Associations")]
    public class Association:Staff
    {
        public int County_ID { get; set; }
        [ForeignKey("County_ID")]
        public County County { get; set; }
        public Association()
        {
            Type = UserType.Association;
        }
    }
}
