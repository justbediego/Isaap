﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model
{
    [Table("Clubs")]
    public class Club:Staff
    {
        public int Province_ID { get; set; }
        [ForeignKey("Province_ID")]
        public Province Province { get; set; }
        public Club()
        {
            Type = UserType.Club;
        }
    }
}
