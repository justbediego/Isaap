﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model
{
    [Table("FieldsOfStudy")]
    public class FieldOfStudy:Base
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string OrginalityCode { get; set; }
        public bool Requires40Hours { get; set; }
        public bool Requires16Hours { get; set; }
    }
}
