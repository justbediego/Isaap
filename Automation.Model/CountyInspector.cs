﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model
{
    [Table("CountyInspectors")]
    public class CountyInspector:Staff
    {
        public int County_ID { get; set; }
        [ForeignKey("County_ID")]
        [JsonIgnore]
        public County County { get; set; }
        public CountyInspector()
        {
            Type = UserType.CountyInspector;
        }
    }
}
