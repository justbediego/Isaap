﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model
{
    [Table("FieldsOfActivity")]
    public class FieldOfActivity : Base
    {
        public string Name { get; set; }
        public enum DangerLevels
        {
            Low = 1,
            Moderate = 2,
            High = 3,
        }
        public DangerLevels DangerLevel { get; set; }
    }
}
