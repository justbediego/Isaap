﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model
{
    public class RequirementRule : Base
    {
        public FieldOfActivity.DangerLevels DangerLevel { get; set; }
        public int Hours { get; set; }
        public Document.LevelOfEducations LevelOfEducation { get; set; }
        public Document.Workshop.SizeCategories SizeCategory { get; set; }
    }
}
