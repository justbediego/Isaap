﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Model
{
    [Table("InspectionOffices")]
    public class InspectionOffice:Staff
    {
        public InspectionOffice()
        {
            Type = UserType.InspectionOffice;
        }
    }
}
