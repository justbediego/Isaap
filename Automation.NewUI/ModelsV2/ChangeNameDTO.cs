﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Automation.NewUI.ModelsV2
{
    public class ChangeNameDTO
    {
        public int ObjectID;
        public string NewName;
    }
}