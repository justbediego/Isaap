﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Automation.NewUI.ModelsV2
{
    public class DeactiveObjectDTO
    {
        public int ObjectID;
        public string Description;
    }
}