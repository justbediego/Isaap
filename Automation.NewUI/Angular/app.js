﻿var app = angular.module('isaap', [
         'ui.router',                    // Routing
         'oc.lazyLoad',                  // ocLazyLoad
         'ui.bootstrap',                 // Ui Bootstrap
         'ngSanitize',
         'ngRoute',
         'toaster',
         'ngIdle'
]);