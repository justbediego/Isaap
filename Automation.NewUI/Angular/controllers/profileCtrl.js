﻿angular.module("isaap").controller('profileCtrl', function ($rootScope, $scope, services, $location, $uibModal, $state) {
    var vm = $scope;
    services.IsLoggedIn(function success(result) {
        if (!result.data)
            $state.go("home");
        else
            services.GetUserType(function (response) {
                switch (response.data) {
                    case 1:
                        if (!$state.includes('expert'))
                            $state.go('home');
                        vm.menuTempleteUrl = '/Statics/Expert/expertMenu.html';
                        vm.description = 'کارشناس ایمنی و حفاظت فنی';
                        $rootScope.GetDocument();
                        break;
                    case 8:
                        if (!$state.includes('admin'))
                            $state.go('home');
                        vm.menuTempleteUrl = '/Statics/Admin/adminMenu.html';
                        vm.description = 'مدیر سیستم';
                        $rootScope.GetStaff();
                        break;
                    case 9:
                        if (!$state.includes('analyzer'))
                            $state.go('home');
                        vm.menuTempleteUrl = '/Statics/Analyzer/analyzerMenu.html';
                        vm.description = 'مسئول بررسی اطلاعات';
                        break;
                    default:
                        if (!$state.includes('staff'))
                            $state.go('home');
                        switch (response.data) {
                            case 2:
                                vm.description = 'انجمن تشخیص صلاحیت';
                                break;
                            case 3:
                                vm.description = 'کانون تشخیص صلاحیت';
                                break;
                            case 4:
                                vm.description = 'بازرس شهرستان';
                                break;
                            case 5:
                                vm.description = 'بازرس استان';
                                break;
                            case 6:
                                vm.description = 'مدیر بازرسی استان';
                                break;
                            case 7:
                                vm.description = 'دفتر بازرسی';
                                break;
                        }
                        vm.menuTempleteUrl = '/Statics/Staff/staffMenu.html';
                        $rootScope.GetStaff();
                        break;
                }
            });
    });
    $rootScope.profileData = [];
    vm.profileData = [];
    vm.Logout = function () {
        services.Logout(function success(result) {
            $state.go("home");
        });
    };
    vm.OpenChangePasswordModal = function () {
        $uibModal.open({
            templateUrl: '/Statics/Modals/changePassword.html',
            animation: true,
            controller: function ($scope, services) {
                $scope.changeData;
                $scope.ChangePassword = function () {
                    if ($scope.changeData.NewPassword !== $scope.changeData.NewPasswordCopy)
                        services.ShowError('خطا در اطلاعات', 'تکرار گذرواژه صحیح نمی باشد');
                    else
                        services.ChangePassword($scope.changeData, function sucess() {
                            services.ShowSuccess('با موفقیت انجام شد');
                            $scope.Close();
                        });
                };
                $scope.Close = function () {
                    this.$close();
                }
            },
        });
    };
    vm.OpenChangeEmailModal = function () {
        $uibModal.open({
            templateUrl: '/Statics/Modals/changeEmail.html',
            animation: true,
            controller: function ($scope, services) {
                $scope.changeData = {
                    CurrentEmail: vm.profileData.Email
                };
                $scope.ChangeEmail = function () {
                    if ($scope.changeData.NewEmail !== $scope.changeData.NewEmailCopy)
                        services.ShowError('خطا در اطلاعات', 'تکرار ایمیل صحیح نمی باشد');
                    else
                        services.ModifyRecoveryEmail($scope.changeData, function sucess() {
                            services.ShowSuccess('با موفقیت انجام شد');
                            //its not worth it
                            $rootScope.profileData.Email = $scope.changeData.NewEmail;
                            vm.profileData.Email = $scope.changeData.NewEmail;
                            $scope.Close();
                        });
                };
                $scope.Close = function () {
                    this.$close();
                }
            },
        });
    };
    $rootScope.GetDocument = function () {
        services.GetMyDocumentForOverview(function success(response) {
            $rootScope.profileData = response.data;
            vm.profileData = $rootScope.profileData;
        });
    };
    $rootScope.GetStaff = function () {
        services.GetStaff(function success(response) {
            $rootScope.profileData = response.data;
            vm.profileData = $rootScope.profileData;
        });
    };
});