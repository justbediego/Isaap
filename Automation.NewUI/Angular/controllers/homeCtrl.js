﻿angular.module("isaap").controller('homeCtrl', function ($scope, services, $uibModal, $state) {
    $scope.loginUser;
    $scope.registerUser;
    $scope.captchaAddress = "/Main/GetCaptchaImage";
    //check if logged in
    services.IsLoggedIn(function success(result) {
        if (result.data) {
            goToRightState();
        };
    });
    function goToRightState() {
        services.GetUserType(function (response) {
            switch (response.data) {
                case 1:
                    $state.go('expert.document');
                    break;
                case 8:
                    $state.go('admin.clubs');
                    break;
                case 9:
                    $state.go('analyzer.documents');
                    break;
                default:
                    $state.go('staff.mydocuments');
                    break;
            }
        });
    };
    $scope.Login = function () {
        if ($scope.loginUser && $scope.loginUser.UserName && $scope.loginUser.Password) {
            services.Login($scope.loginUser, function success(result) {
                goToRightState();
            });
        } else
            services.ShowError("خطا در اطلاعات", "لطفاً تمامی اطلاعات را وارد نمایید");
    };
    $scope.RegisterExpert = function () {
        if ($scope.registerUser && $scope.registerUser.Username && $scope.registerUser.Password && $scope.registerUser.PasswordCopy) {
            if ($scope.registerUser.Password != $scope.registerUser.PasswordCopy) {
                services.ShowError("خطا در اطلاعات", "تکرار گذرواژه صحیح نمی باشد");
                return;
            }
            if (!$scope.registerUser.Agreement) {
                services.ShowError("خطا در اطلاعات", "لطفاً با قوانین موافقت نمایید");
                return;
            }
            services.RegisterExpert($scope.registerUser, function success(result) {
                goToRightState();
            });
            //reload captcha
            $scope.captchaAddress = "/Main/GetCaptchaImage?" + new Date().getTime();
        } else
            services.ShowError("خطا در اطلاعات", "لطفاً تمامی اطلاعات را وارد نمایید");
    };
    $scope.OpenAgreementModal = function () {
        $uibModal.open({
            templateUrl: '/Statics/Modals/agreement.html',
            animation: true,
            controller: function ($scope) {
                $scope.Close = function () {
                    this.$close();
                }
            },
        });
    };
    $scope.OpenForgotPasswordModal = function () {
        $uibModal.open({
            templateUrl: '/Statics/Modals/forgotPassword.html',
            animation: true,
            controller: function ($scope, services, $uibModal) {
                $scope.request;
                $scope.sentEmail = null;
                $scope.isDisabled = false;
                $scope.ResetPassword = function () {
                    if ($scope.request && $scope.request.UsernameOrEmail) {
                        $scope.isDisabled = true;
                        $scope.sentEmail = null;
                        services.ResetPassword(
                            $scope.request, (result) => {
                                $scope.sentEmail = result.data;
                                $scope.request.UsernameOrEmail = '';
                                $scope.isDisabled = false;
                            }, () => {
                                $scope.isDisabled = false;
                            }
                        );
                    }
                };
                $scope.Close = function () {
                    this.$close();
                }
            },
        });
    }
});
