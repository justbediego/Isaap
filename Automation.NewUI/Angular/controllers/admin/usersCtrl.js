﻿angular.module("isaap").controller('usersCtrl', function ($rootScope, $scope, services, $uibModal, $state) {
    const vm = $scope;
    vm.OpenChangeOthersPasswordModal = () => {
        $uibModal.open({
            templateUrl: '/Statics/Admin/Modals/changeOthersPassword.html',
            animation: true,
            controller: function ($scope) {
                var vm2 = $scope;
                vm2.username = '';
                vm2.adminPassword = '';
                vm2.newPassword = '';
                vm2.newPasswordCopy = '';
                vm2.ChangePassword = () => {
                    if (vm2.newPasswordCopy == vm2.newPassword) {
                        services.ChangeOthersPassword({ AdminPassword: vm2.adminPassword, Username: vm2.username, NewPasswrod: vm2.newPassword }, () => {
                            services.ShowSuccess("تغییر گذرواژه", "با موفقیت انجام شد");
                            vm2.Close();
                        });
                    } else
                        services.ShowError("خطا در اطلاعات", "تکرار گذرواژه صحیح نمی باشد");
                };
                vm2.Close = function () {
                    this.$close();
                }
            }
        });
    }
});