﻿angular.module("isaap").controller('countyinspectorsCtrl', function ($rootScope, $scope, services, $uibModal, $state) {
    var vm = $scope;
    reloadData = () => {
        services.GetAllCountyCountyInspectors((result) => {
            vm.countyInspectors = result.data;
        });
    }
    reloadData();
    vm.OpenDeactivationModal = (countyInspectorID) => {
        $uibModal.open({
            templateUrl: '/Statics/Admin/Modals/deactivateStaff.html',
            animation: true,
            controller: function ($scope) {
                var vm2 = $scope;
                vm2.description = '';
                vm2.DeactivateClub = () => {
                    services.DeactivateCountyInspector({ ObjectID: countyInspectorID, Description: vm2.description }, () => {
                        reloadData();
                        vm2.Close();
                    });
                };
                vm2.Close = function () {
                    this.$close();
                }
            }
        });
    }

    vm.OpenChangePasswordModal = (username) => {
        $uibModal.open({
            templateUrl: '/Statics/Admin/Modals/changePassword.html',
            animation: true,
            controller: function ($scope) {
                var vm2 = $scope;
                vm2.username = username;
                vm2.adminPassword = '';
                vm2.newPassword = '';
                vm2.newPasswordCopy = '';
                vm2.ChangePassword = () => {
                    if (vm2.newPasswordCopy == vm2.newPassword) {
                        services.ChangeOthersPassword({ AdminPassword: vm2.adminPassword, Username: vm2.username, NewPasswrod: vm2.newPassword }, () => {
                            reloadData();
                            vm2.Close();
                        });
                    } else
                        services.ShowError("خطا در اطلاعات", "تکرار گذرواژه صحیح نمی باشد");
                };
                vm2.Close = function () {
                    this.$close();
                }
            }
        });
    }

    vm.ActivateCountyInspector = (id) => {
        services.ActivateCountyInspector({ ObjectID: id }, () => {
            reloadData();
        });
    }
    vm.OpenAddCountyModal = () => {
        $uibModal.open({
            templateUrl: '/Statics/Admin/Modals/addCounty.html',
            animation: true,
            controller: function ($scope) {
                var vm2 = $scope;
                vm2.provinceID = "";
                vm2.name = '';
                services.GetAllProvinces((response) => vm2.provinces = response.data);
                vm2.AddCounty = () => {
                    services.AddCounty({ ProvinceID: vm2.provinceID, Name: vm2.name }, () => {
                        reloadData();
                        vm2.Close();
                    });
                };
                vm2.Close = function () {
                    this.$close();
                }
            }
        });
    }
    vm.OpenChangeCountyNameModal = (id, countyName) => {
        $uibModal.open({
            templateUrl: '/Statics/Admin/Modals/changeCountyName.html',
            animation: true,
            controller: function ($scope) {
                var vm2 = $scope;
                vm2.name = countyName;
                vm2.ChangeName = () => {
                    services.ChangeCountyName({ ObjectID: id, NewName: vm2.name }, () => {
                        reloadData();
                        vm2.Close();
                    });
                };
                vm2.Close = function () {
                    this.$close();
                }
            }
        });
    }
    vm.OpenAddCountyInspectorModal = (id, province, county) => {
        $uibModal.open({
            templateUrl: '/Statics/Admin/Modals/addCountyInspector.html',
            animation: true,
            controller: function ($scope) {
                var vm2 = $scope;
                vm2.province = province;
                vm2.county = county;
                vm2.username = '';
                vm2.newPassword = '';
                vm2.newPasswordCopy = '';
                vm2.AddCountyInspector = () => {
                    if (vm2.newPasswordCopy == vm2.newPassword) {
                        services.AddCountyInspector({ CountyID: id, Username: vm2.username, Password: vm2.newPassword }, () => {
                            reloadData();
                            vm2.Close();
                        });
                    } else
                        services.ShowError("خطا در اطلاعات", "تکرار گذرواژه صحیح نمی باشد");
                };
                vm2.Close = function () {
                    this.$close();
                }
            }
        });
    }
});