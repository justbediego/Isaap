﻿angular.module("isaap").controller('clubsCtrl', function ($rootScope, $scope, services, $uibModal, $state) {
    var vm = $scope;
    reloadData = () => {
        services.GetAllProvinceClubs((result) => {
            vm.provinces = result.data;
        });
    }
    reloadData();
    vm.OpenDeactivationModal = (clubID) => {
        $uibModal.open({
            templateUrl: '/Statics/Admin/Modals/deactivateStaff.html',
            animation: true,
            controller: function ($scope) {
                var vm2 = $scope;
                vm2.description = '';
                vm2.DeactivateClub = () => {
                    services.DeactivateClub({ ObjectID: clubID, Description: vm2.description }, () => {
                        reloadData();
                        vm2.Close();
                    });
                };
                vm2.Close = function () {
                    this.$close();
                }
            }
        });
    }

    vm.OpenChangeProvinceChairmanModal = (provinceID, chairmanFullName) => {
        $uibModal.open({
            templateUrl: '/Statics/Admin/Modals/changeProvinceName.html',
            animation: true,
            controller: function ($scope) {
                var vm2 = $scope;
                vm2.type = 'chairman';
                vm2.name = chairmanFullName;
                vm2.ChangeName = () => {
                    services.ChangeProvinceChairmanName({ ObjectID: provinceID, NewName: vm2.name }, () => {
                        reloadData();
                        vm2.Close();
                    });
                };
                vm2.Close = function () {
                    this.$close();
                }
            }
        });
    }

    vm.OpenChangeProvinceHeadModal = (provinceID, headFullName) => {
        $uibModal.open({
            templateUrl: '/Statics/Admin/Modals/changeProvinceName.html',
            animation: true,
            controller: function ($scope) {
                var vm2 = $scope;
                vm2.type = 'head';
                vm2.name = headFullName;
                vm2.ChangeName = () => {
                    services.ChangeProvinceHeadName({ ObjectID: provinceID, NewName: vm2.name }, () => {
                        reloadData();
                        vm2.Close();
                    });
                };
                vm2.Close = function () {
                    this.$close();
                }
            }
        });
    }

    vm.OpenChangePasswordModal = (username) => {
        $uibModal.open({
            templateUrl: '/Statics/Admin/Modals/changePassword.html',
            animation: true,
            controller: function ($scope) {
                var vm2 = $scope;
                vm2.username = username;
                vm2.adminPassword = '';
                vm2.newPassword = '';
                vm2.newPasswordCopy = '';
                vm2.ChangePassword = () => {
                    if (vm2.newPasswordCopy == vm2.newPassword) {
                        services.ChangeOthersPassword({ AdminPassword: vm2.adminPassword, Username: vm2.username, NewPasswrod: vm2.newPassword }, () => {
                            reloadData();
                            vm2.Close();
                        });
                    } else 
                        services.ShowError("خطا در اطلاعات", "تکرار گذرواژه صحیح نمی باشد");
                };
                vm2.Close = function () {
                    this.$close();
                }
            }
        });
    }

    vm.ActivateClub = (id) => {
        services.ActivateClub({ ObjectID: id }, () => {
            reloadData();
        });
    }
    vm.AddClub = (provinceID, username, password) => {
        services.AddClub({ ProvinceID: provinceID, Username: username, Password: password }, () => {
            reloadData();
        });
    }
});