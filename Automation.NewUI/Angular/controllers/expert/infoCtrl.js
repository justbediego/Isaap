﻿angular.module("isaap").controller('infoCtrl', function ($rootScope, $scope, services, $uibModal, $state) {
    var vm = $scope;
    vm.fieldsOfStudy;
    vm.workCounties;
    vm.homeCounties;
    vm.provinces;
    vm.fieldsOfStudy;
    $rootScope.$watch('profileData', function () {
        vm.document = $rootScope.profileData;
        vm.LoadWorkCounties();
        vm.LoadHomeCounties();
    });
    //Load All Provinces
    services.GetAllProvinces(function (response) {
        vm.provinces = response.data;
    });
    services.GetAllFieldsOfStudy(function (response) {
        vm.fieldsOfStudy = response.data;
    });
    vm.LoadWorkCounties = function () {
        if (vm.document.WorkContact.County.Province_ID > 0) {
            services.GetAllCounties(vm.document.WorkContact.County.Province_ID, function (response) {
                vm.workCounties = response.data;
            });
        } else
            vm.workCounties = [];
    };
    vm.LoadHomeCounties = function () {
        if (vm.document.HomeContact.County.Province_ID > 0) {
            services.GetAllCounties(vm.document.HomeContact.County.Province_ID, function (response) {
                vm.homeCounties = response.data;
            });
        } else
            vm.homeCounties = [];
    };
    vm.ModifyBasicInfo = function () {
        services.ModifyBasicInfo(vm.document, function () {
            $rootScope.GetDocument();
        });        
    };
    vm.ModifyAcademicInfo = function () {
        $rootScope.GetDocument();
    };
    vm.ModifyContactInfo = function () {
        services.ModifyContactInfo(vm.document, function () {
            $rootScope.GetDocument();
        });
    };
    vm.ModifyAcademicInfo = function () {
        services.ModifyAcademicInfo(vm.document, function () {
            $rootScope.GetDocument();
        });
    };
});