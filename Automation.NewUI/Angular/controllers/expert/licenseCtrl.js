﻿angular.module("isaap").controller('licenseCtrl', function ($scope, services, $location, $stateParams) {
    var vm = $scope;
    vm.canSign = false;
    services.GetUserType(function (response) {
        vm.canSign = response.data == 5 || response.data == 6 || response.data == 7;
    });
    services.GetLicense($stateParams.username, function success(response) {
        vm.data = response.data;
    });
});