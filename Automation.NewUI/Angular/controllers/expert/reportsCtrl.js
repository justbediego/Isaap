﻿angular.module("isaap").controller('reportsCtrl', function ($scope, services, $location, $uibModal, $state) {
    $scope.availableSeasons = [];
    $scope.initiate = () => {
        services.GetAvailableSeasonalReports((response) => {
            $scope.availableSeasons = response.data;
        });
    }
    $scope.initiate();    
    $scope.seasonFromIndex = (seasonIndex) => {
        switch (seasonIndex) {
            case 1: return "بهار";
            case 2: return "تابستان";
            case 3: return "پاییز";
            case 4: return "زمستان";
        }
    }
    $scope.removeReport = (reportID) => {
        services.RemoveReport({ ObjectID: reportID }, () => {
            $scope.initiate();
        });
    }
    $scope.ToggleReportVisibility = (reportID) => {
        services.ToggleReportVisibility ({ ObjectID: reportID }, () => {
            $scope.initiate();
        });
    }
});