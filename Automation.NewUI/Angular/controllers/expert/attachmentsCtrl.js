﻿angular.module("isaap").controller('attachmentsCtrl', function ($rootScope, $scope, services, $uibModal, $state) {
    var vm = $scope;
    $rootScope.$watch('profileData', function () {
        vm.document = $rootScope.profileData;
    });
    vm.GetDocument = function () {
        $rootScope.GetDocument();
    };
    vm.RemovePhotoScan = function () {
        services.RemovePhotoScan(function () {
            vm.GetDocument();
        });
    };
    vm.RemoveShenasnameScan = function (scan) {
        services.RemoveShenasnameScan(scan, function () {
            vm.GetDocument();
        });
    };
    vm.RemoveNationalCardScan = function (scan) {
        services.RemoveNationalCardScan(scan, function () {
            vm.GetDocument();
        });
    };
    vm.RemoveHighSchoolScan = function () {
        services.RemoveHighSchoolScan(function () {
            vm.GetDocument();
        });
    };
    vm.RemoveDiplomaScan = function () {
        services.RemoveDiplomaScan(function () {
            vm.GetDocument();
        });
    };
    vm.RemoveBachelorScan = function () {
        services.RemoveBachelorScan(function () {
            vm.GetDocument();
        });
    };
    vm.RemoveMasterScan = function () {
        services.RemoveMasterScan(function () {
            vm.GetDocument();
        });
    };
    vm.RemovePhdScan = function () {
        services.RemovePhdScan(function () {
            vm.GetDocument();
        });
    };
    vm.RemoveCertificationScan = function (scan) {
        services.RemoveCertificationScan(scan, function () {
            vm.GetDocument();
        });
    };
    vm.RemoveContractScan = function (scan) {
        services.RemoveContractScan(scan, function () {
            vm.GetDocument();
        });
    };
    vm.RemoveTPCScan = function (scan) {
        services.RemoveTPCScan(scan, function () {
            vm.GetDocument();
        });
    };
    vm.RemoveInsuranceScan = function (scan) {
        services.RemoveInsuranceScan(scan, function () {
            vm.GetDocument();
        });
    };
});