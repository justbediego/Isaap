﻿angular.module("isaap").controller('documentCtrl', function ($rootScope, $scope, services, $uibModal, $state) {
    var vm = $scope;
    vm.objection = { Reason: '' };
    $rootScope.$watch('profileData', function () {
        vm.document = $rootScope.profileData;
        vm.isExpert = true;
    });
    vm.SignAgreement = function () {
        services.SignAgreement(function success() {
            $rootScope.GetDocument();
        });
    };
    vm.RemoveObjectionScan = function (scan) {
        services.RemoveObjectionScan(scan, function success() {
            $rootScope.GetDocument();
        });
    };
    vm.ObjectifyOnce = function () {
        services.ObjectifyOnce(vm.objection, function success() {
            $rootScope.GetDocument();
        });
    };
    vm.ObjectifyTwice = function () {
        services.ObjectifyTwice(vm.objection, function success() {
            $rootScope.GetDocument();
        });
    };
});