﻿angular.module("isaap").controller('requestCtrl', function ($rootScope, $scope, services, $location, $uibModal, $state) {
    var vm = $scope;
    vm.fieldsOfActivity;
    vm.productionTypes;
    $rootScope.$watch('profileData', function () {
        vm.document = $rootScope.profileData;
    });
    services.GetAllFieldsOfActivity(function (response) {
        vm.fieldsOfActivity = response.data;
    });
    services.GetAllProductionTypes(function (response) {
        vm.productionTypes = response.data;
    });
    vm.ShowTechnicalProtectionAlert = function () {
        services.ShowAlertModal('توجه', 'کارفرمای محترم لازم است کمیته حفاظت و بهداشت کار در کارگاه تشکیل شود در غیر اینصورت تایید صلاحیت کارشناس ایمنی و حفاظت فنی امکان پذیر نمی باشد');
    };
    vm.SendRequest = function () {
        services.SendRequest(vm.document, function () {
            $state.go("expert.info");
            $rootScope.GetDocument();
        });
    };
});