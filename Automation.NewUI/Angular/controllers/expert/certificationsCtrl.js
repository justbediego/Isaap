﻿angular.module("isaap").controller('certificationCtrl', function ($rootScope, $scope, services, $uibModal, $state) {
    var vm = $scope;
    $rootScope.$watch('profileData', function () {
        vm.document = $rootScope.profileData;
    });
    vm.RemoveCertification = function (certification) {
        services.RemoveCertification(certification, function () {
            $rootScope.GetDocument();
        });
    }
    vm.EditCertification = function (certification) {
        openEditOrNew(certification);
    }
    vm.OpenInsertCertificationModal = function () {
        openEditOrNew();
    }
    var openEditOrNew = function (certification) {
        $uibModal.open({
            templateUrl: '/Statics/Expert/Modals/newCertification.html',
            animation: true,
            controller: function ($scope, certification) {
                var vm2 = $scope;
                vm2.certification = {
                    Id: -1,
                };
                if (certification != undefined)
                    vm2.certification = JSON.parse(JSON.stringify(certification));
                vm2.AddOrEditCertification = function () {
                    services.AddOrEditCertification(vm2.certification, function () {
                        vm2.Close();
                        $rootScope.GetDocument();
                    });
                },
                vm2.Close = function () {
                    this.$close();
                }
            },
            resolve: {
                certification: function () {
                    return certification;
                },
            },
        });
    };
});