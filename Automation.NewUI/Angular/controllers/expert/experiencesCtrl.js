﻿angular.module("isaap").controller('experiencesCtrl', function ($rootScope, $scope, services, $uibModal, $state) {
    var vm = $scope;
    $rootScope.$watch('profileData', function () {
        vm.document = $rootScope.profileData;
    });
    vm.RemoveExperience = function (experience) {
        services.RemoveExperience(experience, function () {
            $rootScope.GetDocument();
        });
    }
    vm.EditExperience = function (experience) {
        openEditOrNew(experience);
    }
    vm.OpenInsertExperienceModal = function () {
        openEditOrNew();
    }
    var openEditOrNew = function (experience) {
        $uibModal.open({
            templateUrl: '/Statics/Expert/Modals/newExperience.html',
            animation: true,
            controller: function ($scope, experience) {
                var vm2 = $scope;
                vm2.experience = {
                    Id: -1,
                };
                if (experience != undefined)
                    vm2.experience = JSON.parse(JSON.stringify(experience));
                vm2.AddOrEditWorkExperience = function () {
                    services.AddOrEditWorkExperience(vm2.experience, function () {
                        vm2.Close();
                        $rootScope.GetDocument();
                    });
                },
                vm2.Close = function () {
                    this.$close();
                }
            },
            resolve: {
                experience: function () {
                    return experience;
                },
            },
        });
    };
});