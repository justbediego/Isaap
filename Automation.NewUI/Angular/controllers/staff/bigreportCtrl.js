﻿angular.module("isaap").controller('bigreportCtrl', function ($rootScope, $scope, services, $uibModal, $state) {
    var vm = $scope;
    vm.currentFlowChart = null;
    vm.authorizedChart = null;
    vm.certificationChart = null;
    vm.registerChart = null;
    //
    services.GetCurrentFlowReport(function (response) {
        vm.currentFlowChart = {
            chartdata: {
                labels: ["بدون درخواست کارفرما", "ارسال نشده", "بازگشت خورده", "در دست انجمن", "در دست کانون", "در دست بازرس شهرستان", "در دست بازرس استان", "صلاحیت داده شده", "در دست مدیر بازرسی استان", "در دست اداره کل"],
                datasets: [{
                    data: [
                        response.data.NotRequestedCount,
                        response.data.NotSentCount,
                        response.data.RejectedCount,
                        response.data.InAssociationCount,
                        response.data.InClubCount,
                        response.data.InCountyInspectorCount,
                        response.data.InProvinceInspectorCount,
                        response.data.AuthorizedCount,
                        response.data.InProvinceManagerCount,
                        response.data.InInspectionOfficeCount,
                    ],
                    backgroundColor: ['#f8cbc8', '#f87b6a', '#90a8d0', '#035184', '#fadd3a', '#96dbde', '#9896a3', '#df4831', '#b28e6a', '#76c453']
                }]
            },
            options: {
                responsive: true
            },
        };
    });
    //
    services.GetAuthorizationByNumber(function (response) {
        vm.authorizedChart = {
            chartdata: {
                labels: ["كارگاه 25 نفر و کمتر", "كارگاه بالای 25 نفر"],
                datasets: [{
                    data: [
                        response.data.Below25,
                        response.data.Over25,
                    ],
                    backgroundColor: ['#96dbde', '#76c453']
                }]
            },
            options: {
                responsive: true
            },
        };
    });
    //
    services.GetSafetyCertificationReport(function (response) {
        vm.certificationChart = {
            chartdata: {
                labels: ["دوره آموزش عمومی ایمنی", "دوره شناسایی خطر و ارزیابی ریسک", "سایر مدارک"],
                datasets: [
                    {
                        label: "تعداد",
                        backgroundColor: 'rgba(26,179,148,0.5)',
                        borderColor: "rgba(26,179,148,0.7)",
                        pointBackgroundColor: "rgba(26,179,148,1)",
                        pointBorderColor: "#fff",
                        data: [
                            response.data.ForthyHours,
                            response.data.SixteenHourse,
                            response.data.Others
                        ]
                    }
                ]
            },
            options: {
                responsive: true
            },
        }
    });
    //
    services.GetRegisterHistogramReport(function (response) {
        vm.registerChart = {
            chartdata: {
                labels: [],
                datasets: [
                    {
                        label: "تعداد در ماه",
                        backgroundColor: 'rgba(26,179,148,0.5)',
                        borderColor: "rgba(26,179,148,0.7)",
                        pointBackgroundColor: "rgba(26,179,148,1)",
                        pointBorderColor: "#fff",
                        data: []
                    }, {
                        label: "تعداد کل",
                        backgroundColor: 'rgba(220, 220, 220, 0.5)',
                        pointBorderColor: "#fff",
                        data: []
                    }
                ],
            },
            options: {
                responsive: true
            },
        };
        for (var i = 0; i < response.data.Months.length; i++) {
            vm.registerChart.chartdata.labels.push(response.data.Months[i].Name);
            vm.registerChart.chartdata.datasets[0].data.push(response.data.Months[i].Count);
            vm.registerChart.chartdata.datasets[1].data.push(response.data.Months[i].Sum);
        }
    });
});
