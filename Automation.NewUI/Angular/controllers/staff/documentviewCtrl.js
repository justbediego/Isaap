﻿angular.module("isaap").controller('documentviewCtrl', function ($window, $rootScope, $scope, services, $uibModal, $state, $stateParams) {
    var vm = $scope;
    vm.decision = null;
    $rootScope.$watch('profileData', function () {
        vm.staff = $rootScope.profileData;
    });
    services.GetDocumentForOverview($stateParams.username, function (response) {
        vm.document = response.data;
        vm.decision = {
            DocumentUsername: vm.document.UserName,
            Description: ''
        };
    });
    vm.Accept = function () {
        services.AcceptDocument(vm.decision, function () {
            $window.history.back();
        });
    }
    vm.Reject = function () {
        services.RejectDocument(vm.decision, function () {
            $window.history.back();
        });
    }
    vm.Authorize = function () {
        services.AuthorizeDocument(vm.decision, function () {
            $window.history.back();
        });
    }
    vm.Unauthorize = function () {
        services.UnauthorizeDocument(vm.decision, function () {
            $window.history.back();
        });
    }
});