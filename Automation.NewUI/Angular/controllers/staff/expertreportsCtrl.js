﻿angular.module("isaap").controller('expertreportsCtrl', function ($rootScope, $scope, services, $uibModal, $state) {
    var vm = $scope;
    vm.pagination = {
        CurrentPage: 1,
        PageSize: 10,
    };
    vm.reports = [];
    vm.filter = {
        Phrase: '',
        AppliedPhrase: '',
    };
    vm.Search = function () {
        vm.filter.AppliedPhrase = vm.filter.Phrase;
        vm.pagination.CurrentPage = 1;
        vm.LoadReports();
    };
    vm.EmptyFilter = function () {
        vm.filter.AppliedPhrase = '';
        vm.filter.Phrase = '';
        vm.pagination.CurrentPage = 1;
        vm.LoadReports();
    };
    vm.LoadReports = () => {
        services.GetAllExportReports(vm.filter.AppliedPhrase, (vm.pagination.CurrentPage - 1) * vm.pagination.PageSize, vm.pagination.PageSize, (response) => {
            vm.reports = response.data;
        });
    }
    vm.seasonFromIndex = (seasonIndex) => {
        switch (seasonIndex) {
            case 1: return "بهار";
            case 2: return "تابستان";
            case 3: return "پاییز";
            case 4: return "زمستان";
        }
    }
    vm.LoadReports();
    vm.OpenComments = (reportId) => {
        $uibModal.open({
            templateUrl: '/Statics/Staff/Modals/reportComments.html',
            animation: true,
            controller: function ($scope) {
                var vm2 = $scope;
                vm2.newComment = null;
                vm2.isAccepted = false;
                vm2.comments = [];
                services.GetReportComments({ reportId: reportId }, (response) => {
                    vm2.comments = response.data;
                    for (let i = 0; i < vm2.comments.length; i++)
                        if (vm2.comments[i].IsAccepted) {
                            vm2.isAccepted = true;
                            break;
                        }
                });
                vm2.RejectReport = () => {
                    services.AddExpertReportComment({ ReportID: reportId, Text: vm2.newComment, IsAccepted: false }, (response) => {
                        vm2.Close();
                        vm.LoadReports();
                    });
                }
                vm2.AcceptReport = () => {
                    services.AddExpertReportComment({ ReportID: reportId, Text: vm2.newComment, IsAccepted: true }, (response) => {
                        vm2.Close();
                        vm.LoadReports();
                    });
                }
                vm2.Close = function () {
                    this.$close();
                }
            }
        });
    }
    vm.ChangePage = function () {
        vm.LoadReports();
    };
});