﻿angular.module("isaap").controller('waitingdocumentsCtrl', function ($rootScope, $scope, services, $uibModal, $state) {
    var vm = $scope;
    vm.pagination = {
        CurrentPage: 1,
        PageSize: 10,
    };
    vm.documents = [];
    vm.filter = {
        Phrase: '',
        AppliedPhrase: '',
    };
    vm.Search = function () {
        vm.filter.AppliedPhrase = vm.filter.Phrase;
        vm.pagination.CurrentPage = 1;
        vm.GetAllDocuments();
    };
    vm.EmptyFilter = function () {
        vm.filter.AppliedPhrase = '';
        vm.filter.Phrase = '';
        vm.pagination.CurrentPage = 1;
        vm.GetAllDocuments();
    };
    $rootScope.$watch('profileData', function () {
        vm.staff = $rootScope.profileData;
    });
    vm.GetAllDocuments = function () {
        services.GetAllWaitingDocuments(vm.filter.AppliedPhrase, (vm.pagination.CurrentPage - 1) * vm.pagination.PageSize, vm.pagination.PageSize, function (response) {
            vm.documents = response.data;
        });
    }
    vm.GetAllDocuments();
    vm.ChangePage = function () {
        vm.GetAllDocuments();
    };
});