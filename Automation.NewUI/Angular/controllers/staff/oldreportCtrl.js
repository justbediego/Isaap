﻿angular.module("isaap").controller('oldreportCtrl', function ($rootScope, $scope, services, $uibModal, $state) {
    var vm = $scope;
    vm.fieldsOfStudy = [];
    vm.provinces = [];
    vm.homeCounties = [];
    vm.workCounties = [];
    vm.fieldsOfActivity = [];
    vm.productionTypes = [];
    vm.ShowAdvancedSearch = false;
    vm.documents = [];
    vm.currentPage = 1;
    vm.pageSize = 10;
    vm.reportData = {
        FullName: null,
        Mobile: null,
        CurrentFlowType: null,
        PersianBirthdayFrom: null,
        PersianBirthdayTo: null,
        PersianRegisterDateFrom: null,
        PersianRegisterDateTo: null,
        PersianCurrentFlowDateFrom: null,
        PersianCurrentFlowDateTo: null,
        NationalCode: null,
        Shenasname: null,
        PlaceOfBirth: null,
        Gender: null,
        MaritalStatus: null,
        MilitaryStatus: null,
        OlympiadRequest: null,
        FieldOfStudyId: null,
        FieldOfActivityId: null,
        ProductionTypeId: null,
        HomeProvinceId: null,
        HomeCountyId: null,
        WorkProvinceId: null,
        WorkCountyId: null,
        UnitName: null,
        EmployerFullName: null,
        AssociationNumber: null,
        Take: null,
        Skip: null,
    };
    vm.reportDataCopy = [];
    vm.ToggleAdvancedSearch = function () {
        vm.ShowAdvancedSearch = !vm.ShowAdvancedSearch;
    }
    //Load All Provinces
    services.GetAllProvinces(function (response) {
        vm.provinces = response.data;
    });
    services.GetAllFieldsOfStudy(function (response) {
        vm.fieldsOfStudy = response.data;
    });
    services.GetAllFieldsOfActivity(function (response) {
        vm.fieldsOfActivity = response.data;
    });
    services.GetAllProductionTypes(function (response) {
        vm.productionTypes = response.data;
    });
    vm.LoadWorkCounties = function () {
        if (vm.reportData.WorkProvinceId > 0) {
            services.GetAllCounties(vm.reportData.WorkProvinceId, function (response) {
                vm.workCounties = response.data;
            });
        } else
            vm.workCounties = [];
    };
    vm.LoadHomeCounties = function () {
        if (vm.reportData.HomeProvinceId > 0) {
            services.GetAllCounties(vm.reportData.HomeProvinceId, function (response) {
                vm.homeCounties = response.data;
            });
        } else
            vm.homeCounties = [];
    };
    vm.GetResult = function () {
        vm.reportDataCopy = JSON.parse(JSON.stringify(vm.reportData));
        vm.reportDataCopy.Take = vm.pageSize;
        vm.reportDataCopy.Skip = 0;
        services.GetDocumentsByFilter(vm.reportDataCopy, function (response) {
            vm.documents = response.data;
        });
    };
    vm.ChangePage = function () {
        vm.reportDataCopy.Take = vm.pageSize;
        vm.reportDataCopy.Skip = (vm.currentPage - 1) * vm.pageSize;
        services.GetDocumentsByFilter(vm.reportDataCopy, function (response) {
            vm.documents = response.data;
        });
    };
});