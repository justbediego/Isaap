﻿angular.module('isaap').service('services', ['$http', '$uibModal', 'toaster', function ($http, $uibModal, toaster) {
    var vm = this;
    var waitModal = null;
    //************COMMON*********************
    vm.ShowAlertModal = function (title, text) {
        $uibModal.open(
            {
                templateUrl: '/Statics/Modals/alertModal.html',
                animation: true,
                controller: function ($scope) {
                    $scope.title = title;
                    $scope.text = text;
                    $scope.Close = function () {
                        this.$close();
                    }
                },
            });
    };
    vm.ShowWaitModal = function (text) {
        if (waitModal !== null)
            vm.HideWaitModal();
        waitModal = $uibModal.open(
            {
                backdrop: 'static',
                keyboard: false,
                templateUrl: '/Statics/Modals/waitModal.html',
                animation: true,
                controller: function ($scope) {
                    $scope.text = text;
                },
            });
    }
    vm.HideWaitModal = function () {
        if (waitModal !== null)
            waitModal.close();
        waitModal = null;
    }
    vm.ShowError = function (title, text) {
        toaster.error(title, text);
    };
    vm.ShowSuccess = function (title, text) {
        toaster.success(title, text);
    }

    //************CALLER************
    function CallServer(method, location, data, success, fail, showWait, waitText) {
        if (showWait)
            vm.ShowWaitModal(waitText);
        switch (method) {
            case 'post':
                $http.post(location, data).then(function (result) {
                    if (showWait)
                        vm.HideWaitModal();
                    if (success)
                        success(result);
                }, function (e) {
                    if (showWait)
                        vm.HideWaitModal();
                    vm.ShowError('سرور', e.data.ExceptionMessage);
                    if (fail)
                        fail();
                });
                break;
            case 'get':
                $http.get(location, { params: data }).then(function (result) {
                    if (showWait)
                        vm.HideWaitModal();
                    if (success)
                        success(result);
                }, function (e) {
                    if (showWait)
                        vm.HideWaitModal();
                    vm.ShowError('سرور', e.data.ExceptionMessage);
                    if (fail)
                        fail();
                });
                break;
        }
    }

    //************AUTHENTICATION************
    vm.IsLoggedIn = (success, fail) => CallServer('get', '/api/Authentication/IsLoggedIn', null, success, fail, false);
    vm.GetUsername = (success, fail) => CallServer('get', '/api/Authentication/GetUsername', null, success, fail, false);
    vm.Login = (isaapUser, success, fail) => CallServer('post', '/api/Authentication/Login', isaapUser, success, fail, true, 'در حال ورود به سیستم');
    vm.Logout = function (success, fail) {
        CallServer('get', '/api/Authentication/Logout', null, success, fail, false);
    };
    vm.ResetPassword = function (forgotPasswordRequest, success, fail) {
        CallServer('post', '/api/Authentication/ResetPassword', forgotPasswordRequest, success, fail, false);
    };
    vm.RegisterExpert = function (registerExpertData, success, fail) {
        CallServer('post', '/api/Authentication/RegisterExpert', registerExpertData, success, fail, true, 'در حال ثبت نام');
    };
    vm.ChangePassword = function (changePasswordData, success, fail) {
        CallServer('post', '/api/Authentication/ChangePassword', changePasswordData, success, fail, false);
    };
    vm.ModifyRecoveryEmail = (changeEmailData, success, fail) => {
        CallServer('post', '/api/Authentication/ModifyRecoveryEmail', changeEmailData, success, fail, false);
    };
    vm.GetUserType = function (success, fail) {
        CallServer('get', '/api/Authentication/GetUserType', null, success, fail, false);
    };

    //************EXPERT************
    vm.ModifyBasicInfo = function (data, success, fail) {
        CallServer('post', '/api/Expert/ModifyBasicInfo', data, success, fail, true, 'در حال تغییر اطلاعات');
    }
    vm.ModifyContactInfo = function (data, success, fail) {
        CallServer('post', '/api/Expert/ModifyContactInfo', data, success, fail, true, 'در حال تغییر اطلاعات');
    }
    vm.ModifyAcademicInfo = function (data, success, fail) {
        CallServer('post', '/api/Expert/ModifyAcademicInfo', data, success, fail, true, 'در حال تغییر اطلاعات');
    }
    vm.AddOrEditWorkExperience = function (data, success, fail) {
        CallServer('post', '/api/Expert/AddOrEditWorkExperience', data, success, fail, true, 'در حال تغییر سوابق کاری');
    }
    vm.RemoveExperience = function (data, success, fail) {
        CallServer('post', '/api/Expert/RemoveExperience', data, success, fail, true, 'در حال حذف سوابق کاری');
    }
    vm.AddOrEditCertification = function (data, success, fail) {
        CallServer('post', '/api/Expert/AddOrEditCertification', data, success, fail, true, 'در حال تغییر دوره های آموزشی');
    }
    vm.RemoveCertification = function (data, success, fail) {
        CallServer('post', '/api/Expert/RemoveCertification', data, success, fail, true, 'در حال حذف دوره های آموزشی');
    }
    vm.RemovePhotoScan = function (success, fail) {
        CallServer('post', '/api/Expert/RemovePhotoScan', null, success, fail, true, 'در حال حذف تصویر');
    }
    vm.RemoveShenasnameScan = function (data, success, fail) {
        CallServer('post', '/api/Expert/RemoveShenasnameScan', data, success, fail, true, 'در حال حذف تصویر');
    }
    vm.RemoveNationalCardScan = function (data, success, fail) {
        CallServer('post', '/api/Expert/RemoveNationalCardScan', data, success, fail, true, 'در حال حذف تصویر');
    }
    vm.RemoveHighSchoolScan = function (success, fail) {
        CallServer('post', '/api/Expert/RemoveHighSchoolScan', null, success, fail, true, 'در حال حذف تصویر');
    }
    vm.RemoveDiplomaScan = function (success, fail) {
        CallServer('post', '/api/Expert/RemoveDiplomaScan', null, success, fail, true, 'در حال حذف تصویر');
    }
    vm.RemoveBachelorScan = function (success, fail) {
        CallServer('post', '/api/Expert/RemoveBachelorScan', null, success, fail, true, 'در حال حذف تصویر');
    }
    vm.RemoveMasterScan = function (success, fail) {
        CallServer('post', '/api/Expert/RemoveMasterScan', null, success, fail, true, 'در حال حذف تصویر');
    }
    vm.RemovePhdScan = function (success, fail) {
        CallServer('post', '/api/Expert/RemovePhdScan', null, success, fail, true, 'در حال حذف تصویر');
    }
    vm.RemoveCertificationScan = function (data, success, fail) {
        CallServer('post', '/api/Expert/RemoveCertificationScan', data, success, fail, true, 'در حال حذف تصویر');
    }
    vm.RemoveInsuranceScan = function (data, success, fail) {
        CallServer('post', '/api/Expert/RemoveInsuranceScan', data, success, fail, true, 'در حال حذف تصویر');
    }
    vm.RemoveContractScan = function (data, success, fail) {
        CallServer('post', '/api/Expert/RemoveContractScan', data, success, fail, true, 'در حال حذف تصویر');
    }
    vm.RemoveTPCScan = function (data, success, fail) {
        CallServer('post', '/api/Expert/RemoveTPCScan', data, success, fail, true, 'در حال حذف تصویر');
    }
    vm.RemoveInsuranceScan = function (data, success, fail) {
        CallServer('post', '/api/Expert/RemoveInsuranceScan', data, success, fail, true, 'در حال حذف تصویر');
    }
    vm.SignAgreement = function (success, fail) {
        CallServer('post', '/api/Expert/SignAgreement', null, success, fail, true, 'در حال تایید اطلاعات');
    }
    vm.SendRequest = function (data, success, fail) {
        CallServer('post', '/api/Expert/SendRequest', data, success, fail, true, 'در حال ارسال درخواست');
    }
    vm.RemoveObjectionScan = function (data, success, fail) {
        CallServer('post', '/api/Expert/RemoveObjectionScan', data, success, fail, true, 'در حال حذف تصویر');
    }
    vm.ObjectifyOnce = function (data, success, fail) {
        CallServer('post', '/api/Expert/ObjectifyOnce', data, success, fail, true, 'در حال ثبت اعتراض');
    }
    vm.ObjectifyTwice = function (data, success, fail) {
        CallServer('post', '/api/Expert/ObjectifyTwice', data, success, fail, true, 'در حال ثبت اعتراض');
    }
    vm.GetAvailableSeasonalReports = (success, fail) => {
        CallServer('get', '/api/ExpertNew/GetAvailableSeasonalReports', null, success, fail, false);
    };
    vm.RemoveReport = (data, success, fail) => {
        CallServer('post', '/api/ExpertNew/RemoveReport', data, success, fail, true, 'در حال حذف فایل');
    };
    vm.ToggleReportVisibility = (data, success, fail) => CallServer('post', '/api/ExpertNew/ToggleReportVisibility', data, success, fail, true, "در حال ایجاد تغییرات");
    vm.GetMyDocumentForOverview = (success, fail) => CallServer('get', '/api/ExpertNew/GetDocumentForOverview', null, success, fail, true, 'در حال بارگذاری پرونده');

    //************COMMON************
    vm.GetAllProvinces = function (success, fail) {
        CallServer('get', '/api/Common/GetAllProvinces', null, success, fail, false);
    }
    vm.GetAllCounties = function (provinceID, success, fail) {
        CallServer('get', '/api/Common/GetAllCounties', { provinceID: provinceID }, success, fail, false);
    }
    vm.GetAllFieldsOfStudy = function (success, fail) {
        CallServer('get', '/api/Common/GetAllFieldsOfStudy', null, success, fail, false);
    }
    vm.GetAllFieldsOfActivity = function (success, fail) {
        CallServer('get', '/api/Common/GetAllFieldsOfActivity', null, success, fail, false);
    }
    vm.GetAllProductionTypes = function (success, fail) {
        CallServer('get', '/api/Common/GetAllProductionTypes', null, success, fail, false);
    }


    //************STAFF************    
    vm.GetStaff = function (success, fail) {
        CallServer('get', '/api/Staff/GetStaff', null, success, fail, false);
    }
    vm.GetAllMyDocuments = function (phrase, skip, take, success, fail) {
        CallServer('get', '/api/Staff/GetAllMyDocuments', { phrase: phrase, skip: skip, take: take }, success, fail, true, 'در حال بارگذاری پرونده ها');
    }
    vm.GetAllDoneDocuments = function (phrase, skip, take, success, fail) {
        CallServer('get', '/api/Staff/GetAllDoneDocuments', { phrase: phrase, skip: skip, take: take }, success, fail, true, 'در حال بارگذاری پرونده ها');
    }
    vm.GetAllWaitingDocuments = function (phrase, skip, take, success, fail) {
        CallServer('get', '/api/Staff/GetAllWaitingDocuments', { phrase: phrase, skip: skip, take: take }, success, fail, true, 'در حال بارگذاری پرونده ها');
    }
    vm.GetAllWorkingDocuments = function (phrase, skip, take, success, fail) {
        CallServer('get', '/api/Staff/GetAllWorkingDocuments', { phrase: phrase, skip: skip, take: take }, success, fail, true, 'در حال بارگذاری پرونده ها');
    }
    vm.AcceptDocument = function (data, success, fail) {
        CallServer('post', '/api/Staff/AcceptDocument', data, success, fail, false, 'در حال تایید مدارک پرونده');
    }
    vm.RejectDocument = function (data, success, fail) {
        CallServer('post', '/api/Staff/RejectDocument', data, success, fail, false, 'در حال رد مدارک پرونده');
    }
    vm.AuthorizeDocument = function (data, success, fail) {
        CallServer('post', '/api/Staff/AuthorizeDocument', data, success, fail, false, 'در حال تایید صلاحیت');
    }
    vm.UnauthorizeDocument = function (data, success, fail) {
        CallServer('post', '/api/Staff/UnauthorizeDocument', data, success, fail, false, 'در حال رد صلاحیت');
    }
    vm.GetLicense = function (documentUsername, success, fail) {
        CallServer('get', '/api/StaffNew/GetLicensePrintData', { documentUsername: documentUsername }, success, fail, false, '');
    }
    vm.GetCurrentFlowReport = function (success, fail) {
        CallServer('get', '/api/Staff/GetCurrentFlowReport', null, success, fail, false);
    }
    vm.GetAuthorizationByNumber = function (success, fail) {
        CallServer('get', '/api/Staff/GetAuthorizationByNumber', null, success, fail, false);
    }
    vm.GetSafetyCertificationReport = function (success, fail) {
        CallServer('get', '/api/Staff/GetSafetyCertificationReport', null, success, fail, false);
    }
    vm.GetRegisterHistogramReport = function (success, fail) {
        CallServer('get', '/api/Staff/GetRegisterHistogramReport', null, success, fail, false);
    }
    vm.GetDocumentsByFilter = function (filter, success, fail) {
        CallServer('post', '/api/Staff/GetDocumentsByFilter', filter, success, fail, true, 'در حال بارگذاری پرونده ها');
    };
    vm.GetAllExportReports = (phrase, skip, take, success, fail) => CallServer('get', '/api/StaffNew/GetAllExportReports', { phrase: phrase, skip: skip, take: take }, success, fail, true, 'در حال بارگذاری گزارش ها');
    vm.GetReportComments = (data, success, fail) => CallServer('get', '/api/StaffNew/GetReportComments', data, success, fail, false);
    vm.AddExpertReportComment = (data, success, fail) => CallServer('post', '/api/StaffNew/AddExpertReportComment', data, success, false);
    vm.GetDocumentForOverview = (documentUsername, success, fail) => CallServer('get', '/api/StaffNew/GetDocumentForOverview', { documentUsername: documentUsername }, success, fail, true, 'در حال بارگذاری پرونده');

    //************ADMIN************

    vm.GetAllProvinceClubs = function (success, fail) {
        CallServer('get', '/api/Admin/GetAllProvinceClubs', null, success, fail, false);
    };
    vm.ActivateClub = function (data, success, fail) {
        CallServer('post', '/api/Admin/ActivateClub', data, success, fail, false);
    };
    vm.DeactivateClub = function (data, success, fail) {
        CallServer('post', '/api/Admin/DeactivateClub', data, success, fail, false);
    };
    vm.AddClub = function (data, success, fail) {
        CallServer('post', '/api/Admin/AddClub', data, success, fail, false);
    };
    vm.ChangeOthersPassword = function (data, success, fail) {
        CallServer('post', '/api/Admin/ChangeOthersPassword', data, success, fail, false);
    };
    vm.GetAllCountyCountyInspectors = function (success, fail) {
        CallServer('get', '/api/Admin/GetAllCountyCountyInspectors', null, success, fail, true, 'در حال بارگذاری');
    }
    vm.DeactivateCountyInspector = function (data, success, fail) {
        CallServer('post', '/api/Admin/DeactivateCountyInspector', data, success, fail, false);
    }
    vm.ActivateCountyInspector = function (data, success, fail) {
        CallServer('post', '/api/Admin/ActivateCountyInspector', data, success, fail, false);
    }
    vm.AddCountyInspector = function (data, success, fail) {
        CallServer('post', '/api/Admin/AddCountyInspector', data, success, fail, false);
    }
    vm.ChangeProvinceChairmanName = function (data, success, fail) {
        CallServer('post', '/api/Admin/ChangeProvinceChairmanName', data, success, fail, false);
    }
    vm.ChangeProvinceHeadName = function (data, success, fail) {
        CallServer('post', '/api/Admin/ChangeProvinceHeadName', data, success, fail, false);
    }
    vm.AddCounty = (data, success, fail) => CallServer('post', '/api/Admin/AddCounty', data, success, fail, false);
    vm.ChangeCountyName = (data, success, fail) => CallServer('post', '/api/Admin/ChangeCountyName', data, success, fail, false);
}]);