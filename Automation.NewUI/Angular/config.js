﻿angular.module('isaap').config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$ocLazyLoadProvider','IdleProvider',function ($stateProvider, $urlRouterProvider, $locationProvider, $ocLazyLoadProvider, IdleProvider) {
    IdleProvider.idle(5); // in seconds
    IdleProvider.timeout(120); // in seconds
    $locationProvider.hashPrefix('');
    $ocLazyLoadProvider.config({
        debug: false,
    });
    $urlRouterProvider.otherwise('/');
    //states
    $stateProvider
        .state('license', {
            url: '/License/:username',
            controller: 'licenseCtrl',
            templateUrl: '/Statics/Expert/license.html?v=1.6.7',
        })
        .state('home', {
            url: '/',
            controller: 'homeCtrl',
            templateUrl: '/Statics/home.html?v=1.6.7'
        })
        //Expert
        .state('expert', {
            url: '/Expert',
            abstract: true,
            controller: 'profileCtrl',
            templateUrl: '/Statics/profile.html?v=1.6.7'
        })
        .state('expert.document', {
            url: '/Document',
            templateUrl: '/Statics/Expert/document.html?v=1.6.7',
            controller: 'documentCtrl',
        })
        .state('expert.reports', {
            url: '/Reports',
            templateUrl: '/Statics/Expert/reports.html?v=1.6.7',
            controller: 'reportsCtrl',
        })
        .state('expert.attachments', {
            url: '/Attachments',
            templateUrl: '/Statics/Expert/attachments.html?v=1.6.7',
            controller: 'attachmentsCtrl'
        })
        .state('expert.info', {
            url: '/Info',
            templateUrl: '/Statics/Expert/info.html?v=1.6.7',
            controller: 'infoCtrl',
        })
        .state('expert.experiences', {
            url: '/Experiences',
            templateUrl: '/Statics/Expert/experiences.html?v=1.6.7',
            controller: 'experiencesCtrl',
        })
        .state('expert.certifications', {
            url: '/Certifications',
            templateUrl: '/Statics/Expert/certifications.html?v=1.6.7',
            controller: 'certificationCtrl',
        })
        .state('expert.request', {
            url: '/Request',
            templateUrl: '/Statics/Expert/request.html?v=1.6.7',
            controller: 'requestCtrl'
        })
        //Admin
        .state('admin', {
            url: '/Admin',
            abstract: true,
            controller: 'profileCtrl',
            templateUrl: '/Statics/profile.html?v=1.6.7'
        })
        .state('admin.clubs', {
            url: '/Clubs',
            templateUrl: '/Statics/Admin/clubs.html?v=1.6.7',
            controller: 'clubsCtrl',
        })
        .state('admin.countyinspectors', {
            url: '/CountyInspectors',
            templateUrl: '/Statics/Admin/countyinspectors.html?v=1.6.7',
            controller: 'countyinspectorsCtrl',
        })
        .state('admin.systemlock', {
            url: '/SystemLock',
            templateUrl: '/Statics/Admin/systemlock.html?v=1.6.7',
            controller: 'systemlockCtrl',
        })
        .state('admin.users', {
            url: '/Users',
            templateUrl: '/Statics/Admin/users.html?v=1.6.7',
            controller: 'usersCtrl',
        })
        //Staff
        .state('staff', {
            url: '/Staff',
            abstract: true,
            controller: 'profileCtrl',
            templateUrl: '/Statics/profile.html?v=1.6.7'
        })
        .state('staff.documentview', {
            url: '/DocumentView/:username',
            templateUrl: '/Statics/Staff/documentview.html?v=1.6.7',
            controller: 'documentviewCtrl',
        })
        .state('staff.mydocuments', {
            url: '/MyDocuments',
            templateUrl: '/Statics/Staff/documents.html?v=1.6.7',
            controller: 'mydocumentsCtrl',
        })
        .state('staff.workingdocuments', {
            url: '/WorkingDocuments',
            templateUrl: '/Statics/Staff/documents.html?v=1.6.7',
            controller: 'workingdocumentsCtrl',
        })
        .state('staff.waitingdocuments', {
            url: '/WaitingDocuments',
            templateUrl: '/Statics/Staff/documents.html?v=1.6.7',
            controller: 'waitingdocumentsCtrl',
        })
        .state('staff.donedocuments', {
            url: '/DoneDocuments',
            templateUrl: '/Statics/Staff/documents.html?v=1.6.7',
            controller: 'donedocumentsCtrl',
        })
        .state('staff.bigreport', {
            url: '/BigReport',
            templateUrl: '/Statics/Staff/bigreport.html?v=1.6.7',
            controller: 'bigreportCtrl',
        })
        .state('staff.oldreport', {
            url: '/OldReport',
            templateUrl: '/Statics/Staff/oldreport.html?v=1.6.7',
            controller: 'oldreportCtrl',
        })
        .state('staff.expertreports', {
            url: '/ExpertReports',
            templateUrl: '/Statics/Staff/expertreports.html?v=1.6.7',
            controller: 'expertreportsCtrl',
        })
        //Analyzer
        .state('analyzer', {
            url: '/Analyzer',
            abstract: true,
            controller: 'profileCtrl',
            templateUrl: '/Statics/profile.html?v=1.6.7'
        })
        .state('analyzer.documents', {
            url: '/Documents',
            templateUrl: '/Statics/Analyzer/documents.html?v=1.6.7',
            controller: 'allDocumentsCtrl',
        })
        ;
}]).run(['$rootScope','$state',function ($rootScope, $state) {
    $rootScope.$state = $state;
}]);