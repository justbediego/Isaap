﻿using Automation.NewUI.Api.Filters;
using Automation.NewUI.Mvc.Config;
using CodeRepository.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.SessionState;

namespace Automation.NewUI
{
    public class ISAAP : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            MvcConfig.RegisterRoutes(RouteTable.Routes);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configuration.Filters.Add(new ArabicToPersianApiFilter());
            GlobalConfiguration.Configuration.Filters.Add(new ActionLoggerApiFilter());
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        protected void Application_PostAuthorizeRequest()
        {
            if (IsWebApiRequest())
            {
                HttpContext.Current.SetSessionStateBehavior(SessionStateBehavior.Required);
            }
        }

        private bool IsWebApiRequest()
        {
            return HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath.Replace("~","").Replace("/","").Replace("\\","").ToLower().StartsWith("api");
        }
    }
}
