﻿using Automation.Business;
using Automation.Business.Inner;
using Automation.DataAccess;
using Automation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Automation.NewUI.Api
{
    [Authorize]
    public class CommonController : ApiController
    {
        private SystemInfoBusiness systemInfoBusiness;
        public CommonController()
        {
            AutomationContext dbContext = new AutomationContext();
            Validation validation = new Validation(dbContext);
            systemInfoBusiness = new SystemInfoBusiness(dbContext, validation);
        }
        [HttpGet]
        public List<Province> GetAllProvinces()
        {
            return systemInfoBusiness.GetAllProvinces();
        }
        [HttpGet]
        public List<County> GetAllCounties(int provinceID)
        {
            return systemInfoBusiness.GetAllCounties(provinceID);
        }
        [HttpGet]
        public List<FieldOfStudy> GetAllFieldsOfStudy()
        {
            return systemInfoBusiness.GetAllFieldsOfStudy();
        }
        [HttpGet]
        public List<FieldOfActivity> GetAllFieldsOfActivity()
        {
            return systemInfoBusiness.GetAllFieldsOfActivity();
        }
        [HttpGet]
        public List<ProductionType> GetAllProductionTypes()
        {
            return systemInfoBusiness.GetAllProductionTypes();
        }
    }
}
