﻿using Automation.Business;
using Automation.Business.Inner;
using Automation.DataAccess;
using Automation.Model;
using Automation.NewUI.Api.Filters;
using Automation.NewUI.Models;
using Automation.NewUI.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Web;
using System.Web.Http;

namespace Automation.NewUI.Api
{
    public class AuthenticationController : ApiController
    {
        private ExpertBusiness expertBusiness;
        private SystemInfoBusiness systemInfoBusiness;
        private UserBusiness userBusiness;
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.Current.GetOwinContext().Authentication;
            }
        }
        public AuthenticationController()
        {
            AutomationContext dbContext = new AutomationContext();
            Validation validation = new Validation(dbContext);
            FlowEngine flowEngine = new FlowEngine(dbContext, validation);
            systemInfoBusiness = new SystemInfoBusiness(dbContext, validation);
            userBusiness = new UserBusiness(dbContext, validation, systemInfoBusiness);
            expertBusiness = new ExpertBusiness(dbContext, validation, flowEngine);
        }
        [HttpGet]
        public bool IsLoggedIn()
        {
            return !string.IsNullOrEmpty(User.Identity.Name);
        }
        [HttpGet]
        public string GetUsername()
        {
            return User.Identity.Name;
        }
        [HttpGet]
        public int GetUserType()
        {
            return int.Parse(((ClaimsPrincipal)User).FindFirst(ClaimTypes.Role).Value);
        }
        [HttpPost]
        [LoggerIgnoreJsonData("Password")]
        public void Login(IsaapUser model)
        {
            User user = userBusiness.GetUser(model.UserName, model.Password);
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = model.IsPersistent }, IsaapUser.ToClaimsIdentity(user));
        }
        [HttpGet]
        public void Logout()
        {
            AuthenticationManager.SignOut();
        }
        [HttpPost]
        public void ModifyRecoveryEmail(ModifyEmailRequest request)
        {
            userBusiness.ModifyRecoveryEmail(GetUsername(), request.NewEmail); 
        }
        [HttpPost]
        public String ResetPassword(ForgotPasswordRequest request)
        {
            return userBusiness.ResetPassword(request.UsernameOrEmail);
        }
        [HttpPost]
        [LoggerIgnoreJsonData("Password")]
        public void RegisterExpert(RegisterData model)
        {
            MainController main = new MainController();
            string captchaText = HttpContext.Current.Session["Captcha"].ToString();
            HttpContext.Current.Session["Captcha"] = null;
            if (string.IsNullOrEmpty(captchaText) || captchaText.ToLower() != model.Captcha.ToLower())
                throw new Exception("کد تصویر بدرستی وارد نشده است");
            expertBusiness.CreateDocument(model.Username, model.Password);
            Login(new IsaapUser()
            {
                UserName = model.Username,
                Password = model.Password,
            });
        }
        [HttpPost]
        [LoggerIgnoreJsonData("CurrentPassword, NewPassword")]
        public void ChangePassword(ChangePasswordData model)
        {
            userBusiness.ChangePassword(GetUsername(), model.CurrentPassword, model.NewPassword);
        }
    }
}