﻿using System;

namespace Automation.NewUI.Api.Filters
{
    internal class LoggerIgnoreJsonDataAttribute : Attribute
    {
        public string[] HiddenData;
        public LoggerIgnoreJsonDataAttribute(params string[] data)
        {
            HiddenData = data;
        }
    }
}