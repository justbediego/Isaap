﻿using Automation.Business;
using Automation.Business.Inner;
using Automation.BusinessNew.DTO.Document;
using Automation.DataAccess;
using Automation.Model;
using Automation.Model.Report;
using Automation.NewUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Automation.NewUI.Api
{
    [Authorize]
    public class StaffController : ApiController
    {
        private SystemInfoBusiness systemInfoBusiness;
        private AuthenticationController authenticationController;
        private StaffBusiness staffBusiness;
        public StaffController()
        {
            authenticationController = new AuthenticationController();
            AutomationContext dbContext = new AutomationContext();
            Validation validation = new Validation(dbContext);
            FlowEngine flowEngine = new FlowEngine(dbContext, validation);
            systemInfoBusiness = new SystemInfoBusiness(dbContext, validation);
            staffBusiness = new StaffBusiness(dbContext, validation, flowEngine);            
        }
        [HttpGet]
        public Staff GetStaff()
        {
            return staffBusiness.GetStaff(authenticationController.GetUsername());
        }
        [HttpGet]
        public PagingResult<DocumentDTO> GetAllMyDocuments(int? skip = null, int? take = null, string phrase = null)
        {
            PagingResult<DocumentDTO> result = new PagingResult<DocumentDTO>();
            result.Data = staffBusiness.GetAllMyDocuments(authenticationController.GetUsername(), out result.Count, skip, take, phrase).Select(d => DocumentDTO.LoadListData(d)).ToList();
            return result;
        }
        [HttpGet]
        public PagingResult<DocumentDTO> GetAllDoneDocuments(int? skip = null, int? take = null, string phrase = null)
        {
            PagingResult<DocumentDTO> result = new PagingResult<DocumentDTO>();
            result.Data = staffBusiness.GetAllDoneDocuments(authenticationController.GetUsername(), out result.Count, skip, take, phrase).Select(d => DocumentDTO.LoadListData(d)).ToList();
            return result;
        }
        [HttpGet]
        public PagingResult<DocumentDTO> GetAllWaitingDocuments(int? skip = null, int? take = null, string phrase = null)
        {
            PagingResult<DocumentDTO> result = new PagingResult<DocumentDTO>();
            result.Data = staffBusiness.GetAllWaitingDocuments(authenticationController.GetUsername(), out result.Count, skip, take, phrase).Select(d => DocumentDTO.LoadListData(d)).ToList();
            return result;
        }
        [HttpGet]
        public PagingResult<DocumentDTO> GetAllWorkingDocuments(int? skip = null, int? take = null, string phrase = null)
        {
            PagingResult<DocumentDTO> result = new PagingResult<DocumentDTO>();
            result.Data = staffBusiness.GetAllWorkingDocuments(authenticationController.GetUsername(), out result.Count, skip, take, phrase).Select(d => DocumentDTO.LoadListData(d)).ToList();
            return result;
        }
        [HttpPost]
        public void AcceptDocument(StaffDecisionModel data)
        {
            staffBusiness.AcceptDocument(authenticationController.GetUsername(), data.DocumentUsername, data.Description);
        }
        [HttpPost]
        public void RejectDocument(StaffDecisionModel data)
        {
            staffBusiness.RejectDocument(authenticationController.GetUsername(), data.DocumentUsername, data.Description);
        }
        [HttpPost]
        public void AuthorizeDocument(StaffDecisionModel data)
        {
            staffBusiness.AuthorizeDocument(authenticationController.GetUsername(), data.DocumentUsername, data.Description);
        }
        [HttpPost]
        public void UnauthorizeDocument(StaffDecisionModel data)
        {
            staffBusiness.UnauthorizeDocument(authenticationController.GetUsername(), data.DocumentUsername, data.Description);
        }
        //[HttpGet]
        //public DocumentVM GetLicense(string documentUsername)
        //{
        //    return DocumentVM.LoadLicenseData(staffBusiness.GetLicense(authenticationController.GetUsername(), documentUsername));
        //}
        [HttpGet]
        public CurrentFlowReport GetCurrentFlowReport()
        {
            return staffBusiness.GetCurrentFlowReport(authenticationController.GetUsername());
        }
        [HttpGet]
        public AuthorizationByNumberReport GetAuthorizationByNumber()
        {
            return staffBusiness.GetAuthorizationByNumber(authenticationController.GetUsername());
        }
        [HttpGet]
        public SafetyCertificationReport GetSafetyCertificationReport()
        {
            return staffBusiness.GetSafetyCertificationReport(authenticationController.GetUsername());
        }
        [HttpGet]
        public RegisterHistogramReport GetRegisterHistogramReport()
        {
            return staffBusiness.GetRegisterHistogramReport(authenticationController.GetUsername());
        }
        [HttpGet]
        public HttpResponseMessage GetMyExcelData()
        {
            //Starting generation
            string xmlData = "<table border=1><thead><tr>" +
                "<td>شماره پرونده</td>" +
                "<td>نام کاربری</td>" +
                "<td>نام</td>" +
                "<td>نام خانوادگی</td>" +
                "<td>نام پدر</td>" +
                "<td>وضعیت تاهل</td>" +
                "<td>وضعیت سربازی</td>" +
                "<td>تقاضای شرکت در المپیاد</td>"+
                "<td>جنسیت</td>" +
                "<td>محل تولد</td>" +
                "<td>شماره عضویت در انجمن</td>" +
                "<td>شماره شناسنامه</td>" +
                "<td>کدملی</td>" +
                "<td>تلفن همراه</td>" +
                "<td>رشته تحصیلی</td>" +
                "<td>استان محل سکونت</td>" +
                "<td>شهرستان محل سکونت</td>" +
                "<td>نشانی محل سکونت</td>" +
                "<td>نام شرکت</td>" +
                "<td>نام مدیر عامل</td>" +
                "<td>فعالیت شرکت</td>" +
                "<td>نوع تولید</td>" +
                "<td>تعداد کارکنان</td>" +
                "<td>تلفن شرکت</td>" +
                "<td>ایمیل</td>" +
                "<td>استان محل شرکت</td>" +
                "<td>شهرستان محل شرکت</td>" +
                "<td>نشانی شرکت</td>" +
                "<td>تاریخ تشکیل پرونده</td>" +
                "<td>وضعیت فعلی</td>" +
                "<td>پیام وضعیت فعلی</td>" +
                "<td>تاریخ ورود به وضعیت فعلی</td>" +
                "<td>کد صلاحیت کارشناس ایمنی و حفاظت فنی</td>" +
                "</tr></thead><tbody>";
            staffBusiness.GetDocumentsForDataExtract(authenticationController.GetUsername()).ForEach(d =>
              {
                  xmlData += "<tr>" +
                      "<td>" + d.Id + "</td>" +
                      "<td>" + d.UserName + "</td>" +
                      "<td>" + d.FirstName + "</td>" +
                      "<td>" + d.LastName + "</td>" +
                      "<td>" + d.FatherName + "</td>" +
                      "<td>" + (d.MaritalStatus.HasValue ? (d.MaritalStatus.Value == Document.MaritalStates.Married ? "متاهل" : "مجرد") : "نامشخص") + "</td>" +
                      "<td>" + (d.MilitaryStatus.HasValue ? (d.MilitaryStatus.Value == Document.MilitaryServiceStates.Passed ? "گذرانده" : "معاف") : "نامشخص") + "</td>" +
                      "<td>" + (d.OlympiadRequestDate.HasValue ? "دارد" : "ندارد" + "</td>") +
                      "<td>" + (d.Gender.HasValue ? (d.Gender.Value == Document.GenderType.Male ? "مرد" : "زن") : "نامشخص") + "</td>" +
                      "<td>" + d.PlaceOfBirth + "</td>" +
                      "<td>" + d.AssociationMembershipNo + "</td>" +
                      "<td>" + d.ShenasnameNo + "</td>" +
                      "<td>" + d.NationalCode + "</td>" +
                      "<td>" + d.Mobile + "</td>" +
                      "<td>" + (d.HighSchoolCertification != null ? "دیپلم " + d.HighSchoolCertification.FieldOfStudy.Name + "<br />" : "") +
                               (d.DiplomaCertification != null ? "کاردانی " + d.DiplomaCertification.FieldOfStudy.Name + "<br />" : "") +
                               (d.BachelorsCertification != null ? "کارشناسی " + d.BachelorsCertification.FieldOfStudy.Name + "<br />" : "") +
                               (d.MastersCertification != null ? "کارشناسی ارشد " + d.MastersCertification.FieldOfStudy.Name + "<br />" : "") +
                               (d.PhDCertification != null ? "دکترا " + d.PhDCertification.FieldOfStudy.Name + "<br />" : "") + "</td>" +
                      "<td>" + d.HomeContact.County.Province.Name + "</td>" +
                      "<td>" + d.HomeContact.County.Name + "</td>" +
                      "<td>" + d.HomeContact.Address + "</td>" +
                      "<td>" + d.CurrentWorkShop.UnitName + "</td>" +
                      "<td>" + d.CurrentWorkShop.EmployerName + "</td>" +
                      "<td>" + d.CurrentWorkShop.FieldOfActivity.Name + "</td>" +
                      "<td>" + d.CurrentWorkShop.ProductionType.Name + "</td>" +
                      "<td>" + (d.CurrentWorkShop.MaleWorkersNo + d.CurrentWorkShop.FemaleWorkersNo) + "</td>" +
                      "<td>" + d.WorkContact.Phone + "</td>" +
                      "<td>" + d.Email + "</td>" +
                      "<td>" + d.WorkContact.County.Province.Name + "</td>" +
                      "<td>" + d.WorkContact.County.Name + "</td>" +
                      "<td>" + d.WorkContact.Address + "</td>" +
                      "<td>" + PersianDate.ConvertDate.ToFa(d.DateCreated) + "</td>" +
                      "<td>" + (d.CurrentFlow.Type == Flow.FlowType.Authorized ? "دارای صلاحیت" : "") +
                               (d.CurrentFlow.Type == Flow.FlowType.Created ? "در دست کارفرما" : "") +
                               (d.CurrentFlow.Type == Flow.FlowType.NotCompleted || d.CurrentFlow.Type == Flow.FlowType.RejectedOnce || d.CurrentFlow.Type == Flow.FlowType.RejectedTwice ? "در دست فرد" : "") +
                               (d.CurrentFlow.Type == Flow.FlowType.SentToAssociation ? "در دست انجمن" : "") +
                               (d.CurrentFlow.Type == Flow.FlowType.SentToClub ? "در دست کانون" : "") +
                               (d.CurrentFlow.Type == Flow.FlowType.SentToCountyInspector ? "در دست بازرس شهرستان" : "") +
                               (d.CurrentFlow.Type == Flow.FlowType.SentToInspectionOffice ? "در دست اداره کل" : "") +
                               (d.CurrentFlow.Type == Flow.FlowType.SentToProvinceInspector ? "در دست بازرس استان" : "") +
                               (d.CurrentFlow.Type == Flow.FlowType.SentToProvinceManager ? "در دست مدیر بازرسی استان" : "") + "</td>" +
                      "<td>" + d.CurrentFlow.Description + "</td>" +
                      "<td>" + PersianDate.ConvertDate.ToFa(d.CurrentFlow.DateCreated) + "</td>" +
                      "<td>" + d.NezamCode + "</td></tr>";
              });
            xmlData += "</tbody></table>";
            //End of generation
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(xmlData, System.Text.Encoding.UTF8, "application/vnd.ms-excel");
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("file/excel");
            response.Content.Headers.Add("Content-Disposition", "attachment; filename=\"MyDocuments-" + PersianDate.ConvertDate.ToFa(DateTime.Now) + ".xls\"");
            return response;
        }
        [HttpPost]
        public PagingResult<DocumentDTO> GetDocumentsByFilter(StaffReportFilter filter)
        {
            PagingResult<DocumentDTO> result = new PagingResult<DocumentDTO>();
            result.Data = staffBusiness.GetDocumentsByFilter(authenticationController.GetUsername(),
                                                             out result.Count,
                                                             filter.FullName,
                                                             filter.Mobile,
                                                             (Flow.FlowType?)filter.CurrentFlowType,
                                                             filter.BirthdayFrom,
                                                             filter.BirthdayTo,
                                                             filter.RegisterDateFrom,
                                                             filter.RegisterDateTo,
                                                             filter.CurrentFlowDateFrom,
                                                             filter.CurrentFlowDateTo,
                                                             filter.NationalCode,
                                                             filter.Shenasname,
                                                             filter.PlaceOfBirth,
                                                             (Document.GenderType?)filter.Gender,
                                                             (Document.MaritalStates?)filter.MaritalStatus,
                                                             filter.OlympiadRequest,
                                                             (Document.MilitaryServiceStates?)filter.MilitaryStatus,
                                                             filter.FieldOfStudyId,
                                                             filter.FieldOfActivityId,
                                                             filter.ProductionTypeId,
                                                             filter.HomeProvinceId,
                                                             filter.HomeCountyId,
                                                             filter.WorkProvinceId,
                                                             filter.WorkCountyId,
                                                             filter.UnitName,
                                                             filter.EmployerFullName,
                                                             filter.AssociationNumber,
                                                             filter.Skip,
                                                             filter.Take).Select(d => DocumentDTO.LoadXMLData(d)).ToList();
            return result;
        }
    }
}
