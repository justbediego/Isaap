﻿using Automation.BusinessNew;
using Automation.BusinessNew.DTO;
using Automation.BusinessNew.DTO.Document;
using Automation.DataAccess;
using Automation.NewUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Automation.NewUI.Api.V2
{
    [Authorize]
    public class StaffNewController : ApiController
    {
        StaffBusiness staffBusiness;
        AuthenticationController authenticationController;
        public StaffNewController()
        {
            authenticationController = new AuthenticationController();
            AutomationContext dbContext = new AutomationContext();
            ValidationBusiness validationBusiness = new ValidationBusiness(dbContext);
            UtilitiesBusiness utilitiesBusiness = new UtilitiesBusiness();
            FlowEngine flowEngine = new FlowEngine(dbContext, validationBusiness);
            staffBusiness = new StaffBusiness(dbContext, validationBusiness, flowEngine, utilitiesBusiness);
        }
        [HttpGet]
        public LicensePrint GetLicensePrintData(string documentUsername)
        {
            return staffBusiness.GetLicensePrintData(authenticationController.GetUsername(), documentUsername);
        }

        [HttpGet]
        public PagingResult<ExpertReportOverview> GetAllExportReports(int? skip = null, int? take = null, string phrase = null)
        {
            PagingResult<ExpertReportOverview> result = new PagingResult<ExpertReportOverview>();
            result.Data = staffBusiness.GetAllExportReports(authenticationController.GetUsername(), out result.Count, skip, take, phrase);
            return result;
        }
        [HttpGet]
        public List<ExpertReportComment> GetReportComments(int reportId)
        {
            return staffBusiness.GetReportComments(authenticationController.GetUsername(), reportId);
        }
        [HttpPost]
        public void AddExpertReportComment(AddExpertReportComment model)
        {
            staffBusiness.AddExpertReportComment(authenticationController.GetUsername(), model.ReportID, model.Text, model.IsAccepted);
        }

        [HttpGet]
        public DocumentDTO GetDocumentForOverview(string documentUsername)
        {
            return staffBusiness.GetDocumentForOverview(authenticationController.GetUsername(), documentUsername);
        }
    }
}
