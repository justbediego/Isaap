﻿using Automation.BusinessNew;
using Automation.BusinessNew.DTO;
using Automation.DataAccess;
using Automation.NewUI.Api.Filters;
using Automation.NewUI.ModelsV2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Automation.NewUI.Api.V2
{
    [Authorize]
    public class AdminController : ApiController
    {
        AdministratorBusiness administratorBusiness;
        AuthenticationController authenticationController;
        public AdminController()
        {
            authenticationController = new AuthenticationController();
            AutomationContext dbContext = new AutomationContext();
            ValidationBusiness validationBusiness = new ValidationBusiness(dbContext);
            FlowEngine flowEngine = new FlowEngine(dbContext, validationBusiness);
            administratorBusiness = new AdministratorBusiness(dbContext, validationBusiness, flowEngine);
        }
        [HttpGet]
        public List<ProvinceClubDTO> GetAllProvinceClubs()
        {
            return administratorBusiness.GetAllProvinceClubs(authenticationController.GetUsername());
        }
        [HttpPost]
        public void AddClub(AddClubDTO data)
        {
            administratorBusiness.AddClub(authenticationController.GetUsername(), data.ProvinceID, data.Username, data.Password);
        }
        [HttpPost]
        public void ActivateClub(ActivateObjectDTO data)
        {
            administratorBusiness.ActivateClub(authenticationController.GetUsername(), data.ObjectID);
        }
        [HttpPost]
        public void DeactivateClub(DeactiveObjectDTO data)
        {
            administratorBusiness.DeactivateClub(authenticationController.GetUsername(), data.ObjectID, data.Description);
        }
        [LoggerIgnoreJsonData("AdminPassword, NewPassword")]
        [HttpPost]
        public void ChangeOthersPassword(AdminChangePasswordDTO data)
        {
            administratorBusiness.ChangeOthersPassword(authenticationController.GetUsername(), data.AdminPassword, data.Username, data.NewPasswrod);
        }
        [HttpPost]
        public void ChangeProvinceHeadName(ChangeNameDTO data)
        {
            administratorBusiness.ChangeProvinceHeadName(authenticationController.GetUsername(), data.ObjectID, data.NewName);
        }
        [HttpPost]
        public void ChangeProvinceChairmanName(ChangeNameDTO data)
        {
            administratorBusiness.ChangeProvinceChairmanName(authenticationController.GetUsername(), data.ObjectID, data.NewName);
        }

        [HttpGet]
        public List<CountyCountyInspectorDTO> GetAllCountyCountyInspectors()
        {
            return administratorBusiness.GetAllCountyCountyInspectors(authenticationController.GetUsername());
        }
        [HttpPost]
        public void ActivateCountyInspector(ActivateObjectDTO data)
        {
            administratorBusiness.ActivateCountyInspector(authenticationController.GetUsername(), data.ObjectID);
        }
        [HttpPost]
        public void DeactivateCountyInspector(DeactiveObjectDTO data)
        {
            administratorBusiness.DeactivateCountyInspector(authenticationController.GetUsername(), data.ObjectID, data.Description);
        }
        [HttpPost]
        public void AddCountyInspector(AddCountyInspectorDTO data)
        {
            administratorBusiness.AddCountyInspector(authenticationController.GetUsername(), data.CountyID, data.Username, data.Password);
        }
        [HttpPost]
        public void AddCounty(AddCountyDTO data)
        {
            administratorBusiness.AddCounty(authenticationController.GetUsername(), data.ProvinceID, data.Name);
        }
        [HttpPost]
        public void ChangeCountyName(ChangeNameDTO data)
        {
            administratorBusiness.ChangeCountyName(authenticationController.GetUsername(), data.ObjectID, data.NewName);
        }
    }
}
