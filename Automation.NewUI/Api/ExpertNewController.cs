﻿using Automation.BusinessNew;
using Automation.BusinessNew.DTO;
using Automation.BusinessNew.DTO.Document;
using Automation.DataAccess;
using Automation.NewUI.ModelsV2;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Automation.NewUI.Api
{
    [Authorize]
    public class ExpertNewController : ApiController
    {
        ExpertBusiness expertBusiness;
        AuthenticationController authenticationController;
        public ExpertNewController()
        {
            authenticationController = new AuthenticationController();
            AutomationContext dbContext = new AutomationContext();
            ValidationBusiness validationBusiness = new ValidationBusiness(dbContext);
            expertBusiness = new ExpertBusiness(dbContext, validationBusiness);
        }
        [HttpGet]
        public List<SeasonalReportDTO> GetAvailableSeasonalReports()
        {
            return expertBusiness.GetAvailableSeasonalReports(authenticationController.GetUsername());
        }
        [HttpPost]
        public async Task UpdateSeasonalReport()
        {
            var provider = await Request.Content.ReadAsMultipartAsync();
            var file = provider.Contents[1];
            var data = JsonConvert.DeserializeObject<SeasonalReportDTO> (provider.Contents[0].ReadAsStringAsync().Result);
            var forDate = UtilitiesBusiness.GetDateFromSeason(data.Year, data.Season);
            expertBusiness.UpdateSeasonalReport(authenticationController.GetUsername(), forDate, file.Headers.ContentDisposition.FileName, file.ReadAsByteArrayAsync().Result);
        }
        [HttpPost]
        public void RemoveReport(DeleteObjectDTO model)
        {
            expertBusiness.RemoveReport(authenticationController.GetUsername(), model.ObjectID);
        }
        [HttpPost]
        public void ToggleReportVisibility(DeleteObjectDTO model)
        {
            expertBusiness.ToggleReportVisibility(authenticationController.GetUsername(), model.ObjectID);
        }
        [HttpGet]
        public DocumentDTO GetDocumentForOverview()
        {
            return expertBusiness.GetDocumentForOverview(authenticationController.GetUsername());
        }
    }
}
