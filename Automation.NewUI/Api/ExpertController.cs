﻿using Automation.Business;
using Automation.Business.Inner;
using Automation.BusinessNew.DTO.Document;
using Automation.DataAccess;
using Automation.Model;
using Automation.NewUI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Automation.NewUI.Api
{
    [Authorize]
    public class ExpertController : ApiController
    {
        private ExpertBusiness expertBusiness;
        private AuthenticationController authenticationController;
        public ExpertController()
        {
            authenticationController = new AuthenticationController();
            AutomationContext dbContext = new AutomationContext();
            Validation validation = new Validation(dbContext);
            FlowEngine flowEngine = new FlowEngine(dbContext, validation);
            expertBusiness = new ExpertBusiness(dbContext, validation, flowEngine);
        }
        [HttpPost]
        public void ModifyBasicInfo(DocumentDTO document)
        {
            expertBusiness.ModifyBasicInfo(authenticationController.GetUsername(),
                                            document.FirstName,
                                            document.LastName,
                                            document.FatherName,
                                            //must have value
                                            (Document.GenderType)document.Gender,
                                            document.ShenasnameNo,
                                            document.NationalCode,
                                            document.BirthDate.HasValue ? document.BirthDate.Value : DateTime.Now,
                                            document.PlaceOfBirth,
                                            //must have value
                                            (Document.MaritalStates)document.MaritalStatus.Value,
                                            //must have value
                                            document.MilitaryStatus.HasValue ? (Document.MilitaryServiceStates)document.MilitaryStatus.Value : Document.MilitaryServiceStates.Passed,
                                            document.ReasonOfExemption,
                                            document.AssociationMembershipNo);
        }
        [HttpPost]
        public void ModifyContactInfo(DocumentDTO document)
        {
            expertBusiness.ModifyContactInfo(authenticationController.GetUsername(), document.Mobile, document.HomeContact.County.Id, document.HomeContact.Address, document.HomeContact.PostalCode, document.HomeContact.Phone, document.WorkContact.County.Id, document.WorkContact.Address, document.WorkContact.PostalCode, document.WorkContact.Phone);
        }
        [HttpPost]
        public void ModifyAcademicInfo(DocumentDTO document)
        {
            expertBusiness.ModifyAcademicInfo(authenticationController.GetUsername(),
                document.HighSchoolCertification != null && document.HighSchoolCertification.FieldOfStudy_ID != 0 ? (int?)document.HighSchoolCertification.FieldOfStudy_ID : null,
                document.DiplomaCertification != null && document.DiplomaCertification.FieldOfStudy_ID != 0 ? (int?)document.DiplomaCertification.FieldOfStudy_ID : null,
                document.BachelorsCertification != null && document.BachelorsCertification.FieldOfStudy_ID != 0 ? (int?)document.BachelorsCertification.FieldOfStudy_ID : null,
                document.MastersCertification != null && document.MastersCertification.FieldOfStudy_ID != 0 ? (int?)document.MastersCertification.FieldOfStudy_ID : null,
                document.PhDCertification != null && document.PhDCertification.FieldOfStudy_ID != 0 ? (int?)document.PhDCertification.FieldOfStudy_ID : null);
        }
        [HttpPost]
        public void AddOrEditWorkExperience(WorkExperienceDTO experience)
        {
            expertBusiness.AddOrEditWorkExperience(authenticationController.GetUsername(), experience.Id, experience.WorkshopName, experience.EmployerName, experience.DateFrom, experience.DateTo, experience.Description);
        }
        [HttpPost]
        public void RemoveExperience(WorkExperienceDTO experience)
        {
            expertBusiness.RemoveExperience(authenticationController.GetUsername(), experience.Id);
        }
        [HttpPost]
        public void AddOrEditCertification(SafetyCertificationDTO certification)
        {
            expertBusiness.AddOrEditCertification(authenticationController.GetUsername(), certification.Id, (Document.SafetyCertification.Type)certification.CourseType, certification.OtherCourseName, (int?)certification.OtherDurationInHours, certification.DateTaken, certification.IssuerNumber, certification.Description);
        }
        [HttpPost]
        public void RemoveCertification(SafetyCertificationDTO certification)
        {
            expertBusiness.RemoveCertification(authenticationController.GetUsername(), certification.Id);
        }
        [HttpPost]
        public async Task ChangePhotoScan()
        {
            var streamProvider = await Request.Content.ReadAsMultipartAsync();
            var file = streamProvider.Contents[0];
            expertBusiness.ChangePhotoScan(authenticationController.GetUsername(), file.Headers.ContentDisposition.FileName, file.ReadAsByteArrayAsync().Result);
        }
        [HttpPost]
        public void RemovePhotoScan()
        {
            expertBusiness.RemovePhotoScan(authenticationController.GetUsername());
        }
        [HttpPost]
        public async Task AddShenasnameScan()
        {
            var provider = await Request.Content.ReadAsMultipartAsync();
            var file = provider.Contents[0];
            expertBusiness.AddShenasnameScan(authenticationController.GetUsername(), file.Headers.ContentDisposition.FileName, file.ReadAsByteArrayAsync().Result);
        }
        [HttpPost]
        public void RemoveShenasnameScan(AttachmentDTO data)
        {
            expertBusiness.RemoveShenasnameScan(authenticationController.GetUsername(), data.Id);
        }
        [HttpPost]
        public async Task AddNationalCardScan()
        {
            var provider = await Request.Content.ReadAsMultipartAsync();
            var file = provider.Contents[0];
            expertBusiness.AddNationalCardScan(authenticationController.GetUsername(), file.Headers.ContentDisposition.FileName, file.ReadAsByteArrayAsync().Result);
        }
        [HttpPost]
        public void RemoveNationalCardScan(AttachmentDTO data)
        {
            expertBusiness.RemoveNationalCardScan(authenticationController.GetUsername(), data.Id);
        }
        [HttpPost]
        public async Task ChangeHighSchoolScan()
        {
            var provider = await Request.Content.ReadAsMultipartAsync();
            var file = provider.Contents[0];
            expertBusiness.ChangeHighSchoolScan(authenticationController.GetUsername(), file.Headers.ContentDisposition.FileName, file.ReadAsByteArrayAsync().Result);
        }
        [HttpPost]
        public void RemoveHighSchoolScan()
        {
            expertBusiness.RemoveHighSchoolScan(authenticationController.GetUsername());
        }
        [HttpPost]
        public async Task ChangeDiplomaScan()
        {
            var provider = await Request.Content.ReadAsMultipartAsync();
            var file = provider.Contents[0];
            expertBusiness.ChangeDiplomaScan(authenticationController.GetUsername(), file.Headers.ContentDisposition.FileName, file.ReadAsByteArrayAsync().Result);
        }
        [HttpPost]
        public void RemoveDiplomaScan()
        {
            expertBusiness.RemoveDiplomaScan(authenticationController.GetUsername());
        }
        [HttpPost]
        public async Task ChangeBachelorScan()
        {
            var provider = await Request.Content.ReadAsMultipartAsync();
            var file = provider.Contents[0];
            expertBusiness.ChangeBachelorScan(authenticationController.GetUsername(), file.Headers.ContentDisposition.FileName, file.ReadAsByteArrayAsync().Result);
        }
        [HttpPost]
        public void RemoveBachelorScan()
        {
            expertBusiness.RemoveBachelorScan(authenticationController.GetUsername());
        }
        [HttpPost]
        public async Task ChangeMasterScan()
        {
            var provider = await Request.Content.ReadAsMultipartAsync();
            var file = provider.Contents[0];
            expertBusiness.ChangeMasterScan(authenticationController.GetUsername(), file.Headers.ContentDisposition.FileName, file.ReadAsByteArrayAsync().Result);
        }
        [HttpPost]
        public void RemoveMasterScan()
        {
            expertBusiness.RemoveMasterScan(authenticationController.GetUsername());
        }
        [HttpPost]
        public async Task ChangePhDScan()
        {
            var provider = await Request.Content.ReadAsMultipartAsync();
            var file = provider.Contents[0];
            expertBusiness.ChangePhDScan(authenticationController.GetUsername(), file.Headers.ContentDisposition.FileName, file.ReadAsByteArrayAsync().Result);
        }
        [HttpPost]
        public void RemovePhdScan()
        {
            expertBusiness.RemovePhdScan(authenticationController.GetUsername());
        }
        [HttpPost]
        public async Task ChangeCertificationScan()
        {
            var provider = await Request.Content.ReadAsMultipartAsync();
            var file = provider.Contents[1];
            expertBusiness.ChangeCertificationScan(authenticationController.GetUsername(), int.Parse(provider.Contents[0].ReadAsStringAsync().Result), file.Headers.ContentDisposition.FileName, file.ReadAsByteArrayAsync().Result);
        }
        [HttpPost]
        public void RemoveCertificationScan(SafetyCertificationDTO certification)
        {
            expertBusiness.RemoveCertificationScan(authenticationController.GetUsername(), certification.Id);
        }
        [HttpPost]
        public async Task AddContractScan()
        {
            var provider = await Request.Content.ReadAsMultipartAsync();
            var file = provider.Contents[0];
            expertBusiness.AddContractScan(authenticationController.GetUsername(), file.Headers.ContentDisposition.FileName, file.ReadAsByteArrayAsync().Result);
        }
        [HttpPost]
        public void RemoveContractScan(AttachmentDTO scan)
        {
            expertBusiness.RemoveContractScan(authenticationController.GetUsername(), scan.Id);
        }
        [HttpPost]
        public async Task AddTPCScan()
        {
            var provider = await Request.Content.ReadAsMultipartAsync();
            var file = provider.Contents[0];
            expertBusiness.AddTPCScan(authenticationController.GetUsername(), file.Headers.ContentDisposition.FileName, file.ReadAsByteArrayAsync().Result);
        }
        [HttpPost]
        public void RemoveTPCScan(AttachmentDTO scan)
        {
            expertBusiness.RemoveTPCScan(authenticationController.GetUsername(), scan.Id);
        }
        [HttpPost]
        public async Task AddInsuranceScan()
        {
            var provider = await Request.Content.ReadAsMultipartAsync();
            var file = provider.Contents[0];
            expertBusiness.AddInsuranceScan(authenticationController.GetUsername(), file.Headers.ContentDisposition.FileName, file.ReadAsByteArrayAsync().Result);
        }
        [HttpPost]
        public void RemoveInsuranceScan(AttachmentDTO scan)
        {
            expertBusiness.RemoveInsuranceScan(authenticationController.GetUsername(), scan.Id);
        }
        [HttpPost]
        public void SignAgreement()
        {
            expertBusiness.SignAgreement(authenticationController.GetUsername());
        }
        [HttpPost]
        public void SendRequest(DocumentDTO document)
        {
            expertBusiness.SendRequest(authenticationController.GetUsername(),
                                        //must have value
                                        (Document.GenderType)document.Gender.Value,
                                        document.FirstName,
                                        document.LastName,
                                        document.CurrentWorkShop.UnitName,
                                        document.CurrentWorkShop.EmployerName,
                                        //must have value
                                        (Document.GenderType)document.CurrentWorkShop.EmployerGender,
                                        document.CurrentWorkShop.SocialSecurityCode,
                                        //must have value
                                        document.CurrentWorkShop.FieldOfActivity.Id,
                                        //must have value
                                        document.CurrentWorkShop.ProductionType.Id,
                                        document.CurrentWorkShop.MaleWorkersNo,
                                        document.CurrentWorkShop.FemaleWorkersNo,
                                        document.CurrentWorkShop.MorningWorkersNo,
                                        document.CurrentWorkShop.EveningWorkersNo,
                                        document.CurrentWorkShop.NightWorkersNo,
                                        document.CurrentWorkShop.SafetyWorkersNo,
                                        document.CurrentWorkShop.TechnicalProtectionCommittee,
                                        document.CurrentWorkShop.WorkshopAddress,
                                        document.CurrentWorkShop.WorkshopPostalCode,
                                        document.CurrentWorkShop.WorkshopPhone,
                                        document.CurrentWorkShop.WorkshopSize,
                                        document.CurrentWorkShop.DateFrom,
                                        document.Email);
        }
        [HttpPost]
        public async Task AddObjectionScan()
        {
            var provider = await Request.Content.ReadAsMultipartAsync();
            var file = provider.Contents[0];
            expertBusiness.AddObjectionScan(authenticationController.GetUsername(), file.Headers.ContentDisposition.FileName, file.ReadAsByteArrayAsync().Result);
        }
        [HttpPost]
        public void RemoveObjectionScan(AttachmentDTO scan)
        {
            expertBusiness.RemoveObjectionScan(authenticationController.GetUsername(), scan.Id);
        }
        [HttpPost]
        public void ObjectifyOnce(ObjectionData data)
        {
            expertBusiness.ObjectifyOnce(authenticationController.GetUsername(), data.Reason);
        }
        [HttpPost]
        public void ObjectifyTwice(ObjectionData data)
        {
            expertBusiness.ObjectifyTwice(authenticationController.GetUsername(), data.Reason);
        }
    }
}
