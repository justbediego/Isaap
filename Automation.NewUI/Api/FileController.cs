﻿using Automation.Business;
using Automation.DataAccess;
using Automation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Automation.NewUI.Api
{
    [Authorize]
    public class FileController : ApiController
    {    
        private FileBusiness fileBusiness;
        public FileController()
        {
            AutomationContext dbContext = new AutomationContext();
            fileBusiness = new FileBusiness(dbContext);
        }
        [HttpGet]
        public HttpResponseMessage GetImage(Guid ID, int width = -1, int height = -1)
        {
            Attachment image = fileBusiness.GetImage(ID, width, height);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new ByteArrayContent(image.AttachmentData.FileData);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpg");
            response.Content.Headers.Add("Content-Disposition", "inline; filename=\"" + image.Filename.Replace("\"", "") + "\"");
            return response;
        }
        [HttpGet]
        public HttpResponseMessage GetReportFile(Guid ID)
        {
            Attachment file = fileBusiness.GetReportFile(ID);
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new ByteArrayContent(file.AttachmentData.FileData);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            response.Content.Headers.Add("Content-Disposition", "attachment; filename=\"" + file.Filename.Replace("\"", "") + "\"");
            return response;
        }
    }
}
