﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Automation.NewUI.Mvc.Config
{
    public class MvcConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                "MvcRoute",                                              
                "{controller}/{action}",                           
                new { controller = "Main", action = "Start" }  
            );
        }
    }
}