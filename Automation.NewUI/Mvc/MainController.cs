﻿using Automation.NewUI.Api.Filters;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Automation.NewUI.Mvc
{
    public class MainController : Controller
    {
        // GET: Main
        [NoCache]
        public ActionResult Start()
        {
            return View();
        }

        private string GenerateCaptchaText(int minLen, int maxLen)
        {
            string chars = "abcdefghjkmnpqrtuwxz234689";
            string result = string.Empty;
            Random rand = new Random();
            for (int i = 0; i < rand.Next(minLen, maxLen); i++)
                result += rand.NextDouble() > .5 ? chars[rand.Next(chars.Length)] : chars.ToUpper()[rand.Next(chars.Length)];
            return result;
        }

        private Bitmap CreateCaptchaImage(string captchaText, int width, int height)
        {
            Bitmap bitmap = new Bitmap(width, height);
            Graphics graphics = Graphics.FromImage(bitmap);
            Random random = new Random();
            int hatchGrayness = random.Next(150, 250);
            Color hatchColor = Color.FromArgb(hatchGrayness, hatchGrayness, hatchGrayness);
            Color backgroundColor = Color.White;
            HatchStyle[] hatchStyles = new HatchStyle[]
            {
                HatchStyle.Cross,HatchStyle.DashedHorizontal,HatchStyle.Plaid,HatchStyle.Shingle,HatchStyle.SmallCheckerBoard,
                HatchStyle.DashedVertical,HatchStyle.DiagonalBrick,HatchStyle.DiagonalCross,HatchStyle.HorizontalBrick,HatchStyle.LargeCheckerBoard,
                HatchStyle.LargeConfetti,HatchStyle.LargeGrid,HatchStyle.LightDownwardDiagonal,HatchStyle.NarrowHorizontal,HatchStyle.NarrowVertical,
                HatchStyle.SolidDiamond, HatchStyle.Sphere, HatchStyle.Trellis, HatchStyle.SmallGrid,
                HatchStyle.Vertical, HatchStyle.Wave, HatchStyle.Weave,
                HatchStyle.WideDownwardDiagonal, HatchStyle.WideUpwardDiagonal, HatchStyle.ZigZag
            };
            string[] fonts = new string[] { "Helvetica", "Gotham", "Times New Roman" };
            FontStyle[] fontStyles = new FontStyle[] { FontStyle.Bold, FontStyle.Italic, FontStyle.Regular, FontStyle.Underline };

            //Background
            graphics.FillRectangle(new HatchBrush(hatchStyles[random.Next(hatchStyles.Length)], hatchColor, backgroundColor), 0, 0, width, height);

            float fontSizeMax = Math.Min(height, (float)width / (captchaText.Length + 1));
            float fontSizeMin = fontSizeMax / 1.5f;
            for (int i = 0; i < captchaText.Length; i++)
            {
                Matrix matrix = new Matrix();
                float fontSize = (float)random.NextDouble() * (fontSizeMax - fontSizeMin) + fontSizeMin;
                fontSize /= 1.5f;
                Font textFont = new Font(fonts[random.Next(fonts.Length)], fontSize, fontStyles[random.Next(fontStyles.Length - 1)]);
                Color textColor = Color.FromArgb(random.Next(0, 100), random.Next(0, 100), random.Next(0, 100));
                PointF centerOfRotation = new PointF((i + .5f) * fontSizeMax + fontSizeMax / 2, height / 2);
                matrix.RotateAt(random.Next(-20, 20), centerOfRotation);
                graphics.Transform = matrix;
                graphics.DrawString(captchaText.Substring(i, 1), textFont, new SolidBrush(textColor), (i + .5f) * fontSizeMax, (height - fontSizeMax) / 2);
                graphics.ResetTransform();
            }
            return bitmap;
        }

        [HttpGet]
        public ActionResult GetCaptchaImage()
        {
            string captchaText = GenerateCaptchaText(4, 7);
            Session["Captcha"] = captchaText;
            Bitmap captchaImage = CreateCaptchaImage(captchaText, 300, 65);
            var stream = new MemoryStream();
            captchaImage.Save(stream, System.Drawing.Imaging.ImageFormat.Jpeg);
            return base.File(stream.ToArray(), "image/jpeg");
        }
    }
}