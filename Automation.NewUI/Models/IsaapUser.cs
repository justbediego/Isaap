﻿using Automation.Business;
using Automation.Business.Inner;
using Automation.DataAccess;
using Automation.Model;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;

namespace Automation.NewUI.Models
{
    public class IsaapUser : User, IUser<int>
    {
        public bool IsPersistent { get; set; }
        public static ClaimsIdentity ToClaimsIdentity(User user)
        {
            var identity = new ClaimsIdentity(DefaultAuthenticationTypes.ApplicationCookie);
            identity.AddClaim(new Claim(ClaimTypes.Name, user.UserName));
            identity.AddClaim(new Claim(ClaimTypes.Role, ((int)user.Type).ToString()));
            return identity;
        }
        public static User FromClaimsIdentity(IPrincipal user)
        {
            AutomationContext dbContext = new AutomationContext();
            Validation validation = new Validation(dbContext);
            SystemInfoBusiness systemInfoBusiness = new SystemInfoBusiness(dbContext, validation);
            UserBusiness userBusiness = new UserBusiness(dbContext, validation, systemInfoBusiness);
            return userBusiness.GetUser(user.Identity.Name);
        }
    }
}