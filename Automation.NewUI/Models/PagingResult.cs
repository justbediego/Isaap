﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Automation.NewUI.Models
{
    public class PagingResult<T>
    {
        public List<T> Data;
        public int Count;
    }
}