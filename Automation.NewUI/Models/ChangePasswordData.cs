﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Automation.NewUI.Models
{
    public class ChangePasswordData
    {
        public string CurrentPassword;
        public string NewPassword;
    }
}