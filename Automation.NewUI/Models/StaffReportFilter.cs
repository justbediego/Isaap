﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Automation.NewUI.Models
{
    public class StaffReportFilter
    {
        public string FullName;
        public string Mobile;
        public int? CurrentFlowType;
        public DateTime? BirthdayFrom
        {
            get
            {
                return string.IsNullOrEmpty(PersianBirthdayFrom) ? null :
                    (DateTime?)PersianDate.ConvertDate.ToEn(PersianBirthdayFrom);
            }
        }
        public DateTime? BirthdayTo
        {
            get
            {
                return string.IsNullOrEmpty(PersianBirthdayTo) ? null :
                    (DateTime?)PersianDate.ConvertDate.ToEn(PersianBirthdayTo);
            }
        }
        public DateTime? RegisterDateFrom
        {
            get
            {
                return string.IsNullOrEmpty(PersianRegisterDateFrom) ? null :
                    (DateTime?)PersianDate.ConvertDate.ToEn(PersianRegisterDateFrom);
            }
        }
        public DateTime? RegisterDateTo
        {
            get
            {
                return string.IsNullOrEmpty(PersianRegisterDateTo) ? null :
                    (DateTime?)PersianDate.ConvertDate.ToEn(PersianRegisterDateTo);
            }
        }
        public DateTime? CurrentFlowDateFrom
        {
            get
            {
                return string.IsNullOrEmpty(PersianCurrentFlowDateFrom) ? null :
                    (DateTime?)PersianDate.ConvertDate.ToEn(PersianCurrentFlowDateFrom);
            }
        }
        public DateTime? CurrentFlowDateTo
        {
            get
            {
                return string.IsNullOrEmpty(PersianCurrentFlowDateTo) ? null :
                    (DateTime?)PersianDate.ConvertDate.ToEn(PersianCurrentFlowDateTo);
            }
        }
        public string PersianBirthdayFrom;
        public string PersianBirthdayTo;
        public string PersianRegisterDateFrom;
        public string PersianRegisterDateTo;
        public string PersianCurrentFlowDateFrom;
        public string PersianCurrentFlowDateTo;
        public string NationalCode;
        public string Shenasname;
        public string PlaceOfBirth;
        public int? Gender;
        public int? MaritalStatus;
        public int? MilitaryStatus;
        public bool? OlympiadRequest;
        public int? FieldOfStudyId;
        public int? FieldOfActivityId;
        public int? ProductionTypeId;
        public int? HomeProvinceId;
        public int? HomeCountyId;
        public int? WorkProvinceId;
        public int? WorkCountyId;
        public string UnitName;
        public string EmployerFullName;
        public string AssociationNumber;
        public int? Take;
        public int? Skip;
    }
}