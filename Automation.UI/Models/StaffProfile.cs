﻿using Automation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Automation.UI.Models.StaffProfile
{
    public class MyDocuments : ProfileLayoutChildObject
    {
        public List<Document> Documents;
    }
    public class DocumentOverview : ProfileLayoutChildObject
    {
        public Document Document;
    }
    public class ApplicationProcess : ProfileLayoutChildObject
    {
        public Document Document;
    }
}