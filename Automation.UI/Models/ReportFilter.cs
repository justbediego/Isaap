﻿using Automation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Automation.UI.Models.Report
{
    public class ReportFilter : ProfileLayoutChildObject
    {
        public List<ProductionType> ProductionTypes;
        public List<FieldOfActivity> FieldsOfActivity;
        public List<FieldOfStudy> FieldsOfStudy;
        public List<Province> Provinces;
        public int? FieldOfStudy;
        public Document.MilitaryServiceStates? MilitaryService;
        public Document.MaritalStates? MaritalStates;
        public Document.GenderType? Gender;
        public Flow.FlowType? CurrentFlowType;
        public string FullName;
        public string ShenasnameNo;
        public string NationalCode;
        public string BirthDateFrom;
        public string BirthDateTo;
        public string From;
        public string To;
        public string AssociationNo;
        public string Mobile;
        public string PlaceOfBirth;
        public int? HomeCounty;
        public int? HomeProvince;
        public int? WorkCounty;
        public int? WorkProvince;
        public int? FieldOfActivity;
        public string WorkshopName;
        public string EmployerName;
        public string InspectorName;
        public List<Document> Results;
    }
}