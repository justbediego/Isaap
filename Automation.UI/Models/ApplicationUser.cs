﻿using Automation.Business;
using Automation.Model;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;

namespace Automation.UI.Models
{
    public class ApplicationUser : User, IUser<int>
    {
        public static ClaimsIdentity ToClaimsIdentity(User user)
        {
            var identity = new ClaimsIdentity(DefaultAuthenticationTypes.ApplicationCookie);
            identity.AddClaim(new Claim(ClaimTypes.Name, user.UserName));
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()));
            identity.AddClaim(new Claim(ClaimTypes.Role, ((int)user.Type).ToString()));
            return identity;
        }
        public static User FromClaimsIdentity(IPrincipal user)
        {
            UserBusiness userBusiness = new UserBusiness();
            return userBusiness.GetUser(user.Identity.Name);
        }
    }
}