﻿using Automation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Automation.UI.Models.AdministratorProfile
{
    public class ProvinceManagement : ProfileLayoutChildObject
    {
        public List<Province> Provinces;
    }
    public class CountyManagement : ProfileLayoutChildObject
    {
        public List<Province> Provinces;
        public List<County> Counties;
    }
    public class AssociationManagement : ProfileLayoutChildObject
    {
        public List<County> Counties;
        public List<Association> Associations;
    }
    public class ClubManagement : ProfileLayoutChildObject
    {
        public List<Province> Provinces;
        public List<Club> Clubs;
    }
    public class CountyInspectorManagement : ProfileLayoutChildObject
    {
        public List<County> Counties;
        public List<CountyInspector> CountyInspectors;
    }
    public class FieldOfStudyManagement : ProfileLayoutChildObject
    {
        public List<FieldOfStudy> FieldsOfStudy;
    }
    public class FieldOfActivityManagement : ProfileLayoutChildObject
    {
        public List<FieldOfActivity> FieldsOfActivity;
    }
    public class ProductionTypeManagement : ProfileLayoutChildObject
    {
        public List<ProductionType> ProductionTypes;
    }
    public class InquiryFieldManagement : ProfileLayoutChildObject
    {
        public List<InquiryColumn> InquiryFields;
    }
    public class SystemLockManagement : ProfileLayoutChildObject
    {
        public bool IsLocked;
    }
    public class UserManagement : ProfileLayoutChildObject
    {
        public List<User> Users;
    }
    public class RequirementRuleManagement : ProfileLayoutChildObject
    {
        public List<RequirementRule> RequirementRules;
    }
    public class DocumentManagement : ProfileLayoutChildObject
    {
        public List<Document> Documents;
    }
}