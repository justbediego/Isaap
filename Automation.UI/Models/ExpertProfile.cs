﻿using Automation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Automation.UI.Models.ExpertProfile
{
    public class RequestPage : ProfileLayoutChildObject
    {
        public Document Document;
        public List<FieldOfActivity> FieldsOfActivity;
        public List<ProductionType> ProductionTypes;
    }
    public class ExpertInfo : ProfileLayoutChildObject
    {
        public Document Document;
        public List<Province> Provinces;
        public List<County> HomeCounties;
        public List<County> WorkCounties;
        public List<FieldOfStudy> FieldsOfStudy;
    }
    public class WorkHistory : ProfileLayoutChildObject
    {
        public Document Document;
    }
    public class Attachments: ProfileLayoutChildObject
    {
        public Document Document;
    }
    public class Certifications : ProfileLayoutChildObject
    {
        public Document Document;
    }
    public class Overview : ProfileLayoutChildObject
    {
        public Document Document;
    }
    public class Objection: ProfileLayoutChildObject
    {
        public Document Document;
    }
    public class Reports : ProfileLayoutChildObject
    {
        public Document Document;
    }
}