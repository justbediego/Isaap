﻿using Automation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Automation.UI.Models
{
    public class ProfileLayoutMenu{
        public ProfileLayoutMenu()
        {
            Children = null;
        }
        public int Id;
        public string Icon;
        public string Name;
        public string HRef;
        public bool IsActive;
        public bool IsHidden;
        public List<ProfileLayoutMenu> Children;
    }
    public class ProfileLayout
    {
        public string ProfileName;
        public string ProfileDesc;
        public List<ProfileLayoutMenu> Menus;
        public ProfileLayoutChildObject ChildObject;
    }
    public abstract class ProfileLayoutChildObject
    {

    }
}