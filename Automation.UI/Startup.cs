﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Automation.Startup))]
namespace Automation
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
