﻿using Automation.Business;
using Automation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;

namespace Automation.UI.Controllers
{
    public class SystemController : Controller
    {
        private SystemInfoBusiness systemInfo;
        public SystemController()
        {
            systemInfo = new SystemInfoBusiness();
        }
        [HttpPost]
        public string GetAllCounties(int provinceID)
        {
            string result = JsonConvert.SerializeObject(systemInfo.GetAllCountiesBasedOnProvince(provinceID).Select(c => new
            {
                ID = c.Id,
                Name = c.Name,
            }));
            return result;
        }
    }
}
