﻿using Automation.Business;
using Automation.DataAccess;
using Automation.Model;
using Automation.UI.Models;
using Automation.UI.Models.AdministratorProfile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Automation.UI.Controllers
{
    [Authorize(Roles = "8")]
    public class AdminController : Controller
    {
        private ProfileLayout profileLayout;
        private AdministratorBusiness administrator;
        private SystemInfoBusiness systemInfo;
        public AdminController()
        {
            administrator = new AdministratorBusiness();
            systemInfo = new SystemInfoBusiness();
        }
        public void InitialData()
        {
            profileLayout = new ProfileLayout()
            {
                ProfileName = "مدیریت سامانه",
                ProfileDesc = "مدیریت سامانه",
                Menus = new List<ProfileLayoutMenu>()
                {
                    new ProfileLayoutMenu(){
                        Id=1,
                        Icon="gear",
                        Name="استان ها",
                        HRef="/Admin/Provinces"
                    },
                    new ProfileLayoutMenu(){
                        Id=2,
                        Icon="gear",
                        Name="شهرستان ها",
                        HRef="/Admin/Counties"
                    },
                    new ProfileLayoutMenu(){
                        Id=3,
                        Icon="gear",
                        Name="انجمن ها",
                        HRef="/Admin/Associations"
                    },
                    new ProfileLayoutMenu(){
                        Id=4,
                        Icon="gear",
                        Name="کانون ها",
                        HRef="/Admin/Clubs"
                    },
                    new ProfileLayoutMenu(){
                        Id=5,
                        Icon="gear",
                        Name="بازرسان شهرستان",
                        HRef="/Admin/CountyInspectors"
                    },
                    new ProfileLayoutMenu(){
                        Id=6,
                        Icon="th-large",
                        Name="اطلاعات پایه",
                        Children=new List<ProfileLayoutMenu>(){
                            new ProfileLayoutMenu(){
                                Id=1,
                                Name="رشته ها",
                                HRef="/Admin/FieldsOfStudy"
                            },
                            new ProfileLayoutMenu(){
                                Id=2,
                                Name="فعالیت ها",
                                HRef="/Admin/FieldsOfActivity"
                            },
                            new ProfileLayoutMenu(){
                                Id=3,
                                Name="تولید ها",
                                HRef="/Admin/ProductionTypes"
                            },
                            new ProfileLayoutMenu(){
                                Id=4,
                                Name="فیلدهای استعلام",
                                HRef="/Admin/InquiryFields"
                            },
                            new ProfileLayoutMenu(){
                                Id=5,
                                Name="قفل سیستم",
                                HRef="/Admin/SystemLock"
                            },
                            new ProfileLayoutMenu()
                            {
                                Id=6,
                                Name="مقدار نیازمندی",
                                HRef="/Admin/RequirementRules"
                            },
                        },
                    },
                    new ProfileLayoutMenu(){
                        Id=7,
                        Icon="user",
                        Name="کاربران",
                        HRef="/Admin/Users",
                    },
                    new ProfileLayoutMenu()
                    {
                        Id=8,
                        Icon="user",
                        Name="پرونده ها",
                        HRef="/Admin/Documents",
                    }
                },
            };
        }
        public ActionResult Index()
        {
            InitialData();
            return View("Index", profileLayout);
        }
        public ActionResult Provinces()
        {
            InitialData();
            profileLayout.Menus.Where(m => m.Id == 1).FirstOrDefault().IsActive = true;
            profileLayout.ChildObject = new ProvinceManagement()
            {
                Provinces = systemInfo.GetAllProvinces(),
            };
            return View("Provinces", profileLayout);
        }
        public ActionResult Counties()
        {
            InitialData();
            profileLayout.Menus.Where(m => m.Id == 2).FirstOrDefault().IsActive = true;
            profileLayout.ChildObject = new CountyManagement()
            {
                Provinces = systemInfo.GetAllProvinces(),
                Counties = systemInfo.GetAllCounties(),
            };
            return View("Counties", profileLayout);
        }
        public ActionResult Associations()
        {
            InitialData();
            profileLayout.Menus.Where(m => m.Id == 3).FirstOrDefault().IsActive = true;
            profileLayout.ChildObject = new AssociationManagement()
            {
                Associations = systemInfo.GetAllAssociations(),
                //only those without association
                Counties = systemInfo.GetAllCounties().Where(c => c.Associations.Count == 0).ToList(),
            };
            return View("Associations", profileLayout);
        }
        public ActionResult Clubs()
        {
            InitialData();
            profileLayout.Menus.Where(m => m.Id == 4).FirstOrDefault().IsActive = true;
            profileLayout.ChildObject = new ClubManagement()
            {
                //only those without club
                Provinces = systemInfo.GetAllProvinces().Where(p => p.Clubs.Count == 0).ToList(),
                Clubs = systemInfo.GetAllClubs(),
            };
            return View("Clubs", profileLayout);
        }
        public ActionResult CountyInspectors()
        {
            InitialData();
            profileLayout.Menus.Where(m => m.Id == 5).FirstOrDefault().IsActive = true;
            profileLayout.ChildObject = new CountyInspectorManagement()
            {
                //only those without association
                Counties = systemInfo.GetAllCounties().Where(c => c.CountyInspectors.Count == 0).ToList(),
                CountyInspectors = systemInfo.GetAllCountyInspectors(),
            };
            return View("CountyInspectors", profileLayout);
        }
        public ActionResult FieldsOfStudy()
        {
            InitialData();
            profileLayout.Menus.Where(m => m.Id == 6).FirstOrDefault().IsActive = true;
            profileLayout.Menus.Where(m => m.Id == 6).FirstOrDefault().Children.Where(c => c.Id == 1).FirstOrDefault().IsActive = true;
            profileLayout.ChildObject = new FieldOfStudyManagement()
            {
                FieldsOfStudy = systemInfo.GetAllFieldsOfStudy(),
            };
            return View("FieldsOfStudy", profileLayout);
        }
        public ActionResult FieldsOfActivity()
        {
            InitialData();
            profileLayout.Menus.Where(m => m.Id == 6).FirstOrDefault().IsActive = true;
            profileLayout.Menus.Where(m => m.Id == 6).FirstOrDefault().Children.Where(c => c.Id == 2).FirstOrDefault().IsActive = true;
            profileLayout.ChildObject = new FieldOfActivityManagement()
            {
                FieldsOfActivity = systemInfo.GetAllFieldsOfActivity(),
            };
            return View("FieldsOfActivity", profileLayout);
        }
        public ActionResult ProductionTypes()
        {
            InitialData();
            profileLayout.Menus.Where(m => m.Id == 6).FirstOrDefault().IsActive = true;
            profileLayout.Menus.Where(m => m.Id == 6).FirstOrDefault().Children.Where(c => c.Id == 3).FirstOrDefault().IsActive = true;
            profileLayout.ChildObject = new ProductionTypeManagement()
            {
                ProductionTypes = systemInfo.GetAllProductionTypes(),
            };
            return View("ProductionTypes", profileLayout);
        }
        public ActionResult InquiryFields()
        {
            InitialData();
            profileLayout.Menus.Where(m => m.Id == 6).FirstOrDefault().IsActive = true;
            profileLayout.Menus.Where(m => m.Id == 6).FirstOrDefault().Children.Where(c => c.Id == 4).FirstOrDefault().IsActive = true;
            profileLayout.ChildObject = new InquiryFieldManagement()
            {
                InquiryFields = systemInfo.GetAllInquiryColumns(),
            };
            return View("InquiryFields", profileLayout);
        }
        public ActionResult SystemLock()
        {
            InitialData();
            profileLayout.Menus.Where(m => m.Id == 6).FirstOrDefault().IsActive = true;
            profileLayout.Menus.Where(m => m.Id == 6).FirstOrDefault().Children.Where(c => c.Id == 5).FirstOrDefault().IsActive = true;
            profileLayout.ChildObject = new SystemLockManagement()
            {
                IsLocked = systemInfo.GetIsLocked(),
            };
            return View("SystemLock", profileLayout);
        }
        public ActionResult RequirementRules()
        {
            InitialData();
            profileLayout.Menus.Where(m => m.Id == 6).FirstOrDefault().IsActive = true;
            profileLayout.Menus.Where(m => m.Id == 6).FirstOrDefault().Children.Where(c => c.Id == 6).FirstOrDefault().IsActive = true;
            profileLayout.ChildObject = new RequirementRuleManagement()
            {
                RequirementRules = systemInfo.GetAllRequirementRules(),
            };
            return View("RequirementRules", profileLayout);
        }
        public ActionResult Users()
        {
            InitialData();
            profileLayout.Menus.Where(m => m.Id == 7).FirstOrDefault().IsActive = true;
            profileLayout.ChildObject = new UserManagement()
            {
                Users = systemInfo.GetAllUsers().Where(u => u.Type != Model.User.UserType.Expert).ToList(),
            };
            return View("Users", profileLayout);
        }
        public ActionResult Documents()
        {
            InitialData();
            profileLayout.Menus.Where(m => m.Id == 8).FirstOrDefault().IsActive = true;
            profileLayout.ChildObject = new DocumentManagement()
            {
                Documents = systemInfo.GetAllDocuments(null),
            };
            return View("Documents", profileLayout);
        }
        [HttpPost]
        public void DeleteProvince(int ID)
        {
            try
            {
                administrator.RemoveProvince(ID);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void AddProvince(string provinceName, string provinceCode, string inspectorUsername, string inspectorPassword, string managerUsername, string managerPassword, string headFullName)
        {
            try
            {
                administrator.AddProvince(provinceName, provinceCode, inspectorUsername, inspectorPassword, managerUsername, managerPassword, headFullName);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void AddCounty(int provinceID, string countyName)
        {
            try
            {
                administrator.AddCounty(provinceID, countyName);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void DeleteCounty(int ID)
        {
            try
            {
                administrator.RemoveCounty(ID);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void AddAssociation(int countyID, string username, string password)
        {
            try
            {
                administrator.AddAssociation(countyID, username, password);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void DeleteAssociation(int ID)
        {
            try
            {
                administrator.RemoveAssociation(ID);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void AddClub(int provinceID, string username, string password)
        {
            try
            {
                administrator.AddClub(provinceID, username, password);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void DeleteClub(int ID)
        {
            try
            {
                administrator.RemoveClub(ID);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void AddCountyInspector(int countyID, string username, string password)
        {
            try
            {
                administrator.AddCountyInspector(countyID, username, password);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void DeleteCountyInspector(int ID)
        {
            try
            {
                administrator.RemoveCountyInspector(ID);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void AddFieldOfStudy(string name, string code, string orginalityCode, bool required40Hours, bool requires16Hours)
        {
            try
            {
                administrator.AddFieldOfStudy(name, code, orginalityCode, required40Hours, requires16Hours);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void DeleteFieldOfStudy(int ID)
        {
            try
            {
                administrator.RemoveFieldOfStudy(ID);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void AddFieldOfActivity(string name, FieldOfActivity.DangerLevels dangerLevel)
        {
            try
            {
                administrator.AddFieldOfActivity(name, dangerLevel);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void DeleteFieldOfActivity(int ID)
        {
            try
            {
                administrator.RemoveFieldOfActivity(ID);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void AddProductionType(string name)
        {
            try
            {
                administrator.AddProductionType(name);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void DeleteProductionType(int ID)
        {
            try
            {
                administrator.RemoveProductionType(ID);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }

        [HttpPost]
        public void AddRequirementRule(FieldOfActivity.DangerLevels dangerLevel, Document.Workshop.SizeCategories sizeCategory, Document.LevelOfEducations levelOfEducation, int requiredHours)
        {
            try
            {
                administrator.AddRequirementRule(dangerLevel, sizeCategory, levelOfEducation, requiredHours);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void DeleteRequirementRule(int ruleID)
        {
            try
            {
                administrator.RemoveRequirementRule(ruleID);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void AddInquiryField(InquiryColumn.ColumnType column)
        {
            try
            {
                administrator.SetInquiryColumn(column, true);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void DeleteInquiryField(InquiryColumn.ColumnType column)
        {
            try
            {
                administrator.SetInquiryColumn(column, false);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void LockSystem()
        {
            try
            {
                administrator.SetSystemLock(true);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void UnlockSystem()
        {
            try
            {
                administrator.SetSystemLock(false);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void EditUsername(int userID, string username)
        {
            try
            {
                administrator.ChangeUsername(userID, username);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void EditPassword(int userID, string password)
        {
            try
            {
                administrator.ChangePassword(userID, password);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void DeleteDocument(int ID)
        {
            try
            {
                administrator.RemoveDocument(ID);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
    }
}
