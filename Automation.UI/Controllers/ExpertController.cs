﻿using Automation.Business;
using Automation.Model;
using Automation.UI.Models;
using Automation.UI.Models.ExpertProfile;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Automation.UI.Controllers
{
    [Authorize(Roles = "1")]
    public class ExpertController : Controller
    {
        private int documentID
        {
            get
            {
                return ApplicationUser.FromClaimsIdentity(User).Id;
            }
        }
        private Document document = null;
        private ProfileLayout profileLayout;
        private SystemInfoBusiness systemInfo;
        private ExpertBusiness expert;
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        public ExpertController()
        {
            expert = new ExpertBusiness();
            systemInfo = new SystemInfoBusiness();
        }
        public void InitialData()
        {
            systemInfo = new SystemInfoBusiness();
            document = expert.GetDocumentBasicInfo(documentID);
            profileLayout = new ProfileLayout()
            {
                ProfileName = document.FullName,
                ProfileDesc = "مسئول ایمنی",
            };
            profileLayout.Menus = new List<ProfileLayoutMenu>();
            switch (document.CurrentFlow.Type)
            {
                case Flow.FlowType.Created:
                    profileLayout.Menus.Add(new ProfileLayoutMenu()
                    {
                        Id = 1,
                        Icon = "mouse-pointer",
                        Name = "نامه درخواست",
                        HRef = "/Expert/RequestPage"
                    });                    
                    break;
                case Flow.FlowType.NotCompleted:
                    profileLayout.Menus.Add(new ProfileLayoutMenu()
                    {
                        Id = 2,
                        Icon = "user",
                        Name = "مشخصات فرد",
                        HRef = "/Expert/ExpertInfo"
                    });
                    profileLayout.Menus.Add(new ProfileLayoutMenu()
                    {
                        Id = 3,
                        Icon = "file-text",
                        Name = "سوابق کاری",
                        HRef = "/Expert/WorkHistory"
                    });
                    profileLayout.Menus.Add(new ProfileLayoutMenu()
                    {
                        Id = 4,
                        Icon = "certificate",
                        Name = "دوره های آموزشی",
                        HRef = "/Expert/Certifications"
                    });
                    profileLayout.Menus.Add(new ProfileLayoutMenu()
                    {
                        Id = 5,
                        Icon = "file",
                        Name = "پیوست ها",
                        HRef = "/Expert/Attachments"
                    });
                    break;
                case Flow.FlowType.RejectedOnce:
                    profileLayout.Menus.Add(new ProfileLayoutMenu()
                    {
                        Id = 8,
                        Icon = "check",
                        Name = "ثبت اعتراض اولیه",
                        HRef = "/Expert/FirstObjection"
                    });
                    break;
                case Flow.FlowType.RejectedTwice:
                    profileLayout.Menus.Add(new ProfileLayoutMenu()
                    {
                        Id = 9,
                        Icon = "check",
                        Name = "ثبت اعتراض ثانویه",
                        HRef = "/Expert/SecondObjection"
                    });
                    break;
                case Flow.FlowType.Authorized:
                    profileLayout.Menus.Add(new ProfileLayoutMenu()
                    {
                        Id=10,
                        Icon="file",
                        Name="گزارشات من",
                        HRef="/Expert/Reports",
                    });
                    break;
            }
            profileLayout.Menus.Add(new ProfileLayoutMenu()
            {
                Id = 7,
                Icon = "list",
                Name = "پرونده من",
                HRef = "/Expert/Index"
            });
        }
        public ActionResult RequestPage()
        {
            InitialData();
            profileLayout.Menus.Where(m => m.Id == 1).Single().IsActive = true;
            profileLayout.ChildObject = new RequestPage()
            {
                Document = document,
                FieldsOfActivity = systemInfo.GetAllFieldsOfActivity(),
                ProductionTypes = systemInfo.GetAllProductionTypes(),
            };
            return View("RequestPage", profileLayout);
        }
        public ActionResult ExpertInfo()
        {
            InitialData();
            profileLayout.Menus.Where(m => m.Id == 2).Single().IsActive = true;
            profileLayout.ChildObject = new ExpertInfo()
            {
                Document = document,
                Provinces = systemInfo.GetAllProvinces(),
                FieldsOfStudy=systemInfo.GetAllFieldsOfStudy(),
                HomeCounties = document.HomeContact.County == null ? new List<County>() : systemInfo.GetAllCountiesBasedOnProvince(document.HomeContact.County.Province_ID),
                WorkCounties = document.WorkContact.County == null ? new List<County>() : systemInfo.GetAllCountiesBasedOnProvince(document.WorkContact.County.Province_ID),
            };
            return View("ExpertInfo", profileLayout);
        }
        public ActionResult WorkHistory()
        {
            InitialData();
            profileLayout.Menus.Where(m => m.Id == 3).Single().IsActive = true;
            profileLayout.ChildObject = new WorkHistory()
            {
                Document = document,
            };
            return View("WorkHistory", profileLayout);
        }
        public ActionResult Certifications()
        {
            InitialData();
            profileLayout.Menus.Where(m => m.Id == 4).Single().IsActive = true;
            profileLayout.ChildObject = new Certifications()
            {
                Document = document,
            };
            return View("Certifications", profileLayout);
        }
        public ActionResult Attachments()
        {
            InitialData();
            profileLayout.Menus.Where(m => m.Id == 5).Single().IsActive = true;
            profileLayout.ChildObject = new Attachments()
            {
                Document = document,
            };
            return View("Attachments", profileLayout);
        }
        public ActionResult Index()
        {
            InitialData();
            profileLayout.Menus.Where(m => m.Id == 7).Single().IsActive = true;
            profileLayout.ChildObject = new Overview()
            {
                Document = document,
            };
            return View("Index", profileLayout);
        }
        public ActionResult FirstObjection()
        {
            InitialData();
            profileLayout.Menus.Where(m => m.Id == 8).Single().IsActive = true;
            profileLayout.ChildObject = new Objection()
            {
                Document = document,
            };
            return View("FirstObjection", profileLayout);
        }
        public ActionResult SecondObjection()
        {
            InitialData();
            profileLayout.Menus.Where(m => m.Id == 9).Single().IsActive = true;
            profileLayout.ChildObject = new Objection()
            {
                Document = document,
            };
            return View("SecondObjection", profileLayout);
        }
        public ActionResult Reports()
        {
            InitialData();
            profileLayout.Menus.Where(m => m.Id == 10).Single().IsActive = true;
            profileLayout.ChildObject = new Reports()
            {
                Document = document,
            };
            return View("Reports", profileLayout);
        }
        [HttpPost]
        public void SendRequest(Document.GenderType gender, string firstName, string lastName, string unitName, string employerName, Document.GenderType employerGender, string socialSecurityCode, int fieldOfActivityID, int productionTypeID, int maleWorkersNo, int femaleWorkersNo, int morningWorkersNo, int eveningWorkersNo, int nightWorkersNo, int safetyWorkersNo, bool technicalProtectionCommittee, string workshopAddress, string workshopPostalCode, string workshopPhone, double workshopSize, string dateFrom, string email)
        {
            try
            {
                DateTime convertedDateFrom = PersianDate.ConvertDate.ToEn(dateFrom);
                expert.SendRequest(documentID, gender, firstName, lastName, unitName, employerName, employerGender, socialSecurityCode, fieldOfActivityID, productionTypeID, maleWorkersNo, femaleWorkersNo, morningWorkersNo, eveningWorkersNo, nightWorkersNo, safetyWorkersNo, technicalProtectionCommittee, workshopAddress, workshopPostalCode, workshopPhone, workshopSize, convertedDateFrom, email);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void ModifyBasicInfo(string firstName, string lastName, string fatherName, Document.GenderType gender, string shenasnameNo, string melliCode, string birthDate, string placeOfBirth, Document.MaritalStates maritalStatus, Document.MilitaryServiceStates militaryServiceStates, string reasonOfExemption, string associationNumber)
        {
            try
            {
                DateTime convertedBirthDate = PersianDate.ConvertDate.ToEn(birthDate);
                expert.ModifyBasicInfo(documentID, firstName,lastName, fatherName, gender, shenasnameNo, melliCode, convertedBirthDate, placeOfBirth, maritalStatus, militaryServiceStates, reasonOfExemption, associationNumber);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void ModifyContactInfo(string mobilePhone, int homeCountyID, string homeAddress, string homePostalCode, string homePhone, int workCountyID, string workAddress, string workPostalCode, string workPhone)
        {
            try
            {
                expert.ModifyContactInfo(documentID, mobilePhone, homeCountyID, homeAddress, homePostalCode, homePhone, workCountyID, workAddress, workPostalCode, workPhone);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void ModifyAcademicInfo(bool hasHighSchool ,bool hasDiploma, bool hasBachelor, bool hasMaster, bool hasPhD, int? fieldOfHighSchool, int? fieldOfDiploma, int? fieldOfBachelor, int? fieldOfMaster, int? fieldOfPhD)
        {
            try
            {
                expert.ModifyAcademicInfo(documentID, hasHighSchool, hasDiploma, hasBachelor, hasMaster, hasPhD, fieldOfHighSchool, fieldOfDiploma, fieldOfBachelor, fieldOfMaster, fieldOfPhD);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        //[HttpPost]
        //public void ModifyEmployerInfo(string employerFullName, Document.GenderType employerGender, string employerCompany)
        //{
        //    try
        //    {
        //        expert.ModifyEmployerInfo(documentID, employerFullName, employerGender, employerCompany);
        //    }
        //    catch (Exception ex)
        //    {
        //        Response.StatusCode = 500;
        //        Response.Write(ex.Message);
        //    }
        //}
        [HttpPost]
        public void ChangePhotoScan()
        {
            try
            {
                MemoryStream fileData = new MemoryStream();
                Request.Files[0].InputStream.CopyTo(fileData);
                expert.ChangePhotoScan(documentID, Request.Files[0].FileName, fileData.ToArray());
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;      
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void RemovePhotoScan()
        {
            try
            {
                expert.RemovePhotoScan(documentID);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void RemoveShenasnameScan(int ID)
        {
            try
            {
                expert.RemoveShenasnameScan(documentID, ID);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void AddShenasnameScan()
        {
            try
            {
                MemoryStream fileData = new MemoryStream();
                Request.Files[0].InputStream.CopyTo(fileData);
                expert.AddShenasnameScan(documentID, Request.Files[0].FileName, fileData.ToArray());
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void RemoveNationalCardScan(int ID)
        {
            try
            {
                expert.RemoveNationalCardScan(documentID, ID);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void AddNationalCardScan()
        {
            try
            {
                MemoryStream fileData = new MemoryStream();
                Request.Files[0].InputStream.CopyTo(fileData);
                expert.AddNationalCardScan(documentID, Request.Files[0].FileName, fileData.ToArray());
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void AddOrEditWorkExperience(int workExperienceID, string unitName, string employerName, string dateFrom, string dateTo, string description)
        {
            try
            {
                DateTime convertedDateFrom = PersianDate.ConvertDate.ToEn(dateFrom);
                DateTime convertedDateTo = PersianDate.ConvertDate.ToEn(dateTo);
                expert.AddOrEditWorkExperience(documentID, workExperienceID, unitName, employerName, convertedDateFrom, convertedDateTo, description);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void RemoveExperience(int ID)
        {
            try
            {
                expert.RemoveExperience(documentID, ID);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void AddOrEditCertification(int certificationID, Document.SafetyCertification.Type courseType, string otherCourseName, int? otherDurationInHours, string dateTakenInString ,string issuerNumber, string description)
        {
            try
            {
                DateTime dateTaken = PersianDate.ConvertDate.ToEn(dateTakenInString);
                expert.AddOrEditCertification(documentID, certificationID, courseType, otherCourseName, otherDurationInHours, dateTaken, issuerNumber, description);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void RemoveCertification(int ID)
        {
            try
            {
                expert.RemoveCertification(documentID, ID);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void RemoveHighSchoolScan()
        {
            try
            {
                expert.RemoveHighSchoolScan(documentID);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void ChangeHighSchoolScan()
        {
            try
            {
                MemoryStream fileData = new MemoryStream();
                Request.Files[0].InputStream.CopyTo(fileData);
                expert.ChangeHighSchoolScan(documentID, Request.Files[0].FileName, fileData.ToArray());
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void RemoveDiplomaScan()
        {
            try
            {
                expert.RemoveDiplomaScan(documentID);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void ChangeDiplomaScan()
        {
            try
            {
                MemoryStream fileData = new MemoryStream();
                Request.Files[0].InputStream.CopyTo(fileData);
                expert.ChangeDiplomaScan(documentID, Request.Files[0].FileName, fileData.ToArray());
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void RemoveBachelorScan()
        {
            try
            {
                expert.RemoveBachelorScan(documentID);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void ChangeBachelorScan()
        {
            try
            {
                MemoryStream fileData = new MemoryStream();
                Request.Files[0].InputStream.CopyTo(fileData);
                expert.ChangeBachelorScan(documentID, Request.Files[0].FileName, fileData.ToArray());
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void RemoveMasterScan()
        {
            try
            {
                expert.RemoveMasterScan(documentID);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void ChangeMasterScan()
        {
            try
            {
                MemoryStream fileData = new MemoryStream();
                Request.Files[0].InputStream.CopyTo(fileData);
                expert.ChangeMasterScan(documentID, Request.Files[0].FileName, fileData.ToArray());
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void RemovePhDScan()
        {
            try
            {
                expert.RemovePhdScan(documentID);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void ChangePhDScan()
        {
            try
            {
                MemoryStream fileData = new MemoryStream();
                Request.Files[0].InputStream.CopyTo(fileData);
                expert.ChangePhDScan(documentID, Request.Files[0].FileName, fileData.ToArray());
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void ChangeCertificationScan(int ID)
        {
            try
            {
                MemoryStream fileData = new MemoryStream();
                Request.Files[0].InputStream.CopyTo(fileData);
                expert.ChangeCertificationScan(documentID, ID, Request.Files[0].FileName, fileData.ToArray());
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void RemoveCertificationScan(int ID)
        {
            try
            {
                expert.RemoveCertificationScan(documentID, ID);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void RemoveInsuranceScan(int ID)
        {
            try
            {
                expert.RemoveInsuranceScan(documentID, ID);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void AddInsuranceScan()
        {
            try
            {
                MemoryStream fileData = new MemoryStream();
                Request.Files[0].InputStream.CopyTo(fileData);
                expert.AddInsuranceScan(documentID, Request.Files[0].FileName, fileData.ToArray());
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void SignAgreement()
        {
            try
            {
                expert.SignAgreement(documentID);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void ObjectifyOnce(string reasonOfObjection)
        {
            try
            {
                expert.ObjectifyOnce(documentID, reasonOfObjection);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void ObjectifyTwice(string reasonOfObjection)
        {
            try
            {
                expert.ObjectifyTwice(documentID, reasonOfObjection);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void AddObjectionScan()
        {
            try
            {
                MemoryStream fileData = new MemoryStream();
                Request.Files[0].InputStream.CopyTo(fileData);
                expert.AddObjectionScan(documentID, Request.Files[0].FileName, fileData.ToArray());
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void RemoveObjectionScan(int ID)
        {
            try
            {
                expert.RemoveObjectionScan(documentID, ID);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void RemoveContractScan(int scanID)
        {
            try
            {
                expert.RemoveContractScan(documentID, scanID);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void RemoveTPCScan(int scanID)
        {
            try
            {
                expert.RemoveTPCScan(documentID, scanID);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void AddTPCScan()
        {
            try
            {
                MemoryStream fileData = new MemoryStream();
                Request.Files[0].InputStream.CopyTo(fileData);
                expert.AddTPCScan(documentID, Request.Files[0].FileName, fileData.ToArray());
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void AddContractScan()
        {
            try
            {
                MemoryStream fileData = new MemoryStream();
                Request.Files[0].InputStream.CopyTo(fileData);
                expert.AddContractScan(documentID, Request.Files[0].FileName, fileData.ToArray());
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void RemoveReport(int reportID)
        {
            try
            {
                expert.RemoveReport(documentID, reportID);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void AddReport(string reportDate, string description)
        {
            try
            {
                MemoryStream fileData = new MemoryStream();
                DateTime convertedReportDate = PersianDate.ConvertDate.ToEn(reportDate);
                Request.Files[0].InputStream.CopyTo(fileData);
                expert.AddReport(documentID, Request.Files[0].FileName, fileData.ToArray(),convertedReportDate, description);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
    }
}
