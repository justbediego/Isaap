﻿using Automation.Business;
using Automation.Model;
using Automation.UI.Models;
using Automation.UI.Models.Report;
using Automation.UI.Models.StaffProfile;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Automation.UI.Controllers
{
    [Authorize(Roles = "2,3,4,5,6,7")]
    public class StaffController : Controller
    {
        private int staffID
        {
            get
            {
                return ApplicationUser.FromClaimsIdentity(User).Id;
            }
        }
        private Staff staff = null;
        private ProfileLayout profileLayout;
        private SystemInfoBusiness systemInfo;
        private StaffBusiness staffBusiness;
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        public StaffController()
        {
            staffBusiness = new StaffBusiness();
            systemInfo = new SystemInfoBusiness();
        }
        public void InitialData()
        {
            systemInfo = new SystemInfoBusiness();
            staff = staffBusiness.GetSpecificStaff(staffID);
            profileLayout = new ProfileLayout();
            profileLayout.Menus = new List<ProfileLayoutMenu>();
            switch (staff.Type)
            {
                case Model.User.UserType.Association:
                    profileLayout.ProfileName = "انجمن";
                    profileLayout.ProfileDesc = "شهرستان " + ((Association)staff).County.Name;
                    break;
                case Model.User.UserType.Club:
                    profileLayout.ProfileName = "کانون";
                    profileLayout.ProfileDesc = "استان " + ((Club)staff).Province.Name;
                    break;
                case Model.User.UserType.CountyInspector:
                    profileLayout.ProfileName = "اداره بازرسی کار";
                    profileLayout.ProfileDesc = "شهرستان " + ((CountyInspector)staff).County.Name;
                    break;
                case Model.User.UserType.InspectionOffice:
                    profileLayout.ProfileName = "اداره کل بازرسی کار";
                    break;
                case Model.User.UserType.ProvinceInspector:
                    profileLayout.ProfileName = "اداره بازرسی کار";
                    profileLayout.ProfileDesc = "استان " + ((ProvinceInspector)staff).Province.Name;
                    break;
                case Model.User.UserType.ProvinceManager:
                    profileLayout.ProfileName = "مدیر کل بازرسی";
                    profileLayout.ProfileDesc = "استان " + ((ProvinceManager)staff).Province.Name;
                    break;
            }
            profileLayout.Menus.Add(new ProfileLayoutMenu()
            {
                Id = 1,
                Icon = "copy",
                Name = "پرونده ها",
                HRef = "/Staff/Index"
            });
            profileLayout.Menus.Add(new ProfileLayoutMenu()
            {
                Id = 2,
                Icon = "th",
                Name = "گزارشگیری",
                HRef = "/Staff/Reports"
            });
        }
        public void InitialData(int documentID)
        {
            InitialData();
            Document document = null;
            document = staffBusiness.GetDocument(staffID, documentID);
            bool IsCurrent = document.CurrentFlow.Staff != null && document.CurrentFlow.Staff.Id == staffID;
            profileLayout.Menus.Add(new ProfileLayoutMenu()
            {
                Id = 3,
                Icon = "file",
                Name = document.FullName,
                Children = new List<ProfileLayoutMenu>()
                {
                    new ProfileLayoutMenu()
                    {
                        Id=1,
                        HRef="/Staff/DocumentOverview?Id="+documentID,
                        Name="مشاهده",
                    },
                    new ProfileLayoutMenu()
                    {
                        Id=2,
                        Name="تاییدیه انجمن",
                        HRef="/Staff/AssociationAcceptData?Id="+documentID,
                        IsHidden=!IsCurrent || staff.Type!=Model.User.UserType.Association,
                    },
                    new ProfileLayoutMenu()
                    {
                        Id=3,
                        Name="تاییدیه کانون",
                        HRef="/Staff/ClubAcceptData?Id="+documentID,
                        IsHidden=!IsCurrent || staff.Type!=Model.User.UserType.Club,
                    },
                    new ProfileLayoutMenu()
                    {
                        Id=4,
                        Icon="file",
                        Name="تاییدیه بازرسی شهرستان",
                        HRef="/Staff/CountyInspectorAcceptData?Id="+documentID,
                        IsHidden=!IsCurrent || staff.Type!=Model.User.UserType.CountyInspector,
                    },
                    new ProfileLayoutMenu()
                    {
                        Id=5,
                        Name="تصمیم بازرسی استان",
                        HRef="/Staff/ProvinceInspectorDecision?Id="+documentID,
                        IsHidden=!IsCurrent || staff.Type!=Model.User.UserType.ProvinceInspector,
                    },
                    new ProfileLayoutMenu()
                    {
                        Id=6,
                        Name="تصمیم مدیر کل استان",
                        HRef="/Staff/ProvinceManagerDecision?Id="+documentID,
                        IsHidden=!IsCurrent || staff.Type!=Model.User.UserType.ProvinceManager,
                    },
                    new ProfileLayoutMenu()
                    {
                        Id=7,
                        Name="تصمیم اداره بازرسی کشور",
                        HRef="/Staff/InspectionOfficeDecision?Id="+documentID,
                        IsHidden=!IsCurrent || staff.Type!=Model.User.UserType.InspectionOffice,
                    }
                },
            });
        }
        public ActionResult Index()
        {
            InitialData();
            profileLayout.Menus.Where(m => m.Id == 1).Single().IsActive = true;
            profileLayout.ChildObject = new MyDocuments()
            {
                Documents = staffBusiness.GetPendingDocuments(staffID),
            };
            return View("Index", profileLayout);
        }
        public ActionResult Reports(int? FieldOfStudy, Document.MilitaryServiceStates? MilitaryService, Document.MaritalStates? MaritalStates, Document.GenderType? Gender, Flow.FlowType? CurrentFlowType, string FullName, string ShenasnameNo, string NationalCode, string BirthDateFrom, string BirthDateTo, string From, string To, string AssociationNo, string Mobile, string PlaceOfBirth, int? HomeCounty, int? HomeProvince, int? WorkCounty, int? WorkProvince, string WorkshopName, string EmployerName, int? FieldOfActivity, string InspectorName)
        {
            InitialData();
            profileLayout.Menus.Where(m => m.Id == 2).Single().IsActive = true;
            ReportFilter viewModel = new ReportFilter()
            {
                FieldsOfStudy = systemInfo.GetAllFieldsOfStudy(),
                FieldsOfActivity = systemInfo.GetAllFieldsOfActivity(),
                ProductionTypes = systemInfo.GetAllProductionTypes(),
                Provinces = systemInfo.GetAllProvinces(),
                AssociationNo = AssociationNo,
                FieldOfActivity = FieldOfActivity,
                FieldOfStudy = FieldOfStudy,
                BirthDateFrom = BirthDateFrom,
                BirthDateTo = BirthDateTo,
                CurrentFlowType = CurrentFlowType,
                FullName = FullName,
                Gender = Gender,
                HomeCounty = HomeCounty,
                HomeProvince = HomeProvince,
                WorkCounty = WorkCounty,
                WorkProvince = WorkProvince,
                MaritalStates = MaritalStates,
                MilitaryService = MilitaryService,
                Mobile = Mobile,
                NationalCode = NationalCode,
                PlaceOfBirth = PlaceOfBirth,
                From = From,
                To = To,
                WorkshopName = WorkshopName,
                EmployerName = EmployerName,
                ShenasnameNo = ShenasnameNo,
                InspectorName = InspectorName,
            };
            Staff specificStaff = staffBusiness.GetSpecificStaff(staffID);
            IQueryable<Flow> result = staffBusiness.GetEntireDocuments(specificStaff)
                                                   .Select(d => d.Flows)
                                                   .SelectMany(f => f)
                                                   .Where(r => r.Type != Flow.FlowType.Created && r.Type != Flow.FlowType.NotCompleted);
            if (!string.IsNullOrEmpty(AssociationNo))
            {
                result = result.Where(r => r.Document.AssociationMembershipNo == AssociationNo);
            }
            if (FieldOfStudy.HasValue)
            {
                result = result.Where(r => (r.Document.HighSchoolCertification != null && r.Document.HighSchoolCertification.FieldOfStudy.Id == FieldOfStudy.Value) ||
                                           (r.Document.DiplomaCertification != null && r.Document.DiplomaCertification.FieldOfStudy.Id == FieldOfStudy.Value) ||
                                           (r.Document.BachelorsCertification != null && r.Document.BachelorsCertification.FieldOfStudy.Id == FieldOfStudy.Value) ||
                                           (r.Document.MastersCertification != null && r.Document.MastersCertification.FieldOfStudy.Id == FieldOfStudy.Value) ||
                                           (r.Document.PhDCertification != null && r.Document.PhDCertification.FieldOfStudy.Id == FieldOfStudy.Value));
            }
            if (!string.IsNullOrEmpty(BirthDateFrom))
            {
                DateTime convertedBirthDateFrom = PersianDate.ConvertDate.ToEn(BirthDateFrom);
                result = result.Where(r => r.Document.BirthDate >= convertedBirthDateFrom);
            }
            if (!string.IsNullOrEmpty(BirthDateTo))
            {
                DateTime convertedBirthDateTo = PersianDate.ConvertDate.ToEn(BirthDateTo);
                result = result.Where(r => r.Document.BirthDate <= convertedBirthDateTo);
            }
            if (Gender.HasValue)
            {
                result = result.Where(r => r.Document.Gender == Gender);
            }
            if (HomeCounty.HasValue)
            {
                result = result.Where(r => r.Document.HomeContact.County.Id == HomeCounty);
            }
            if (HomeProvince.HasValue)
            {
                result = result.Where(r => r.Document.HomeContact.County.Province.Id == HomeProvince);
            }
            if (WorkCounty.HasValue)
            {
                result = result.Where(r => r.Document.WorkContact.County.Id == WorkCounty);
            }
            if (WorkProvince.HasValue)
            {
                result = result.Where(r => r.Document.WorkContact.County.Province.Id == WorkProvince);
            }
            if (MaritalStates.HasValue)
            {
                result = result.Where(r => r.Document.MaritalStatus == MaritalStates);
            }
            if (MilitaryService.HasValue)
            {
                result = result.Where(r => r.Document.MilitaryStatus == MilitaryService);
            }
            if (!string.IsNullOrEmpty(Mobile))
            {
                result = result.Where(r => r.Document.Mobile.Contains(Mobile));
            }
            if (!string.IsNullOrEmpty(NationalCode))
            {
                result = result.Where(r => r.Document.NationalCode == NationalCode);
            }
            if (!string.IsNullOrEmpty(PlaceOfBirth))
            {
                result = result.Where(r => r.Document.PlaceOfBirth.Contains(PlaceOfBirth));
            }
            if (!string.IsNullOrEmpty(From))
            {
                DateTime convertedFrom = PersianDate.ConvertDate.ToEn(From);
                result = result.Where(r => r.DateCreated >= convertedFrom);
            }
            if (!string.IsNullOrEmpty(To))
            {
                DateTime convertedTo = PersianDate.ConvertDate.ToEn(To);
                result = result.Where(r => r.DateCreated <= convertedTo);
            }
            if (!string.IsNullOrEmpty(ShenasnameNo))
            {
                result = result.Where(r => r.Document.ShenasnameNo == ShenasnameNo);
            }
            if (!string.IsNullOrEmpty(WorkshopName))
            {
                result = result.Where(r => r.Document.CurrentWorkShop.UnitName.Contains(WorkshopName));
            }
            if (!string.IsNullOrEmpty(EmployerName))
            {
                result = result.Where(r => r.Document.CurrentWorkShop.EmployerName.Contains(EmployerName));
            }
            //if (!string.IsNullOrEmpty(InspectorName))
            //{
            //    result = result.Where(r =>r.Document.WorkContact.County.CountyInspectors.Select(ci=>ci.));
            //}
            //List
            result = result.ToList().AsQueryable();
            if (CurrentFlowType.HasValue)
            {
                result = result.Where(r => r.Document.CurrentFlow.Type == CurrentFlowType);
            }
            if (!string.IsNullOrEmpty(FullName))
            {
                result = result.Where(r => r.Document.FullName.Contains(FullName));
            }
            viewModel.Results = result.Select(r => r.Document).Distinct().ToList();
            profileLayout.ChildObject = viewModel;
            return View("Reports", profileLayout);
        }
        public ActionResult DocumentOverview(int Id)
        {
            InitialData(Id);
            profileLayout.Menus.Where(m => m.Id == 3).Single().IsActive = true;
            profileLayout.Menus.Where(m => m.Id == 3).Single().Children.Where(c => c.Id == 1).Single().IsActive = true;
            profileLayout.ChildObject = new DocumentOverview()
            {
                Document = staffBusiness.GetDocument(staffID, Id),
            };
            return View("DocumentOverview", profileLayout);
        }
        public ActionResult AssociationAcceptData(int Id)
        {
            InitialData(Id);
            profileLayout.Menus.Where(m => m.Id == 3).Single().IsActive = true;
            profileLayout.Menus.Where(m => m.Id == 3).Single().Children.Where(c => c.Id == 2).Single().IsActive = true;
            profileLayout.ChildObject = new ApplicationProcess()
            {
                Document = staffBusiness.GetDocument(staffID, Id),
            };
            return View("AssociationAcceptData", profileLayout);
        }
        public ActionResult CountyInspectorAcceptData(int Id)
        {
            InitialData(Id);
            profileLayout.Menus.Where(m => m.Id == 3).Single().IsActive = true;
            profileLayout.Menus.Where(m => m.Id == 3).Single().Children.Where(c => c.Id == 4).Single().IsActive = true;
            profileLayout.ChildObject = new ApplicationProcess()
            {
                Document = staffBusiness.GetDocument(staffID, Id),
            };
            return View("CountyInspectorAcceptData", profileLayout);
        }
        public ActionResult ClubAcceptData(int Id)
        {
            InitialData(Id);
            profileLayout.Menus.Where(m => m.Id == 3).Single().IsActive = true;
            profileLayout.Menus.Where(m => m.Id == 3).Single().Children.Where(c => c.Id == 3).Single().IsActive = true;
            profileLayout.ChildObject = new ApplicationProcess()
            {
                Document = staffBusiness.GetDocument(staffID, Id),
            };
            return View("ClubAcceptData", profileLayout);
        }
        public ActionResult ProvinceInspectorDecision(int Id)
        {
            InitialData(Id);
            profileLayout.Menus.Where(m => m.Id == 3).Single().IsActive = true;
            profileLayout.Menus.Where(m => m.Id == 3).Single().Children.Where(c => c.Id == 5).Single().IsActive = true;
            profileLayout.ChildObject = new ApplicationProcess()
            {
                Document = staffBusiness.GetDocument(staffID, Id),
            };
            return View("ProvinceInspectorDecision", profileLayout);
        }
        public ActionResult ProvinceManagerDecision(int Id)
        {
            InitialData(Id);
            profileLayout.Menus.Where(m => m.Id == 3).Single().IsActive = true;
            profileLayout.Menus.Where(m => m.Id == 3).Single().Children.Where(c => c.Id == 6).Single().IsActive = true;
            profileLayout.ChildObject = new ApplicationProcess()
            {
                Document = staffBusiness.GetDocument(staffID, Id),
            };
            return View("ProvinceManagerDecision", profileLayout);
        }
        public ActionResult InspectionOfficeDecision(int Id)
        {
            InitialData(Id);
            profileLayout.Menus.Where(m => m.Id == 3).Single().IsActive = true;
            profileLayout.Menus.Where(m => m.Id == 3).Single().Children.Where(c => c.Id == 7).Single().IsActive = true;
            profileLayout.ChildObject = new ApplicationProcess()
            {
                Document = staffBusiness.GetDocument(staffID, Id),
            };
            return View("InspectionOfficeDecision", profileLayout);
        }
        public void GetWholeExcel()
        {
            Response.Clear();
            Response.ContentType = "file/excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=AuthenticatedExcel.xls");
            Response.Write("<table border=1 ><thead><tr><td>آخرین پیام در سامانه</td><td>تاریخ صدور مدرک</td><td>تاریخ تشکیل پرونده</td><td>نشانی شرکت</td><td>شهر محل شرکت</td><td>استان محل شرکت</td><td>ایمیل</td><td>تلفن شرکت</td><td>تعداد کارکنان</td><td>فعالیت شرکت</td><td>نام مدیر عامل</td><td>نام شرکت</td><td>نشانی محل سکونت</td><td>شهر محل سکونت</td><td>استان محل سکونت</td><td>رشته تحصیلی</td><td>تلفن همراه</td><td>کدملی</td><td>شماره شناسنامه</td><td>نام پدر</td><td>نام خانوادگی</td><td>نام</td><td>کد سامانه</td></tr></thead><tbody>");
            var authorizedDocuments = systemInfo.GetAllAuthorizedDocuments().Select(d => d.Id);
            var result = systemInfo.GetAllDocuments(d=>authorizedDocuments.Contains(d.Id), d => d.Flows
                                                                                               , d => d.CurrentWorkShop.FieldOfActivity
                                                                                               , d => d.SafetyCertifications
                                                                                               , d => d.WorkContact.County.Province
                                                                                               , d => d.HomeContact.County.Province
                                                                                               , d => d.PhDCertification.FieldOfStudy
                                                                                               , d => d.MastersCertification.FieldOfStudy
                                                                                               , d => d.BachelorsCertification.FieldOfStudy
                                                                                               , d => d.DiplomaCertification.FieldOfStudy
                                                                                               , d => d.HighSchoolCertification.FieldOfStudy)
                                                                                               .ToList();
            result.ForEach(d =>
            {
                Response.Write("<tr>");
                Response.Write("<td>" + d.CurrentFlow.Description + "</td>");
                Response.Write("<td>" + PersianDate.ConvertDate.ToFa(d.CurrentFlow.DateCreated) + "</td>");
                Response.Write("<td>" + PersianDate.ConvertDate.ToFa(d.DateCreated) + "</td>");
                Response.Write("<td>" + d.WorkContact.Address + "</td>");
                Response.Write("<td>" + d.WorkContact.County.Name + "</td>");
                Response.Write("<td>" + d.WorkContact.County.Province.Name + "</td>");
                Response.Write("<td>" + d.CurrentWorkShop.Email + "</td>");
                Response.Write("<td>" + d.WorkContact.Phone + "</td>");
                Response.Write("<td>" + (d.CurrentWorkShop.MaleWorkersNo + d.CurrentWorkShop.FemaleWorkersNo) + "</td>");
                Response.Write("<td>" + d.CurrentWorkShop.FieldOfActivity.Name + "</td>");
                Response.Write("<td>" + d.CurrentWorkShop.EmployerName + "</td>");
                Response.Write("<td>" + d.CurrentWorkShop.UnitName + "</td>");
                Response.Write("<td>" + d.HomeContact.Address + "</td>");
                Response.Write("<td>" + d.HomeContact.County.Name + "</td>");
                Response.Write("<td>" + d.HomeContact.County.Province.Name + "</td>");
                if (d.PhDCertification != null)
                    Response.Write("<td>" + d.PhDCertification.FieldOfStudy.Name + "</td>");
                else if(d.MastersCertification!=null)
                    Response.Write("<td>" + d.MastersCertification.FieldOfStudy.Name + "</td>");
                else if (d.BachelorsCertification != null)
                    Response.Write("<td>" + d.BachelorsCertification.FieldOfStudy.Name + "</td>");
                else if (d.DiplomaCertification != null)
                    Response.Write("<td>" + d.DiplomaCertification.FieldOfStudy.Name + "</td>");
                else if (d.BachelorsCertification != null)
                    Response.Write("<td>" + d.BachelorsCertification.FieldOfStudy.Name + "</td>");
                else
                    Response.Write("<td>بدون تحصیلات</td>");
                Response.Write("<td>" + d.Mobile + "</td>");
                Response.Write("<td>" + d.NationalCode + "</td>");
                Response.Write("<td>" + d.ShenasnameNo + "</td>");
                Response.Write("<td>" + d.FatherName + "</td>");
                Response.Write("<td>" + d.LastName + "</td>");
                Response.Write("<td>" + d.FirstName + "</td>");
                Response.Write("<td>" + d.NezamCode + "</td>");
                Response.Write("</tr>");
            });
            Response.Write("</tbody></table>");
            Response.End();
        }
        public void GetNumericalExcel()
        {
            Response.Clear();
            Response.ContentType = "file/excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=NumericalExcel.xls");
            var documents = systemInfo.GetAllDocuments(null, d => d.Flows, d => d.CurrentWorkShop, d => d.SafetyCertifications);
            Response.Write("<table border=1 ><thead><tr><td rowspan=2>تعداد پرونده هاي تشكيل شده براي متقاضيان</td><td colspan=2>تعداد مسئولين ايمني تاييد صلاحيت شده</td><td rowspan=2>تعداد پرونده هاي در دست اقدام</td><td rowspan=2>تعداد عدم صلاحيت</td><td colspan=3>دوره هاي آموزشي برگزار شده (نفر)</td></tr><tr><td>كارگاه زير 25 نفر كارگر</td><td>كارگاه بالاي 25 نفر كارگر</td><td>16 ساعته</td><td>40 ساعته</td><td>دوره های دیگر</td></tr></thead><tbody><tr>" +
                "<td>" + documents.Count() + "</td>" +
                "<td>" + documents.Where(d => (d.CurrentWorkShop.FemaleWorkersNo + d.CurrentWorkShop.MaleWorkersNo) < 25 && d.CurrentFlow.Type == Flow.FlowType.Authorized).Count() + "</td>" +
                "<td>" + documents.Where(d => (d.CurrentWorkShop.FemaleWorkersNo + d.CurrentWorkShop.MaleWorkersNo) >= 25 && d.CurrentFlow.Type == Flow.FlowType.Authorized).Count() + "</td>" +
                "<td>" + documents.Where(d => d.CurrentFlow.Type != Flow.FlowType.Authorized && d.CurrentFlow.Type != Flow.FlowType.Created && d.CurrentFlow.Type != Flow.FlowType.NotCompleted).Count()+ "</td>" +
                "<td>" + documents.Where(d => d.CurrentFlow.Type == Flow.FlowType.RejectedOnce || d.CurrentFlow.Type == Flow.FlowType.RejectedOnce).Count() + "</td>" +
                "<td>" + documents.Where(d => d.SafetyCertifications.Any(sc => sc.CourseType == Document.SafetyCertification.Type.SixteenHours)).Count() + "</td>" +
                "<td>" + documents.Where(d => d.SafetyCertifications.Any(sc => sc.CourseType == Document.SafetyCertification.Type.ForthyHours)).Count() + "</td>" +
                "<td>" + documents.Where(d => d.SafetyCertifications.Any(sc => sc.CourseType == Document.SafetyCertification.Type.Others)).Count() + "</td>" +
                "</tr></tbody></table>");
            Response.End();
        }

        [HttpPost]
        public void AcceptDataByAssociation(int Id)
        {
            try
            {
                staffBusiness.AcceptDocumentByAssociation(staffID, Id);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void RejectDataByAssociation(int Id, string reason)
        {
            try
            {
                staffBusiness.RejectDocumentByAssociation(staffID, Id, reason);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void AcceptDataByCountyInspector(int Id)
        {
            try
            {
                staffBusiness.AcceptDocumentByCountyInspector(staffID, Id);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void RejectDataByCountyInspector(int Id, string reason)
        {
            try
            {
                staffBusiness.RejectDocumentByCountyInspector(staffID, Id, reason);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void AcceptDataByClub(int Id)
        {
            try
            {
                staffBusiness.AcceptDocumentByClub(staffID, Id);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void RejectDataByClub(int Id, string reason)
        {
            try
            {
                staffBusiness.RejectDocumentByClub(staffID, Id, reason);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void AcceptDocumentByProvinceInspector(int Id)
        {
            try
            {
                staffBusiness.AuthorizeByProvinceInspector(staffID, Id);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void RejectDocumentByProvinceInspector(int Id, string reason)
        {
            try
            {
                staffBusiness.UnAuthorizeByProvinceInspector(staffID, Id, reason);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void RejectDataAuthorizationByProvinceInspector(int Id, string reason)
        {
            try
            {
                staffBusiness.UnAcceptedByProvinceInspector(staffID, Id, reason);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void AcceptDocumentByProvinceManager(int Id)
        {
            try
            {
                staffBusiness.AuthorizeByProvinceManager(staffID, Id);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void RejectDocumentByProvinceManager(int Id, string reason)
        {
            try
            {
                staffBusiness.UnAuthorizeByProvinceManager(staffID, Id, reason);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void AcceptDocumentByInspectionOffice(int Id)
        {
            try
            {
                staffBusiness.AuthorizeByInspectionOffice(staffID, Id);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void RejectDocumentByInspectionOffice(int Id, string reason)
        {
            try
            {
                staffBusiness.UnAuthorizeByInspectionOffice(staffID, Id, reason);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
    }
}