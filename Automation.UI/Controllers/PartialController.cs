﻿using Automation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Automation.UI.Controllers
{
    public class PartialController : Controller
    {
        [ChildActionOnly]
        public ActionResult DocumentOverview(Document document)
        {
            return PartialView("DocumentOverview", document);
        }
    }
}