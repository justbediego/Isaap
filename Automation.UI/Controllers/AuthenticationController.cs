﻿using Automation.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using Automation.Model;
using Microsoft.AspNet.Identity;
using Automation.UI.Models;
using System.Security.Claims;
using BotDetect.Web.UI.Mvc;
using BotDetect.Web;

namespace Automation.UI.Controllers
{
    public class AuthenticationController : Controller
    {
        private ExpertBusiness expertBusiness;
        private SystemInfoBusiness systemInfoBusiness;
        private UserBusiness userBusiness;
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        public AuthenticationController()
        {
            expertBusiness = new ExpertBusiness();
            systemInfoBusiness = new SystemInfoBusiness();
            userBusiness = new UserBusiness();
        }
        public void HandleRedirects()
        {
            if (User.Identity.IsAuthenticated)
            {
                var applicationUser = ApplicationUser.FromClaimsIdentity(User);
                switch (applicationUser.Type)
                {
                    case Model.User.UserType.Admin:
                        Response.Redirect("/Admin");
                        break;
                    case Model.User.UserType.Association:
                    case Model.User.UserType.Club:
                    case Model.User.UserType.CountyInspector:
                    case Model.User.UserType.InspectionOffice:
                    case Model.User.UserType.ProvinceInspector:
                    case Model.User.UserType.ProvinceManager:
                        Response.Redirect("/Staff");
                        break;
                    case Model.User.UserType.Expert:
                        Response.Redirect("/Expert");
                        break;
                }
            }
        }
        public ActionResult Index()
        {
            HandleRedirects();
            return View();
        }
        public ActionResult Register()
        {
            HandleRedirects();
            return View();
        }
        [HttpPost]
        public void RegisterExpert(string username, string password, string captchaCode,string captchaInstanceID)
        {
            try
            {
                if (!CaptchaControl.Validate("registerCaptcha", captchaCode, captchaInstanceID))
                    throw new Business.Inner.CustomExceptions.WrongInputException("کد امنیتی");
                expertBusiness.CreateDocument(username, password);
                Login(username, password, false);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void Login(string username, string password, bool rememberMe)
        {
            try
            {
                User user = userBusiness.GetUser(username, password);
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
                AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = rememberMe }, ApplicationUser.ToClaimsIdentity(user));
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void Logout()
        {
            try
            {
                AuthenticationManager.SignOut();
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        [HttpPost]
        public void ResetPassword(string username, string mobile)
        {
            try
            {
                userBusiness.ResetPassword(username, mobile);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
        public void ChangePassword(string currentPassword, string newPassword, string cNewPassword)
        {
            try
            {
                userBusiness.ChangePassword(ApplicationUser.FromClaimsIdentity(User).UserName, currentPassword, newPassword);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
    }
}
