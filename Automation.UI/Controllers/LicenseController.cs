﻿using Automation.Business;
using Automation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Automation.UI.Controllers
{
    public class LicenseController : Controller
    {
        private ExpertBusiness expert;
        private SystemInfoBusiness systemInfo;
        public LicenseController()
        {
            expert = new ExpertBusiness();
            systemInfo = new SystemInfoBusiness();
        }
        [Authorize]
        public ActionResult Index(int Id)
        {
            Document document = expert.GetDocumentBasicInfo(Id);
            if (document.CurrentFlow.Type != Flow.FlowType.Authorized)
                return null;
            return View("Index", document);
        }
        public void GetAllAuthorized()
        {
            var documents = systemInfo.GetAllAuthorizedDocuments();
            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment; filename=AuthorizedDocuments.xls");
            Response.Write("<table><tr><th>نام و نام خانوادگی</th><th>همراه</th><th>آخرین مدرک تحصیلی</th></tr>");
            foreach (var document in documents)
            {
                Response.Write("<tr><td>" + document.FullName + "</td><td>" + document.Mobile + "</td><td>");
                if (document.PhDCertification != null)
                {
                    Response.Write("دکترا در " + document.PhDCertification.FieldOfStudy.Name);
                }else if (document.MastersCertification != null)
                {
                    Response.Write("کارشناسی ارشد در " + document.MastersCertification.FieldOfStudy.Name);
                }
                else if (document.BachelorsCertification != null)
                {
                    Response.Write("کارشناسی در " + document.BachelorsCertification.FieldOfStudy.Name);
                }
                else if (document.DiplomaCertification != null)
                {
                    Response.Write("کاردانی در " + document.DiplomaCertification.FieldOfStudy.Name);
                }
                Response.Write("</td></tr>");
            }
            Response.Write("</table>");
        }
        [HttpPost]
        public bool IsAuthorized(string nezamCode)
        {
            return systemInfo.GetDocumentByNezamCode(nezamCode) != null;
        }
    }
}