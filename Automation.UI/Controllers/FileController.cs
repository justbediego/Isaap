﻿using Automation.Business;
using Automation.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Automation.UI.Controllers
{
    public class FileController : Controller
    {
        private FileBusiness fileBusiness;
        public FileController()
        {
            fileBusiness = new FileBusiness();
        }
        [HttpGet]
        public void GetImage(int ID, int width = -1, int height = -1)
        {
            try
            {
                Attachment image = fileBusiness.GetImage(ID, width, height);
                Response.Clear();
                Response.ContentType = "image/jpg";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + image.Filename);
                Response.OutputStream.Write(image.FileData, 0, image.FileData.Length);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }           
        }
        [HttpGet]
        public void GetFile(int ID)
        {
            try
            {
                Attachment file = fileBusiness.GetFile(ID);
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Filename);
                Response.OutputStream.Write(file.FileData, 0, file.FileData.Length);
            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.Write(ex.Message);
            }
        }
    }
}
